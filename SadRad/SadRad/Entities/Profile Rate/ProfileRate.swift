//
//  ProfileRate.swift
//  SadRad
//
//  Created by Omar Ibrahim on 11/4/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class ProfileRate: Mappable {
    
    var clientName: String?
    var tripRate: Float?
    var clientImage: String?
    var date: String?
    var description: String?
    
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        clientName <- map["user"]
        tripRate <- map["reviewTrip"]
        clientImage <- map["userimage"]
        date <- map["date"]
        description <- map["note"]
    }
    
}
