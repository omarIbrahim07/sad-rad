//
//  ForgetPassword.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 12/4/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class ForgetPassword : Mappable {
    
    var status : Bool?
    var error : String?
    var verifyCode : Int?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        status <- map["status"]
        error <- map["error"]
        verifyCode <- map["verify_code"]
    }
    
}
