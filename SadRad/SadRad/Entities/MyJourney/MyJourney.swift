//
//  MyJourney.swift
//  SadRad
//
//  Created by Omar Ibrahim on 10/20/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class MyJourney: Mappable {
    
    var id: Int?
    var price: Int?
    var negotiablePriceBool: Bool?
    var deliveryPackagePriceBool: Bool?
    var day: String?
    var time: String?
    var notes: String?
    var createdAt: String?
    var packupCity: City?
    var packupGovernorate: Governorate?
    var dropOffCity: City?
    var dropOffGovernorate: Governorate?
    var myCar: MyCar?
    var driverInfo: DriverInfo?
    var status: Int?
    var tripIsAboutToStart: Int?
    var ratingCount: Int?
    var clientName: String?
    var clientPhone: String?
            
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        id <- map["id"]
        price <- map["price"]
        negotiablePriceBool <- map["negotiable_price"]
        deliveryPackagePriceBool <- map["delivery_of_aparcel"]
        day <- map["day"]
        time <- map["time"]
        notes <- map["notes"]
        createdAt <- map["created_at"]
        packupCity <- map["packupcity"]
        packupGovernorate <- map["packupgovernoratment"]
        dropOffCity <- map["dropoffcity"]
        dropOffGovernorate <- map["dropoffgovernoratment"]
        myCar <- map["car"]
        driverInfo <- map["user"]
        status <- map["status"]
        tripIsAboutToStart <- map["flag"]
        ratingCount <- map["ratingcount"]
        clientName <- map["client.name"]
        clientPhone <- map["client.phone_number"]
    }
    
}

