//
//  Governorate.swift
//  SadRad
//
//  Created by Omar Ibrahim on 10/15/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class Governorate: Mappable {
    
    var id: Int?
    var nameEn: String?
    var nameAr: String?
    
    var nameLocalized: String? {
        if "Lang".localized == "Ar" {
            return nameAr
        }
        return nameEn
    }
            
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
//        name <- map["name"]
        nameEn <- map["governorate_name_en"]
        nameAr <- map["governorate_name_ar"]
    }
    
}
