//
//  NewTripInfo.swift
//  SadRad
//
//  Created by Omar Ibrahim on 12/13/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class NewTripInfo: Mappable {
    
    var tripID: Int?
    var carID: Int?
    var price: Int?
    var negotialPrice: Bool?
    var status: Int?
    var carType: CarType?
    var carBrand: CarBrand?
    var carModel: CarModel?
    var pickupGovernorate: Governorate?
    var dropoffGovernorate: Governorate?
    var pickupCity: City?
    var dropoffCity: City?
    var driverID: Int?
    var driverName: String?
    var driverRating: Float?
    var driverPhoneNumber: String?
    var driverImage: String?
    var tripImages: [String]?
    var airCondition: Bool?
    var notes: String?
        
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        tripID <- map["id"]
        carID <- map["car_id"]
        price <- map["price"]
        negotialPrice <- map["negotiable_price"]
        status <- map["status"]
        carType <- map["car_type"]
        carBrand <- map["car_brand"]
        carModel <- map["car_model"]
        pickupGovernorate <- map["packupgovernoratment"]
        dropoffGovernorate <- map["dropoffgovernoratment"]
        pickupCity <- map["packupcity"]
        dropoffCity <- map["dropoffcity"]
        driverID <- map["driver.id"]
        driverName <- map["driver.name"]
        driverRating <- map["driver.rating"]
        driverPhoneNumber <- map["driver.phone_number"]
        driverImage <- map["driver.image"]
        tripImages <- map["car.images"]
        airCondition <- map["car.Aircondtion"]
        notes <- map["notes"]
    }
}
