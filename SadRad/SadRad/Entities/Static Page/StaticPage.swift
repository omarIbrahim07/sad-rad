//
//  StaticPage.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 11/18/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class StaticPage: Mappable {
    
    var id: Int?
    var contentAr: String?
    var contentEn: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        contentAr <- map["content_ar"]
        contentEn <- map["content_en"]
    }
    
}
