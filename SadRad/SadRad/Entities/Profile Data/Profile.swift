//
//  ProfileData.swift
//  SadRad
//
//  Created by Omar Ibrahim on 11/4/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class ProfileData: Mappable {
    
    var id: Int?
    var userName: String?
    var phoneNumber: Int?
    var email: String?
    var overallRating: Float?
    var tripsRate: Float?
    var driverRate: Float?
    var carRate: Float?
    var navigationRate: Float?
    var image: String?
    var numberOfTrips: Int?
    var profileRates: [ProfileRate]?
    
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        id <- map["profile.id"]
        userName <- map["profile.name"]
        phoneNumber <- map["profile.phone_number"]
        email <- map["profile.email"]
        overallRating <- map["profile.rating"]
        tripsRate <- map["profile.ratingtrip"]
        driverRate <- map["profile.ratingdriver"]
        carRate <- map["profile.ratingcar"]
        navigationRate <- map["profile.ratingnavigation"]
        image <- map["profile.profile.image"]
        numberOfTrips <- map["profile.tripscount"]
        profileRates <- map["ratings"]
    }
}
