//
//  CarType.swift
//  SadRad
//
//  Created by Omar Ibrahim on 10/12/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class CarType: Mappable {
    
    var id: Int?
    var nameEn: String?
    var nameAr: String?
    var statusRequired: String?
    var hasMaximumWeight: Int?
            
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
//        name <- map["name"]
        nameEn <- map["name_en"]
        nameAr <- map["name_ar"]
        statusRequired <- map["status_required"]
        hasMaximumWeight <- map["has_maxweight"]
    }
    
}
