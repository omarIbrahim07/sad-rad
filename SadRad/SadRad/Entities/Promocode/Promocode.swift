//
//  Promocode.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 11/28/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class PromocodeResponse: Mappable {
    
    var saved: Bool?
    var message: String?
    var promocode: Promocode?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        saved <- map["saved"]
        message <- map["message"]
        promocode <- map["promocode"]
    }
    
}

class PromocodeObj: Mappable {
    
    var found: Bool?
    var promoCode: Promocode?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        found <- map["found"]
        promoCode <- map["data"]
    }
    
}


class Promocode: Mappable {
    
    var id: Int?
    var code: String?
    var description: String?
    var expireDate: String?
    var value: Int?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        code <- map["code"]
        description <- map["description"]
        expireDate <- map["expire_date"]
        value <- map["value"]
    }
    
}
