//
//  TripFilter.swift
//  SadRad
//
//  Created by Omar Ibrahim on 1/8/21.
//  Copyright © 2021 EgyDesigner. All rights reserved.
//

import Foundation

struct TripFilter {
    var nameAr: String?
    var nameEn: String?
    var id: Int?
}
