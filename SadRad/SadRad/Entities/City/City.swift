//
//  City.swift
//  SadRad
//
//  Created by Omar Ibrahim on 10/15/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class City: Mappable {
    
    var id: Int?
    var nameEn: String?
    var nameAr: String?
            
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
//        name <- map["name"]
        nameEn <- map["city_name_en"]
        nameAr <- map["city_name_ar"]
    }
    
}
