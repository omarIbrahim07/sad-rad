//
//  Notification.swift
//  El-Zafer
//
//  Created by Omar Ibrahim on 7/29/19.
//  Copyright © 2019 Egy designer. All rights reserved.
//

import Foundation
import ObjectMapper

class Notificationn: Mappable {
    
    var orderId : Int!
    var message : String!
    var messageEN : String!
    var updatedAt : String!
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        orderId <- map["order_id"]
        message <- map["message"]
        messageEN <- map["messageEN"]
        updatedAt <- map["updated_at"]
    }
    
}
