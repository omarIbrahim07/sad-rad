//
//  MyCar.swift
//  SadRad
//
//  Created by Omar Ibrahim on 10/11/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class MyCar: Mappable {
    
    var id: Int?
    var image: String?
    var typeCarID: Int?
    var userID: Int?
    var brand: String?
    var model: Int?
    var maximimWeight: Int?
    var numberOfPersons: Int?
    var airConditioned: Bool?
    var status: Int?
    var carType: CarType?
    var carBrand: CarBrand?
    var carModel: CarModel?
    var images: [String]?
            
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        image <- map["image"]
        typeCarID <- map["type_carid"]
        userID <- map["user_id"]
        brand <- map["brand_car"]
        model <- map["model"]
        maximimWeight <- map["maximum_weight"]
        numberOfPersons <- map["number_of_persons"]
        airConditioned <- map["Aircondtion"]
        status <- map["status"]
        carType <- map["types_cars"]
        carModel <- map["model_cars"]
        carBrand <- map["brand_cars"]
        images <- map["images"]
    }
    
}
