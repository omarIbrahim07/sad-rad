//
//  NewReservation.swift
//  SadRad
//
//  Created by Omar Ibrahim on 12/13/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class NewReservation: Mappable {
    
    var tripID: Int?
    var pickupGovernorate: Governorate?
    var dropoffGovernorate: Governorate?
    var carType: CarType?
    var carBrand: CarBrand?
    var tripDate: String?
    var tripTime: String?
    var status: Int?
    var reviewStatus: Int?
    var images: [String]?
    var carID: Int?
    var numberOfPersons: Int?
    var driverName: String?
    var driverPhoneNumber: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        tripID <- map["trip_id"]
        pickupGovernorate <- map["pickup_governoratment"]
        dropoffGovernorate <- map["dropoff_governoratment"]
        carType <- map["car_type"]
        carBrand <- map["car_brand"]
        tripDate <- map["trip_date"]
        tripTime <- map["trip_time"]
        status <- map["status"]
        reviewStatus <- map["review_status"]
        images <- map["car.images"]
        carID <- map["car_id"]
        numberOfPersons <- map["car.number_of_persons"]
        driverName <- map["driver_name"]
        driverPhoneNumber <- map["driver_phone"]
    }
}
