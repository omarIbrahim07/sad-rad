//
//  DriverInfo.swift
//  SadRad
//
//  Created by Omar Ibrahim on 10/22/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class DriverInfo: Mappable {
    
    var id: Int?
    var name: String?
    var phoneNumber: Int?
    var rating: Float?
    var image: String?
            
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        phoneNumber <- map["phone_number"]
        rating <- map["rating"]
        image <- map["image"]
    }
}
