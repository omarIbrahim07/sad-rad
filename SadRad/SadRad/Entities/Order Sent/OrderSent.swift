//
//  OrderSent.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 11/21/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class OrderSent: Mappable {
    
    var id: Int?
    var price: Int?
    var startDate: String?
    var status: Int?
    var workerId: Int?
    var userId : Int?
    var rating: Double?
    var longitude: String?
    var latitude: String?
    var orderStatusId: Int?
    var service: String?
    var serviceEN: String?
    var distance: Float?
    var timestamp2: String?
    var area: String?
    var areaEN: String?
    var workerName: String?
    var clientName: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        price <- map["price"]
        startDate <- map["start_date"]
        status <- map["status"]
        workerId <- map["worker_id"]
        userId <- map["user_id"]
        rating <- map["rating"]
        longitude <- map["longitude"]
        latitude <- map["latitude"]
        orderStatusId <- map["orderStatus_id"]
        service <- map["service"]
        serviceEN <- map["serviceEN"]
        distance <- map["distance"]
        timestamp2 <- map["timestamp2"]
        area <- map["area"]
        areaEN <- map["areaEN"]
        workerName <- map["worker_name"]
        clientName <- map["client_name"]
    }
    
}
