//
//  TripOffer.swift
//  SadRad
//
//  Created by Omar Ibrahim on 10/25/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

//import Foundation
//import ObjectMapper
//
//class TripOffer: Mappable {
//
//    var id: Int?
//    var tripID: Int?
//    var driverInfo: DriverInfo?
//    var tripInfo: TripInfo?
//
//    required init?(map: Map) {
//
//    }
//
//    func mapping(map: Map) {
//        reservationID <- map["id"]
//        tripID <- map["trip_id"]
//        driverInfo <- map["user"]
//        tripInfo <- map["trips"]
//    }
//
//}
