//
//  RoomObject.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 12/18/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class RoomObject: Mappable {
    
    var found: Bool?
    var roomID: Int?
    var saved: Bool?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        found <- map["found"]
        roomID <- map["room_id"]
        saved <- map["saved"]
    }
    
}
