//
//  TripInfo.swift
//  SadRad
//
//  Created by Omar Ibrahim on 10/22/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class TripInfo: Mappable {
    
    var id: Int?
    var day: String?
    var time: String?
    var price: Int?
    var packupCity: City?
    var packupGovernorate: Governorate?
    var dropOffCity: City?
    var dropOffGovernorate: Governorate?
    var myCar: MyCar?
    var status: Int?
            
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        id <- map["id"]
        day <- map["day"]
        time <- map["time"]
        packupCity <- map["packupcity"]
        packupGovernorate <- map["packupgovernoratment"]
        dropOffCity <- map["dropoffcity"]
        dropOffGovernorate <- map["dropoffgovernoratment"]
        myCar <- map["car"]
        price <- map["price"]
        status <- map["status"]
    }
    
}
