//
//  FilterYear.swift
//  SadRad
//
//  Created by Omar Ibrahim on 12/27/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class FilterYear: Mappable {
    
    var id: Int?
    var nameEn: String?
    var nameAr: String?
            
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
//        name <- map["name"]
        nameEn <- map["name_en"]
        nameAr <- map["name_ar"]
    }
    
}
