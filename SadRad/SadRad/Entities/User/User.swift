//
//  User.swift
//  Blabber
//
//  Created by Hassan on 12/22/17.
//  Copyright © 2017 Hassan. All rights reserved.
//

import UIKit
import ObjectMapper

class User: NSObject, Mappable, NSCoding {
    
    var id : Int?
    var name : String?
//    var lastName : String?
    var email : String?
    var phone : String?
//    var address: String?
    var image : String?
    var imageID: Int?
//    var isVerified: Int?
//    var areaID: Int?
//    var area: String?
//    var areaEn: String?
    
    required override init() {
        
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
//        lastName <- map["last_name"]
        email <- map["email"]
        phone <- map["phone_number"]
//        address <- map["address"]
//        isVerified <- map["is_verified"]
        image <- map["profile.image"]
        imageID <- map["profile.id"]
//        areaID <- map["area_id"]
//        area <- map["area_name"]
//        areaEn <- map["area_nameEN"]
    }
    
    //MARK: - NSCoding -
    required init(coder aDecoder: NSCoder) {
        self.id = aDecoder.decodeObject(forKey: "id") as? Int
        self.name = aDecoder.decodeObject(forKey: "name") as? String
//        self.lastName = aDecoder.decodeObject(forKey: "last_name") as? String
        self.email = aDecoder.decodeObject(forKey: "email") as? String
        self.phone = aDecoder.decodeObject(forKey: "phone_number") as? String
//        self.address = aDecoder.decodeObject(forKey: "address") as? String
//        self.isVerified = aDecoder.decodeObject(forKey: "is_verified") as? Int
        self.image = aDecoder.decodeObject(forKey: "image") as? String
        self.imageID = aDecoder.decodeObject(forKey: "imageID") as? Int
//        self.areaID = aDecoder.decodeObject(forKey: "area_id") as? Int
//        self.area = aDecoder.decodeObject(forKey: "area_name") as? String
//        self.areaEn = aDecoder.decodeObject(forKey: "area_nameEN") as? String
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(name, forKey: "name")
//        aCoder.encode(lastName, forKey: "last_name")
        aCoder.encode(email, forKey: "email")
        aCoder.encode(phone, forKey: "phone_number")
//        aCoder.encode(address, forKey: "address")
//        aCoder.encode(isVerified, forKey: "is_verified")
        aCoder.encode(image, forKey: "image")
        aCoder.encode(imageID, forKey: "imageID")
//        aCoder.encode(areaID, forKey: "area_id")
//        aCoder.encode(areaEn, forKey: "area_nameEN")
//        aCoder.encode(area, forKey: "area_name")
    }
    
}
