//
//  CarModel.swift
//  SadRad
//
//  Created by Omar Ibrahim on 10/13/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class CarModel: Mappable {
    
    var id: Int?
    var nameEn: String?
    var nameAr: String?
            
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
//        name <- map["name"]
        nameEn <- map["name_en"]
        nameAr <- map["name_ar"]
    }
    
}
