//
//  ReasonForCancel.swift
//  SadRad
//
//  Created by Omar Ibrahim on 11/4/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class ReasonForCancel: Mappable {
    
    var id: Int?
    var nameEn: String?
    var nameAr: String?
    var choosed: Bool? = false

    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        id <- map["id"]
        //        name <- map["name"]
        nameEn <- map["reason_en"]
        nameAr <- map["reason_ar"]
    }
    
}
