//
//  Reservation.swift
//  SadRad
//
//  Created by Omar Ibrahim on 10/22/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class Reservation: Mappable {
    
    var reservationID: Int?
    var tripID: Int?
    var driverInfo: DriverInfo?
    var tripInfo: TripInfo?
    var reviewStatus: Int?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        reservationID <- map["id"]
        tripID <- map["trip_id"]
        driverInfo <- map["user"]
        tripInfo <- map["trips"]
        reviewStatus <- map["review_status"]
    }
    
}
