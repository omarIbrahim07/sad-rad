//
//  BaseViewController.swift
//  GameOn
//
//  Created by Hassan on 11/23/18.
//  Copyright © 2018 GameOn. All rights reserved.
//

import UIKit
import Toast_Swift
import PKHUD

class BaseViewController: UIViewController, UIPopoverControllerDelegate, UINavigationControllerDelegate {
    
    var timer: Timer?
    
    func updateUserInterface() {
        switch Network.reachability.status {
        case .unreachable:
//            view.backgroundColor = .red
            print("s")
        case .wwan:
            view.backgroundColor = .yellow
        case .wifi:
            view.backgroundColor = .green
        }
        
        if Network.reachability.isReachable == false {
            let apiError = APIError()
            apiError.message = "No internet connection".localized
            showError(error: apiError)
        } else if Network.reachability.isReachable == true {
//            viewDidLoad()
        }
        
        print("Reachability Summary")
        print("Status:", Network.reachability.status)
        print("HostName:", Network.reachability.hostname ?? "nil")
        print("Reachable:", Network.reachability.isReachable)
        print("Wifi:", Network.reachability.isReachableViaWiFi)
    }
    
    @objc func statusManager(_ notification: Notification) {
        updateUserInterface()
    }
    
    
    let picker : UIImagePickerController? = {
        return UIImagePickerController()
    }()
        
    override func viewDidLoad() {
        super.viewDidLoad()
        ToastManager.shared.isQueueEnabled = true
        NotificationCenter.default
            .addObserver(self,
                         selector: #selector(statusManager),
                         name: .flagsChanged,
                         object: nil)
        if Network.reachability.isReachable == false {
            updateUserInterface()
        }
        
//        timer = Timer.scheduledTimer(timeInterval: 30 * 60, target: self, selector: #selector(action), userInfo: nil, repeats: true)
        timer = Timer.scheduledTimer(timeInterval: 30 * 60, target: self, selector: #selector(action), userInfo: nil, repeats: true)
    }
    
    @objc func action () {
        print("done")
        self.refrshToken()
    }
    
    func refrshToken() {
        let params: [String : AnyObject] = [:]
        
        AuthenticationAPIManager().refreshToken(basicDictionary: params, onSuccess: { (token) in

            print(token)
        }) { (error) in
            print(error.responseStatusCode)
        }
    }
        
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureNavigationBarWithCloseButton()
    }
    
//    //MARK:- Navigation
//    func presentHomeScreen() {
//        let storyboard : UIStoryboard = UIStoryboard.init(name: "Menu", bundle: nil)
//        let viewController = storyboard.instantiateInitialViewController()!
//        appDelegate.window!.rootViewController = viewController
//    }
    
    //MARK:- Navigation
//    func presentHomeScreen(isPushNotification: Bool = false) {
//        //        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
//        //        let viewController = storyboard.instantiateViewController(withIdentifier: "HomeViewController")
//        //        let naviagationController = UINavigationController(rootViewController: viewController)
//        //        appDelegate.window!.rootViewController = naviagationController
//        //        appDelegate.window!.makeKeyAndVisible()
//        if let _ = UserDefaultManager.shared.currentUser, let _ = UserDefaultManager.shared.authorization {
//            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
//            let viewController = storyboard.instantiateViewController(withIdentifier: "SWRevealViewController")
//            appDelegate.window!.rootViewController = viewController
//            appDelegate.window!.makeKeyAndVisible()
//        }
//    }
    
    
    func presentMyHomeScreen() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "MenuViewController")
        appDelegate.window!.rootViewController = viewController
        appDelegate.window!.makeKeyAndVisible()
    }
    
    func goToLogInn() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "LoginNavigationVC")
        appDelegate.window!.rootViewController = viewController
        appDelegate.window!.makeKeyAndVisible()
    }
    
    func presentSplashScreen() {
        let mainStoryboard : UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
        let splashScreenVC = mainStoryboard.instantiateViewController(withIdentifier: "LoginOptionsViewController")
        let navigationController = UINavigationController(rootViewController: splashScreenVC)
        appDelegate.window!.rootViewController = navigationController
    }
    
    //    func showPostDetails(postModel: PostModel, delegateHost: PostDetailsViewControllerDelegate? = nil) {
    //        let storyboard : UIStoryboard = UIStoryboard.init(name: "TabBar", bundle: nil)
    //        let viewController: PostDetailsViewController = storyboard.instantiateViewController(withIdentifier: "PostDetailsViewController") as! PostDetailsViewController
    //        viewController.postModel = postModel
    //        viewController.delegate = delegateHost
    //        self.navigationController?.pushViewController(viewController, animated: true)
    //    }
    
    func showPhotoOption() {
        let optionMenu = UIAlertController(title: nil, message: "ChoosePhoto", preferredStyle: .actionSheet)
        
        let deleteAction = UIAlertAction(title: "Gallery", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.openGallery()
        })
        let saveAction = UIAlertAction(title: "TakePhoto", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.openCamera()
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Cancelled")
        })
        
        optionMenu.addAction(deleteAction)
        optionMenu.addAction(saveAction)
        optionMenu.addAction(cancelAction)
        
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    func openGallery()
    {
        picker!.allowsEditing = false
        picker!.sourceType = UIImagePickerController.SourceType.photoLibrary
        picker!.navigationBar.adjustDefaultNavigationBar()
        present(picker!, animated: true, completion: nil)
    }
    
    
    func openCamera() {
        if (UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)) {
            picker!.allowsEditing = false
            picker!.sourceType = UIImagePickerController.SourceType.camera
            picker!.cameraCaptureMode = .photo
            present(picker!, animated: true, completion: nil)
        }
        else {
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }
    }
    
    func showError(error: APIError) {
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
        var style: ToastStyle = ToastStyle.init()
        style.imageSize = CGSize(width: 44, height: 44)
        self.view.makeToast(error.message, duration: 3.0, position: .bottom, title: nil, image: nil, style: style, completion: nil)
    }
    
    func showError(error: APIError, complete: @escaping (Bool)->Void) {
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
        var style: ToastStyle = ToastStyle.init()
        style.imageSize = CGSize(width: 44, height: 44)
        let messageModified: String = error.message! + "\nPlease press here to retry"
        
        self.view.makeToast(messageModified, duration: 60.0, position: .bottom, title: nil, image: nil, style: style, completion: complete)
    }
    
    func startLoading() {
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
        
        HUD.show(.progress)
    }
    
    func stopLoadingWithSuccess() {
        HUD.hide()
    }
    
    func stopLoadingWithError(error: APIError) {
        HUD.hide()
        showError(error: error)
    }
        
    func showAlert(title: String, message: String?, alertActions: [UIAlertAction]) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        for action in alertActions {
            alert.addAction(action)
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func makeLoginAlert() {
        let openAction = UIAlertAction(title: "go to login button".localized, style: .default) { (action) in
            self.goToLogInn()
        }
        let cancelAction = UIAlertAction(title: "cancel go to login".localized, style: .cancel)
        self.showAlert(title: "go to login title".localized, message: "go to login alert message".localized, alertActions: [openAction, cancelAction])
    }
        
    func checkLoginAlert() {
        guard let id: Int = UserDefaultManager.shared.currentUser?.id, id > 0 else {
            self.makeLoginAlert()
            return
        }
    }
    
    func showDeleteConfirmationAlert(content: String, confirmAction: @escaping (UIAlertAction) -> Void) {
        let noAction: UIAlertAction = UIAlertAction(title: "لا", style: .cancel, handler: nil)
        
        let yesAction: UIAlertAction = UIAlertAction(title: "نعم", style: .default, handler: confirmAction)
        
        showAlert(title: "هل تريد مسح \(content)", message: nil, alertActions: [yesAction, noAction])
    }
    
}

