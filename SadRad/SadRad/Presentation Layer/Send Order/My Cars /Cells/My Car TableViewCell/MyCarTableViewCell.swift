//
//  MyCarTableViewCell.swift
//  SadRad
//
//  Created by Omar Ibrahim on 9/9/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

protocol CarButtonsDelegate {
    func editCarButtonIsPressed(carID: Int)
    func deleteCarButtonIsPressed(carID: Int)
}

class MyCarTableViewCell: UITableViewCell {
    
    var carButtonsDelegate: CarButtonsDelegate?
    
    var myCarCellViewModel : MyCarCellViewModel? {
        didSet {
            bindData()
        }
    }
    
    
    @IBOutlet weak var myCarImageView: UIImageView!
    @IBOutlet weak var containView: UIView!
    @IBOutlet weak var carBrandLabel: UILabel!
    @IBOutlet weak var personsValueLabel: UILabel!
    @IBOutlet weak var conditionedLabel: UILabel!
    @IBOutlet weak var editLabel: UILabel!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var removeLabel: UILabel!
    @IBOutlet weak var removeButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
        configureView()
    }
    
    func configureView() {
        containView.addCornerRadius(raduis: 20, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        editLabel.text = "edit car button title".localized
        removeLabel.text = "delete car button title".localized
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
        configureView()
    }
    
    func bindData() {
        if let carID: Int = self.myCarCellViewModel?.id {
            self.tag = carID
            self.editButton.tag = carID
        }
        
        if let status: Int = self.myCarCellViewModel?.status {
            if status == 1 {
                self.editButton.isUserInteractionEnabled = false
                self.removeButton.isUserInteractionEnabled = false
            } else if status == 0 {
                self.editButton.isUserInteractionEnabled = true
                self.removeButton.isUserInteractionEnabled = true
            }
        }
        
        if self.myCarCellViewModel?.images?.count ?? 0 > 0 {
            let carImage = self.myCarCellViewModel?.images?[0]
            myCarImageView.loadImageFromUrl(imageUrl: MY_CARS_IMAGES_URL + carImage!)
        } else {
            myCarImageView.image = UIImage(named: "broken")
        }
        
        if let carBrand: String = self.myCarCellViewModel?.brand, let carModel: String = self.myCarCellViewModel?.model {
            self.carBrandLabel.text = carBrand + " " + carModel
        }
        
        if let numberOfPersonsPerCar: Int = self.myCarCellViewModel?.numberOfPersons {
            if "Lang".localized == "ar" {
                self.personsValueLabel.text = String(numberOfPersonsPerCar) + " " + "أشخاص"
            } else if "Lang".localized == "en" {
                self.personsValueLabel.text = String(numberOfPersonsPerCar) + " " + "Persons"
            }
        }
        
        if let airConditionedBool: Bool = self.myCarCellViewModel?.airCondition {
            if airConditionedBool == true {
                if "Lang".localized == "ar" {
                    conditionedLabel.text = "مكيف"
                } else if "Lang".localized == "en" {
                    conditionedLabel.text = "Conditioned"
                }
            } else if airConditionedBool == false {
                if "Lang".localized == "ar" {
                    conditionedLabel.text = "غير مكيف"
                } else if "Lang".localized == "en" {
                    conditionedLabel.text = "Not conditioned"
                }
            }
        }
        
    }
    
    
    func editCarButtonIsPressed(carID: Int) {
        if let delegateValue = carButtonsDelegate {
            delegateValue.editCarButtonIsPressed(carID: carID)
        }
    }
    
    func deleteCarButtonIsPressed(carID: Int) {
        if let delegateValue = carButtonsDelegate {
            delegateValue.deleteCarButtonIsPressed(carID: carID)
        }
    }
    
    @IBAction func editButtonIsPressed(_ sender: Any) {
        print("Edit")
        editCarButtonIsPressed(carID: self.editButton.tag)
    }
    
    @IBAction func removeButtonIsPressed(_ sender: Any) {
        print("Remove")
        deleteCarButtonIsPressed(carID: self.editButton.tag)
    }
    
}
