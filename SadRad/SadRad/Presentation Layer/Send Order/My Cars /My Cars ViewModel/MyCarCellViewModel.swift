//
//  MyCarCellViewModel.swift
//  SadRad
//
//  Created by Omar Ibrahim on 10/11/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation

struct MyCarCellViewModel {
    let id: Int?
//    let image: String?
    let brand: String?
    let model: String?
    let numberOfPersons: Int?
    let airCondition: Bool?
    let images: [String]?
    let status: Int?
}
