//
//  MyCarsViewModel.swift
//  SadRad
//
//  Created by Omar Ibrahim on 10/11/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation

class MyCarsViewModel {
    
    private var myCars: [MyCar] = [MyCar]()
    var selectedCar: MyCar?
    
    enum CarsExistanceState {
        case noCarsExisted
        case carsExisted
    }
    
    var error: APIError?
    
    var myCarCellViewModels: [MyCarCellViewModel] = [MyCarCellViewModel]() {
        didSet {
            self.reloadTableViewClosure?()
        }
    }
    
    var carsExistance: CarsExistanceState = .noCarsExisted {
        didSet {
            self.carExistanceStateClosure?()
        }
    }
        
    var reloadTableViewClosure: (()->())?
    var updateLoadingStatus: (()->())?
    var deleteMyCarClosure: (()->())?
    var carExistanceStateClosure: (()->())?
    
    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    var deleted: Bool = false {
        didSet {
            self.deleteMyCarClosure?()
        }
    }

    var numberOfMyCarsCells: Int {
        return myCarCellViewModels.count
    }
    
    func initFetch() {
        state = .loading
        
        let params: [String : AnyObject] = [:]
        
        CarsAPIManager().getMyCars(basicDictionary: params, onSuccess: { (myCars) in
            
            self.processMyCars(myCars: myCars)
            self.state = .populated

        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    func deleteMyCar(carID: Int) {
        state = .loading

        let params: [String : AnyObject] = [
            "car_id" : carID as AnyObject
        ]

        CarsAPIManager().deleteMyCar(basicDictionary: params, onSuccess: { (deleted) in

            print(deleted)
            self.deleted = deleted            
            self.state = .populated

        }) { (error) in
            self.error = error
            self.state = .error
        }
    }

    func processMyCars( myCars: [MyCar] ) {
        
        if myCars.count == 0 {
            self.carsExistance = .noCarsExisted
        } else {
            self.carsExistance = .carsExisted
        }
        
        self.myCars = myCars // Cache
        var vms = [MyCarCellViewModel]()
        for myCar in myCars {
            vms.append( createMyCarCellViewModel(myCar: myCar))
        }
        self.myCarCellViewModels = vms
    }
    
    func createMyCarCellViewModel( myCar: MyCar ) -> MyCarCellViewModel {
        if "Lang".localized == "en" {
            return MyCarCellViewModel(id: myCar.id, brand: myCar.carBrand?.nameEn, model: myCar.carModel?.nameEn, numberOfPersons: myCar.numberOfPersons, airCondition: myCar.airConditioned, images: myCar.images, status: myCar.status)
        } else if "Lang".localized == "ar" {
            return MyCarCellViewModel(id: myCar.id, brand: myCar.carBrand?.nameAr, model: myCar.carModel?.nameAr, numberOfPersons: myCar.numberOfPersons, airCondition: myCar.airConditioned, images: myCar.images, status: myCar.status)
        } else {
            return MyCarCellViewModel(id: myCar.id, brand: myCar.carBrand?.nameEn, model: myCar.carModel?.nameEn, numberOfPersons: myCar.numberOfPersons, airCondition: myCar.airConditioned, images: myCar.images, status: myCar.status)
        }
    }
    
    func getError() -> APIError {
        return self.error!
    }
    
    func getMyCarCellViewModel( at indexPath: IndexPath ) -> MyCarCellViewModel {
        return myCarCellViewModels[indexPath.row]
    }
    
    func getDeletedBool() -> Bool {
        return self.deleted
    }
    
    func myCarPressed( at indexPath: Int ){
        let myCar = self.myCars[indexPath]

        self.selectedCar = myCar
    }
    
}
