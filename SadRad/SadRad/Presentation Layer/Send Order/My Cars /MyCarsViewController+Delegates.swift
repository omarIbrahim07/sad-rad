//
//  MyCarsViewController+Delegates.swift
//  SadRad
//
//  Created by Omar Ibrahim on 9/9/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

extension MyCarsViewController: CarButtonsDelegate {
    func editCarButtonIsPressed(carID: Int) {
        print(carID)
        goToEditCar(carID: carID)
    }
    
    func deleteCarButtonIsPressed(carID: Int) {
        self.deleteMyCarAlert(carID: carID, title: "delete my car title".localized, message: "delete my car message".localized)
//        self.viewModel.deleteMyCar(carID: carID)
    }
}

extension MyCarsViewController: UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfMyCarsCells
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell: MyCarTableViewCell = tableView.dequeueReusableCell(withIdentifier: "MyCarTableViewCell") as? MyCarTableViewCell {
            
            let cellVM = viewModel.getMyCarCellViewModel( at: indexPath )
            cell.myCarCellViewModel = cellVM
            cell.carButtonsDelegate = self

            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
