//
//  MyCarsViewController.swift
//  SadRad
//
//  Created by Omar Ibrahim on 9/9/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class MyCarsViewController: BaseViewController {

    lazy var viewModel: MyCarsViewModel = {
        return MyCarsViewModel()
    }()
    
    var error: APIError?

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addCarButton: UIButton!
    @IBOutlet weak var noCarsLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setNavigationTitle()
        configureView()
        configureTableView()
        initVM()
    }
    
    func setNavigationTitle() {
        navigationItem.title = "my cars vc title".localized
    }
    
    func configureView() {
        addCarButton.addCornerRadius(raduis: addCarButton.frame.height / 2, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        addCarButton.setTitle("add car button title".localized, for: .normal)
        self.noCarsLabel.text = "no cars label".localized
        self.noCarsLabel.isHidden = true
    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "MyCarTableViewCell", bundle: nil), forCellReuseIdentifier: "MyCarTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 20))
        tableView.tableFooterView = UIView()
        
    }
    
    func initVM() {

        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                
                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                case .error:
                    self.error = self.viewModel.getError()
                    if let error = self.error {
                        self.stopLoadingWithError(error: error)
                    }
                case .empty:
                    print("")
                }
            }
        }
        
        viewModel.carExistanceStateClosure = { [weak self] () in
            guard let self = self else {
                return
            }
            
            switch self.viewModel.carsExistance {
                
            case .noCarsExisted:
                self.noCarsLabel.isHidden = false
                self.tableView.isHidden = true
            case .carsExisted:
                self.noCarsLabel.isHidden = true
                self.tableView.isHidden = false
            }
        }

        viewModel.reloadTableViewClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
        
        viewModel.deleteMyCarClosure = { [weak self] () in
            DispatchQueue.main.async {
                if let deleted: Bool = self?.viewModel.getDeletedBool() {
                    if deleted == true {
                        self?.showCarPopUp(title: "delete car alert title".localized, message: "delete car alert message".localized, actionTitle: "done action title".localized)
                    }
                }
            }
        }
                
        viewModel.initFetch()
    }
    
    func showCarPopUp(title: String, message: String, actionTitle: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let openAction = UIAlertAction(title: actionTitle, style: .default) { (action) in
            self.viewModel.initFetch()
        }
//        let cancelAction = UIAlertAction(title: "cancel delete car".localized, style: .cancel)
        alertController.addAction(openAction)
//        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func deleteMyCarAlert(carID: Int, title: String, message: String) {
        let deleteMyTripAction = UIAlertAction(title: "confirm delete my car action title".localized, style: .default) { (action) in
            self.viewModel.deleteMyCar(carID: carID)
        }
        let cancelAction = UIAlertAction(title: "cancel delete my car action title".localized, style: .cancel)
        self.showAlert(title: title, message: message, alertActions: [deleteMyTripAction, cancelAction])
    }
    
    // MARK:- Actions
    @IBAction func addCarButtonIsPressed(_ sender: Any) {
        print("ADD CAR")
        goToAddCar()
    }
    
    // MARK: - Navigation
    func goToAddCar() {
        if let addCarVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AddCarViewController") as? AddCarViewController {
            //                rootViewContoller.test = "test String"
//            self.present(addCarVC, animated: true, completion: nil)
            self.navigationController?.pushViewController(addCarVC, animated: true)
            print("Add Car")
        }
    }
    
    func goToEditCar(carID: Int) {
        if let editCarVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AddCarViewController") as? AddCarViewController {
            //                rootViewContoller.test = "test String"
            editCarVC.editBool = true
            editCarVC.carID = carID
//            self.present(editCarVC, animated: true, completion: nil)
            self.navigationController?.pushViewController(editCarVC, animated: true)
            print("Add Car")
        }
    }
    
}
