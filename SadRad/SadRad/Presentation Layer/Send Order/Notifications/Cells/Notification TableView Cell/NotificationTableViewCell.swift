//
//  NotificationTableViewCell.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 12/1/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {

    @IBOutlet weak var stateLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    var photoListCellViewModel : NotificationCellViewModel? {
        didSet {
            if let message: String = photoListCellViewModel?.message {
                stateLabel.text = message
            }
            
            if let date: String = photoListCellViewModel?.date {
                dateLabel.text = date
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureView()
    }
    
    func configureView() {
        containerView.layer.cornerRadius = 8.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
