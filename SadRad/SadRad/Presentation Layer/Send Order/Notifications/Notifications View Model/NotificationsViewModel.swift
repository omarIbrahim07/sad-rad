//
//  NotificationsViewModel.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 12/12/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation

class NotificationsViewModel {
    
    private var notifications: [Notificationn] = [Notificationn]()
    var selectedNotification: Notificationn?
    
    var error: APIError?
    
    var cellViewModels: [NotificationCellViewModel] = [NotificationCellViewModel]() {
           didSet {
               self.reloadTableViewClosure?()
           }
       }
    
    var reloadTableViewClosure: (()->())?
    var updateLoadingStatus: (()->())?
    
    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    var numberOfCells: Int {
         return cellViewModels.count
     }
    
    func initFetch() {
        state = .loading
        
        NotificationsAPIManager().getNotifications(basicDictionary: [:], onSuccess: { (notifications) in
            
            self.processFetchedPhoto(notifications: notifications)
            self.state = .populated

        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    func processFetchedPhoto( notifications: [Notificationn] ) {
          self.notifications = notifications // Cache
          var vms = [NotificationCellViewModel]()
          for notification in notifications {
              vms.append( createCellViewModel(notification: notification) )
          }
          self.cellViewModels = vms
      }
    
    func createCellViewModel( notification: Notificationn ) -> NotificationCellViewModel {
        
        if "Lang".localized == "ar" {
//            return HomeCellViewModel(titleText: service.name!, imageUrl: service.image!)
            return NotificationCellViewModel(orderID: notification.orderId!, message: notification.message, date: notification.updatedAt!)
        } else if "Lang".localized == "en" {
            return NotificationCellViewModel(orderID: notification.orderId!, message: notification.messageEN, date: notification.updatedAt!)
        }
        
        return NotificationCellViewModel(orderID: notification.orderId!, message: notification.messageEN, date: notification.updatedAt!)
    }
    
    func getError() -> APIError {
        return self.error!
    }
    
    func getCellViewModel( at indexPath: IndexPath ) -> NotificationCellViewModel {
        return cellViewModels[indexPath.row]
    }
    
    func userPressed( at indexPath: IndexPath ){
        let notification = self.notifications[indexPath.row]

        self.selectedNotification = notification
    }

    
}
