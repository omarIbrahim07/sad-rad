//
//  RatesViewController.swift
//  SadRad
//
//  Created by Omar Ibrahim on 9/15/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit
import Cosmos

class RatesViewController: BaseViewController {

    lazy var viewModel: RatesViewModel = {
       return RatesViewModel()
    }()
    
    var error: APIError?
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var profileUserNameLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var profileRatingView: CosmosView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
        configureTableView()
        initVM()
    }
        
    func initVM() {
        
        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }
            
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                    
                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                case .error:
                    self.error = self.viewModel.getError()
                    if let error = self.error {
                        self.stopLoadingWithError(error: error)
                    }
                case .empty:
                    print("")
                }
            }
        }
        
        viewModel.reloadTableViewClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
        
        viewModel.updateProfileDataClosure = { [weak self] () in
            DispatchQueue.main.async {
                if let profileData: ProfileData = self?.viewModel.profileData {
                    self?.bindProfileData(profileData: profileData)
                }
            }
        }
                
        if self.viewModel.isDriverID == false {
            viewModel.fetchProfileData()
        } else if self.viewModel.isDriverID == true {
            if let driverID: Int = self.viewModel.driverID {
                self.viewModel.fetchDriverProfileData(driverID: driverID)
            }
        }
    }

    func bindProfileData(profileData: ProfileData) {
        if let userName: String = profileData.userName {
            profileUserNameLabel.text = userName
        }
        if let userImage: String = profileData.image {
            profileImageView.loadImageFromUrl(imageUrl: PROFILE_IMAGE_URL + userImage)
        }
        if let userRate: Float = profileData.overallRating {
            profileRatingView.rating = Double(userRate)
        }
    }
    
    func configureView() {
        tableView.roundCorners([.topLeft, .topRight], radius: 40)
        navigationItem.title = "rates".localized
        profileImageView.addCornerRadius(raduis: profileImageView.frame.height / 2, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "ClientRateTableViewCell", bundle: nil), forCellReuseIdentifier: "ClientRateTableViewCell")
        tableView.register(UINib(nibName: "RatesTitleTableViewCell", bundle: nil), forCellReuseIdentifier: "RatesTitleTableViewCell")
        tableView.register(UINib(nibName: "ImagesInfoTableViewCell", bundle: nil), forCellReuseIdentifier: "ImagesInfoTableViewCell")
        tableView.register(UINib(nibName: "NumberOfJourniesTableViewCell", bundle: nil), forCellReuseIdentifier: "NumberOfJourniesTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
//        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 25))
    }
    
    // MARK: - Navigation

}
