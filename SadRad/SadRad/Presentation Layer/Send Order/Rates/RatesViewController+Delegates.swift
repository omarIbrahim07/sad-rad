//
//  RatesViewController+Delegates.swift
//  SadRad
//
//  Created by Omar Ibrahim on 9/15/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

extension RatesViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(3 + self.viewModel.profileRateCellViewModels.count)
        return 3 + self.viewModel.profileRateCellViewModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            if let cell: NumberOfJourniesTableViewCell = tableView.dequeueReusableCell(withIdentifier: "NumberOfJourniesTableViewCell") as? NumberOfJourniesTableViewCell {
                
                let cellVM = viewModel.getProfileNumberOfTripsCellViewModel()
                cell.numberOfCellsCellViewModel = cellVM

                return cell
            }
        } else if indexPath.row == 1 {
            if let cell: ImagesInfoTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ImagesInfoTableViewCell") as? ImagesInfoTableViewCell {
                
                let cellVM = viewModel.getProfileMainRatesCellViewModel()
                cell.profileMainRatesCellViewModel = cellVM
                
                return cell
            }
        } else if indexPath.row == 2 {
            if let cell: RatesTitleTableViewCell = tableView.dequeueReusableCell(withIdentifier: "RatesTitleTableViewCell") as? RatesTitleTableViewCell {
                
                return cell
            }
        }
        
        if let cell: ClientRateTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ClientRateTableViewCell") as? ClientRateTableViewCell {
            
            let cellVM = viewModel.getClientRateCellViewModel(at: indexPath)
            cell.clientRateCellViewModel = cellVM
            
            let lastIndex = 2 + self.viewModel.profileRateCellViewModels.count
            if indexPath.row == lastIndex {
                cell.bottomView.isHidden = true
            }

            return cell
        }
        
        return UITableViewCell()
    }
    
    
}
