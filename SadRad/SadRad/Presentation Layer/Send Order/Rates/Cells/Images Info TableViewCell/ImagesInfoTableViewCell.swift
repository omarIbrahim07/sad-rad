//
//  ImagesInfoTableViewCell.swift
//  SadRad
//
//  Created by Omar Ibrahim on 9/15/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class ImagesInfoTableViewCell: UITableViewCell {
    
    var profileMainRatesCellViewModel: ProfileMainRatesCellViewModel? {
        didSet {
            if let transportationRate = self.profileMainRatesCellViewModel?.transportationRate {
                self.transportationRateLabel.text = String(transportationRate)
            }
            if let carRate = self.profileMainRatesCellViewModel?.carRate {
                self.carRateLabel.text = String(carRate)
            }
            if let driveryRate = self.profileMainRatesCellViewModel?.driveryRate {
                self.driveryRateLabel.text = String(driveryRate)
            }
        }
    }

    @IBOutlet weak var containView: UIView!
    @IBOutlet weak var transportationView: UIView!
    @IBOutlet weak var carView: UIView!
    @IBOutlet weak var driveryView: UIView!
    @IBOutlet weak var transportationRateLabel: UILabel!
    @IBOutlet weak var carRateLabel: UILabel!
    @IBOutlet weak var driveryRateLabel: UILabel!
    @IBOutlet weak var transportationTitleLabel: UILabel!
    @IBOutlet weak var carTitleLabel: UILabel!
    @IBOutlet weak var driveryTitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureView()
        setLoclization()
    }
    
    func setLoclization() {
        transportationTitleLabel.text = "transportation label".localized
        carTitleLabel.text = "car label".localized
        driveryTitleLabel.text = "drivery label".localized
    }


    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureView() {
        containView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0)
        transportationView.addCornerRadius(raduis: transportationView.frame.height / 2, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        carView.addCornerRadius(raduis: carView.frame.height / 2, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        driveryView.addCornerRadius(raduis: driveryView.frame.height / 2, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
    }
    
}
