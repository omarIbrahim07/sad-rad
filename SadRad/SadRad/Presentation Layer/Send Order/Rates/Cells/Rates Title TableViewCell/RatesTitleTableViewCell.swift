//
//  RatesTitleTableViewCell.swift
//  SadRad
//
//  Created by Omar Ibrahim on 9/15/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class RatesTitleTableViewCell: UITableViewCell {

    @IBOutlet weak var ratesTitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setLoclization()
    }
    
    func setLoclization() {
        ratesTitleLabel.text = "rates label".localized
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
