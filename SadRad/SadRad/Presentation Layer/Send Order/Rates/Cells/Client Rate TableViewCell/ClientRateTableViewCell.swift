//
//  ClientRateTableViewCell.swift
//  SadRad
//
//  Created by Omar Ibrahim on 9/15/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit
import Cosmos

class ClientRateTableViewCell: UITableViewCell {
    
    var clientRateCellViewModel: ProfileRateCellViewModel? {
        didSet {
            if let userName: String = self.clientRateCellViewModel?.clientName {
                self.userNameLabel.text = userName
            }
            if let userImage: String = self.clientRateCellViewModel?.clientImage {
                self.userImageView.loadImageFromUrl(imageUrl: PROFILE_IMAGE_URL + userImage)
            }
            if let date: String = self.clientRateCellViewModel?.date {
                self.dateLabel.text = date
            }
            if let description: String = self.clientRateCellViewModel?.description {
                self.userDescriptionLabel.text = description
            }
            if let userRate: Float = self.clientRateCellViewModel?.tripRate {
                self.userRateCosmosView.rating = Double(userRate)
            }
        }
    }

    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var containView: UIView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userRateCosmosView: CosmosView!
    @IBOutlet weak var userDescriptionLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var bottomView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureView() {
        containView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0)
        userImageView.addCornerRadius(raduis: userImageView.frame.height / 2, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
    }
}
