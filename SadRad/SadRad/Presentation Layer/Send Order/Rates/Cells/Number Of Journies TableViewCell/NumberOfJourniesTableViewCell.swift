//
//  NumberOfJourniesTableViewCell.swift
//  SadRad
//
//  Created by Omar Ibrahim on 9/15/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class NumberOfJourniesTableViewCell: UITableViewCell {

    var numberOfCellsCellViewModel: ProfileNumberOfTripsCellViewModel? {
        didSet {
            if let numberOfTrips: Int = self.numberOfCellsCellViewModel?.numberOfTrips {
                self.numberOfTripsValueLabel.text = String(numberOfTrips)
            }
        }
    }
    
    @IBOutlet weak var numberOfTripsTitleLabel: UILabel!
    @IBOutlet weak var numberOfTripsValueLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setLoclization()
    }
    
    func setLoclization() {
        numberOfTripsTitleLabel.text = "number of trips label".localized
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
