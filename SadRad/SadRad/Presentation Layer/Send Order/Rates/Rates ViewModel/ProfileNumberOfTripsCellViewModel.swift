//
//  ProfileNumberOfTripsCellViewModel.swift
//  SadRad
//
//  Created by Omar Ibrahim on 11/4/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation

struct ProfileNumberOfTripsCellViewModel {
    let numberOfTrips: Int?
}
