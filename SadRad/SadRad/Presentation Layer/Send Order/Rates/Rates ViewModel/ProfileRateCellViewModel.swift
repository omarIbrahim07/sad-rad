//
//  ProfileRateCellViewModel.swift
//  SadRad
//
//  Created by Omar Ibrahim on 11/4/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation

struct ProfileRateCellViewModel {
    let clientName: String?
    let tripRate: Float?
    let clientImage: String?
    let date: String?
    let description: String?
}
