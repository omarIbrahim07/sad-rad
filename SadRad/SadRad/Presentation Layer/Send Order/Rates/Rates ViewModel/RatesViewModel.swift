//
//  RatesViewModel.swift
//  SadRad
//
//  Created by Omar Ibrahim on 11/4/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation

class RatesViewModel {
    
    var driverID: Int?
    var isDriverID: Bool? = false
    var profileData: ProfileData?
    var profileRates: [ProfileRate] = [ProfileRate]()
    
    var updateLoadingStatus: (()->())?
    var reloadTableViewClosure: (()->())?
    var updateProfileDataClosure: (()->())?

    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    var updateProfile: Bool = false {
        didSet {
            self.updateProfileDataClosure?()
        }
    }

    var profileRateCellViewModels: [ProfileRateCellViewModel] = [ProfileRateCellViewModel]() {
        didSet {
            self.reloadTableViewClosure?()
        }
    }
    
    var profileMainRateCellViewModel: ProfileMainRatesCellViewModel? {
        didSet {
            self.reloadTableViewClosure?()
        }
    }
    
    var profileNumberOfTripsCellViewModel: ProfileNumberOfTripsCellViewModel? {
        didSet {
            self.reloadTableViewClosure?()
        }
    }

    var error: APIError?
    
    func fetchProfileData() {
        state = .loading
        
        let params: [String : AnyObject] = [:]
        
        AuthenticationAPIManager().getProfileData(basicDictionary: params, onSuccess: { (profileData) in
            
            self.profileData = profileData
            self.updateProfile = true
            self.profileNumberOfTripsCellViewModel = self.createNumberOfPersonsViewModel(profileData: profileData)
            self.profileMainRateCellViewModel = self.createProfileMainRatesViewModel(profileData: profileData)
            if profileData.profileRates?.count ?? 0 > 0 {
                self.processClientsRates(rates: profileData.profileRates!)
            }
            self.state = .populated
            
        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    func fetchDriverProfileData(driverID: Int) {
        state = .loading
        
        let params: [String : AnyObject] = [
            "driver_id" : driverID as AnyObject
        ]
        
        AuthenticationAPIManager().getDriverProfileData(basicDictionary: params, onSuccess: { (profileData) in
            
            self.profileData = profileData
            self.updateProfile = true
            self.profileNumberOfTripsCellViewModel = self.createNumberOfPersonsViewModel(profileData: profileData)
            self.profileMainRateCellViewModel = self.createProfileMainRatesViewModel(profileData: profileData)
            if profileData.profileRates?.count ?? 0 > 0 {
                self.processClientsRates(rates: profileData.profileRates!)
            }
            self.state = .populated
            
        }) { (error) in
            self.error = error
            self.state = .error
        }
    }

    
    func processClientsRates( rates: [ProfileRate] ) {
        self.profileRates = rates // Cache
        var vms = [ProfileRateCellViewModel]()
        for rate in rates {
            vms.append( createClientRateCellViewModel(rate: rate))
        }
        self.profileRateCellViewModels = vms
    }
        
    func createNumberOfPersonsViewModel( profileData: ProfileData ) -> ProfileNumberOfTripsCellViewModel {
        return ProfileNumberOfTripsCellViewModel(numberOfTrips: profileData.numberOfTrips)
    }

    func createProfileMainRatesViewModel( profileData: ProfileData ) -> ProfileMainRatesCellViewModel {
        return ProfileMainRatesCellViewModel(transportationRate: profileData.navigationRate, carRate: profileData.carRate, driveryRate: profileData.driverRate)
    }
        
    func createClientRateCellViewModel( rate: ProfileRate ) -> ProfileRateCellViewModel {
        return ProfileRateCellViewModel(clientName: rate.clientName, tripRate: rate.tripRate, clientImage: rate.clientImage, date: rate.date, description: rate.description)
    }
        
//    func processReasonsForCancelTrip( reasons: [ReasonForCancel] ) {
//        self.reasons = reasons // Cache
//        var vms = [CancelJourneyCellViewModel]()
//        for reason in reasons {
//            vms.append( createCancelTripCellViewModel(reason: reason))
//        }
//        self.reasonsCellViewModels = vms
//    }
    
    func getClientRateCellViewModel( at indexPath: IndexPath ) -> ProfileRateCellViewModel {
        return self.profileRateCellViewModels[indexPath.row - 3]
    }
    
    func getProfileNumberOfTripsCellViewModel() -> ProfileNumberOfTripsCellViewModel? {
        return self.profileNumberOfTripsCellViewModel
    }
    
    func getProfileMainRatesCellViewModel() -> ProfileMainRatesCellViewModel? {
        return self.profileMainRateCellViewModel
    }

    func getError() -> APIError {
        return self.error!
    }
}
