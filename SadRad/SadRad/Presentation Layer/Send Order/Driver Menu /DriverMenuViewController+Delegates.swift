//
//  DriverMenuViewController+Delegates.swift
//  SadRad
//
//  Created by Omar Ibrahim on 12/26/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

extension DriverMenuViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titlesEn.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell: ProfileTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ProfileTableViewCell") as? ProfileTableViewCell {
            
            if "Lang".localized == "en" {
                cell.configureCell(index: indexPath.row, title: titlesEn[indexPath.row], image: images[indexPath.row])
            } else if "Lang".localized == "ar" {
                cell.configureCell(index: indexPath.row, title: titlesAr[indexPath.row], image: images[indexPath.row])
            }
            
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        checkLoginAlert()
        guard let id: Int = UserDefaultManager.shared.currentUser?.id, id > 0 else {
            return
        }
        if indexPath.row == 0 {
            self.goToMyCars()
        } else if indexPath.row == 1 {
            self.goToAddJourney()
        } else if indexPath.row == 2 {
            self.goToMyJourneis()
        } else if indexPath.row == 3 {
            self.goToMyProfile()
        }
    }
    
}
