//
//  DriverMenuViewController.swift
//  SadRad
//
//  Created by Omar Ibrahim on 12/26/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class DriverMenuViewController: BaseViewController {

    var images: [String] = ["my-cars", "Add--trip", "My-travels", "proile"]
    var titlesEn: [String] = ["My Cars", "+ Add Trip", "My Trips", "Profile"]
    var titlesAr: [String] = [
        "عربياتي",
        "إضافة رحلة",
        "الرحلات",
        "الملف الشخصي",
    ]
    
    @IBOutlet weak var driverImageView: UIImageView!
    @IBOutlet weak var driverNameLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var driverPhoneLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
        bindProfileData()
        configureTableView()
    }

    func configureView() {
        driverImageView.addCornerRadius(raduis: driverImageView.frame.height / 2, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
    }
    
    func bindProfileData() {
        if let user = UserDefaultManager.shared.currentUser {
            if let name: String = user.name {
                driverNameLabel.text = name
            }
            
            if let image: String = user.image {
                driverImageView.loadImageFromUrl(imageUrl: ImageURL_USERS + image)
            }
            
            if let phone: String = user.phone {
                self.driverPhoneLabel.text = phone
            }
        }
    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "ProfileTableViewCell", bundle: nil), forCellReuseIdentifier: "ProfileTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 33, right: 0)
    }

    // MARK: - Navigation
    func goToMyCars() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "MyCarsViewController") as! MyCarsViewController
        navigationController?.pushViewController(viewController, animated: true)
    }
    func goToAddJourney() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "AddJourneyViewController") as! AddJourneyViewController
        navigationController?.pushViewController(viewController, animated: true)
    }
    func goToMyJourneis() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "MyJourniesViewController") as! MyJourniesViewController
        navigationController?.pushViewController(viewController, animated: true)
    }
    func goToMyProfile() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "RatesViewController") as! RatesViewController
        viewController.viewModel.driverID = UserDefaultManager.shared.currentUser?.id
        viewController.viewModel.isDriverID = true
        self.present(viewController, animated: true, completion: nil)
    }
}
