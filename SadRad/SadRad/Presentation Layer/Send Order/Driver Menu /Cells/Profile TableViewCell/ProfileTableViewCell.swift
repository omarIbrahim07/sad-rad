//
//  ProfileTableViewCell.swift
//  Global On
//
//  Created by Omar Ibrahim on 11/4/20.
//  Copyright © 2020 Global On. All rights reserved.
//

import UIKit

class ProfileTableViewCell: UITableViewCell {

    @IBOutlet weak var itemView: UIView!
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backImageView: UIImageView!
    @IBOutlet weak var bottomView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
    }

    func configureCell(index: Int, title: String, image: String) {
        titleLabel.text = title
        itemImageView.image = UIImage(named: image)
        if "Lang".localized == "en" {
            self.backImageView.image = UIImage(named: "backk")
        } else if "Lang".localized == "ar" {
            self.backImageView.image = UIImage(named: "bacck")
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
