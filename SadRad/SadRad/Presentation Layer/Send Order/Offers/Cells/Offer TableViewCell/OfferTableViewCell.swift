//
//  OfferTableViewCell.swift
//  SadRad
//
//  Created by Omar Ibrahim on 9/8/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit
import Cosmos

class OfferTableViewCell: UITableViewCell {
    
    var tripOfferCellViewModel : OfferCellViewModel? {
        didSet {
            if let tripID: Int = self.tripOfferCellViewModel?.tripID {
                self.tag = tripID
//                self.cancelTripButton.tag = reservationID
            }
            
            //            if let carImage: String = self.myCarCellViewModel?.image {
            //                myCarImageView.loadImageFromUrl(imageUrl: MY_CARS_IMAGES_URL + carImage)
            //            }
            if self.tripOfferCellViewModel?.images?.count ?? 0 > 0 {
                let carImage = self.tripOfferCellViewModel?.images?[0]
                offerImageView.loadImageFromUrl(imageUrl: MY_CARS_IMAGES_URL + carImage!)
            }
            
            if let price: Int = self.tripOfferCellViewModel?.price {
                self.journeyPriceLabel.text = String(price)
                if "Lang".localized == "en" {
                    self.currencyLabel.text = "EGP"
                } else if "Lang".localized == "ar" {
                    self.currencyLabel.text = "ج.م"
                }
            }
            if let numberOfPersons: Int = self.tripOfferCellViewModel?.numberOfPersons {
                if "Lang".localized == "en" {
                    self.numberOfPersonsLabel.text = String(numberOfPersons)
                    self.personLabel.text = " Person"
                } else if "Lang".localized == "ar" {
                    self.numberOfPersonsLabel.text = String(numberOfPersons)
                    self.personLabel.text = " راكب"
                }
            }
            if let brand: String = self.tripOfferCellViewModel?.brand, let model: String = self.tripOfferCellViewModel?.model {
                self.carBrandLabel.text = brand + " " + model
            }
            if let rating: Float = self.tripOfferCellViewModel?.rating {
                self.rateCosmosView.rating = Double(rating)
            }
            if let numberOfRates: Int = self.tripOfferCellViewModel?.numberOfRates {
                self.numberOfRatesLabel.text = String(numberOfRates)
            }
            if let date: String = self.tripOfferCellViewModel?.date ,let time: String = self.tripOfferCellViewModel?.time {
                self.dateLabel.text = date + "  " + time.substring(to: 5)
            }
//            if let driverName: String = self.tripOfferCellViewModel?.driverName {
//                self.carDriverNameLabel.text = driverName
//            }
        }
    }

    @IBOutlet weak var offerImageView: UIImageView!
    @IBOutlet weak var containView: UIView!
    @IBOutlet weak var carBrandLabel: UILabel!
    @IBOutlet weak var journeyPriceLabel: UILabel!
    @IBOutlet weak var numberOfPersonsLabel: UILabel!
    @IBOutlet weak var personLabel: UILabel!
    @IBOutlet weak var numberOfRatesLabel: UILabel!
    @IBOutlet weak var rateCosmosView: CosmosView!
    @IBOutlet weak var currencyLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureView()
        self.selectionStyle = .none
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configureView() {
        containView.addCornerRadius(raduis: 8, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
    }
    
}
