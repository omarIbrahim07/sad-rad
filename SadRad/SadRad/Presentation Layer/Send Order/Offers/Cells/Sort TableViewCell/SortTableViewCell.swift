//
//  SortTableViewCell.swift
//  SadRad
//
//  Created by Omar Ibrahim on 10/28/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

protocol SortButtonsDelegate {
    func sortButtonIsPressed(sort: Int)
}

class SortTableViewCell: UITableViewCell {
    
    var sortDelegate: SortButtonsDelegate?

    @IBOutlet weak var containView: UIView!
    @IBOutlet weak var timeAndDateButton: UIButton!
    @IBOutlet weak var leastPriceButton: UIButton!
    @IBOutlet weak var bestRateButton: UIButton!
    @IBOutlet weak var timeAndDateCheckmarkImageView: UIImageView!
    @IBOutlet weak var leastPriceCheckmarkImageView: UIImageView!
    @IBOutlet weak var bestRateCheckmarkImageView: UIImageView!
    @IBOutlet weak var timeAndDateLabel: UILabel!
    @IBOutlet weak var leastPriceLabel: UILabel!
    @IBOutlet weak var bestRateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureView()
        setLocalization()
    }
    
    func setLocalization() {
        timeAndDateLabel.text = "time and rate title".localized
        leastPriceLabel.text = "least price title".localized
        bestRateLabel.text = "best rate title".localized
    }
    
    func configureView() {
        timeAndDateCheckmarkImageView.isHidden = true
        leastPriceCheckmarkImageView.isHidden = true
        bestRateCheckmarkImageView.isHidden = true
        containView.roundCorners([.bottomLeft, .bottomRight], radius: 12)
    }
    
    func checkSortChoosed(sort: Int?) {
        if sort == 1 {
            timeAndDateCheckmarkImageView.isHidden = false
            leastPriceCheckmarkImageView.isHidden = true
            bestRateCheckmarkImageView.isHidden = true
        } else if sort == 2 {
            timeAndDateCheckmarkImageView.isHidden = true
            leastPriceCheckmarkImageView.isHidden = false
            bestRateCheckmarkImageView.isHidden = true
        } else if sort == 3 {
            timeAndDateCheckmarkImageView.isHidden = true
            leastPriceCheckmarkImageView.isHidden = true
            bestRateCheckmarkImageView.isHidden = false
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func sortButtonIsPressed(sort: Int) {
        if let delegateValue = sortDelegate {
            delegateValue.sortButtonIsPressed(sort: sort)
        }
    }

    
    @IBAction func timeAndDateButtonIsPressed(_ sender: Any) {
        sortButtonIsPressed(sort: 1)
    }
    
    
    @IBAction func leastPriceButtonIsPressed(_ sender: Any) {
        sortButtonIsPressed(sort: 2)
    }
    
    @IBAction func bestRateButtonIsPressed(_ sender: Any) {
        sortButtonIsPressed(sort: 3)
    }
    
}
