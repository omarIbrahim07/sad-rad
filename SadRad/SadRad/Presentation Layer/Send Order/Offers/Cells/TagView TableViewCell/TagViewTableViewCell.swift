//
//  TagViewTableViewCell.swift
//  SadRad
//
//  Created by Omar Ibrahim on 9/17/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit
import TagListView

class TagViewTableViewCell: UITableViewCell {
    
    @IBOutlet weak var containView: UIView!
    @IBOutlet weak var filterTagView: TagListView!
    @IBOutlet weak var modelLabel: UILabel!
    @IBOutlet weak var sliderView: UISlider!
    @IBOutlet weak var fromYearLabel: UILabel!
    @IBOutlet weak var toYearLabel: UILabel!
    @IBOutlet weak var tripValueLabel: UILabel!
    @IBOutlet weak var doneButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureView()
        configureTagView()
        addModels()
    }

        func configureView() {
            containView.roundCorners([.bottomLeft, .bottomRight], radius: 12)
            doneButton.addCornerRadius(raduis: doneButton.frame.height / 2 - 8, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        }
        
        func addModels() {
            self.filterTagView.addTag("E")
            self.filterTagView.addTag("G")
            self.filterTagView.addTag("Y")
            self.filterTagView.addTag("Designer")
            self.filterTagView.addTag("Company")
            self.filterTagView.addTag("Designer")
            self.filterTagView.addTag("Company")
            self.filterTagView.addTag("E")
            self.filterTagView.addTag("G")
            self.filterTagView.addTag("Y")
            self.filterTagView.addTag("Messi")
        }
        
        //MARK:- Configuration
        func configureTagView() {
            filterTagView.delegate = self
    //        filterTagView.alignment = .center
        }

    // MARK:- Actions
    @IBAction func doneButtonIsPressed(_ sender: Any) {
        print("Done is pressed")
    }
    
    @IBAction func removeButtonIsPressed(_ sender: Any) {
        print("Remove is pressed")
    }
    
}
