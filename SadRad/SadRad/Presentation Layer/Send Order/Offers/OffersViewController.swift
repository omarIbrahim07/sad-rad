//
//  OffersViewController.swift
//  SadRad
//
//  Created by Omar Ibrahim on 9/8/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class OffersViewController: BaseViewController {
    
    lazy var viewModel: OffersViewModel = {
        return OffersViewModel()
    }()
    
    var error: APIError?
    
    @IBOutlet weak var filterView: UIView!
    @IBOutlet weak var researchButton: UIButton!
    @IBOutlet weak var sortButton: UIButton!
    @IBOutlet weak var filterButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var sortImageView: UIImageView!
    @IBOutlet weak var filterImageView: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tagViewTableView: UITableView!
    @IBOutlet weak var researchLabel: UILabel!
    @IBOutlet weak var sortLabel: UILabel!
    @IBOutlet weak var filterLabel: UILabel!
    @IBOutlet weak var noOffersLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setLocalization()
        setNavigationTitle()
        configureView()
        configureTableView()
        //        configureCollectionView()
        initVM()
        closeKeypad()
    }
    
    func setNavigationTitle() {
        navigationItem.title = "offers vc title".localized
    }
    
    func setLocalization() {
        filterLabel.text = "filter title".localized
        sortLabel.text = "sort title".localized
        researchLabel.text = "research title".localized
        noOffersLabel.text = "no offers label".localized
    }

    func initVM() {
        
        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }
            
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                    
                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                case .error:
                    self.error = self.viewModel.getError()
                    if let error = self.error {
                        self.stopLoadingWithError(error: error)
                    }
                case .empty:
                    print("")
                }
            }
        }
        
        viewModel.reloadTableViewClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
        
        viewModel.technicianStateClosure = { [weak self] () in
            guard let self = self else {
                return
            }
            
            switch self.viewModel.techsExistance {
                
            case .noTechs:
                self.noOffersLabel.isHidden = false
                self.tableView.isHidden = true
            case .techs:
                self.noOffersLabel.isHidden = true
                self.tableView.isHidden = false
            }
        }
                
        viewModel.initFetchOffers(sort: 1)
    }
    
    func configureView() {
        self.noOffersLabel.isHidden = true
        filterView.addCornerRadius(raduis: 12, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "OfferTableViewCell", bundle: nil), forCellReuseIdentifier: "OfferTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 20))
        tableView.tableFooterView = UIView()
        
        tagViewTableView.register(UINib(nibName: "TagViewTableViewCell", bundle: nil), forCellReuseIdentifier: "TagViewTableViewCell")
        tagViewTableView.register(UINib(nibName: "SortTableViewCell", bundle: nil), forCellReuseIdentifier: "SortTableViewCell")
        tagViewTableView.delegate = self
        tagViewTableView.dataSource = self
        tagViewTableView.tableFooterView = UIView()
    }
    
    func closeKeypad() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        tapGesture.cancelsTouchesInView = false
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc func viewTapped() {
        sortImageView.image = UIImage(named: "Icon awesome-sort")
        tagViewTableView.isHidden = true
    }
    
    // MARK:- Navigation
    func goToFilter() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "FilterViewController") as! FilterViewController
        //        viewController.selectedService = service
        viewController.viewModel.choosedCarConditioning = self.viewModel.airConditioning
        viewController.viewModel.choosedDeliveryPackage = self.viewModel.deliveryPackage
        
        if self.viewModel.airConditioning == nil {
            viewController.viewModel.choosedCarConditioning = false
        }
        if self.viewModel.deliveryPackage == nil {
            viewController.viewModel.choosedDeliveryPackage = false
        }
        if self.viewModel.modelYear != nil {
            viewController.viewModel.modelYear = self.viewModel.modelYear
        }
        viewController.delegate = self
        
        navigationController?.present(viewController, animated: true, completion: nil)
    }
    
    func goToTripDetails(tripID: Int) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "JourneyViewController") as! JourneyViewController
        viewController.viewModel.tripID = tripID
        
//        navigationController?.present(viewController, animated: true, completion: nil)
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    
    // MARK:- Actions
    @IBAction func researchButtonIsPressed(_ sender: Any) {
        print("Research")
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func sortButtonIsPressed(_ sender: Any) {
        sortImageView.image = UIImage(named: "Icon awesome-sort2")
        print("Sort")
        filterImageView.image = UIImage(named: "Icon feather-filter2")
        filterView.addCornerRadius(raduis: 12, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        self.filterView.roundCorners([.topLeft, .topRight], radius: 12)
        tagViewTableView.isHidden = false
        tagViewTableView.reloadData()
//        self.tagViewTableView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.2)
        self.view.backgroundColor = #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1)
    }
    
    @IBAction func filterButtonIsPressed(_ sender: Any) {
        print("Filter")
//        sortImageView.image = UIImage(named: "Icon awesome-sort")
//        filterImageView.image = UIImage(named: "Icon feather-filter-1")
//        //        collectionView.isHidden = false
//        self.filterView.roundCorners([.topLeft, .topRight], radius: 12)
//        tagViewTableView.isHidden = false
//        self.view.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.2)
//        self.tagViewTableView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.2)
        goToFilter()
    }
    
}
