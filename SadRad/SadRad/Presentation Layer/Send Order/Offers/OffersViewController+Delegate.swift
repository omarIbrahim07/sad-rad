//
//  OffersViewController+Delegate.swift
//  SadRad
//
//  Created by Omar Ibrahim on 9/8/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

extension OffersViewController: SendDataDelegate {
    func sendData(airConditioning: Bool, deliveryPackage: Bool, model: Int) {
        print(airConditioning)
        print(deliveryPackage)
        print(model)
        self.viewModel.airConditioning = airConditioning
        self.viewModel.deliveryPackage = deliveryPackage
        self.viewModel.modelYear = model
        self.viewModel.filter = true
        self.viewModel.initFetchOffers(sort: self.viewModel.sortChoosed)
    }
    
    func sendData(airConditioning: Bool, deliveryPackage: Bool) {
        print(airConditioning)
        print(deliveryPackage)
        self.viewModel.airConditioning = airConditioning
        self.viewModel.deliveryPackage = deliveryPackage
    }    
}

extension OffersViewController: SortButtonsDelegate {
    func sortButtonIsPressed(sort: Int) {
        self.viewModel.sortChoosed = sort
    }
}

extension OffersViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.tableView {
            return self.viewModel.numberOfOffersCells
        } else if tableView == tagViewTableView {
            return 1
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == self.tableView {
            if let cell: OfferTableViewCell = tableView.dequeueReusableCell(withIdentifier: "OfferTableViewCell") as? OfferTableViewCell {
                
                let cellVM = viewModel.getTripOfferCellViewModel(at: indexPath)
                cell.tripOfferCellViewModel = cellVM

                return cell
            }
        } else if tableView == self.tagViewTableView {
            
            if let cell: SortTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SortTableViewCell") as? SortTableViewCell {
                
                cell.checkSortChoosed(sort: self.viewModel.getSortChoosed())
                
                cell.sortDelegate = self
                return cell
            }
            
//            if let cell: TagViewTableViewCell = tableView.dequeueReusableCell(withIdentifier: "TagViewTableViewCell") as? TagViewTableViewCell {
//
//                return cell
//            }
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.viewModel.tripOfferPressed(at: indexPath)
        if let tripChoosed: MyJourney = self.viewModel.getTripOffer() {
            if let tripID: Int = tripChoosed.id {
                self.goToTripDetails(tripID: tripID)
            }
        }
    }
    
}
