//
//  OffersViewModel.swift
//  SadRad
//
//  Created by Omar Ibrahim on 10/25/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation

class OffersViewModel {
    
    enum techsState {
        case noTechs
        case techs
    }
    
    private var offers: [MyJourney] = [MyJourney]()
    var selectedOffer: MyJourney?
    
    var error: APIError?
    
    var sortChoosed: Int? = 1 {
        didSet {
            self.initFetchOffers(sort: self.sortChoosed)
        }
    }
    
    var techsExistance: techsState = .noTechs {
        didSet {
            self.technicianStateClosure?()
        }
    }
    
    var typeCarID: Int?
    var packupGovernorateID: Int?
    var packupCityID: Int?
    var dropoffGovernorateID: Int?
    var dropoffCityID: Int?
    var day: String?
    var time: String?
    
    var deliveryPackage: Bool?
    var airConditioning: Bool?
    var modelYear: Int?
    var filter: Bool? = false
    
    var offerCellViewModels: [OfferCellViewModel] = [OfferCellViewModel]() {
        didSet {
            self.reloadTableViewClosure?()
        }
    }
    
    var reloadTableViewClosure: (()->())?
    var updateLoadingStatus: (()->())?
    var deleteMyCarClosure: (()->())?
    var technicianStateClosure: (()->())?
    
    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    var deleted: Bool = false {
        didSet {
            self.deleteMyCarClosure?()
        }
    }
    
    var numberOfOffersCells: Int {
        return offerCellViewModels.count
    }
    
//    func initFetchOffers(typeCarID: Int, packupGovID: Int, packupCityID: Int, dropoffGovID: Int, dropOffCityID: Int, day: String, time: String) {
    func initFetchOffers(sort: Int?) {
        state = .loading
        
        var par: [String : AnyObject]?
        
        if filter == false {
            let params: [String : AnyObject] = [
                "typecar" : typeCarID as AnyObject,
                "packupgov" : packupGovernorateID as AnyObject,
                "packupcity" : packupCityID as AnyObject,
                "dropoffgov" : dropoffGovernorateID as AnyObject,
                "dropoffcity" : dropoffCityID as AnyObject,
                "day" : day as AnyObject,
                "time" : time as AnyObject,
                "sort" : sort as AnyObject,
            ]
            par = params
        } else if filter == true {
            let params: [String : AnyObject] = [
                "typecar" : typeCarID as AnyObject,
                "packupgov" : packupGovernorateID as AnyObject,
                "packupcity" : packupCityID as AnyObject,
                "dropoffgov" : dropoffGovernorateID as AnyObject,
                "dropoffcity" : dropoffCityID as AnyObject,
                "day" : day as AnyObject,
                "time" : time as AnyObject,
                "sort" : sort as AnyObject,
                "Aircondtion" : airConditioning as AnyObject,
                "deliveryofaparcel" : deliveryPackage as AnyObject,
                "model_id" : modelYear as AnyObject,
            ]
            par = params
        }
        
        SearchAPIManager().getTripsOffers(basicDictionary: par, onSuccess: { (tripsOffers) in
            
            self.processTripsOffers(tripsOffers: tripsOffers)
            self.state = .populated
            
        }) { (error) in
            self.error = error
            self.state = .error
        }

    }
    
    func deleteMyCar(carID: Int) {
        state = .loading
        
        let params: [String : AnyObject] = [
            "car_id" : carID as AnyObject
        ]
        
        CarsAPIManager().deleteMyCar(basicDictionary: params, onSuccess: { (deleted) in
            
            print(deleted)
            self.deleted = deleted
            self.state = .populated
            
        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    func processTripsOffers( tripsOffers: [MyJourney] ) {
        
        if tripsOffers.count == 0 {
            self.techsExistance = .noTechs
        } else {
            self.techsExistance = .techs
        }
        
        self.offers = tripsOffers // Cache
        var vms = [OfferCellViewModel]()
        for tripOffer in tripsOffers {
            vms.append( createTripOfferCellViewModel(tripOffer: tripOffer))
        }
        self.offerCellViewModels = vms
    }
    
    func createTripOfferCellViewModel( tripOffer: MyJourney ) -> OfferCellViewModel {
        
        if "Lang".localized == "en" {
            return OfferCellViewModel(tripID: tripOffer.id, fromGovernorate: tripOffer.packupGovernorate?.nameEn, toGovernorate: tripOffer.dropOffGovernorate?.nameEn, fromCity: tripOffer.packupCity?.nameEn, toCity: tripOffer.dropOffCity?.nameEn, brand: tripOffer.myCar?.carBrand?.nameEn, model: tripOffer.myCar?.carModel?.nameEn, date: tripOffer.day, time: tripOffer.time, rating: tripOffer.driverInfo?.rating, price: tripOffer.price, numberOfPersons: tripOffer.myCar?.numberOfPersons, images: tripOffer.myCar?.images, numberOfRates: tripOffer.ratingCount)
        } else if "Lang".localized == "ar" {
            return OfferCellViewModel(tripID: tripOffer.id, fromGovernorate: tripOffer.packupGovernorate?.nameAr, toGovernorate: tripOffer.dropOffGovernorate?.nameAr, fromCity: tripOffer.packupCity?.nameAr, toCity: tripOffer.dropOffCity?.nameAr, brand: tripOffer.myCar?.carBrand?.nameAr, model: tripOffer.myCar?.carModel?.nameAr, date: tripOffer.day, time: tripOffer.time, rating: tripOffer.driverInfo?.rating, price: tripOffer.price, numberOfPersons: tripOffer.myCar?.numberOfPersons, images: tripOffer.myCar?.images, numberOfRates: tripOffer.ratingCount)
        } else {
            return OfferCellViewModel(tripID: tripOffer.id, fromGovernorate: tripOffer.packupGovernorate?.nameEn, toGovernorate: tripOffer.dropOffGovernorate?.nameEn, fromCity: tripOffer.packupCity?.nameEn, toCity: tripOffer.dropOffCity?.nameEn, brand: tripOffer.myCar?.carBrand?.nameEn, model: tripOffer.myCar?.carModel?.nameEn, date: tripOffer.day, time: tripOffer.time, rating: tripOffer.driverInfo?.rating, price: tripOffer.price, numberOfPersons: tripOffer.myCar?.numberOfPersons, images: tripOffer.myCar?.images, numberOfRates: tripOffer.ratingCount)
        }
    }
    
    func getError() -> APIError {
        return self.error!
    }
    
    func getTripOfferCellViewModel( at indexPath: IndexPath ) -> OfferCellViewModel {
        return self.offerCellViewModels[indexPath.row]
    }
    
    func getDeletedBool() -> Bool {
        return self.deleted
    }
    
    func tripOfferPressed( at indexPath: IndexPath ){
        let tripOffer = self.offers[indexPath.row]
        
        self.selectedOffer = tripOffer
    }
    
    func getTripOffer() -> MyJourney? {
        return self.selectedOffer
    }
    
    func getSortChoosed() -> Int? {
        return sortChoosed
    }
    
}
