//
//  OfferCellViewModel.swift
//  SadRad
//
//  Created by Omar Ibrahim on 10/25/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation

struct OfferCellViewModel {
    let tripID: Int?
    //    let image: String?
    let fromGovernorate: String?
    let toGovernorate: String?
    let fromCity: String?
    let toCity: String?
    let brand: String?
    let model: String?
    let date: String?
    let time: String?
//    let phoneNumber: Int?
//    let driverName: String?
    let rating: Float?
    let price: Int?
    let numberOfPersons: Int?
    let images: [String]?
    let numberOfRates: Int?
}
