//
//  Homecell.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 2/16/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit
import FSPagerView

class Homecell: FSPagerViewCell {

    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    var viewModels : Offer? {
        didSet {
            bindData()
        }
    }
    
    func bindData() {
        
        if let offer = viewModels {
            descriptionLabel.text = offer.title
            if let price: Int = offer.price {
                priceLabel.text = String(price) + " SAR"
            }
//            self.image.loadImageFromUrl(imageUrl: ImageURLOffers + Offer)
        }
        
    }

}
