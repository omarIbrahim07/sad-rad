//
//  HomeViewController+Delegates.swift
//  SadRad
//
//  Created by Omar Ibrahim on 10/21/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit
import DateTimePicker

extension HomeViewController: DateTimePickerDelegate {
    
    func dateTimePicker(_ picker: DateTimePicker, didSelectDate: Date) {
        
        print(picker.selectedDateString)
        
        var f: String?
        f = picker.selectedDateString
        let subString = f?.prefix(5)
                
        if "Lang".localized == "ar" {
//            self.timeLabel.text = String(subString!) + ":00"
            self.timeLabel.text = String(subString!).withWesternNumbers
        } else if "Lang".localized == "en" {
//            self.timeLabel.text = String(subString!) + ":00"
            self.timeLabel.text = String(subString!).withWesternNumbers
        }
        self.timeLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
    
}
