//
//  HomeViewController.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 10/10/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit
import SideMenu
import CoreLocation
import FSPagerView
import DatePickerDialog
import TimePicker
import DateTimePicker
import GoogleMobileAds

class HomeViewController: BaseViewController {
    
    var todayString: String?

    lazy var viewModel: HomeViewModel = {
        return HomeViewModel()
    }()
    
    var error: APIError?
    var offers: [Offer] = [Offer]()
    
    let locationManager = CLLocationManager()
    
    private let banner: GADBannerView = {
        let banner = GADBannerView()
        banner.adUnitID = GoogleBannerUnitID
        banner.load(GADRequest())
        return banner
    }()
        
//    @IBOutlet weak var pickUpTitleLabel: UILabel!
//    @IBOutlet weak var fromCityTitleLabel: UILabel!
//    @IBOutlet weak var dropOffTitleLabel: UILabel!
//    @IBOutlet weak var toCityTitleLabel: UILabel!
//    @IBOutlet weak var dayTitleLabel: UILabel!
//    @IBOutlet weak var timeTitleLabel: UILabel!
//    @IBOutlet weak var carTypeTitleLabel: UILabel!
    @IBOutlet weak var advertisementView: UIView!
    @IBOutlet weak var pickupView: UIView!
    @IBOutlet weak var fromCityView: UIView!
    @IBOutlet weak var dropOffView: UIView!
    @IBOutlet weak var toCityView: UIView!
    @IBOutlet weak var dayView: UIView!
    @IBOutlet weak var timeView: UIView!
    @IBOutlet weak var carTypeView: UIView!
    @IBOutlet weak var pickUpLabel: UILabel!
    @IBOutlet weak var fromCityLabel: UILabel!
    @IBOutlet weak var dropOffLabel: UILabel!
    @IBOutlet weak var toCityLabel: UILabel!
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var carTypeLabel: UILabel!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var searchForTripLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        banner.rootViewController = self
        advertisementView.addSubview(banner)
        setNavigationTitle()
        configureView()
        makeCustomLogoutBarButtonItem(imageName: "menuu", buttonType: "left")
        makeNotificationIcon(imageName: "bell", buttonType: "right")
        
        initVM()
//        self.view.addCornerRadius(raduis: 40, borderColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0), borderWidth: 0)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        banner.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 80).integral
    }
    
    func setNavigationTitle() {
        navigationItem.title = "home vc title".localized
    }

    func configureView() {
        pickupView.addCornerRadius(raduis: 8.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        fromCityView.addCornerRadius(raduis: 8.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        dropOffView.addCornerRadius(raduis: 8.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        toCityView.addCornerRadius(raduis: 8.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        dayView.addCornerRadius(raduis: 8.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        timeView.addCornerRadius(raduis: 8.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        carTypeView.addCornerRadius(raduis: 8.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        searchButton.addCornerRadius(raduis: searchButton.frame.height / 2, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        pickUpLabel.text = "pick up governorate title".localized
        fromCityLabel.text = "pick up city title".localized
        dropOffLabel.text = "drop off governorate title".localized
        toCityLabel.text = "drop off city title".localized
        timeLabel.text = "time title".localized
        dayLabel.text = "day title".localized
        carTypeLabel.text = "car type title".localized
//        pickUpTitleLabel.text = "pick up governorate title".localized
//        fromCityTitleLabel.text = "pick up city title".localized
//        dropOffTitleLabel.text = "drop off governorate title".localized
//        toCityTitleLabel.text = "drop off city title".localized
//        dayTitleLabel.text = "day title".localized
//        timeTitleLabel.text = "time title".localized
//        carTypeTitleLabel.text = "car type title".localized
        searchButton.setTitle("search button title".localized, for: .normal)
        searchForTripLabel.text = "search for trip label".localized
    }

    // MARK:- Init View Model
    func initVM() {
        
        //        viewModel.updateTimeValueClosure = { [weak self] () in
        //            DispatchQueue.main.async { [weak self] in
        //
        //            }
        //        }
        
        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }
            
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                    
                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                case .error:
                    self.error = self.viewModel.getError()
                    if let error = self.error {
                        self.stopLoadingWithError(error: error)
//                        self.showError(error: error)
                    }
                case .empty:
                    print("")
                }
            }
        }
        
        viewModel.chooseCarTypeClosure = { [weak self] () in
            DispatchQueue.main.async { [weak self] in
                if let choosedCarType: CarType = self?.viewModel.choosedCarType {
                    self?.bindCarType(carType: choosedCarType)
                }
            }
        }
        
        viewModel.chooseFromGovernrateClosure = { [weak self] () in
            DispatchQueue.main.async { [weak self] in
                if let governorate: Governorate = self?.viewModel.choosedFromGovernrate {
                    self?.bindGovernorateModel(toBool: false, governorate: governorate)
                    self?.viewModel.choosedFromCity = nil
                }
            }
        }
        
        viewModel.chooseToGovernrateClosure = { [weak self] () in
            DispatchQueue.main.async { [weak self] in
                if let governorate: Governorate = self?.viewModel.choosedToGovernrate {
                    self?.bindGovernorateModel(toBool: true, governorate: governorate)
                    self?.viewModel.choosedToCity = nil
                }
            }
        }
        
        viewModel.chooseToCityClosure = { [weak self] () in
            DispatchQueue.main.async { [weak self] in
                if let city: City = self?.viewModel.choosedToCity {
                    self?.bindCityModel(toBool: true, city: city)
                } else {
                    self?.toCityLabel.text = ""
                }
            }
        }
        
        viewModel.chooseFromCityClosure = { [weak self] () in
            DispatchQueue.main.async { [weak self] in
                if let city: City = self?.viewModel.choosedFromCity {
                    self?.bindCityModel(toBool: false, city: city)
                } else {
                    self?.fromCityLabel.text = ""
                }
            }
        }
        
        viewModel.fetchCarTypes()
        viewModel.fetchGovernrates()
    }
    
    func datePickerTapped() {
        let datePickeDialog = DatePickerDialog()
        datePickeDialog.show("DatePicker", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", minimumDate: Date().addingTimeInterval(60 * 60 * 24 * 0), datePickerMode: .date) {
            (date) -> Void in
            if let dt = date {
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd"
                formatter.locale = Locale.init(identifier: "en")
                self.dayLabel.text = formatter.string(from: dt)
                self.dayLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                self.checkDate()
            }
        }
    }
    
    func checkDate() {
        let date = Date().addingTimeInterval(60 * 60 * 24 * 0)
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.locale = Locale.init(identifier: "en")
        todayString = formatter.string(from: date)
        print(todayString)
        if dayLabel.text == todayString {
            print("Enahrda")
        }
    }
    
    // MARK:- Set the time
    func setTheTime() {
        var min: Date?
        if dayLabel.text == todayString {
            min = Date().addingTimeInterval(0)
        } else {
            min = Date().addingTimeInterval(-60 * 60 * 24 * 4)
        }
        //        let min = Date().addingTimeInterval(0)
        let max = Date().addingTimeInterval(60 * 60 * 24 * 4)
        let picker = DateTimePicker.create(minimumDate: min, maximumDate: max)
        picker.frame = CGRect(x: 0, y: 100, width: picker.frame.size.width, height: picker.frame.size.height)
        picker.highlightColor = UIColor(red: 255.0/255.0, green: 138.0/255.0, blue: 138.0/255.0, alpha: 1)
        picker.isTimePickerOnly = true
        picker.is12HourFormat = false

        self.view.addSubview(picker)
                
        //        picker.doneButtonTitle = "DONE"
        picker.doneBackgroundColor = UIColor(red: 255.0/255.0, green: 138.0/255.0, blue: 138.0/255.0, alpha: 1)
        
        picker.cancelButton.isHidden = true
        picker.todayButton.isHidden = true
        
        //        picker.donePicking(sender: picker.doneButton)
        
        picker.delegate = self
        
        picker.show()
    }
    
    func setupCarsTypes(carTypes: [CarType]) {
        
        let sheet = UIAlertController(title: "car type alert title".localized, message: "car type alert message".localized, preferredStyle: .actionSheet)
        for carType in carTypes {
            if "Lang".localized == "ar" {
                sheet.addAction(UIAlertAction(title: carType.nameAr, style: .default, handler: {_ in
                    self.viewModel.userChoossedCarType(carType: carType)
                }))
            } else if "Lang".localized == "en" {
                sheet.addAction(UIAlertAction(title: carType.nameEn, style: .default, handler: {_ in
                    self.viewModel.userChoossedCarType(carType: carType)
                }))
            }
        }
        sheet.addAction(UIAlertAction(title: "cancel car type".localized, style: .cancel, handler: nil))
        
        //        sheet.popoverPresentationController?.sourceView = sortChoiceLabel
        
        self.present(sheet, animated: true, completion: nil)
    }
    
    func setupGovernrate(toBool: Bool, governrates: [Governorate]) {
        
        let sheet = UIAlertController(title: "governorate alert title".localized, message: "governorate alert message".localized, preferredStyle: .actionSheet)
        
        if toBool == true {
            for governrate in governrates {
                if "Lang".localized == "ar" {
                    sheet.addAction(UIAlertAction(title: governrate.nameAr, style: .default, handler: {_ in
                        self.viewModel.userChoossedToGovernrate(governorate: governrate)
                    }))
                } else if "Lang".localized == "en" {
                    sheet.addAction(UIAlertAction(title: governrate.nameEn, style: .default, handler: {_ in
                        self.viewModel.userChoossedToGovernrate(governorate: governrate)
                    }))
                }
            }
        } else if toBool == false {
            for governrate in governrates {
                if "Lang".localized == "ar" {
                    sheet.addAction(UIAlertAction(title: governrate.nameAr, style: .default, handler: {_ in
                        self.viewModel.userChoossedFromGovernrate(governorate: governrate)
                    }))
                } else if "Lang".localized == "en" {
                    sheet.addAction(UIAlertAction(title: governrate.nameEn, style: .default, handler: {_ in
                        self.viewModel.userChoossedFromGovernrate(governorate: governrate)
                    }))
                }
            }
        }
        sheet.addAction(UIAlertAction(title: "cancel governorate".localized, style: .cancel, handler: nil))
        
        //        sheet.popoverPresentationController?.sourceView = sortChoiceLabel
        
        self.present(sheet, animated: true, completion: nil)
    }
    
    func setupCities(toBool: Bool, cities: [City]) {
        
        let sheet = UIAlertController(title: "city alert title".localized, message: "city alert message".localized, preferredStyle: .actionSheet)
        if toBool == true {
            for city in cities {
                if "Lang".localized == "ar" {
                    sheet.addAction(UIAlertAction(title: city.nameAr, style: .default, handler: {_ in
                        self.viewModel.userChoossedToCity(city: city)
                    }))
                } else if "Lang".localized == "en" {
                    sheet.addAction(UIAlertAction(title: city.nameEn, style: .default, handler: {_ in
                        self.viewModel.userChoossedToCity(city: city)
                    }))
                }
            }
        } else if toBool == false {
            for city in cities {
                if "Lang".localized == "ar" {
                    sheet.addAction(UIAlertAction(title: city.nameAr, style: .default, handler: {_ in
                        self.viewModel.userChoossedFromCity(city: city)
                    }))
                } else if "Lang".localized == "en" {
                    sheet.addAction(UIAlertAction(title: city.nameEn, style: .default, handler: {_ in
                        self.viewModel.userChoossedFromCity(city: city)
                    }))
                }
            }
        }
        sheet.addAction(UIAlertAction(title: "cancel city".localized, style: .cancel, handler: nil))
        
        //        sheet.popoverPresentationController?.sourceView = sortChoiceLabel
        
        self.present(sheet, animated: true, completion: nil)
    }
    
    
    
    func bindCarType(carType: CarType) {
        if "Lang".localized == "ar" {
            self.carTypeLabel.text = carType.nameAr
        } else if "Lang".localized == "en" {
            self.carTypeLabel.text = carType.nameEn
        }
    }
    
    func bindGovernorateModel(toBool: Bool, governorate: Governorate) {
        if toBool == true {
            if "Lang".localized == "ar" {
                self.dropOffLabel.text = governorate.nameAr
            } else if "Lang".localized == "en" {
                self.dropOffLabel.text = governorate.nameEn
            }
        } else if toBool == false {
            if "Lang".localized == "ar" {
                self.pickUpLabel.text = governorate.nameAr
            } else if "Lang".localized == "en" {
                self.pickUpLabel.text = governorate.nameEn
            }
        }
    }
    
    func bindCityModel(toBool: Bool, city: City) {
        if toBool == true {
            if "Lang".localized == "ar" {
                self.toCityLabel.text = city.nameAr
            } else if "Lang".localized == "en" {
                self.toCityLabel.text = city.nameEn
            }
        } else if toBool == false {
            if "Lang".localized == "ar" {
                self.fromCityLabel.text = city.nameAr
            } else if "Lang".localized == "en" {
                self.fromCityLabel.text = city.nameEn
            }
        }
    }
    
    func makeCustomLogoutBarButtonItem(imageName: String, buttonType: String) {
        
        let barButton = UIButton()
        
        let containerButton = UIButton(frame: CGRect(x: -5, y: 0, width: 35, height: 35))
        
        containerButton.setImage(UIImage(named: imageName), for: .normal)
        
        containerButton.setImage(UIImage(named: imageName), for: .highlighted)
        
        containerButton.imageView?.contentMode = .scaleAspectFit
        
        //        action: #selector(presentLeftMenuViewController(_:)))
        
        if "Lang".localized == "en" {
            containerButton.addTarget(self, action: #selector(presentLeftMenuViewController(_:)), for: UIControl.Event.touchUpInside)
        } else if "Lang".localized == "ar" {
            containerButton.addTarget(self, action: #selector(presentRightMenuViewController(_:)), for: UIControl.Event.touchUpInside)
        }
        
        barButton.addSubview(containerButton)
        
        if buttonType == "right" {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: barButton)
        } else if buttonType == "left" {
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: barButton)
        }
    }
    
    func makeNotificationIcon(imageName: String, buttonType: String) {
        
        let barButton = UIButton()
        
        let containerButton = UIButton(frame: CGRect(x: -5, y: 0, width: 35, height: 35))
        
        containerButton.setImage(UIImage(named: imageName), for: .normal)
        
        containerButton.setImage(UIImage(named: imageName), for: .highlighted)
        
        containerButton.imageView?.contentMode = .scaleAspectFit
        
        //        containerButton.addTarget(self, action: #selector(self.goToNotifications), for: UIControl.Event.touchUpInside)
        
        barButton.addSubview(containerButton)
        
        if buttonType == "right" {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: barButton)
        } else if buttonType == "left" {
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: barButton)
        }
    }
    
    func showCantLoginPopUp() {
        let alertController = UIAlertController(title: "go to login title".localized, message: "go to login alert message".localized, preferredStyle: .alert)
        //        let cancelAction = UIAlertAction(title: "إلغاء", style: .cancel, handler: nil)
        //        alertController.addAction(cancelAction)
        let openAction = UIAlertAction(title: "go to login button".localized, style: .default) { (action) in
            self.goToLogIn()
        }
        let cancelAction = UIAlertAction(title: "cancel go to login".localized, style: .cancel)
        alertController.addAction(openAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    // MARK:- Actions    
    @IBAction func fromGovernorateButtonIsPressed(_ sender: Any) {
        print("FESS")
        if let fromGovernorates: [Governorate] = viewModel.governrates {
            self.setupGovernrate(toBool: false, governrates: fromGovernorates)
        }
    }
    
    @IBAction func fromCityButtonIsPressed(_ sender: Any) {
        if let fromCities: [City] = viewModel.fromCities {
            self.setupCities(toBool: false, cities: fromCities)
        }
    }
    
    @IBAction func toGovernorateButtonIsPressed(_ sender: Any) {
        if let toGovernorates: [Governorate] = viewModel.governrates {
            self.setupGovernrate(toBool: true, governrates: toGovernorates)
        }
    }
    
    @IBAction func toCityButtonIsPressed(_ sender: Any) {
        if let toCities: [City] = viewModel.toCities {
            self.setupCities(toBool: true, cities: toCities)
        }
    }
    
    @IBAction func dayButtonIsPressed(_ sender: Any) {
        self.timeLabel.text = ""
        datePickerTapped()
    }
    
    @IBAction func timeButtonIsPressed(_ sender: Any) {
        setTheTime()
    }
    
    @IBAction func carsTypesButtonIsPressed(_ sender: Any) {
        if let carsTypes: [CarType] = self.viewModel.getCarsTypes() {
            if carsTypes.count > 0 {
                self.setupCarsTypes(carTypes: carsTypes)
            }
        }
    }
    
    @IBAction func searchButtonIsPressed(_ sender: Any) {
        print("Search")
        var date: String!
        var time: String!
        
        guard let pickupGovernorateID: Int = self.viewModel.choosedFromGovernrate?.id, pickupGovernorateID > 0 else {
            let apiError = APIError()
            apiError.message = "error pick up governorate".localized
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }

//        guard let pickupCityID: Int = self.viewModel.choosedFromCity?.id, pickupCityID > 0 else {
//            let apiError = APIError()
//            apiError.message = "error pick up city".localized
//            self.viewModel.error = apiError
//            self.viewModel.state = .error
//            return
//        }
        
        guard let dropOffGovernorateID: Int = self.viewModel.choosedToGovernrate?.id, dropOffGovernorateID > 0 else {
            let apiError = APIError()
            apiError.message = "error drop off governorate".localized
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }

        self.viewModel.dropOffCityID = self.viewModel.choosedToCity?.id
        
        self.viewModel.pickupCityID = self.viewModel.choosedFromCity?.id
        
//        guard let dropOffCityID: Int = self.viewModel.choosedToCity?.id, dropOffCityID > 0 else {
//            let apiError = APIError()
//            apiError.message = "error drop off city".localized
//            self.viewModel.error = apiError
//            self.viewModel.state = .error
//            return
//        }
        
//        guard let date: String = self.dayLabel.text, date.count >= 10 else {
//            let apiError = APIError()
//            apiError.message = "error enter day".localized
//            self.viewModel.error = apiError
//            self.viewModel.state = .error
//            return
//        }
        
        if self.dayLabel.text?.count ?? 0 != 10 {
            let datee = Date().addingTimeInterval(60 * 60 * 24 * 0)
            print(datee)
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            date = formatter.string(from: datee)
            date = date.withWesternNumbers
        } else {
            date = self.dayLabel.text
        }
        
        if self.timeLabel.text?.contains("i") == true || self.timeLabel.text?.contains("و") == true {
            let datee = Date().addingTimeInterval(60 * 60 * 24 * 0)
            print(datee)
            let formatter = DateFormatter()
            formatter.dateFormat = "HH:mm:00"
            time = formatter.string(from: datee)
            time = time.withWesternNumbers
        } else {
            time = self.timeLabel.text! + ":00"
        }
                                
//        guard let time: String = self.timeLabel.text, time.count >= 8 else {
//            let apiError = APIError()
//            apiError.message = "error enter time".localized
//            self.viewModel.error = apiError
//            self.viewModel.state = .error
//            return
//        }
        
        guard let typeCarID: Int = self.viewModel.choosedCarType?.id, typeCarID > 0 else {
            let apiError = APIError()
            apiError.message = "error enter car type".localized
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }

        goToOffers(typeCarID: typeCarID, packupGovID: pickupGovernorateID, packupCityID: self.viewModel.pickupCityID, dropoffGovID: dropOffGovernorateID, dropOffCityID: self.viewModel.dropOffCityID, day: date, time: time)
    }
    
    
    // MARK:- Navigation
    func goToLogIn() {
        if let signInNavigationController = storyboard?.instantiateViewController(withIdentifier: "LoginNavigationVC") as? UINavigationController, let loginViewController = signInNavigationController.viewControllers[0] as? LoginViewController {
            
            UIApplication.shared.keyWindow?.rootViewController = signInNavigationController
        }
    }
    
    func goToOffers(typeCarID: Int, packupGovID: Int, packupCityID: Int?, dropoffGovID: Int, dropOffCityID: Int?, day: String, time: String) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "OffersViewController") as! OffersViewController
        viewController.viewModel.typeCarID = typeCarID
        viewController.viewModel.packupGovernorateID = packupGovID
        viewController.viewModel.packupCityID = packupCityID
        viewController.viewModel.dropoffGovernorateID = dropoffGovID
        viewController.viewModel.dropoffCityID = dropOffCityID
        viewController.viewModel.day = day
        viewController.viewModel.time = time
        
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func goToDriverMenu() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "DriverMenuViewController") as! DriverMenuViewController
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func goToMyCars() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "MyCarsViewController") as! MyCarsViewController        
        navigationController?.pushViewController(viewController, animated: true)
    }
    func goToAddJourney() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "AddJourneyViewController") as! AddJourneyViewController
        navigationController?.pushViewController(viewController, animated: true)
    }
    func goToMyJourneis() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "MyJourniesViewController") as! MyJourniesViewController
        navigationController?.pushViewController(viewController, animated: true)
    }
    func goToMyBookings() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "MyReservationsViewController") as! MyReservationsViewController
        navigationController?.pushViewController(viewController, animated: true)
    }
    func goToEditProfile() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "EditProfileViewController") as! EditProfileViewController
        navigationController?.pushViewController(viewController, animated: true)
    }
}
