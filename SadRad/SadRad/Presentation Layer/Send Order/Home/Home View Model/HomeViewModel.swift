//
//  HomeViewModel.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 11/11/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation
import Toast_Swift

public enum State {
    case loading
    case error
    case populated
    case empty
}

class HomeViewModel {
    
    var selectedService: Service?
    
    private var services: [Service] = [Service]()
    var error: APIError?
    
    var governrates: [Governorate]?
    var fromCities: [City]?
    var toCities: [City]?
    var carsTypes: [CarType]?
    
    var dropOffCityID: Int?
    var pickupCityID: Int?
    
    var showAlertClosure: (()->())?
    var updateLoadingStatus: (()->())?
    var chooseFromGovernrateClosure: (()->())?
    var chooseToGovernrateClosure: (()->())?
    var chooseFromCityClosure: (()->())?
    var chooseToCityClosure: (()->())?
    var chooseCarTypeClosure: (()->())?
    
    var choosedFromGovernrate: Governorate? {
        didSet {
            self.chooseFromGovernrateClosure?()
        }
    }
    
    var choosedToGovernrate: Governorate? {
        didSet {
            self.chooseToGovernrateClosure?()
        }
    }
    
    var choosedFromCity: City? {
        didSet {
            self.chooseFromCityClosure?()
        }
    }
    
    var choosedToCity: City? {
        didSet {
            self.chooseToCityClosure?()
        }
    }
    
    var choosedCarType: CarType? {
        didSet {
            self.chooseCarTypeClosure?()
        }
    }
    
    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    func fetchGovernrates() {
        state = .loading
        
        let params: [String : AnyObject] = [:]
        
        AddJourneyAPIManager().getGovernrates(basicDictionary: params, onSuccess: { (governrates) in
            
            self.governrates = governrates // Cache
            self.state = .populated
            
        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    func fetchCities(toBool: Bool, governrateID: Int) {
        state = .loading
        
        let params: [String : AnyObject] = [
            "governoratment_id" : governrateID as AnyObject,
        ]
        
        AddJourneyAPIManager().getCities(basicDictionary: params, onSuccess: { (cities) in
            
            if toBool == true {
                self.toCities = cities // Cache
            } else if toBool == false {
                self.fromCities = cities // Cache
            }
            self.state = .populated
            
        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    func fetchCarTypes() {
        state = .loading
        
        let params: [String : AnyObject] = [:]
        
        CarsAPIManager().getCarsTypes(basicDictionary: params, onSuccess: { (carsTypes) in

        self.carsTypes = carsTypes // Cache
        self.state = .populated

        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
//    func search(packUpCityID: Int, packupGovernoratmentID: Int, dropOffCityID: Int, dropOffGovernoratmentID: Int, day: String, time: String, typeCarID: Int) {
//
//            let params: [String : AnyObject] = [
//                "packupcity_id" : packUpCityID as AnyObject,
//                "packupgovernoratment_id" : packupGovernoratmentID as AnyObject,
//                "dropoffcity_id" : dropOffCityID as AnyObject,
//                "dropoffgovernoratment_id" : dropOffGovernoratmentID as AnyObject,
//                "day" : day as AnyObject,
//                "time" : time as AnyObject,
//                //                "car_id" : typeCarID as AnyObject,
//            ]
//
//        self.state = .loading
//
//        CarsAPIManager().getTripsOffers(basicDictionary: params, onSuccess: { (saved) in
//            if saved == true {
//                print("Success")
//                self.state = .populated
//            } else {
//                self.state = .empty
//            }
//        }) { (error) in
//            self.error = error
//            self.state = .error
//        }
//    }

    
    func getGovernrates() -> [Governorate]? {
        return governrates
    }
    
    func getCarsTypes() -> [CarType]? {
        return carsTypes
    }
    
    func userChoossedFromGovernrate(governorate: Governorate) {
        self.choosedFromGovernrate = governorate
        if let governrateID: Int = governorate.id {
            self.fetchCities(toBool: false, governrateID: governrateID)
        }
    }
        
    func userChoossedToGovernrate(governorate: Governorate) {
        self.choosedToGovernrate = governorate
        if let governrateID: Int = governorate.id {
            self.fetchCities(toBool: true, governrateID: governrateID)
        }
    }
        
    func getToCities() -> [City]? {
        return toCities
    }
    
    func getFromCities() -> [City]? {
        return fromCities
    }
    
    func userChoossedFromCity(city: City) {
        self.choosedFromCity = city
    }
    
    func userChoossedToCity(city: City) {
        self.choosedToCity = city
    }
    
    func userChoossedCarType(carType: CarType) {
        self.choosedCarType = carType
    }

    func getError() -> APIError {
        return self.error!
    }
    
}
