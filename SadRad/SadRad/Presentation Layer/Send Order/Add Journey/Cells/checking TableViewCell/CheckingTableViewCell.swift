//
//  CheckingTableViewCell.swift
//  SadRad
//
//  Created by Omar Ibrahim on 9/8/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class CheckingTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var asteriskImageView: UIImageView!
    @IBOutlet weak var containView: UIView!
    @IBOutlet weak var checkButton: UIButton!
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var itemLabel: UILabel!
    @IBOutlet weak var checkImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureView()
    }
    
    func configureView() {
        containView.addCornerRadius(raduis: 8, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
