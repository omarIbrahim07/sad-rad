//
//  AddJourneyViewModel.swift
//  SadRad
//
//  Created by Omar Ibrahim on 10/12/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation

class AddJourneyViewModel {
    
    var myCars: [MyCar] = [MyCar]()
        
    var error: APIError? {
        didSet {
            state = .error
        }
    }
        
    var selectedCar: MyCar? {
        didSet {
            self.myCarIsChoosedClosure?()
        }
    }
    
    var updateLoadingStatus: (()->())?
    var chooseCarTypeClosure: (()->())?
    var chooseCarBrandClosure: (()->())?
    var chooseCarModelClosure: (()->())?
    var chooseCarConditioningClosure: (()->())?
    var choosePriceNegotiableClosure: (()->())?
    var chooseDeliveringPackageClosure: (()->())?
    var chooseFromGovernrateClosure: (()->())?
    var chooseToGovernrateClosure: (()->())?
    var chooseFromCityClosure: (()->())?
    var chooseToCityClosure: (()->())?
    var updateMyCarInfoClosure: (()->())?
    var myCarIsChoosedClosure: (()->())?
    var reloadTableViewClosure: (() -> ())?
    var updateTimeValueClosure: (() -> ())?
    var updateDateValueClosure: (() -> ())?
    var updateJourneyInfoClosure: (() -> ())?
    var journeyAddedClosure: (() -> ())?
    var journeyUpdatedClosure: (() -> ())?
    
    // callback for interfaces
    var carsTypes: [CarType]?
    var carsModels: [CarModel]?
    var carsBrands: [CarBrand]?
    var governrates: [Governorate]?
    var fromCities: [City]?
    var toCities: [City]?
    
    var isFirstTimeFromGovBool: Bool? = true
    var isFirstTimeToGovBool: Bool? = true
    
    var choosedCarType: CarType? {
        didSet {
            self.chooseCarTypeClosure?()
        }
    }
    
    var choosedCarBrand: CarBrand? {
        didSet {
            self.chooseCarBrandClosure?()
        }
    }
    
    var choosedCarModel: CarModel? {
        didSet {
            self.chooseCarModelClosure?()
        }
    }
    
    var myCarInfo: MyCar? {
        didSet {
            self.updateMyCarInfoClosure?()
        }
    }
    
    var myJourneyInfo: MyJourney? {
        didSet {
            self.updateJourneyInfoClosure?()
        }
    }
    
    var choosedCarConditioning: Bool? = false {
        didSet {
            self.chooseCarConditioningClosure?()
        }
    }
    
    var choosedPriceNegotiable: Bool? = false {
        didSet {
            self.choosePriceNegotiableClosure?()
        }
    }
    
    var choosedDeliveryPackage: Bool? = false {
        didSet {
            self.chooseDeliveringPackageClosure?()
        }
    }
    
    var choosedFromGovernrate: Governorate? {
        didSet {
            self.chooseFromGovernrateClosure?()
        }
    }
    
    var choosedToGovernrate: Governorate? {
        didSet {
            self.chooseToGovernrateClosure?()
        }
    }

    var choosedFromCity: City? {
        didSet {
            self.chooseFromCityClosure?()
        }
    }
    
    var choosedToCity: City? {
        didSet {
            self.chooseToCityClosure?()
        }
    }
        
    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    var journeyIsAdded: Bool = false {
        didSet {
            self.journeyAddedClosure?()
        }
    }

    var journeyIsUpdated: Bool = false {
        didSet {
            self.journeyUpdatedClosure?()
        }
    }

    func fetchCarTypes() {
        state = .loading
        
        let params: [String : AnyObject] = [:]
        
        CarsAPIManager().getCarsTypes(basicDictionary: params, onSuccess: { (carsTypes) in
            
            self.carsTypes = carsTypes // Cache
            self.state = .populated
            
        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    func fetchCarBrands() {
        state = .loading
        
        let params: [String : AnyObject] = [:]
        
        CarsAPIManager().getCarsBrands(basicDictionary: params, onSuccess: { (carsBrands) in
            
            self.carsBrands = carsBrands // Cache
            self.state = .populated
            
        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    func fetchCarModels() {
        state = .loading
        
        let params: [String : AnyObject] = [:]
        
        CarsAPIManager().getCarsModels(basicDictionary: params, onSuccess: { (carsModels) in
            
            self.carsModels = carsModels // Cache
            self.state = .populated
            
        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    func fetchGovernrates() {
        state = .loading
        
        let params: [String : AnyObject] = [:]
        
        AddJourneyAPIManager().getGovernrates(basicDictionary: params, onSuccess: { (governrates) in
            
            self.governrates = governrates // Cache
            self.state = .populated
            
        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    func fetchCities(toBool: Bool, governrateID: Int) {
        state = .loading
        
        let params: [String : AnyObject] = [
            "governoratment_id" : governrateID as AnyObject,
        ]
        
        AddJourneyAPIManager().getCities(basicDictionary: params, onSuccess: { (cities) in
            
            if toBool == true {
                self.toCities = cities // Cache
            } else if toBool == false {
                self.fromCities = cities // Cache
            }
            self.state = .populated
            
        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    func fetchMyCars() {
        state = .loading
        
        let params: [String : AnyObject] = [:]
        
        CarsAPIManager().getMyCars(basicDictionary: params, onSuccess: { (myCars) in
            
            self.myCars = myCars
            self.state = .populated

        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    func fetchMyCarInformation(carID: Int) {
        state = .loading
        
        let params: [String : AnyObject] = [
            "car_id" : carID as AnyObject
        ]
        
        CarsAPIManager().getMyCarInfo(basicDictionary: params, onSuccess: { (carInfo) in
            
            self.myCarInfo = carInfo // Cache
            self.state = .populated
            
        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    func fetchMyJourneyInformation(tripID: Int) {
        state = .loading
        
        let params: [String : AnyObject] = [
            "trip_id" : tripID as AnyObject
        ]
        
        AddJourneyAPIManager().getMyJourneyInfo(basicDictionary: params, onSuccess: { (journeyInfo) in
            
            self.myJourneyInfo = journeyInfo // Cache
            self.selectedCar = journeyInfo.myCar
            self.choosedFromGovernrate = journeyInfo.packupGovernorate
            self.choosedFromCity = journeyInfo.packupCity
            self.choosedToGovernrate = journeyInfo.dropOffGovernorate
            self.choosedToCity = journeyInfo.dropOffCity
            self.state = .populated
            
        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    func addJourney(edit:Bool, tripID: Int?, carID: Int, packUpCityID: Int?, packupGovernoratmentID: Int, dropOffCityID: Int?, dropOffGovernoratmentID: Int, userID: Int, price: String, day: String, time: String, negotiablePrice: Bool?, deliveryPackage: Bool?, notes: String?) {
        
        let params: [String : AnyObject] = [
            "car_id" : carID as AnyObject,
            "packupcity_id" : packUpCityID as AnyObject,
            "packupgovernoratment_id" : packupGovernoratmentID as AnyObject,
            "dropoffcity_id" : dropOffCityID as AnyObject,
            "dropoffgovernoratment_id" : dropOffGovernoratmentID as AnyObject,
            "user_id" : userID as AnyObject,
            "price" : price as AnyObject,
            "day" : day as AnyObject,
            "time" : time as AnyObject,
            "negotiable_price" : negotiablePrice as AnyObject,
            "delivery_of_aparcel" : deliveryPackage as AnyObject,
            "notes" : notes as AnyObject,
            "trip_id" : tripID as AnyObject,
        ]
                
        self.state = .loading
        
        if !edit {
            AddJourneyAPIManager().addJourney(basicDictionary: params, onSuccess: { (saved) in
                if saved == true {
                    print("Success")
                    
                    self.journeyIsAdded = true
                    self.state = .populated
                    //                self.sendingOrder = .success
                } else {
                    self.state = .empty
                    //                self.sendingOrder = .failed
                }
            }) { (error) in
                self.error = error
                self.state = .error
                //            self.sendingOrder = .failed
            }
        } else {
            AddJourneyAPIManager().updateJourney(basicDictionary: params, onSuccess: { (saved) in
                if saved == true {
                    print("Success")
                    self.journeyIsUpdated = true
                    self.state = .populated
                    //                self.sendingOrder = .success
                } else {
                    self.state = .empty
                    //                self.sendingOrder = .failed
                }
            }) { (error) in
                self.error = error
                self.state = .error
                //            self.sendingOrder = .failed
            }
        }
    }

                
    func changeCarConditioning() {
        self.choosedCarConditioning = !self.choosedCarConditioning!
    }
    
    func changePriceNegotiable() {
        self.choosedPriceNegotiable = !self.choosedPriceNegotiable!
    }

    func changeDeliveryPackage() {
        self.choosedDeliveryPackage = !self.choosedDeliveryPackage!
    }
        
    func userChoossedMyCar(myCar: MyCar) {
        self.selectedCar = myCar
    }
    
    func getCarsTypes() -> [CarType]? {
        return carsTypes
    }
    
    func userChoossedCarType(carType: CarType) {
        self.choosedCarType = carType
    }
    
    func getCarsBrands() -> [CarBrand]? {
        return carsBrands
    }
    
    func userChoossedCarBrand(carBrand: CarBrand) {
        self.choosedCarBrand = carBrand
    }
    
    func getCarsModels() -> [CarModel]? {
        return carsModels
    }
    
    func userChoossedCarModel(carModel: CarModel) {
        self.choosedCarModel = carModel
    }

    func getGovernrates() -> [Governorate]? {
        return governrates
    }
    
    func userChoossedFromGovernrate(governorate: Governorate) {
        self.choosedFromGovernrate = governorate
        if let governrateID: Int = governorate.id {
            self.fetchCities(toBool: false, governrateID: governrateID)
        }
    }
    
//    func getChoossedFromGovernrate() -> Governorate? {
//        return choosedFromGovernrate
//    }
    
    func userChoossedToGovernrate(governorate: Governorate) {
        self.choosedToGovernrate = governorate
        if let governrateID: Int = governorate.id {
            self.fetchCities(toBool: true, governrateID: governrateID)
        }
    }
    
//    func getChoossedToGovernrate() -> Governorate? {
//        return choosedToGovernrate
//    }

    func getToCities() -> [City]? {
        return toCities
    }
    
    func getFromCities() -> [City]? {
        return fromCities
    }
    
    func userChoossedFromCity(city: City) {
        self.choosedFromCity = city
    }
        
    func userChoossedToCity(city: City) {
        self.choosedToCity = city
    }
    
    func getCarConditioningBool() -> Bool? {
        return choosedCarConditioning
    }
    
    func getCarPriceNegotiabel() -> Bool? {
        return choosedPriceNegotiable
    }

    func getCarDeliveryPackage() -> Bool? {
        return choosedDeliveryPackage
    }
    
    func getCarInfo() -> MyCar? {
        return myCarInfo
    }
    
    func getJourneyInfo() -> MyJourney? {
        return myJourneyInfo
    }
    
    func getError() -> APIError {
        return self.error!
    }
    
}
