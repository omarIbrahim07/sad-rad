//
//  AddJourneyViewController.swift
//  SadRad
//
//  Created by Omar Ibrahim on 9/8/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit
import DatePickerDialog
import TimePicker
import DateTimePicker

class AddJourneyViewController: BaseViewController {
    
    var todayString: String?
    
    lazy var viewModel: AddJourneyViewModel = {
        return AddJourneyViewModel()
    }()

    var error: APIError?
    var editBool: Bool? = false
    var journeyID: Int?

    @IBOutlet weak var myCarsView: UIView!
    @IBOutlet weak var myCarLabel: UILabel!
    @IBOutlet weak var fromGovernorateView: UIView!
    @IBOutlet weak var fromCityView: UIView!
    @IBOutlet weak var toGovernorateView: UIView!
    @IBOutlet weak var toCityView: UIView!
    @IBOutlet weak var dateView: UIView!
    @IBOutlet weak var timeView: UIView!
    @IBOutlet weak var priceView: UIView!
    @IBOutlet weak var priceNegotiableView: UIView!
    @IBOutlet weak var deliveryPackageView: UIView!
    @IBOutlet weak var addNoteView: UIView!
    @IBOutlet weak var egpLabel: UILabel!
    @IBOutlet weak var priceNegotiableImageView: UIImageView!
    @IBOutlet weak var deliveryPackageImageView: UIImageView!
    @IBOutlet weak var fromGovernorateValueLabel: UILabel!
    @IBOutlet weak var fromCityValueLabel: UILabel!
    @IBOutlet weak var toGovernorateValueLabel: UILabel!
    @IBOutlet weak var toCityValueLabel: UILabel!
    @IBOutlet weak var timeValueLabel: UILabel!
    @IBOutlet weak var priceTextField: UITextField!
//    @IBOutlet weak var addNoteTextField: UITextField!
    @IBOutlet weak var addNoteTextView: UITextView!
    @IBOutlet weak var addJourneyButton: UIButton!
    @IBOutlet weak var dateValueLabel: UILabel!
    @IBOutlet weak var priceNegotiableValueLabel: UILabel!
    @IBOutlet weak var deliveryPackageValueLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setNavigationTitle()
        setAddTripLocalization()
        configureView()
        configureTableView()
        initVM()
    }
    
    func setNavigationTitle() {
        if self.editBool == true {
            navigationItem.title = "update trip vc title".localized
        } else {
            navigationItem.title = "add trip vc title".localized
        }
    }
    
    func setAddTripLocalization() {
        myCarLabel.text = "add from my cars title".localized
        fromGovernorateValueLabel.text = "from governorate title".localized
        fromCityValueLabel.text = "from city title".localized
        toGovernorateValueLabel.text = "to governorate title".localized
        toCityValueLabel.text = "to city title".localized
        dateValueLabel.text = "add trip date".localized
        timeValueLabel.text = "add trip time".localized
        priceNegotiableValueLabel.text = "price negotiable title".localized
        deliveryPackageValueLabel.text = "delivery package title".localized
//        addNoteTextField.placeholder = "add note placeholder".localized
        addNoteTextView.delegate = self
        addNoteTextView.text = "add note placeholder".localized
        addNoteTextView.textColor = UIColor.white
//        addNoteTextView.placeholder = "add note placeholder".localized
        priceTextField.placeholder = "price placeholder".localized
        priceNegotiableValueLabel.text = "price negotiable title".localized
        if self.editBool == true {
            addJourneyButton.setTitle("update trip button title".localized, for: .normal)
        } else {
            addJourneyButton.setTitle("add trip button title".localized, for: .normal)
        }
    }
    
    func datePickerTapped() {
        let datePickeDialog = DatePickerDialog()
        datePickeDialog.show("DatePicker", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", minimumDate: Date().addingTimeInterval(60 * 60 * 24 * 0), datePickerMode: .date) {
            (date) -> Void in
            if let dt = date {
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd"
                formatter.locale = Locale.init(identifier: "en")
                self.dateValueLabel.text = formatter.string(from: dt)
                self.dateValueLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                self.setTodayString()
            }
        }
    }
    
    func setTodayString() {
        let date = Date().addingTimeInterval(60 * 60 * 24 * 0)
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.locale = Locale.init(identifier: "en")
        todayString = formatter.string(from: date)
    }
        
    // MARK:- Set the time
    ///This method handle the time configuration
    func setTheTime() {
        var min: Date?
        if dateValueLabel.text == todayString {
            /// You can make trip after minimum two hours today
            ///
            min = Date().addingTimeInterval(60 * 60 * 2)
        } else {
            min = Date().addingTimeInterval(-60 * 60 * 24 * 4)
        }
        let max = Date().addingTimeInterval(60 * 60 * 24 * 4)
        let picker = DateTimePicker.create(minimumDate: min, maximumDate: max)
        picker.frame = CGRect(x: 0, y: 100, width: picker.frame.size.width, height: picker.frame.size.height)
        picker.highlightColor = UIColor(red: 255.0/255.0, green: 138.0/255.0, blue: 138.0/255.0, alpha: 1)
        self.view.addSubview(picker)
                
        picker.isTimePickerOnly = true
        picker.is12HourFormat = false
        
//        picker.doneButtonTitle = "DONE"
        picker.doneBackgroundColor = UIColor(red: 255.0/255.0, green: 138.0/255.0, blue: 138.0/255.0, alpha: 1)
        
        picker.cancelButton.isHidden = true
        picker.todayButton.isHidden = true
        
//        picker.donePicking(sender: picker.doneButton)
        
        picker.delegate = self
        
        picker.show()
    }


    // MARK:- Init View Model
    func initVM() {
                
        viewModel.updateTimeValueClosure = { [weak self] () in
                DispatchQueue.main.async { [weak self] in
                
            }
        }
        
        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                
                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                case .error:
                    self.error = self.viewModel.getError()
                    if let error = self.error {
//                        self.showError(error: error)
                        self.stopLoadingWithError(error: error)
                    }
                case .empty:
                    print("")
                }
            }
        }
        
        viewModel.chooseCarTypeClosure = { [weak self] () in
            DispatchQueue.main.async { [weak self] in
                if let choosedCarType: CarType = self?.viewModel.choosedCarType {
                    self?.bindCarType(carType: choosedCarType)
                    self?.bindRequiredFields(carType: choosedCarType)
                }
            }
        }
        
        viewModel.chooseCarBrandClosure = { [weak self] () in
            DispatchQueue.main.async { [weak self] in
                if let choosedCarBrand: CarBrand = self?.viewModel.choosedCarBrand {
                    self?.bindCarBrand(carBrand: choosedCarBrand)
                }
            }
        }

        viewModel.chooseCarModelClosure = { [weak self] () in
            DispatchQueue.main.async { [weak self] in
                if let choosedCarModel: CarModel = self?.viewModel.choosedCarModel {
                    self?.bindCarModel(carModel: choosedCarModel)
                }
            }
        }
        
        viewModel.chooseCarConditioningClosure = { [weak self] () in
            DispatchQueue.main.async { [weak self] in
                if let choosedCarConditioningBool: Bool = self?.viewModel.getCarConditioningBool() {
                    self?.bindCarConditioningState(carConditioningBool: choosedCarConditioningBool)
                }
            }
        }
        
        viewModel.chooseDeliveringPackageClosure = { [weak self] () in
            DispatchQueue.main.async { [weak self] in
                if let choosedCarDeliveryPackageBool: Bool = self?.viewModel.getCarDeliveryPackage() {
                    self?.bindCarDeliveryPackage(carDeliveryPackageBool: choosedCarDeliveryPackageBool)
                }
            }
        }

        viewModel.choosePriceNegotiableClosure = { [weak self] () in
            DispatchQueue.main.async { [weak self] in
                if let choosedCarPriceNegotiableBool: Bool = self?.viewModel.getCarPriceNegotiabel() {
                    self?.bindCarPriceNegotiation(carPriceNegotiationBool: choosedCarPriceNegotiableBool)
                }
            }
        }
        
        viewModel.updateMyCarInfoClosure = { [weak self] () in
            DispatchQueue.main.async { [weak self] in
                if let myCarInfo: MyCar = self?.viewModel.getCarInfo() {
                    self?.bindMyCarInfo(myCarInfo: myCarInfo)
                }
            }
        }
        
        viewModel.chooseFromGovernrateClosure = { [weak self] () in
            DispatchQueue.main.async { [weak self] in
                if let governorate: Governorate = self?.viewModel.choosedFromGovernrate {
                    self?.bindGovernorateModel(toBool: false, governorate: governorate)
                    if self?.viewModel.isFirstTimeFromGovBool == true {
                        self?.viewModel.isFirstTimeFromGovBool = false
                    } else {
                        self?.viewModel.choosedFromCity = nil
                    }
                }
            }
        }
        
        viewModel.chooseToGovernrateClosure = { [weak self] () in
            DispatchQueue.main.async { [weak self] in
                if let governorate: Governorate = self?.viewModel.choosedToGovernrate {
                    self?.bindGovernorateModel(toBool: true, governorate: governorate)
                    if self?.viewModel.isFirstTimeToGovBool == true {
                        self?.viewModel.isFirstTimeToGovBool = false
                    } else {
                        self?.viewModel.choosedToCity = nil
                    }
                }
            }
        }
        
        viewModel.chooseToCityClosure = { [weak self] () in
            DispatchQueue.main.async { [weak self] in
                if let city: City = self?.viewModel.choosedToCity {
                    self?.bindCityModel(toBool: true, city: city)
                } else {
                    self?.toCityValueLabel.text = ""
                }
            }
        }
        
        viewModel.chooseFromCityClosure = { [weak self] () in
            DispatchQueue.main.async { [weak self] in
                if let city: City = self?.viewModel.choosedFromCity {
                    self?.bindCityModel(toBool: false, city: city)
                } else {
                    self?.fromCityValueLabel.text = ""
                }
            }
        }

        viewModel.updateMyCarInfoClosure = { [weak self] () in
            DispatchQueue.main.async { [weak self] in
                if let myCarInfo: MyCar = self?.viewModel.getCarInfo() {
                    self?.bindMyCarInfo(myCarInfo: myCarInfo)
                }
            }
        }

        viewModel.updateJourneyInfoClosure = { [weak self] () in
            DispatchQueue.main.async { [weak self] in
                if let myJourneyInfo: MyJourney = self?.viewModel.getJourneyInfo() {
                    self?.bindMyJourneyInfo(myJourneyInfo: myJourneyInfo)
                }
            }
        }

        
        viewModel.myCarIsChoosedClosure = { [weak self] () in
            DispatchQueue.main.async { [weak self] in
                if let myCar: MyCar = self?.viewModel.selectedCar {
                    self?.bindMyCarInfo(myCarInfo: myCar)
                }
            }
        }
        
        viewModel.journeyAddedClosure = { [weak self] () in
            DispatchQueue.main.async { [weak self] in
                if let journeyIsAdded: Bool = self?.viewModel.journeyIsAdded {
                    if journeyIsAdded == true {
                        print("Journey is added")
                        self?.showJourneyPopUp(title: "add journey alert title".localized, message: "add journy alert message".localized, actionTitle: "done action title".localized)
                    }
                }
            }
        }

        viewModel.journeyUpdatedClosure = { [weak self] () in
            DispatchQueue.main.async { [weak self] in
                if let journeyIsUpdated: Bool = self?.viewModel.journeyIsUpdated {
                    if journeyIsUpdated == true {
                        print("Journey is updated")
                        self?.showJourneyPopUp(title: "update journey alert title".localized, message: "update journy alert message".localized, actionTitle: "done action title".localized)
                    }
                }
            }
        }
                        
        viewModel.fetchCarTypes()
        viewModel.fetchCarBrands()
        viewModel.fetchCarModels()
        viewModel.fetchGovernrates()
        viewModel.fetchMyCars()
        
        if self.editBool == true {
            if let journeyID: Int = self.journeyID {
                self.viewModel.fetchMyJourneyInformation(tripID: journeyID)
            }
        }
    }
    
    func showJourneyPopUp(title: String, message: String, actionTitle: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        //        let cancelAction = UIAlertAction(title: "إلغاء", style: .cancel, handler: nil)
        //        alertController.addAction(cancelAction)
        let openAction = UIAlertAction(title: actionTitle, style: .default) { (action) in
            self.presentMyHomeScreen()
        }
//        let cancelAction = UIAlertAction(title: "cancel go to login".localized, style: .cancel)
        alertController.addAction(openAction)
//        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    // MARK:- Show Time Options Picker
    func setupMyCars(myCars: [MyCar]) {
        
        let sheet = UIAlertController(title: "choose from my cars alert title".localized, message: "choose from my cars alert message".localized, preferredStyle: .actionSheet)
        for myCar in myCars {
            if "Lang".localized == "ar" {
                sheet.addAction(UIAlertAction(title: String((myCar.carBrand?.nameAr ?? "") + (myCar.carModel?.nameAr ?? "")), style: .default, handler: {_ in
                    self.viewModel.userChoossedMyCar(myCar: myCar)
                }))
            } else if "Lang".localized == "en" {
                sheet.addAction(UIAlertAction(title: String((myCar.carBrand?.nameEn ?? "") + (myCar.carModel?.nameEn ?? "")), style: .default, handler: {_ in
                    self.viewModel.userChoossedMyCar(myCar: myCar)
                }))
            }
        }
        sheet.addAction(UIAlertAction(title: "cancel choose from my cars".localized, style: .cancel, handler: nil))
        
        //        sheet.popoverPresentationController?.sourceView = sortChoiceLabel
        
        self.present(sheet, animated: true, completion: nil)
    }

    func setupCarsTypes(carTypes: [CarType]) {
        
        let sheet = UIAlertController(title: "car type alert title".localized, message: "car type alert message".localized, preferredStyle: .actionSheet)
        for carType in carTypes {
            if "Lang".localized == "ar" {
                sheet.addAction(UIAlertAction(title: carType.nameAr, style: .default, handler: {_ in
                    self.viewModel.userChoossedCarType(carType: carType)
                }))
            } else if "Lang".localized == "en" {
                sheet.addAction(UIAlertAction(title: carType.nameEn, style: .default, handler: {_ in
                    self.viewModel.userChoossedCarType(carType: carType)
                }))
            }
        }
        sheet.addAction(UIAlertAction(title: "cancel car type".localized, style: .cancel, handler: nil))
        
        //        sheet.popoverPresentationController?.sourceView = sortChoiceLabel
        
        self.present(sheet, animated: true, completion: nil)
    }
    
    func setupCarsBrands(carBrands: [CarBrand]) {
        
        let sheet = UIAlertController(title: "car brand alert title".localized, message: "car brand alert message".localized, preferredStyle: .actionSheet)
        for carBrand in carBrands {
            if "Lang".localized == "ar" {
                sheet.addAction(UIAlertAction(title: carBrand.nameAr, style: .default, handler: {_ in
                    self.viewModel.userChoossedCarBrand(carBrand: carBrand)
                }))
            } else if "Lang".localized == "en" {
                sheet.addAction(UIAlertAction(title: carBrand.nameEn, style: .default, handler: {_ in
                    self.viewModel.userChoossedCarBrand(carBrand: carBrand)
                }))
            }
        }
        sheet.addAction(UIAlertAction(title: "cancel car brand".localized, style: .cancel, handler: nil))
        
        //        sheet.popoverPresentationController?.sourceView = sortChoiceLabel
        
        self.present(sheet, animated: true, completion: nil)
    }

    func setupCarsModels(carModels: [CarModel]) {
        
        let sheet = UIAlertController(title: "car model alert title".localized, message: "car model alert message".localized, preferredStyle: .actionSheet)
        for carModel in carModels {
            if "Lang".localized == "ar" {
                sheet.addAction(UIAlertAction(title: carModel.nameAr, style: .default, handler: {_ in
                    self.viewModel.userChoossedCarModel(carModel: carModel)
                }))
            } else if "Lang".localized == "en" {
                sheet.addAction(UIAlertAction(title: carModel.nameEn, style: .default, handler: {_ in
                    self.viewModel.userChoossedCarModel(carModel: carModel)
                }))
            }
        }
        sheet.addAction(UIAlertAction(title: "cancel car model".localized, style: .cancel, handler: nil))
        
        //        sheet.popoverPresentationController?.sourceView = sortChoiceLabel
        
        self.present(sheet, animated: true, completion: nil)
    }
    
    func setupGovernrate(toBool: Bool, governrates: [Governorate]) {
        
        let sheet = UIAlertController(title: "governorate alert title".localized, message: "governorate alert message".localized, preferredStyle: .actionSheet)
        
        if toBool == true {
            for governrate in governrates {
                if "Lang".localized == "ar" {
                    sheet.addAction(UIAlertAction(title: governrate.nameAr, style: .default, handler: {_ in
                        self.viewModel.userChoossedToGovernrate(governorate: governrate)
                    }))
                } else if "Lang".localized == "en" {
                    sheet.addAction(UIAlertAction(title: governrate.nameEn, style: .default, handler: {_ in
                        self.viewModel.userChoossedToGovernrate(governorate: governrate)
                    }))
                }
            }
        } else if toBool == false {
            for governrate in governrates {
                if "Lang".localized == "ar" {
                    sheet.addAction(UIAlertAction(title: governrate.nameAr, style: .default, handler: {_ in
                        self.viewModel.userChoossedFromGovernrate(governorate: governrate)
                    }))
                } else if "Lang".localized == "en" {
                    sheet.addAction(UIAlertAction(title: governrate.nameEn, style: .default, handler: {_ in
                        self.viewModel.userChoossedFromGovernrate(governorate: governrate)
                    }))
                }
            }
        }
        sheet.addAction(UIAlertAction(title: "cancel governorate".localized, style: .cancel, handler: nil))
        
        //        sheet.popoverPresentationController?.sourceView = sortChoiceLabel
        
        self.present(sheet, animated: true, completion: nil)
    }

    func setupCities(toBool: Bool, cities: [City]) {
        
        let sheet = UIAlertController(title: "city alert title".localized, message: "city alert message".localized, preferredStyle: .actionSheet)
        if toBool == true {
            for city in cities {
                if "Lang".localized == "ar" {
                    sheet.addAction(UIAlertAction(title: city.nameAr, style: .default, handler: {_ in
                        self.viewModel.userChoossedToCity(city: city)
                    }))
                } else if "Lang".localized == "en" {
                    sheet.addAction(UIAlertAction(title: city.nameEn, style: .default, handler: {_ in
                        self.viewModel.userChoossedToCity(city: city)
                    }))
                }
            }
        } else if toBool == false {
            for city in cities {
                if "Lang".localized == "ar" {
                    sheet.addAction(UIAlertAction(title: city.nameAr, style: .default, handler: {_ in
                        self.viewModel.userChoossedFromCity(city: city)
                    }))
                } else if "Lang".localized == "en" {
                    sheet.addAction(UIAlertAction(title: city.nameEn, style: .default, handler: {_ in
                        self.viewModel.userChoossedFromCity(city: city)
                    }))
                }
            }
        }
        sheet.addAction(UIAlertAction(title: "cancel city".localized, style: .cancel, handler: nil))
        
        //        sheet.popoverPresentationController?.sourceView = sortChoiceLabel
        
        self.present(sheet, animated: true, completion: nil)
    }


    
    func bindCarType(carType: CarType) {
        if "Lang".localized == "ar" {
//            self.carTypeValueLabel.text = carType.nameAr
        } else if "Lang".localized == "en" {
//            self.carTypeValueLabel.text = carType.nameEn
        }
    }
    
    func bindCarBrand(carBrand: CarBrand) {
        if "Lang".localized == "ar" {
//            self.brandValueLabel.text = carBrand.nameAr
        } else if "Lang".localized == "en" {
//            self.brandValueLabel.text = carBrand.nameEn
        }
    }

    func bindCarModel(carModel: CarModel) {
        if "Lang".localized == "ar" {
//            self.modelValueLabel.text = carModel.nameAr
        } else if "Lang".localized == "en" {
//            self.modelValueLabel.text = carModel.nameEn
        }
    }
    
    func bindGovernorateModel(toBool: Bool, governorate: Governorate) {
        if toBool == true {
            if "Lang".localized == "ar" {
                self.toGovernorateValueLabel.text = governorate.nameAr
            } else if "Lang".localized == "en" {
                self.toGovernorateValueLabel.text = governorate.nameEn
            }
        } else if toBool == false {
            if "Lang".localized == "ar" {
                self.fromGovernorateValueLabel.text = governorate.nameAr
            } else if "Lang".localized == "en" {
                self.fromGovernorateValueLabel.text = governorate.nameEn
            }
        }
    }

    func bindCityModel(toBool: Bool, city: City) {
        if toBool == true {
            if "Lang".localized == "ar" {
                self.toCityValueLabel.text = city.nameAr
            } else if "Lang".localized == "en" {
                self.toCityValueLabel.text = city.nameEn
            }
        } else if toBool == false {
            if "Lang".localized == "ar" {
                self.fromCityValueLabel.text = city.nameAr
            } else if "Lang".localized == "en" {
                self.fromCityValueLabel.text = city.nameEn
            }
        }
    }
    
    func bindRequiredFields(carType: CarType) {
        if carType.statusRequired == "1" {
//            maximumWeightStarImageView.isHidden = true
//            numberOfPersonsStarImageView.isHidden = false
        } else if carType.statusRequired == "2" {
//            maximumWeightStarImageView.isHidden = false
//            numberOfPersonsStarImageView.isHidden = true
        }
    }
    
    func bindCarConditioningState(carConditioningBool: Bool) {
        if carConditioningBool == true {
//            airConditioningImageView.image = UIImage(named: "Checked")
        } else if carConditioningBool == false {
//            airConditioningImageView.image = UIImage(named: "UnChecked")
        }
    }
    
    func bindCarDeliveryPackage(carDeliveryPackageBool: Bool) {
        if carDeliveryPackageBool == true {
            deliveryPackageImageView.image = UIImage(named: "Checked")
        } else if carDeliveryPackageBool == false {
            deliveryPackageImageView.image = UIImage(named: "UnChecked")
        }
    }

    func bindCarPriceNegotiation(carPriceNegotiationBool: Bool) {
        if carPriceNegotiationBool == true {
            priceNegotiableImageView.image = UIImage(named: "Checked")
        } else if carPriceNegotiationBool == false {
            priceNegotiableImageView.image = UIImage(named: "UnChecked")
        }
    }
    
    func bindMyJourneyInfo(myJourneyInfo: MyJourney) {
        if let carInfo: MyCar = myJourneyInfo.myCar {
            self.bindMyCarInfo(myCarInfo: carInfo)
        }
        if "Lang".localized == "en" {
            if let fromGovernorate: String = myJourneyInfo.packupGovernorate?.nameEn {
                self.fromGovernorateValueLabel.text = fromGovernorate
            }
            if let fromCity: String = myJourneyInfo.packupCity?.nameEn {
                self.fromCityValueLabel.text = fromCity
            }
            if let toGovernorate: String = myJourneyInfo.dropOffGovernorate?.nameEn {
                self.toGovernorateValueLabel.text = toGovernorate
            }
            if let toCity: String = myJourneyInfo.dropOffCity?.nameEn {
                self.toCityValueLabel.text = toCity
            }
        } else if "Lang".localized == "ar" {
            if let fromGovernorate: String = myJourneyInfo.packupGovernorate?.nameAr {
                self.fromGovernorateValueLabel.text = fromGovernorate
            }
            if let fromCity: String = myJourneyInfo.packupCity?.nameAr {
                self.fromCityValueLabel.text = fromCity
            }
            if let toGovernorate: String = myJourneyInfo.dropOffGovernorate?.nameAr {
                self.toGovernorateValueLabel.text = toGovernorate
            }
            if let toCity: String = myJourneyInfo.dropOffCity?.nameAr {
                self.toCityValueLabel.text = toCity
            }
        }
        if let day: String = myJourneyInfo.day {
            dateValueLabel.text = day
        }
        if let time: String = myJourneyInfo.time {
            timeValueLabel.text = time
        }
        if let price: Int = myJourneyInfo.price {
            priceTextField.text = String(price)
        }
        if let negotiable = myJourneyInfo.negotiablePriceBool {
            if negotiable == true {
                self.viewModel.choosedPriceNegotiable = true
            } else if negotiable == false {
                self.viewModel.choosedPriceNegotiable = false
            }
        }
        if let packageBool = myJourneyInfo.deliveryPackagePriceBool {
            if packageBool == true {
                self.viewModel.choosedDeliveryPackage = true
            } else if packageBool == false {
                self.viewModel.choosedDeliveryPackage = false
            }
        }
        if let notes: String = myJourneyInfo.notes {
//            self.addNoteTextField.text = notes
            self.addNoteTextView.text = notes
        }
    }
    
    func bindMyCarInfo(myCarInfo: MyCar) {
        if let carType: CarType = myCarInfo.carType {
            self.viewModel.choosedCarType = carType
        }
        if let carModel: CarModel = myCarInfo.carModel {
            self.viewModel.choosedCarModel = carModel
        }
        if let carBrand: CarBrand = myCarInfo.carBrand {
            self.viewModel.choosedCarBrand = carBrand
        }
        if let maximumWeight: Int = myCarInfo.maximimWeight {
//            self.maximumWeightTextfield.text = String(maximumWeight)
        }
        if let numberOfPersons: Int = myCarInfo.numberOfPersons {
//            self.numberOfPersonsTextfield.text = String(numberOfPersons)
        }
        if let airConditiong: Bool = myCarInfo.airConditioned {
            if airConditiong == true {
                self.viewModel.choosedCarConditioning = true
            } else if airConditiong == false {
                self.viewModel.choosedCarConditioning = false
            }
        }
        if "Lang".localized == "en" {
            if let carTypeName: String = myCarInfo.carType?.nameEn {
//                self.carTypeValueLabel.text = carTypeName
            }
            if let carBrandName: String = myCarInfo.carBrand?.nameEn {
//                self.brandValueLabel.text = carBrandName
            }
            if let carModelName: String = myCarInfo.carModel?.nameEn, let carBrandName: String = myCarInfo.carBrand?.nameEn {
                myCarLabel.text = carBrandName + " " + carModelName
            }
        } else if "Lang".localized == "ar" {
            if let carTypeName: String = myCarInfo.carType?.nameAr {
//                self.carTypeValueLabel.text = carTypeName
            }
            if let carBrandName: String = myCarInfo.carBrand?.nameAr {
//                self.brandValueLabel.text = carBrandName
            }
            if let carModelName: String = myCarInfo.carModel?.nameAr, let carBrandName: String = myCarInfo.carBrand?.nameAr {
                myCarLabel.text = carBrandName + " " + carModelName
            }
        }
    }
        
    func closeKeypad() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc func viewTapped() {
        priceTextField.endEditing(true)
//        addNoteTextField.endEditing(true)
        addNoteTextView.endEditing(true)
//        numberOfPersonsTextfield.endEditing(true)
//        maximumWeightTextfield.endEditing(true)
    }
    
    func configureView() {
        myCarsView.addCornerRadius(raduis: 8, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        fromGovernorateView.addCornerRadius(raduis: 8, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        fromCityView.addCornerRadius(raduis: 8, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        toGovernorateView.addCornerRadius(raduis: 8, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        toCityView.addCornerRadius(raduis: 8, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        dateView.addCornerRadius(raduis: 8, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        timeView.addCornerRadius(raduis: 8, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        priceView.addCornerRadius(raduis: 8, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        priceNegotiableView.addCornerRadius(raduis: 8, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        deliveryPackageView.addCornerRadius(raduis: 8, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        addNoteView.addCornerRadius(raduis: 8, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        
//        maximumWeightTextfield.isUserInteractionEnabled = false
//        numberOfPersonsTextfield.isUserInteractionEnabled = false
        
        if "Lang".localized == "en" {
//            numberOfPersonsTextfield.textAlignment = .left
//            maximumWeightTextfield.textAlignment = .left
            priceTextField.textAlignment = .left
//            addNoteTextField.textAlignment = .left
            addNoteTextView.textAlignment = .left
        } else if "Lang".localized == "ar" {
    //            numberOfPersonsTextfield.textAlignment = .right
    //            maximumWeightTextfield.textAlignment = .right
            priceTextField.textAlignment = .right
//            addNoteTextField.textAlignment = .right
            addNoteTextView.textAlignment = .right
        }

        addJourneyButton.addCornerRadius(raduis: addJourneyButton.frame.height / 2, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        closeKeypad()
        priceTextField.delegate = self
    }
    
    func checkCarSelection() {
        guard let _ = self.viewModel.selectedCar else {
            let apiError = APIError()
            apiError.message = "error choose from your cars".localized
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
    }
    
    func configureTableView() {
//        tableView.register(UINib(nibName: "CheckingTableViewCell", bundle: nil), forCellReuseIdentifier: "CheckingTableViewCell")
//        tableView.register(UINib(nibName: "TextfieldTableViewCell", bundle: nil), forCellReuseIdentifier: "TextfieldTableViewCell")
//        tableView.register(UINib(nibName: "TimeDateTableViewCell", bundle: nil), forCellReuseIdentifier: "TimeDateTableViewCell")
//        tableView.register(UINib(nibName: "MultipleChoiceTableViewCell", bundle: nil), forCellReuseIdentifier: "MultipleChoiceTableViewCell")
//        tableView.delegate = self
//        tableView.dataSource = self
//        tableView.tableFooterView = UIView()
    }
    
    func addJourney() {
        var notes: String?
        
        guard let carID: Int = self.viewModel.selectedCar?.id, carID > 0 else {
            let apiError = APIError()
            apiError.message = "error choose car id".localized
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
                
        guard let pickupGovernorateID: Int = self.viewModel.choosedFromGovernrate?.id, pickupGovernorateID > 0 else {
            let apiError = APIError()
            apiError.message = "error pick up governorate".localized
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }

//        guard let pickupCityID: Int = self.viewModel.choosedFromCity?.id, pickupCityID > 0 else {
//            let apiError = APIError()
//            apiError.message = "error pick up city".localized
//            self.viewModel.error = apiError
//            self.viewModel.state = .error
//            return
//        }
        
        guard let dropOffGovernorateID: Int = self.viewModel.choosedToGovernrate?.id, dropOffGovernorateID > 0 else {
            let apiError = APIError()
            apiError.message = "error drop off governorate".localized
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }

//        guard let dropOffCityID: Int = self.viewModel.choosedToCity?.id, dropOffCityID > 0 else {
//            let apiError = APIError()
//            apiError.message = "error drop off city".localized
//            self.viewModel.error = apiError
//            self.viewModel.state = .error
//            return
//        }
        
        guard let price: String = self.priceTextField.text, price.count > 0 else {
            let apiError = APIError()
            apiError.message = "error price".localized
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        

        guard let date: String = self.dateValueLabel.text, date.count >= 10 else {
            let apiError = APIError()
            apiError.message = "error enter day".localized
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }

        guard let time: String = self.timeValueLabel.text, time.count >= 8 else {
            let apiError = APIError()
            apiError.message = "error enter time".localized
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
          
        if self.addNoteTextView.text != "add note placeholder".localized {
            notes = self.addNoteTextView.text
        }
        
        self.viewModel.addJourney(edit: editBool ?? false, tripID: self.journeyID, carID: carID, packUpCityID: self.viewModel.choosedFromCity?.id, packupGovernoratmentID: pickupGovernorateID, dropOffCityID: self.viewModel.choosedToCity?.id, dropOffGovernoratmentID: dropOffGovernorateID, userID: UserDefaultManager.shared.currentUser?.id ?? 0, price: price.withWesternNumbers, day: date, time: time, negotiablePrice: self.viewModel.choosedPriceNegotiable, deliveryPackage: self.viewModel.choosedPriceNegotiable, notes: notes)
    }
    
    func showAddCarAlert() {
        let openAction = UIAlertAction(title: "go to add car button title".localized, style: .default) { (action) in
            self.goToAddCar()
        }
        let cancelAction = UIAlertAction(title: "cancel go to add car button".localized, style: .cancel)
        self.showAlert(title: "go to add car alert title".localized, message: "go to add car alert message".localized, alertActions: [openAction, cancelAction])
    }

    // MARK:- Actions
    @IBAction func myCarsButtonIsPressed(_ sender: Any) {
        if self.viewModel.myCars.count > 0 {
            self.setupMyCars(myCars: self.viewModel.myCars)
        } else if self.viewModel.myCars.count == 0 {
            self.showAddCarAlert()
        }
    }
    
    @IBAction func fromGovernorateButtonIsPressed(_ sender: Any) {
        print("FESS")
        if let fromGovernorates: [Governorate] = viewModel.governrates {
            self.setupGovernrate(toBool: false, governrates: fromGovernorates)
        }
    }
    
    @IBAction func fromCityButtonIsPressed(_ sender: Any) {
        if let fromCities: [City] = viewModel.fromCities {
            self.setupCities(toBool: false, cities: fromCities)
        }
    }
    
    @IBAction func toGovernorateButtonIsPressed(_ sender: Any) {
        if let toGovernorates: [Governorate] = viewModel.governrates {
            self.setupGovernrate(toBool: true, governrates: toGovernorates)
        }
    }
    
    @IBAction func toCityButtonIsPressed(_ sender: Any) {
        if let toCities: [City] = viewModel.toCities {
            self.setupCities(toBool: true, cities: toCities)
        }
    }
    
    @IBAction func dayButtonIsPressed(_ sender: Any) {
        self.timeValueLabel.text = ""
        datePickerTapped()
    }
    
    @IBAction func timeButtonIsPressed(_ sender: Any) {
        setTheTime()
    }
    
    @IBAction func priceNegotiableButtonIsPressed(_ sender: Any) {
        self.viewModel.changePriceNegotiable()
    }
    
    @IBAction func deliveryPackageButtonIsPressed(_ sender: Any) {
        self.viewModel.changeDeliveryPackage()
    }
        
    @IBAction func addJourneyButtonIsPressed(_ sender: Any) {
        self.addJourney()
    }
    
//    @IBAction func airConditionedButtonIsPressed(_ sender: Any) {
//        self.checkCarSelection()
//    }
//
//    @IBAction func carsTypesButtonIsPressed(_ sender: Any) {
//        self.checkCarSelection()
//    }
//
//    @IBAction func carsBrandsButtonIsPressed(_ sender: Any) {
//        self.checkCarSelection()
//    }
//
//    @IBAction func carsModelsButtonIsPressed(_ sender: Any) {
//        self.checkCarSelection()
//    }
    
    // MARK: - Navigation
    func goToAddCar() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "MyCarsViewController") as! MyCarsViewController
        navigationController?.pushViewController(viewController, animated: true)
    }

}

extension AddJourneyViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //For numers
        if textField == priceTextField {
            let allowedCharacters = CharacterSet(charactersIn:"0123456789٠١٢٣٤٥٦٧٨٩")//Here change this characters based on your requirement
            let characterSet = CharacterSet(charactersIn: string)
            return allowedCharacters.isSuperset(of: characterSet)
        }
        return true
    }
}
