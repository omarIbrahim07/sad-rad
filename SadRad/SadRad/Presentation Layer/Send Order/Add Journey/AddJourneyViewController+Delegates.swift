//
//  AddJourneyViewController+Delegates.swift
//  SadRad
//
//  Created by Omar Ibrahim on 9/8/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit
import DateTimePicker

extension AddJourneyViewController: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.white {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "add note placeholder".localized
            textView.textColor = UIColor.white
        }
    }
    
}

extension AddJourneyViewController: DateTimePickerDelegate {
    func dateTimePicker(_ picker: DateTimePicker, didSelectDate: Date) {
        
        print(picker.selectedDateString)
        
        var f: String?
        f = picker.selectedDateString
        let subString = f?.prefix(5)
        if "Lang".localized == "ar" {
            self.timeValueLabel.text = String(subString!).withWesternNumbers + ":00"
        } else if "Lang".localized == "en" {
            self.timeValueLabel.text = String(subString!).withWesternNumbers + ":00"
        }
        self.timeValueLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
    
}

//extension AddJourneyViewController: UITableViewDelegate, UITableViewDataSource {
//
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return 13
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//
//        let index = indexPath.row
//
//        if index == 0 || index == 1 || index == 2 || index == 3 || index == 7 {
//            if let cell: MultipleChoiceTableViewCell = tableView.dequeueReusableCell(withIdentifier: "MultipleChoiceTableViewCell") as? MultipleChoiceTableViewCell {
//
//                return cell
//            }
//        } else if index == 4 || index == 8 {
//            if let cell: TimeDateTableViewCell = tableView.dequeueReusableCell(withIdentifier: "TimeDateTableViewCell") as? TimeDateTableViewCell {
//
//                return cell
//            }
//        } else if index == 5 || index == 9 || index == 10 || index == 12 {
//            if let cell: TextfieldTableViewCell = tableView.dequeueReusableCell(withIdentifier: "TextfieldTableViewCell") as? TextfieldTableViewCell {
//
//                return cell
//            }
//        } else if index == 6 || index == 11 {
//            if let cell: CheckingTableViewCell = tableView.dequeueReusableCell(withIdentifier: "CheckingTableViewCell") as? CheckingTableViewCell {
//
//                return cell
//            }
//        }
//
//        return UITableViewCell()
//    }
//
//
//}
