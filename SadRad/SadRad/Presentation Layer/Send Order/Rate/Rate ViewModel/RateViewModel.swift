//
//  RateViewModel.swift
//  SadRad
//
//  Created by Omar Ibrahim on 10/25/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation

class RateViewModel {
    
    var error: APIError?
    
    var updateLoadingStatus: (()->())?
    var rateIsSavedClosure: (()->())?
    
    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    var rateSaved: Bool? = false {
        didSet {
            self.rateIsSavedClosure?()
        }
    }
    
    var tripID: Int?
    var note: String?
    
    var rateArray: [String] = []
    
    func initRate() {
        for _ in 0...2 {
            self.rateArray.append("5.0")
        }
    }
    
    func rateTrip(tripID: Int, reviewArray: [String], note: String) {
        let params: [String : AnyObject] = [
            "trip_id" : tripID as AnyObject,
            "reviewtrip" : 5 as AnyObject,
            "reviewcar" : reviewArray[0] as AnyObject,
            "reviewuser" : reviewArray[1] as AnyObject,
            "navigation" : reviewArray[2] as AnyObject,
            "note" : note as AnyObject,
        ]
        
        self.state = .loading
        
        RateAPIManager().rateTrip(basicDictionary: params, onSuccess: { (saved) in
            if saved != nil {
                self.rateSaved = true
                print("Success")
                self.state = .populated
            } else {
                self.state = .empty
            }
        }) { (error) in
            self.error = error
            self.state = .error
        }
        
    }
    
    
    func getError() -> APIError {
        return self.error!
    }
    
}
