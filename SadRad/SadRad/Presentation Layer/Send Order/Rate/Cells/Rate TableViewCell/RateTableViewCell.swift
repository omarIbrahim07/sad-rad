//
//  RateTableViewCell.swift
//  SadRad
//
//  Created by Omar Ibrahim on 9/14/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit
import Cosmos

protocol FeedBackTableViewCellDelegate {
    func didfeedBackViewRated(tag: Int, rate: Float)
}

class RateTableViewCell: UITableViewCell {

    var feedbackdelegate: FeedBackTableViewCellDelegate?
    var tagArray: [Double] = []
    
//    var viewModelsfeedbackItems : [FeedbackItem] = []{
//        didSet {
//            getTagArray()
//        }
//    }

    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var ratingCosmosView: CosmosView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        ratingCosmosView.settings.fillMode = .full
        ratingCosmosView.didTouchCosmos = { rating in
            self.rate(cosmosViewIndex: self.ratingCosmosView.tag)
        }
    }
        
    func getTagArray() {
//        for extraService in self.viewModelsfeedbackItems {
        for _ in 0...2 {
            self.tagArray.append(5.0)
        }
        print(tagArray.count)
        print(tagArray)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func didfeedBackViewRated(tag: Int, rate: Float) {
        if let delegateValue = feedbackdelegate {
            delegateValue.didfeedBackViewRated(tag: tag, rate: rate)
        }
    }
    
    func rate(cosmosViewIndex: Int) {
        didfeedBackViewRated(tag: cosmosViewIndex, rate: Float(self.ratingCosmosView!.rating))
    }

}
