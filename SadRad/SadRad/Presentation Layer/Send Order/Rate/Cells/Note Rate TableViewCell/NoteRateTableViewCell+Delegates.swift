//
//  NoteRateTableViewCell+Delegates.swift
//  SadRad
//
//  Created by Omar Ibrahim on 12/17/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

extension NoteRateTableViewCell: UITextViewDelegate {
            
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1) {
            textView.text = nil
            textView.textColor = #colorLiteral(red: 0.420279026, green: 0.8111016154, blue: 0.7780727744, alpha: 1)
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "add note textfield placeholder".localized
            textView.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        addNoteTextViewPressed(note: textView.text)
    }
    
}

