//
//  NoteRateTableViewCell.swift
//  SadRad
//
//  Created by Omar Ibrahim on 10/25/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

protocol AddNoteTableViewCellDelegate {
    func addNoteTextFieldPressed(note: String)
}

protocol AddNoteTextViewTableViewCellDelegate {
    func addNoteTextViewPressed(note: String)
}

class NoteRateTableViewCell: UITableViewCell {
    
    var addNoteDelegate: AddNoteTableViewCellDelegate?
    var addNoteTextViewDelegate: AddNoteTextViewTableViewCellDelegate?

    @IBOutlet weak var closeView: UIView!
    @IBOutlet weak var ratingLabel: UILabel!
//    @IBOutlet weak var addNoteTextField: UITextField!
    @IBOutlet weak var addNoteTextView: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureView()
        setLocalization()
//        addNoteTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
    }
    
    func setLocalization() {
//        addNoteTextField.placeholder = "add note textfield placeholder".localized
        addNoteTextView.delegate = self
        addNoteTextView.text = "add note textfield placeholder".localized
        addNoteTextView.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configureView() {
        if "Lang".localized == "en" {
            addNoteTextView.textAlignment = .left
//            addNoteTextField.textAlignment = .left
        } else if "Lang".localized == "ar" {
//            addNoteTextField.textAlignment = .right
            addNoteTextView.textAlignment = .right
        }
        closeKeypad()
    }
    
    func addNoteTextViewPressed(note: String) {
        if let delegateValue = addNoteTextViewDelegate {
            delegateValue.addNoteTextViewPressed(note: note)
        }
    }
    
    func addNoteTextFieldPressed(note: String) {
        if let delegateValue = addNoteDelegate {
            delegateValue.addNoteTextFieldPressed(note: note)
        }
    }
    
//    @objc func textFieldDidChange(_ textField: UITextField) {
//        addNoteTextFieldPressed(note: textField.text!)
//    }
    
    func closeKeypad() {
        let tapGesture = UILongPressGestureRecognizer(target: closeView, action: #selector(viewTapped))
        tapGesture.cancelsTouchesInView = false
        self.addGestureRecognizer(tapGesture)
    }

    @objc func viewTapped() {
//        addNoteTextField.endEditing(true)
//        addNoteTextView.endEditing(true)
        self.addNoteTextView.resignFirstResponder()
    }
}
