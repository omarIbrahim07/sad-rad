//
//  RateViewController+Delegates.swift
//  SadRad
//
//  Created by Omar Ibrahim on 9/14/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

extension RateViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view?.isDescendant(of: self.tableView) == true {
            return false
        } else {
            view.endEditing(true)
            return true
        }
    }
}
extension RateViewController: FeedBackTableViewCellDelegate {
    func didfeedBackViewRated(tag: Int, rate: Float) {
        self.viewModel.rateArray[tag] = String(rate)
        print(self.viewModel.rateArray)
    }
}

extension RateViewController: AddNoteTableViewCellDelegate {
    func addNoteTextFieldPressed(note: String) {
        print(note)
        self.viewModel.note = note
    }
}

extension RateViewController: AddNoteTextViewTableViewCellDelegate {
    func addNoteTextViewPressed(note: String) {
        print(note)
        self.viewModel.note = note
    }
}

extension RateViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 3 {
            if let cell: NoteRateTableViewCell = tableView.dequeueReusableCell(withIdentifier: "NoteRateTableViewCell") as? NoteRateTableViewCell {
                
                if "Lang".localized == "en" {
                    cell.ratingLabel.text = self.rateItemsTitleEn[indexPath.row]
                } else if "Lang".localized == "ar" {
                    cell.ratingLabel.text = self.rateItemsTitleAr[indexPath.row]
                }
//                cell.addNoteDelegate = self
                cell.addNoteTextViewDelegate = self
                return cell
            }
        }
        
        if let cell: RateTableViewCell = tableView.dequeueReusableCell(withIdentifier: "RateTableViewCell") as? RateTableViewCell {
            
//            cell.ratingLabel.text = feedbackItems[indexPath.row].name
//            cell.ratingView.tag = feedbackItems[indexPath.row].id
//            cell.viewModelsfeedbackItems = self.feedbackItems
            if "Lang".localized == "en" {
                cell.ratingLabel.text = self.rateItemsTitleEn[indexPath.row]
            } else if "Lang".localized == "ar" {
                cell.ratingLabel.text = self.rateItemsTitleAr[indexPath.row]
            }
            cell.ratingCosmosView.tag = indexPath.row
            cell.feedbackdelegate = self
            
            return cell
        }
        
        return UITableViewCell()
    }
}
