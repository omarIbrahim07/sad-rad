//
//  RateViewController.swift
//  SadRad
//
//  Created by Omar Ibrahim on 9/14/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class RateViewController: BaseViewController {
    
    lazy var viewModel: RateViewModel = {
        return RateViewModel()
    }()
    
    var error: APIError?
//    var rateItemsTitleEn: [String] = ["Trip", "Car", "Driver", "Transportation", "Note"]
//    var rateItemsTitleAr: [String] = ["الرحلة" ,"العربية" ,"السائق" ,"التنقُل" ,"ملحوظة"]
    var rateItemsTitleEn: [String] = ["Car", "Driver", "Transportation", "Note"]
    var rateItemsTitleAr: [String] = ["العربية" ,"السائق" ,"التنقُل" ,"ملحوظة"]

    @IBOutlet weak var tripRateLabel: UILabel!
    @IBOutlet weak var driverImageView: UIImageView!
    @IBOutlet weak var driverNameLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var confirmButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
        setLocalization()
        configureTableView()
        initVM()
    }
    
    func setLocalization() {
        tripRateLabel.text = "rate vc title".localized
    }
    
    // MARK:- Init View Model
    func initVM() {
                
        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }
            
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                    
                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                case .error:
                    self.error = self.viewModel.getError()
                    if let error = self.error {
//                        self.showError(error: error)
                        self.stopLoadingWithError(error: error)
                    }
                case .empty:
                    print("")
                }
            }
        }
                
        viewModel.rateIsSavedClosure = { [weak self] () in
            DispatchQueue.main.async { [weak self] in
                if let rateSaved: Bool = self?.viewModel.rateSaved {
                    if rateSaved == true {
                        print("Mesa2 el fol ll nas el kol")
                        print("7obbbbbbby")
                        self?.showRatePopUp(title: "rate trip alert title".localized, message: "rate trip alert message".localized, actionTitle: "done action title".localized)
                    } else {
                        print("5eeba")
                    }
                }
            }
        }
                
        viewModel.initRate()
    }

    
    func configureView() {
        navigationItem.title = "rate".localized
//        let tap = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
//        tap.delegate = self
//        view.addGestureRecognizer(tap)
    }
//
//    @objc func viewTapped() {
//        view.endEditing(true)
//    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "RateTableViewCell", bundle: nil), forCellReuseIdentifier: "RateTableViewCell")
        tableView.register(UINib(nibName: "NoteRateTableViewCell", bundle: nil), forCellReuseIdentifier: "NoteRateTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 15))
        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 15))
    }
    
    func showRatePopUp(title: String, message: String, actionTitle: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        //        let cancelAction = UIAlertAction(title: "إلغاء", style: .cancel, handler: nil)
        //        alertController.addAction(cancelAction)
        let openAction = UIAlertAction(title: actionTitle, style: .default) { (action) in
            self.presentMyHomeScreen()
        }
        //        let cancelAction = UIAlertAction(title: "cancel go to login".localized, style: .cancel)
        alertController.addAction(openAction)
        //        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }

    func rateTrip() {
        
//        guard let tripID: Int = self.viewModel.tripID, tripID > 0 else {
//            let apiError = APIError()
//            apiError.message = "برجاء إدخال رقم الرحلة"
//            self.viewModel.error = apiError
//            self.viewModel.state = .error
//            return
//        }
        
        guard let note: String = self.viewModel.note, note.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال ملاحظاتك"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
//        self.viewModel.rateTrip(tripID: tripID, reviewArray: self.viewModel.rateArray, note: note)
        if let tripID: Int = self.viewModel.tripID {
            self.viewModel.rateTrip(tripID: tripID, reviewArray: self.viewModel.rateArray, note: note)
        }
    }
        
    // MARK:- Actions
    @IBAction func confirmRateButtonIsPressed(_ sender: Any) {
        self.rateTrip()
    }
        
    // MARK: - Navigation
    

}
