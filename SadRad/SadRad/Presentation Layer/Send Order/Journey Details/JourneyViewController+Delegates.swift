//
//  JourneyViewController+Delegates.swift
//  SadRad
//
//  Created by Omar Ibrahim on 9/14/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit
import FSPagerView

extension JourneyViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.viewModel.tripDetailsCellViewModel?.notes != nil {
            return 6
        }
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell: JourneyDescriptionTableViewCell = tableView.dequeueReusableCell(withIdentifier: "JourneyDescriptionTableViewCell") as? JourneyDescriptionTableViewCell {
            
            cell.journeyDescriptionImageView.image = UIImage(named: self.tableViewImages[indexPath.row])
            
//            if indexPath.row == 0 {
//                cell.journeySecondValueLabel.text = "Negotiable"
//            }
            cell.journeyDescriptionCellViewModel = self.viewModel.getTripDetailsViewModel()
            cell.setIndex(index: indexPath.row)
            
            if self.viewModel.numberOfTripDetailsCells == 6 {
                if indexPath.row == 4 {
                    cell.bottomView.isHidden = false
                } else if indexPath.row == 5 {
                    cell.bottomView.isHidden = true
                }
            } else if self.viewModel.numberOfTripDetailsCells == 5 {
                if indexPath.row == 4 {
                    cell.bottomView.isHidden = true
                }
            }
            
            return cell
        }
        
        return UITableViewCell()
    }
}

extension JourneyViewController: FSPagerViewDataSource, FSPagerViewDelegate {
    
    public func numberOfItems(in pagerView: FSPagerView) -> Int {
        if self.viewModel.numberOfTripImagesCells > 0 {
            return self.viewModel.numberOfTripImagesCells
        }
        return 1
    }
    
    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "JourneyImagePagerViewCell", at: index) as! JourneyImagePagerViewCell
//        cell.viewModels = self.offerDetailsImages![index]
        if self.viewModel.numberOfTripImagesCells > 0 {
            let cellVM = viewModel.getTripImageCellViewModel(at: index)
            cell.tripImageCellViewModel = cellVM
        } else {
            cell.pagerViewImageView.image = UIImage(named: "broken")
        }
        
        return cell
    }
        
    // MARK:- FSPagerView Delegate
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        print("Hena")
        pagerView.deselectItem(at: index, animated: true)
        pagerView.scrollToItem(at: index, animated: true)
    }
    
    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
        print("Hena2")
        self.pageControlView.currentPage = targetIndex
//        self.currentIndex = targetIndex
    }
    
    func pagerViewDidEndScrollAnimation(_ pagerView: FSPagerView) {
        print("Hena3")
        self.pageControlView.currentPage = pagerView.currentIndex
//        self.currentIndex = pagerView.currentIndex
    }
    
//    @IBAction func sliderValueChanged(_ sender: UISlider) {
//        switch sender.tag {
//        case 1:
//            let newScale = 0.5+CGFloat(sender.value)*0.5 // [0.5 - 1.0]
//            self.pagerView.itemSize = self.pagerView.frame.size.applying(CGAffineTransform(scaleX: newScale, y: newScale))
//        case 2:
//            self.pagerView.interitemSpacing = CGFloat(sender.value) * 20 // [0 - 20]
//        case 3:
//            self.numberOfItems = Int(roundf(sender.value*7.0))
//            if let numOfPages: Int = self.viewModel.tripDetailsImageCellViewModels.count {
//                self.pageControlView.numberOfPages = numOfPages
//            }
//        default:
//            break
//        }
//        self.pagerView.reloadData()
//    }

}


