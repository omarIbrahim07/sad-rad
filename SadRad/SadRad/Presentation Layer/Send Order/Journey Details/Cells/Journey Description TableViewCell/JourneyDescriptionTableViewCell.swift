//
//  JourneyDescriptionTableViewCell.swift
//  SadRad
//
//  Created by Omar Ibrahim on 9/14/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class JourneyDescriptionTableViewCell: UITableViewCell {

    var journeyDescriptionCellViewModel: JourneyDetailsCellViewModel?
    
    @IBOutlet weak var journeyDescriptionImageView: UIImageView!
    @IBOutlet weak var journeyDescriptionValueLabel: UILabel!
    @IBOutlet weak var journeySecondValueLabel: UILabel!
    @IBOutlet weak var bottomView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setIndex(index: Int) {
        if index == 0 {
            if let negotiable: Bool = self.journeyDescriptionCellViewModel?.negotiable {
                if "Lang".localized == "en" {
                    if negotiable == false {
                        self.journeySecondValueLabel.text = "Not negotiable"
                    } else if negotiable == true {
                        self.journeySecondValueLabel.text = "Negotiable"
                    }
                } else if "Lang".localized == "ar" {
                    if negotiable == false {
                        self.journeySecondValueLabel.text = "غير قابل للنقاش"
                    } else if negotiable == true {
                        self.journeySecondValueLabel.text = "قابل للنقاش"
                    }
                }
            }
            if let price: Int = self.journeyDescriptionCellViewModel?.price {
                if "Lang".localized == "en" {
                    self.journeyDescriptionValueLabel.text = String(price) + " " + "EGP"
                } else if "Lang".localized == "ar" {
                    self.journeyDescriptionValueLabel.text = String(price) + " " + "ج.م"
                }
            }
        } else if index == 1 {
            if "Lang".localized == "en" {
//                if let fromGovernorate: String = self.journeyDescriptionCellViewModel?.fromGovernorate, let fromCity: String = self.journeyDescriptionCellViewModel?.fromCity {
//                    self.journeyDescriptionValueLabel.text = "From " + fromGovernorate + "," + " " + fromCity
                if let fromGovernorate: String = self.journeyDescriptionCellViewModel?.fromGovernorate {
                    self.journeyDescriptionValueLabel.text = "From " + fromGovernorate
                    if let fromCity: String = self.journeyDescriptionCellViewModel?.fromCity {
                        self.journeyDescriptionValueLabel.text = "From " + fromGovernorate + "," + " " + fromCity
                    }
                }
            } else if "Lang".localized == "ar" {
                if let fromGovernorate: String = self.journeyDescriptionCellViewModel?.fromGovernorate {
                    self.journeyDescriptionValueLabel.text = "من " + fromGovernorate
                    if let fromCity: String = self.journeyDescriptionCellViewModel?.fromCity {
                        self.journeyDescriptionValueLabel.text = "من " + fromGovernorate + "،" + " " + fromCity
                    }
                }
            }
        } else if index == 2 {
            if "Lang".localized == "en" {
                if let toGovernorate: String = self.journeyDescriptionCellViewModel?.toGovernorate {
                    self.journeyDescriptionValueLabel.text = "To " + toGovernorate
                    if let toCity: String = self.journeyDescriptionCellViewModel?.toCity {
                        self.journeyDescriptionValueLabel.text = "To " + toGovernorate + "," + " " + toCity
                    }
                }
            } else if "Lang".localized == "ar" {
                if let toGovernorate: String = self.journeyDescriptionCellViewModel?.toGovernorate {
                    self.journeyDescriptionValueLabel.text = "إلى " + toGovernorate
                    if let toCity: String = self.journeyDescriptionCellViewModel?.toCity {
                    self.journeyDescriptionValueLabel.text = "إلى " + toGovernorate + "،" + " " + toCity
                    }
                }
            }
        } else if index == 3 {
            if let typeCar: String = self.journeyDescriptionCellViewModel?.typeCar, let brand: String = self.journeyDescriptionCellViewModel?.brand, let model: String = self.journeyDescriptionCellViewModel?.model {
                if "Lang".localized == "en" {
                    self.journeyDescriptionValueLabel.text = brand + "," + " " + typeCar + "," + " " + model
                } else if "Lang".localized == "ar" {
                    self.journeyDescriptionValueLabel.text = brand + "،" + " " + typeCar + "،" + " " + model
                }
            }
        } else if index == 4 {
            if let conditionable: Bool = self.journeyDescriptionCellViewModel?.airConditionable {
                if "Lang".localized == "en" {
                    if conditionable == false {
                        self.journeyDescriptionValueLabel.text = "No aircondition"
                    } else if conditionable == true {
                        self.journeyDescriptionValueLabel.text = "Aircondition"
                    }
                } else if "Lang".localized == "ar" {
                    if conditionable == false {
                        self.journeyDescriptionValueLabel.text = "غير مكيف"
                    } else if conditionable == true {
                        self.journeyDescriptionValueLabel.text = "مكيف"
                    }
                }
            }
        } else if index == 5 {
            if let notes: String = self.journeyDescriptionCellViewModel?.notes {
                self.journeyDescriptionValueLabel.text = notes
            }
        }
    }
    
}
