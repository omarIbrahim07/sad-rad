//
//  JourneyImagePagerViewCell.swift
//  SadRad
//
//  Created by Omar Ibrahim on 9/14/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit
import FSPagerView

class JourneyImagePagerViewCell: FSPagerViewCell {
    
    var tripImageCellViewModel : TripImageDetailsCellViewModel? {
        didSet {
            if let image = self.tripImageCellViewModel?.image {
                pagerViewImageView.loadImageFromUrl(imageUrl: MY_CARS_IMAGES_URL + image)
            }
        }
    }
    
    @IBOutlet weak var pagerViewImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
