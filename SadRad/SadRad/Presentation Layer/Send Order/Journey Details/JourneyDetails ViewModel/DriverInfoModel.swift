//
//  DriverInfoModel.swift
//  SadRad
//
//  Created by Omar Ibrahim on 10/26/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation

struct DriverInfoModel {
    let id: Int?
    let image: String?
    let name: String?
    let rating: Float?
    let phoneNumber: String?
}
