//
//  JourneyDetailsViewModel.swift
//  SadRad
//
//  Created by Omar Ibrahim on 10/26/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation

class JourneyDetailsViewModel {
    
    var tripID: Int?
    var carID: Int?
    var driverID: Int?
    var tripDetails: MyJourney?
    var newTripDetails: NewTripInfo?
    var driverPhoneNumber: String?
    
    var numberOfTripDetailsCells: Int? = 5
    
    var error: APIError?
    
    var tripDetailsCellViewModel: JourneyDetailsCellViewModel? {
        didSet {
            self.reloadTableViewClosure?()
        }
    }
    
    var tripDetailsImageCellViewModels: [TripImageDetailsCellViewModel] = [TripImageDetailsCellViewModel]() {
        didSet {
            self.reloadPagerViewClosure?()
        }
    }

    var driverInfoViewModel: DriverInfoModel? {
        didSet {
            self.updateDriverInfoClosure?()
        }
    }
    
    var numberOfTripImagesCells: Int {
        return tripDetailsImageCellViewModels.count
    }

    var reloadTableViewClosure: (()->())?
    var updateDriverInfoClosure: (()->())?
    var reloadPagerViewClosure: (()->())?
    var updateLoadingStatus: (()->())?
    var tripIsReservedClosure: (()->())?
    
    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    var isReserved: Bool = false {
        didSet {
            self.tripIsReservedClosure?()
        }
    }
    
    //    var numberOfCarImagesPagerViewCell: Int {
    //        return myCarCellViewModels.count
    //    }
    
//    func initFetch() {
//        state = .loading
//
//        let params: [String : AnyObject] = [
//            "trip_id" : tripID as AnyObject
//        ]
//
//        AddJourneyAPIManager().getMyJourneyInfo(basicDictionary: params, onSuccess: { (tripDetails) in
//
//            //            self.processMyCars(myCars: myCars)
//            self.tripDetails = tripDetails
//            if let carID = self.tripDetails?.myCar?.id {
//                self.carID = carID
//            }
//            if let driverID = self.tripDetails?.driverInfo?.id {
//                self.driverID = driverID
//            }
//            self.tripDetailsCellViewModel = self.createMyCarCellViewModel(tripDetails: tripDetails)
//            if tripDetails.myCar?.images?.count ?? 0 > 0 {
//                self.processTripImages(tripsImages: (tripDetails.myCar?.images!)!)
//            }
//            self.driverInfoViewModel = self.createDriverViewModel(tripDetails: tripDetails)
//
//            self.state = .populated
//
//        }) { (error) in
//            self.error = error
//            self.state = .error
//        }
//    }
    
    func initFetch() {
        state = .loading
        
        let params: [String : AnyObject] = [
            "trip_id" : tripID as AnyObject
        ]
        
        AddJourneyAPIManager().getNewTripDetails(basicDictionary: params, onSuccess: { (tripDetails) in
            
            //            self.processMyCars(myCars: myCars)
            self.newTripDetails = tripDetails
            if let carID = self.newTripDetails?.carID {
                self.carID = carID
            }
            if let driverID = self.newTripDetails?.driverID {
                self.driverID = driverID
            }
            self.tripDetailsCellViewModel = self.createMyCarCellViewModel(tripDetails: tripDetails)
            if self.newTripDetails?.tripImages?.count ?? 0 > 0 {
                self.processTripImages(tripsImages: (self.newTripDetails?.tripImages!)!)
            }
            self.driverInfoViewModel = self.createDriverViewModel(tripDetails: tripDetails)
            
            self.state = .populated
            
        }) { (error) in
            self.error = error
            self.state = .error
        }
    }

    
    func reserveTrip() {
                
        let params: [String : AnyObject] = [
            "trip_id" : tripID as AnyObject,
            "car_id" : carID as AnyObject,
        ]
        
        self.state = .loading
        
        SearchAPIManager().reserveTrip(basicDictionary: params, onSuccess: { (saved) in
            if saved == true {
                self.state = .populated
                self.isReserved = true
            } else {
                self.state = .empty
            }
        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    
    func processTripImages( tripsImages: [String] ) {
        //        self.myCars = myCars // Cache
        var vms = [TripImageDetailsCellViewModel]()
        for tripImage in tripsImages {
            vms.append( createTripImageCellViewModel(tripImage: tripImage))
        }
        self.tripDetailsImageCellViewModels = vms
    }
    
    func createTripImageCellViewModel( tripImage: String ) -> TripImageDetailsCellViewModel {
        return TripImageDetailsCellViewModel(image: tripImage)
    }
    
//    func createMyCarCellViewModel( tripDetails: MyJourney ) -> JourneyDetailsCellViewModel {
    func createMyCarCellViewModel( tripDetails: NewTripInfo ) -> JourneyDetailsCellViewModel {
        if "Lang".localized == "en" {
            return JourneyDetailsCellViewModel(price: tripDetails.price, negotiable: tripDetails.negotialPrice ?? false, fromGovernorate: tripDetails.pickupGovernorate?.nameEn, fromCity: tripDetails.pickupCity?.nameEn, toGovernorate: tripDetails.dropoffGovernorate?.nameEn, toCity: tripDetails.dropoffCity?.nameEn, typeCar: tripDetails.carType?.nameEn, brand: tripDetails.carBrand?.nameEn, model: tripDetails.carModel?.nameEn, airConditionable: tripDetails.airCondition ?? false, notes: tripDetails.notes)
//            return JourneyDetailsCellViewModel(price: tripDetails.price ?? 0, negotiable: tripDetails.negotiablePriceBool ?? false, fromGovernorate: tripDetails.packupGovernorate?.nameEn ?? "", fromCity: tripDetails.packupCity?.nameEn ?? "", toGovernorate: tripDetails.dropOffGovernorate?.nameEn ?? "", toCity: tripDetails.dropOffCity?.nameEn ?? "", typeCar: tripDetails.myCar?.carType?.nameEn ?? "", brand: tripDetails.myCar?.carBrand?.nameEn ?? "", model: tripDetails.myCar?.carModel?.nameEn ?? "", airConditionable: tripDetails.myCar?.airConditioned ?? false)
        } else if "Lang".localized == "ar" {
            return JourneyDetailsCellViewModel(price: tripDetails.price, negotiable: tripDetails.negotialPrice ?? false, fromGovernorate: tripDetails.pickupGovernorate?.nameAr, fromCity: tripDetails.pickupCity?.nameAr, toGovernorate: tripDetails.dropoffGovernorate?.nameAr, toCity: tripDetails.dropoffCity?.nameAr, typeCar: tripDetails.carType?.nameAr, brand: tripDetails.carBrand?.nameAr, model: tripDetails.carModel?.nameAr, airConditionable: tripDetails.airCondition ?? false, notes: tripDetails.notes)
//            return JourneyDetailsCellViewModel(price: tripDetails.price ?? 0, negotiable: tripDetails.negotiablePriceBool ?? false, fromGovernorate: tripDetails.packupGovernorate?.nameAr ?? "", fromCity: tripDetails.packupCity?.nameAr ?? "", toGovernorate: tripDetails.dropOffGovernorate?.nameAr ?? "", toCity: tripDetails.dropOffCity?.nameAr ?? "", typeCar: tripDetails.myCar?.carType?.nameAr ?? "", brand: tripDetails.myCar?.carBrand?.nameAr ?? "", model: tripDetails.myCar?.carModel?.nameAr ?? "", airConditionable: tripDetails.myCar?.airConditioned ?? false)
        } else {
//            return JourneyDetailsCellViewModel(price: tripDetails.price ?? 0, negotiable: tripDetails.negotiablePriceBool ?? false, fromGovernorate: tripDetails.packupGovernorate?.nameEn ?? "", fromCity: tripDetails.packupCity?.nameEn ?? "", toGovernorate: tripDetails.dropOffGovernorate?.nameEn ?? "", toCity: tripDetails.dropOffCity?.nameEn ?? "", typeCar: tripDetails.myCar?.carType?.nameEn ?? "", brand: tripDetails.myCar?.carBrand?.nameEn ?? "", model: tripDetails.myCar?.carModel?.nameEn ?? "", airConditionable: tripDetails.myCar?.airConditioned ?? false)
            return JourneyDetailsCellViewModel(price: tripDetails.price, negotiable: tripDetails.negotialPrice ?? false, fromGovernorate: tripDetails.pickupGovernorate?.nameEn, fromCity: tripDetails.pickupCity?.nameEn, toGovernorate: tripDetails.dropoffGovernorate?.nameEn, toCity: tripDetails.dropoffCity?.nameEn, typeCar: tripDetails.carType?.nameEn, brand: tripDetails.carBrand?.nameEn, model: tripDetails.carModel?.nameEn, airConditionable: tripDetails.airCondition ?? false, notes: tripDetails.notes)
        }
    }
    
//    func createDriverViewModel( tripDetails: MyJourney ) -> DriverInfoModel {
    func createDriverViewModel( tripDetails: NewTripInfo ) -> DriverInfoModel {
        return DriverInfoModel(id: tripDetails.driverID, image: tripDetails.driverImage, name: tripDetails.driverName, rating: tripDetails.driverRating, phoneNumber: tripDetails.driverPhoneNumber)
//        return DriverInfoModel(id: tripDetails.driverInfo?.id, image: tripDetails.driverInfo?.image, name: tripDetails.driverInfo?.name, rating: tripDetails.driverInfo?.rating, phoneNumber: tripDetails.driverInfo?.phoneNumber)
    }
    
    func getError() -> APIError {
        return self.error!
    }
    
    func getTripDetailsViewModel() -> JourneyDetailsCellViewModel? {
        if self.tripDetailsCellViewModel?.notes != nil {
            self.numberOfTripDetailsCells = 6
        }
        return tripDetailsCellViewModel
    }
    
    func getTripImageCellViewModel( at indexPath: Int ) -> TripImageDetailsCellViewModel {
        return tripDetailsImageCellViewModels[indexPath]
    }

    func getDriverInfoViewModel() -> DriverInfoModel? {
        return driverInfoViewModel
    }
    
    //    func myCarPressed( at indexPath: Int ){
    //        let myCar = self.myCars[indexPath]
    //
    //        self.selectedCar = myCar
    //    }
    
}
