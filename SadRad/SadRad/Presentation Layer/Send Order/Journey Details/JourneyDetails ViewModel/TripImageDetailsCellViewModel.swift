//
//  TripImageDetailsCellViewModel.swift
//  SadRad
//
//  Created by Omar Ibrahim on 11/22/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation

struct TripImageDetailsCellViewModel {
    let image: String?
}
