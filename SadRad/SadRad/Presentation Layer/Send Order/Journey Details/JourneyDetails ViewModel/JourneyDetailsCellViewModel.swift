//
//  JourneyDetailsCellViewModel.swift
//  SadRad
//
//  Created by Omar Ibrahim on 10/26/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation

struct JourneyDetailsCellViewModel {
    let price: Int?
    let negotiable: Bool?
    let fromGovernorate: String?
    let fromCity: String?
    let toGovernorate: String?
    let toCity: String?
    let typeCar: String?
    let brand: String?
    let model: String?
    let airConditionable: Bool?
    let notes: String?
}
