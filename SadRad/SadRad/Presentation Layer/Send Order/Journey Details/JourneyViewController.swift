//
//  JourneyViewController.swift
//  SadRad
//
//  Created by Omar Ibrahim on 9/14/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit
import FSPagerView
import Cosmos
import GoogleMobileAds

class JourneyViewController: BaseViewController {
    
    private let banner: GADBannerView = {
        let banner = GADBannerView()
        banner.adUnitID = GoogleBannerUnitID
        banner.load(GADRequest())
        return banner
    }()
    
    lazy var viewModel: JourneyDetailsViewModel = {
        return JourneyDetailsViewModel()
    }()
    
    var error: APIError?
    
    var numberOfItems : Int? = 0
    var currentIndex: Int? = 0
    
    var tableViewImages = ["Icon ionic-ios-wallet", "Icon material-my-location", "Icon metro-location", "Icon awesome-car-side", "Icon awesome-snowflake", "Icon material-speaker-notes"]
    
    @IBOutlet weak var advertisementView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var driverImageView: UIImageView!
    @IBOutlet weak var driverNameLAbel: UILabel!
    @IBOutlet weak var driverRateCosmosView: CosmosView!
    @IBOutlet weak var orderNowButton: UIButton!
    @IBOutlet weak var callView: UIView!
    @IBOutlet weak var callButton: UIButton!
    @IBOutlet weak var pagerView: FSPagerView! {
        didSet {
            self.pagerView.register(UINib(nibName: "JourneyImagePagerViewCell", bundle: nil), forCellWithReuseIdentifier: "JourneyImagePagerViewCell")
            self.pagerView.itemSize = FSPagerView.automaticSize
        }
    }
    @IBOutlet weak var pageControlView: FSPageControl! {
        didSet {
            self.pageControlView.numberOfPages = numberOfItems ?? 0
            self.pageControlView.contentHorizontalAlignment = .center
            self.pageControlView.contentInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
            self.pageControlView.setFillColor(#colorLiteral(red: 0.2274509804, green: 0.2784313725, blue: 0.3490196078, alpha: 1), for: .selected)
            self.pageControlView.setFillColor(#colorLiteral(red: 0.3607843137, green: 0.7764705882, blue: 0.7294117647, alpha: 1), for: .normal)
            //                self.pageControlView.setStrokeColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
            //            pageControlView.setImage(UIImage(named:"AppIcon"), for: .selected)
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        banner.rootViewController = self
        advertisementView.addSubview(banner)
        setNavigationTitle()
        setLocalization()
        configureView()
        configureFSPageControl()
        configureTableView()
        initVM()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        banner.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 80).integral
    }
        
    func setNavigationTitle() {
        navigationItem.title = "trip details vc title".localized
    }

    func setLocalization() {
        orderNowButton.setTitle("order now button title".localized, for: .normal)
    }
    
    func initVM() {
        
        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }
            
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                    
                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                case .error:
                    self.error = self.viewModel.getError()
                    if let error = self.error {
                        self.stopLoadingWithError(error: error)
                    }
                case .empty:
                    print("")
                }
            }
        }
        
        viewModel.reloadTableViewClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
        
        viewModel.reloadPagerViewClosure = { [weak self] () in
            DispatchQueue.main.async {
                if let images = self?.viewModel.numberOfTripImagesCells {
                    self!.pageControlView.numberOfPages = images
                }
                self?.pagerView.reloadData()
                self?.pageControlView.reloadInputViews()
            }
        }
        
//        viewModel.updateImages = { [weak self] () in
//            DispatchQueue.main.async { [weak self] () in
//                self?.offerDetailsImages = self!.viewModel.offerDetailsImages
//                //                print(self?.offerDetailsImages)
//                //                self?.tableView.reloadData()
//                if let images = self!.offerDetailsImages {
//                    self!.pageControlView.numberOfPages = images.count
//                    self?.pagerView.reloadData()
//                    self!.pagerView.transformer = FSPagerViewTransformer(type: .cubic)
//                    self!.pagerView.automaticSlidingInterval = 5.0
//                }
//                //                        self.pageControl.numberOfPages = 1
//                self!.pageControlView.contentHorizontalAlignment = .right
//                self!.pageControlView.contentInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
//            }
//        }
                
        viewModel.updateDriverInfoClosure = { [weak self] () in
            DispatchQueue.main.async {
                if let driverInfoViewModel: DriverInfoModel = self?.viewModel.getDriverInfoViewModel() {
                    self?.bindDriverData(driverInfoModel: driverInfoViewModel)
                }
            }
        }
        
        viewModel.tripIsReservedClosure = { [weak self] () in
            DispatchQueue.main.async {
                if let reserved: Bool = self?.viewModel.isReserved {
                    if reserved == true {
                        self?.goToReserved()
                    } else {
                        print("A7la m3l4")
                    }
                }
            }
        }
                
        viewModel.initFetch()
    }
    
    func configureView() {
        callView.addCornerRadius(raduis: callView.frame.height / 2, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        orderNowButton.addCornerRadius(raduis: orderNowButton.frame.height / 2, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        driverImageView.addCornerRadius(raduis: driverImageView.frame.height / 2, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "JourneyDescriptionTableViewCell", bundle: nil), forCellReuseIdentifier: "JourneyDescriptionTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 10))
    }
    
    func configureFSPageControl() {
        pagerView.delegate = self
        pagerView.dataSource = self
        pagerView.reloadData()
    }
        
    func bindDriverData(driverInfoModel: DriverInfoModel) {
        if let driverID: Int = driverInfoModel.id {
            self.viewModel.driverID = driverID
        }
        if let image: String = driverInfoModel.image {
            self.driverImageView.loadImageFromUrl(imageUrl: PROFILE_IMAGE_URL + image)
        }
        if let name: String = driverInfoModel.name {
            self.driverNameLAbel.text = name
        }
        if let phoneNumber: String = driverInfoModel.phoneNumber {
            self.viewModel.driverPhoneNumber = phoneNumber
        }
        if let rating = driverInfoModel.rating {
            driverRateCosmosView.rating = Double(rating)
        }
    }
    
    func showCallAlert(phoneNumber: String?) {
        
        guard let userPhone = phoneNumber, let url = URL(string: "telprompt://\(userPhone)") else {
            return
        }
        
        // create the alert
        let alert = UIAlertController(title: "show call alert title".localized, message: userPhone, preferredStyle: UIAlertController.Style.alert)
        // add the actions (buttons)
        alert.addAction(UIAlertAction(title: "show call alert action".localized, style: UIAlertAction.Style.destructive, handler: { action in
            UIApplication.shared.canOpenURL(url)
        }))
        alert.addAction(UIAlertAction(title: "cancel show call alert".localized, style: UIAlertAction.Style.cancel, handler: nil))
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    func makeCantReserveAlert() {
        let openAction = UIAlertAction(title: "الذهاب لتسجيل الدخول".localized, style: .default) { (action) in
            self.goToLogInn()
        }
        let cancelAction = UIAlertAction(title: "إغلاق".localized, style: .cancel)
        self.showAlert(title: "حجز الرحلة".localized, message: "لا يمكنك حجز الرحلة بدون تسجيل الدخول".localized, alertActions: [openAction, cancelAction])
    }
    
    func makeCantPhoneAlert() {
        let openAction = UIAlertAction(title: "الذهاب لتسجيل الدخول".localized, style: .default) { (action) in
            self.goToLogInn()
        }
        let cancelAction = UIAlertAction(title: "إغلاق".localized, style: .cancel)
        self.showAlert(title: "الاتصال بسائق الرحلة".localized, message: "لا يمكنك الاتصال بدون تسجيل الدخول".localized, alertActions: [openAction, cancelAction])
    }
                
    // MARK: - Navigation
    func goToReserved() {
        if let reservedVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ReservedViewController") as? ReservedViewController {
            //                rootViewContoller.test = "test String"
            self.present(reservedVC, animated: true, completion: nil)
            print("Reserved")
        }
    }
    
    func goToDriverProfile(driverID: Int) {
        if let driverProfileVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RatesViewController") as? RatesViewController {
            //                rootViewContoller.test = "test String"
            driverProfileVC.viewModel.isDriverID = true
            driverProfileVC.viewModel.driverID = driverID
            self.present(driverProfileVC, animated: true, completion: nil)
            print("Driver profile")
        }
    }

    // MARK:- Actions
    @IBAction func orderNowButtonIsPressed(_ sender: Any) {
        guard let id: Int = UserDefaultManager.shared.currentUser?.id, id > 0 else {
            self.makeCantReserveAlert()
            return
        }
        self.viewModel.reserveTrip()
    }
    
    @IBAction func callButtonIsPressed(_ sender: Any) {
        guard let id: Int = UserDefaultManager.shared.currentUser?.id, id > 0 else {
            self.makeCantPhoneAlert()
            return
        }
        showCallAlert(phoneNumber: self.viewModel.driverPhoneNumber)
    }
    
    @IBAction func driverProfileButtonIsPressed(_ sender: Any) {
        print("Driver profile button is pressed")
        if let driverID: Int = self.viewModel.driverID {
            self.goToDriverProfile(driverID: driverID)
        }
    }
}
