//
//  MyJourniesViewController+Delegates.swift
//  SadRad
//
//  Created by Omar Ibrahim on 9/9/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

extension MyJourniesViewController: JourneyButtonsDelegate {
    func callClientButtonIsPressed(clientPhone: String) {
        print(clientPhone)
        self.showCallAlert(phoneNumber: clientPhone)
    }
    
    func srartTripButtonIsPressed(journeyID: Int) {
        print(journeyID)
        self.viewModel.startTrip(journeyID: journeyID)
    }
    
    func editJourneyButtonIsPressed(journeyID: Int) {
        print(journeyID)
        self.goToEditJourney(journeyID: journeyID)
    }
    
    func deleteJourneyButtonIsPressed(journeyID: Int) {
        self.deleteMyTripAlert(tripID: journeyID, title: "delete my trip title".localized, message: "delete my trip message".localized)
//        self.viewModel.deleteMyJourney(journeyID: journeyID)
    }
    
    func doneJourneyButtonIsPressed(journeyID: Int) {
        self.viewModel.myJourneyIsDone(journeyID: journeyID)
    }
}

extension MyJourniesViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfMyJourniesCells
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell: MyJourneyTableViewCell = tableView.dequeueReusableCell(withIdentifier: "MyJourneyTableViewCell") as? MyJourneyTableViewCell {
            
            let cellVM = viewModel.getMyJourneyCellViewModel( at: indexPath )
            cell.myJourneyCellViewModel = cellVM
            cell.journeyButtonsDelegate = self

            return cell
        }
        return UITableViewCell()
    }
}
