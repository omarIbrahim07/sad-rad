//
//  MyJourniesViewController.swift
//  SadRad
//
//  Created by Omar Ibrahim on 9/9/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class MyJourniesViewController: BaseViewController {
        
    lazy var viewModel: MyJourniesViewModel = {
        return MyJourniesViewModel()
    }()
    
    var error: APIError?
    
    @IBOutlet weak var filterView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noTripsLabel: UILabel!
    @IBOutlet weak var filterLabel: UILabel!
    
    override func viewWillAppear(_ animated: Bool) {
        initVM()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setNavigationTitle()
        setMyJourneysLocalization()
        configureView()
        configureTableView()
        initVM()
    }
    
    func setNavigationTitle() {
        navigationItem.title = "my trips vc title".localized
    }
    
    func setMyJourneysLocalization() {
        
    }

    func initVM() {
        
        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }
            
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                    
                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                case .error:
                    self.error = self.viewModel.getError()
                    if let error = self.error {
                        self.stopLoadingWithError(error: error)
                    }
                case .empty:
                    print("")
                }
            }
        }
        
        viewModel.reloadTableViewClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
        
        viewModel.doneMyJourneyClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.showDoneMyJournyPopUp(title: "done journey alert title".localized, message: "done journey alert message".localized, actionTitle: "done action title".localized)
            }
        }
        
        viewModel.deleteMyJourneyClosure = { [weak self] () in
            DispatchQueue.main.async {
                if let deleted: Bool = self?.viewModel.getDeletedBool() {
                    if deleted == true {
                        self?.showDeleteMyJournyPopUp(title: "delete trip alert title".localized, message: "delete trip alert message".localized, actionTitle: "done action title".localized)
                    }
                }
            }
        }
        
        viewModel.startMyJourneyClosure = { [weak self] () in
            DispatchQueue.main.async {
                if let tripIsStarted: String = self?.viewModel.tripIsStarted {
                    if tripIsStarted == "start Trip" {
                        self?.tableView.reloadData()
                    }
                }
            }
        }
        
        viewModel.filterStateClosure = { [weak self] () in
            guard let self = self else {
                return
            }

            if let choosedFilter = self.viewModel.getChoossedFilterTrip() {
                self.bindFilterTrip(choosedFilterTrip: choosedFilter)
            }
        }
        
        viewModel.tripsExistanceStateClosure = { [weak self] () in
            guard let self = self else {
                return
            }
            
            switch self.viewModel.tripsExistance {
                
            case .noTripsExisted:
                self.noTripsLabel.isHidden = false
                self.tableView.isHidden = true
            case .tripsExisted:
                self.noTripsLabel.isHidden = true
                self.tableView.isHidden = false
            }
        }
        
        viewModel.choosedFilterTrip = viewModel.filterTripStatus[0]
        viewModel.initFetch(status: self.viewModel.choosedFilterTrip?.id ?? 0)
    }
    
    func bindFilterTrip(choosedFilterTrip: TripFilter) {
        if let choosedFilterTripID: Int = choosedFilterTrip.id {
            self.viewModel.initFetch(status: choosedFilterTripID)
            if "Lang".localized == "en" {
                self.filterLabel.text = self.viewModel.filterTripStatus[choosedFilterTripID].nameEn
            } else if "Lang".localized == "ar" {
                self.filterLabel.text = self.viewModel.filterTripStatus[choosedFilterTripID].nameAr
            }
        }
    }
    
    func setupTripFilter(tripFilters: [TripFilter]) {
        
        let sheet = UIAlertController(title: "trip filter alert title".localized, message: "trip filter alert message".localized, preferredStyle: .actionSheet)
        for tripFilter in tripFilters {
            if "Lang".localized == "ar" {
                sheet.addAction(UIAlertAction(title: tripFilter.nameAr, style: .default, handler: {_ in
                    self.viewModel.userChoossedFilterTrip(filterTrip: tripFilter)
                }))
            } else if "Lang".localized == "en" {
                sheet.addAction(UIAlertAction(title: tripFilter.nameEn, style: .default, handler: {_ in
                    self.viewModel.userChoossedFilterTrip(filterTrip: tripFilter)
                }))
            }
        }
        sheet.addAction(UIAlertAction(title: "cancel trip filter".localized, style: .cancel, handler: nil))
        
        //        sheet.popoverPresentationController?.sourceView = sortChoiceLabel
        
        self.present(sheet, animated: true, completion: nil)
    }
    
    func showDeleteMyJournyPopUp(title: String, message: String, actionTitle: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let openAction = UIAlertAction(title: actionTitle, style: .default) { (action) in
            
            if let choosedFilter = self.viewModel.choosedFilterTrip?.id {
                self.viewModel.initFetch(status: choosedFilter)
            }
        }
        //        let cancelAction = UIAlertAction(title: "cancel delete car".localized, style: .cancel)
        alertController.addAction(openAction)
        //        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }

    func showDoneMyJournyPopUp(title: String, message: String, actionTitle: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let openAction = UIAlertAction(title: actionTitle, style: .default) { (action) in
            self.presentMyHomeScreen()
        }
        //        let cancelAction = UIAlertAction(title: "cancel delete car".localized, style: .cancel)
        alertController.addAction(openAction)
        //        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }

    func configureView() {
        filterView.addCornerRadius(raduis: 12, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        noTripsLabel.text = "no trips label".localized
        noTripsLabel.isHidden = true
    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "MyJourneyTableViewCell", bundle: nil), forCellReuseIdentifier: "MyJourneyTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 20))
        tableView.tableFooterView = UIView()
    }
    
    func showCallAlert(phoneNumber: String?) {
        
        guard let userPhone = phoneNumber, let url = URL(string: "telprompt://\(userPhone)") else {
            return
        }
        
        // create the alert
        let alert = UIAlertController(title: "show call alert title".localized, message: userPhone, preferredStyle: UIAlertController.Style.alert)
        // add the actions (buttons)
        alert.addAction(UIAlertAction(title: "show call alert action".localized, style: UIAlertAction.Style.destructive, handler: { action in
            UIApplication.shared.canOpenURL(url)
        }))
        alert.addAction(UIAlertAction(title: "cancel show call alert".localized, style: UIAlertAction.Style.cancel, handler: nil))
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    func deleteMyTripAlert(tripID: Int, title: String, message: String) {
        let deleteMyTripAction = UIAlertAction(title: "confirm delete my trip action title".localized, style: .default) { (action) in
            self.viewModel.deleteMyJourney(journeyID: tripID)
        }
        let cancelAction = UIAlertAction(title: "cancel delete my trip action title".localized, style: .cancel)
        self.showAlert(title: title, message: message, alertActions: [deleteMyTripAction, cancelAction])
    }
    
    @IBAction func filterButtonIsPressed(_ sender: Any) {
        setupTripFilter(tripFilters: self.viewModel.filterTripStatus)
    }
    
    // MARK: - Navigation
    func goToEditJourney(journeyID: Int) {
        if let addJourneyVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AddJourneyViewController") as? AddJourneyViewController {
            //                rootViewContoller.test = "test String"
            addJourneyVC.editBool = true
            addJourneyVC.journeyID = journeyID
            self.navigationController?.pushViewController(addJourneyVC, animated: true)
//            self.present(addJourneyVC, animated: true, completion: nil)
            print("Add Journey")
        }
    }

}
