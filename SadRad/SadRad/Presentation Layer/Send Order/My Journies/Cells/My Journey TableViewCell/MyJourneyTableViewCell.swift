//
//  MyJourneyTableViewCell.swift
//  SadRad
//
//  Created by Omar Ibrahim on 9/9/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

protocol JourneyButtonsDelegate {
    func srartTripButtonIsPressed(journeyID: Int)
    func editJourneyButtonIsPressed(journeyID: Int)
    func deleteJourneyButtonIsPressed(journeyID: Int)
    func doneJourneyButtonIsPressed(journeyID: Int)
    func callClientButtonIsPressed(clientPhone: String)
}

class MyJourneyTableViewCell: UITableViewCell {
    
    var journeyButtonsDelegate: JourneyButtonsDelegate?
    
    var myJourneyCellViewModel : MyJourneyCellViewModel? {
        didSet {
            if let journeyID: Int = self.myJourneyCellViewModel?.id {
                self.tag = journeyID
                self.editButton.tag = journeyID
            }
            
            if let tripIsAboutToStart: Int = self.myJourneyCellViewModel?.tripIsAboutStart {
                if tripIsAboutToStart == 1 {
                    editLabel.text = "trip is started title".localized
                    editImageView.isHidden = true
//                    editButton.isUserInteractionEnabled = false
                }
            }
            
            if let status: Int = self.myJourneyCellViewModel?.status {
                if status == 0 {
                    editLabel.text = "edit trip button title".localized
                    removeLabel.text = "delete trip button title".localized
                    removeImageView.image = UIImage(named: "Icon material-delete")
                    editImageView.image = UIImage(named: "Icon material-edit")
                    editImageView.isHidden = false
                    removeImageView.isHidden = false
                    editButton.isUserInteractionEnabled = true
                    removeButton.isUserInteractionEnabled = true
                    editView.backgroundColor = #colorLiteral(red: 0.3607843137, green: 0.7764705882, blue: 0.7294117647, alpha: 1)
                    removeView.backgroundColor = #colorLiteral(red: 0.3607843137, green: 0.7764705882, blue: 0.7294117647, alpha: 1)
                }
                if status == 6 {
                    editLabel.text = "trip booked button title".localized
                    removeLabel.text = "trip is started title".localized
                    editImageView.isHidden = false
                    removeImageView.isHidden = true
                    editImageView.image = UIImage(named: "ic_done_all_24px")
                    editButton.isUserInteractionEnabled = false
                    removeButton.isUserInteractionEnabled = true
                    editView.backgroundColor = #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1)
                    removeView.backgroundColor = #colorLiteral(red: 0.3607843137, green: 0.7764705882, blue: 0.7294117647, alpha: 1)
                } else if status == 1 {
                    if let clientName: String = self.myJourneyCellViewModel?.clientName {
                        editLabel.text = clientName
                    }
                    removeLabel.text = "trip done title".localized
                    editImageView.isHidden = false
                    removeImageView.isHidden = true
                    editImageView.image = UIImage(named: "Icon feather-phone")
                    editButton.isUserInteractionEnabled = true
                    removeButton.isUserInteractionEnabled = true
                    editView.backgroundColor = #colorLiteral(red: 0.3607843137, green: 0.7764705882, blue: 0.7294117647, alpha: 1)
                    removeView.backgroundColor = #colorLiteral(red: 0.3607843137, green: 0.7764705882, blue: 0.7294117647, alpha: 1)
                } else if status == 2 {
                    editLabel.text = "trip booked button title".localized
                    removeLabel.text = "trip finish title".localized
                    editImageView.isHidden = false
                    removeImageView.isHidden = true
                    editImageView.image = UIImage(named: "ic_done_all_24px")
                    editButton.isUserInteractionEnabled = false
                    removeButton.isUserInteractionEnabled = false
                    editView.backgroundColor = #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1)
                    removeView.backgroundColor = #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1)
                }
            }
            
            if self.myJourneyCellViewModel?.myCar?.images?.count ?? 0 > 0 {
                let carImage = self.myJourneyCellViewModel?.myCar?.images?[0]
                myCarImageView.loadImageFromUrl(imageUrl: MY_CARS_IMAGES_URL + carImage!)
            } else {
                myCarImageView.image = UIImage(named: "broken")
            }
            
            if "Lang".localized == "en" {
                if let fromGovernorate: String = self.myJourneyCellViewModel?.packupGovernorate?.nameEn, let fromCity: String = self.myJourneyCellViewModel?.packupCity?.nameEn {
                    self.fromValueLabel.text = "From " + fromGovernorate + ", " + fromCity
                }
                if let toGovernorate: String = self.myJourneyCellViewModel?.dropOffGovernorate?.nameEn, let toCity: String = self.myJourneyCellViewModel?.dropOffCity?.nameEn {
                    self.toValueLabel.text = "To " + toGovernorate + ", " + toCity
                }
                if let brand: String = self.myJourneyCellViewModel?.myCar?.carBrand?.nameEn, let model: String = self.myJourneyCellViewModel?.myCar?.carModel?.nameEn {
                    self.carBrandLabel.text = brand + " " + model
                }
//                if let numberOfPersons: Int = self.myJourneyCellViewModel?.myCar?.numberOfPersons {
//                    self.personsValueLabel.text = String(numberOfPersons) + " Persons"
//                }
            } else if "Lang".localized == "ar" {
                if let fromGovernorate: String = self.myJourneyCellViewModel?.packupGovernorate?.nameAr {
                    self.fromValueLabel.text = "من " + fromGovernorate
                }
                if let toGovernorate: String = self.myJourneyCellViewModel?.dropOffGovernorate?.nameAr {
                    self.toValueLabel.text = "إلى " + toGovernorate
                }
                if let brand: String = self.myJourneyCellViewModel?.myCar?.carBrand?.nameAr, let model: String = self.myJourneyCellViewModel?.myCar?.carModel?.nameAr {
                    self.carBrandLabel.text = brand + " " + model
                }
//                if let numberOfPersons: Int = self.myJourneyCellViewModel?.myCar?.numberOfPersons {
//                    self.personsValueLabel.text = String(numberOfPersons) + " أشخاص"
//                }
            }
            
            if let date: String = self.myJourneyCellViewModel?.day ,let time: String = self.myJourneyCellViewModel?.time {
                self.dateLabel.text = date + "   " + time.substring(to: 5)
            }            
        }
    }
    
    @IBOutlet weak var myCarImageView: UIImageView!
    @IBOutlet weak var containView: UIView!
    @IBOutlet weak var fromValueLabel: UILabel!
    @IBOutlet weak var toValueLabel: UILabel!
    @IBOutlet weak var carBrandLabel: UILabel!
//    @IBOutlet weak var personsValueLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var editLabel: UILabel!
    @IBOutlet weak var removeLabel: UILabel!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var removeButton: UIButton!
    @IBOutlet weak var editImageView: UIImageView!
    @IBOutlet weak var removeImageView: UIImageView!
    @IBOutlet weak var editView: UIView!
    @IBOutlet weak var removeView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureView()
        selectionStyle = .none
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configureView() {
        editLabel.text = "edit trip button title".localized
        removeLabel.text = "delete trip button title".localized
        containView.addCornerRadius(raduis: 20, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
    }
    
    func editJourneyButtonIsPressed(journeyID: Int) {
        if let delegateValue = journeyButtonsDelegate {
            delegateValue.editJourneyButtonIsPressed(journeyID: journeyID)
        }
    }

    func srartTripButtonIsPressed(journeyID: Int) {
        if let delegateValue = journeyButtonsDelegate {
            delegateValue.srartTripButtonIsPressed(journeyID: journeyID)
        }
    }
    
    func deleteJourneyButtonIsPressed(journeyID: Int) {
        if let delegateValue = journeyButtonsDelegate {
            delegateValue.deleteJourneyButtonIsPressed(journeyID: journeyID)
        }
    }
    
    func doneJourneyButtonIsPressed(journeyID: Int) {
        if let delegateValue = journeyButtonsDelegate {
            delegateValue.doneJourneyButtonIsPressed(journeyID: journeyID)
        }
    }
    
    func callClientButtonIsPressed(clientPhone: String) {
        if let delegateValue = journeyButtonsDelegate {
            delegateValue.callClientButtonIsPressed(clientPhone: clientPhone)
        }
    }

    
    @IBAction func editButtonIsPressed(_ sender: Any) {
        print("Edit")
        if self.myJourneyCellViewModel?.status == 0 {
            editJourneyButtonIsPressed(journeyID: self.editButton.tag)
        }
        if self.myJourneyCellViewModel?.status == 1 {
            if let clientPhone: String = self.myJourneyCellViewModel?.clientPhone {
                callClientButtonIsPressed(clientPhone: clientPhone)
            }
        }
    }
    
    @IBAction func removeButtonIsPressed(_ sender: Any) {
        if self.myJourneyCellViewModel?.status == 0 {
            deleteJourneyButtonIsPressed(journeyID: self.editButton.tag)
        } else if self.myJourneyCellViewModel?.status == 1 {
            print("Trip is done")
            doneJourneyButtonIsPressed(journeyID: self.editButton.tag)
        } else if self.myJourneyCellViewModel?.status == 6 {
            srartTripButtonIsPressed(journeyID: self.editButton.tag)
        }  else if self.myJourneyCellViewModel?.status == 2 {
//            doneJourneyButtonIsPressed(journeyID: self.editButton.tag)
        }
    }
    
}
