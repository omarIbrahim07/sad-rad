//
//  MyJourniesViewModel.swift
//  SadRad
//
//  Created by Omar Ibrahim on 10/20/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation

class MyJourniesViewModel {
    
    enum TripsExistanceState {
        case noTripsExisted
        case tripsExisted
    }
        
    var filterTripStatus = [TripFilter(nameAr: "الرحلات المتاحة", nameEn: "Available", id: 0), TripFilter(nameAr: "الرحلات المحجوزة", nameEn: "Booked", id: 1), TripFilter(nameAr: "الرحلات المكتملة", nameEn: "Completed", id: 2), TripFilter(nameAr: "الرحلات المنتهية", nameEn: "Expired", id: 3)]

    private var myJournies: [MyJourney] = [MyJourney]()
    var selectedJourney: MyJourney?    
    var error: APIError?
    
    var myJourneyCellViewModels: [MyJourneyCellViewModel] = [MyJourneyCellViewModel]() {
        didSet {
            self.reloadTableViewClosure?()
        }
    }
    
    var tripsExistance: TripsExistanceState = .noTripsExisted {
        didSet {
            self.tripsExistanceStateClosure?()
        }
    }
    
    var choosedFilterTrip: TripFilter? {
        didSet {
            self.filterStateClosure?()
        }
    }
    
    var reloadTableViewClosure: (()->())?
    var updateLoadingStatus: (()->())?
    var deleteMyJourneyClosure: (()->())?
    var startMyJourneyClosure: (()->())?
    var doneMyJourneyClosure: (()->())?
    var tripsExistanceStateClosure: (()->())?
    var filterStateClosure: (()->())?
    
    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    var deleted: Bool = false {
        didSet {
            self.deleteMyJourneyClosure?()
        }
    }
    
    var donee: String? {
        didSet {
            self.doneMyJourneyClosure?()
        }
    }
    
    var tripIsStarted: String? {
        didSet {
            self.startMyJourneyClosure?()
        }
    }
    
    var numberOfMyJourniesCells: Int {
        return myJourneyCellViewModels.count
    }
    
    func initFetch(status: Int) {
        state = .loading
        
        let params: [String : AnyObject] = [
            "status" : status as AnyObject
        ]
        
        AddJourneyAPIManager().getMyJournies(basicDictionary: params, onSuccess: { (myJournies) in
            
            self.processMyJournies(myJournies: myJournies)
            self.state = .populated
            
        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    func deleteMyJourney(journeyID: Int) {
        state = .loading
        
        let params: [String : AnyObject] = [
            "trip_id" : journeyID as AnyObject
        ]
        
        AddJourneyAPIManager().deleteMyJourney(basicDictionary: params, onSuccess: { (deleted) in
            
            print(deleted)
            self.deleted = deleted
            self.state = .populated
            
        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    func myJourneyIsDone(journeyID: Int) {
        state = .loading
        
        let params: [String : AnyObject] = [
            "trip_id" : journeyID as AnyObject
        ]
        
        AddJourneyAPIManager().doneMyJourney(basicDictionary: params, onSuccess: { (done) in
            
            print(done)
            self.donee = done
            self.state = .populated
            
        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    func startTrip(journeyID: Int) {
        state = .loading
        
        let params: [String : AnyObject] = [
            "trip_id" : journeyID as AnyObject
        ]
        
        AddJourneyAPIManager().startTrip(basicDictionary: params, onSuccess: { (tripStarted) in
            
            print(tripStarted)
            self.tripIsStarted = tripStarted
            self.state = .populated
            
        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    func processMyJournies( myJournies: [MyJourney] ) {
        
        if myJournies.count == 0 {
            self.tripsExistance = .noTripsExisted
        } else {
            self.tripsExistance = .tripsExisted
        }

        self.myJournies = myJournies // Cache
        var vms = [MyJourneyCellViewModel]()
        for myJourney in myJournies {
            vms.append( createMyJourneyCellViewModel(myJourney: myJourney))
        }
        self.myJourneyCellViewModels = vms
    }
    
    func createMyJourneyCellViewModel( myJourney: MyJourney ) -> MyJourneyCellViewModel {
        return MyJourneyCellViewModel(id: myJourney.id, price: myJourney.price, negotiablePriceBool: myJourney.negotiablePriceBool, deliveryPackagePriceBool: myJourney.deliveryPackagePriceBool, day: myJourney.day, time: myJourney.time, notes: myJourney.notes, createdAt: myJourney.createdAt, packupCity: myJourney.packupCity, packupGovernorate: myJourney.packupGovernorate, dropOffCity: myJourney.dropOffCity, dropOffGovernorate: myJourney.dropOffGovernorate, myCar: myJourney.myCar, status: myJourney.status ?? 0, tripIsAboutStart: myJourney.tripIsAboutToStart, clientName: myJourney.clientName, clientPhone: myJourney.clientPhone)
    }
    
    func getError() -> APIError {
        return self.error!
    }
    
    func getMyJourneyCellViewModel( at indexPath: IndexPath ) -> MyJourneyCellViewModel {
        return myJourneyCellViewModels[indexPath.row]
    }
    
    func getDeletedBool() -> Bool {
        return self.deleted
    }
    
    func userChoossedFilterTrip(filterTrip: TripFilter) {
        self.choosedFilterTrip = filterTrip
    }
    
    func getChoossedFilterTrip() -> TripFilter? {
        return self.choosedFilterTrip
    }
        
    func myJourneyPressed( at indexPath: Int ) {
        let myJourney = self.myJournies[indexPath]
        
        self.selectedJourney = myJourney
    }
    
}
