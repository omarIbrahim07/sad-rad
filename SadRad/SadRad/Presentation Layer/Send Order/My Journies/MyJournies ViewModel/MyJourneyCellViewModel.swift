//
//  MyJourneyCellViewModel.swift
//  SadRad
//
//  Created by Omar Ibrahim on 10/20/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation

struct MyJourneyCellViewModel {
    
    let id: Int?
    let price: Int?
    let negotiablePriceBool: Bool?
    let deliveryPackagePriceBool: Bool?
    let day: String?
    let time: String?
    let notes: String?
    let createdAt: String?
    let packupCity: City?
    let packupGovernorate: Governorate?
    let dropOffCity: City?
    let dropOffGovernorate: Governorate?
    let myCar: MyCar?
    let status: Int?
    let tripIsAboutStart: Int?
    let clientName: String?
    let clientPhone: String?
}
