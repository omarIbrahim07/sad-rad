//
//  EditProfileViewController.swift
//  SadRad
//
//  Created by Omar Ibrahim on 12/27/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit
import AssetsPickerViewController
import Photos

class EditProfileViewController: BaseViewController {

    lazy var viewModel: EditProfileViewModel = {
        return EditProfileViewModel()
    }()

    var error: APIError?
    
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var driverNameLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var driverPhoneLabel: UILabel!
    @IBOutlet weak var editView: UIView!
    @IBOutlet weak var closeView: UIView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var editImageButton: UIButton!
    @IBOutlet weak var penButton: UIButton!
    @IBOutlet weak var gradientView: Gradient!
    @IBOutlet weak var viewOfImage: UIView!
    @IBOutlet weak var changePasswordTitle: UILabel!
    @IBOutlet weak var saveProfileTitle: UILabel!
    @IBOutlet weak var nameTitle: UILabel!
    @IBOutlet weak var emailTitle: UILabel!
    @IBOutlet weak var phoneTitleLabel: UILabel!
    @IBOutlet weak var passwordTitleLabel: UILabel!
    @IBOutlet weak var passwordContainerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setLocalization()
        configureView()
        closeKeypad()
                
        initVM()
        
        if let user: User = self.viewModel.initAccountData() {
            bindAccountData(user: user)
        }
        
    }
    
    func setLocalization() {
        self.navigationItem.title = "edit profile vc title".localized
        if "Lang".localized == "en" {
            nameTextField.textAlignment = .left
            emailTextField.textAlignment = .left
            phoneTextField.textAlignment = .left
            passwordTextField.textAlignment = .left
        } else if "Lang".localized == "ar" {
            nameTextField.textAlignment = .right
            emailTextField.textAlignment = .right
            phoneTextField.textAlignment = .right
            passwordTextField.textAlignment = .right
        }
        nameTextField.placeholder = "first name".localized
        emailTextField.placeholder = "email".localized
        changePasswordTitle.text = "edit profile change password button title".localized
        saveProfileTitle.text = "edit profile save button title".localized
        nameTitle.text = "edit profile name title".localized
        emailTitle.text = "edit profile email title".localized
        phoneTitleLabel.text = "edit profile phone title".localized
        passwordTitleLabel.text = "edit profile password title".localized
    }
    
    func configureView() {
        userImageView.addCornerRadius(raduis: userImageView.frame.height / 2, borderColor: #colorLiteral(red: 0.2274509817, green: 0.2784313858, blue: 0.3490196168, alpha: 1), borderWidth: 2)
        editView.addCornerRadius(raduis: editView.frame.height / 2, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        closeView.addCornerRadius(raduis: closeView.frame.height / 2, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
//        gradientView.roundCorners([.bottomLeft, .bottomRight], radius: gradientView.frame.height / 2)
        viewOfImage.addCornerRadius(raduis: viewOfImage.frame.height / 2, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        
        emailTextField.borderStyle = .none
        nameTextField.borderStyle = .none
        phoneTextField.borderStyle = .none
        emailTextField.isUserInteractionEnabled = false
        nameTextField.isUserInteractionEnabled = false
        phoneTextField.isUserInteractionEnabled = false
        passwordContainerView.isHidden = true
//        emailTextField.text = "a@e.com"
        nameTextField.backgroundColor = .clear
        emailTextField.backgroundColor = .clear
        phoneTextField.backgroundColor = .clear
        editView.isHidden = false
        closeView.isHidden = true
        saveButton.isUserInteractionEnabled = false
        editImageButton.isUserInteractionEnabled = false
        penButton.isHidden = true
    }
    
    // MARK:- Init View Model
    func initVM() {
        
        viewModel.updateEditingUser = { [weak self] () in
            
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.editingUser {
                case .success:
                    self.showDonePopUp()
                case .failed:
                    print("Failed")
                }
            }
        }
        
        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }
            
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                    
                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                case .error:
                    self.error = self.viewModel.getError()
                    if let error = self.error {
                        self.stopLoadingWithError(error: error)
                        //                        self.showError(error: error)
                    }
                case .empty:
                    print("")
                }
            }
        }
                
    }
    
    func closeKeypad() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc func viewTapped() {
        nameTextField.endEditing(true)
        emailTextField.endEditing(true)
        phoneTextField.endEditing(true)
        passwordTextField.endEditing(true)
    }
        
    // MARK:- Bind User Data
    func bindAccountData(user: User) {
        print(user)
        if let name: String = user.name {
            nameTextField.text = name
        }
                
        if let email: String = user.email {
            emailTextField.text = email
        }
        
        if let image: String = user.image {
            userImageView.loadImageFromUrl(imageUrl: ImageURL_USERS + image)
        }
        
        if let phone: String = user.phone {
            phoneTextField.text = phone
        }
    }
    
    func showDonePopUp() {
        let alertController = UIAlertController(title: "edit profile alert title".localized, message: "edit profile alert message".localized, preferredStyle: .alert)
        //        let cancelAction = UIAlertAction(title: "إلغاء", style: .cancel, handler: nil)
        //        alertController.addAction(cancelAction)
        let openAction = UIAlertAction(title: "done action title".localized, style: .default) { (action) in
            self.presentMyHomeScreen()
        }
        alertController.addAction(openAction)
        self.present(alertController, animated: true, completion: nil)
    }
        
    // MARK:- Networking
    func editProfile() {
        
        guard let name: String = nameTextField.text, name.count > 0 else {
              let apiError = APIError()
            apiError.message = "error name textfield".localized
              self.viewModel.error = apiError
              self.viewModel.state = .error
              return
          }
        
        if name.count < 60 {
        } else {
            let apiError = APIError()
            apiError.message = "error name more than sixty".localized
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
                        
        guard let email: String = emailTextField.text, email.isValidEmailAddress() == true else {
            let apiError = APIError()
            apiError.message = "error email textfield".localized
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        guard let phone: String = phoneTextField.text, phone.isValidMobileNumber == true else {
            let apiError = APIError()
            apiError.message = "error phone textfield".localized
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        guard let password: String = passwordTextField.text, password.count >= 8 else {
            let apiError = APIError()
            apiError.message = "error edit profile your paswword".localized
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }

        self.viewModel.editProfile(name: name, email: email, image: self.viewModel.imageData, phone: phone, password: password)
    }

    func getAssetThumbnail(asset: PHAsset) -> UIImage {
        let manager = PHImageManager.default()
        let option = PHImageRequestOptions()
        var thumbnail = UIImage()
        option.isSynchronous = true
        manager.requestImage(for: asset, targetSize: CGSize(width: 100, height: 100), contentMode: .aspectFit, options: option, resultHandler: {(result, info)->Void in
            thumbnail = result!
        })
        return thumbnail
    }
    
    @IBAction func clearButtonIsPressed(_ sender: Any) {
        configureView()
    }
    
    @IBAction func editAccountButtonIsPressed(_ sender: Any) {
        print("Edit profile")
        passwordContainerView.isHidden = false
        emailTextField.borderStyle = .roundedRect
        emailTextField.isUserInteractionEnabled = true
        nameTextField.borderStyle = .roundedRect
        nameTextField.isUserInteractionEnabled = true
        phoneTextField.borderStyle = .roundedRect
        phoneTextField.isUserInteractionEnabled = true
        passwordTextField.borderStyle = .roundedRect
        passwordTextField.isUserInteractionEnabled = true
        emailTextField.backgroundColor = .white
        nameTextField.backgroundColor = .white
        phoneTextField.backgroundColor = .white
        passwordTextField.backgroundColor = .white
        editView.isHidden = false
        closeView.isHidden = false
        saveButton.isUserInteractionEnabled = true
        editImageButton.isUserInteractionEnabled = true
        penButton.isHidden = false
    }

    @IBAction func changePasswordButtonIsPressed(_ sender: Any) {
        goToEditProfilePassword()
    }
    
    @IBAction func editButtonIsPressed(_ sender: Any) {
        self.editProfile()
    }
    
    @IBAction func editImageButtonIsPressed(_ sender: Any) {
        let picker = AssetsPickerViewController()
        picker.pickerDelegate = self
        present(picker, animated: true, completion: nil)
    }
    
    
    // MARK: - Navigation
    func goToEditProfilePassword() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "EditProfilePasswordViewController") as! EditProfilePasswordViewController
        navigationController?.pushViewController(viewController, animated: true)
    }
}
