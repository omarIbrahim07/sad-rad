//
//  EditProfileViewController+Delegates.swift
//  SadRad
//
//  Created by Omar Ibrahim on 12/27/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit
import AssetsPickerViewController
import Photos

extension EditProfileViewController: AssetsPickerViewControllerDelegate {
    
    func assetsPickerCannotAccessPhotoLibrary(controller: AssetsPickerViewController) {}
    
    func assetsPickerDidCancel(controller: AssetsPickerViewController) {}
    
    func assetsPicker(controller: AssetsPickerViewController, selected assets: [PHAsset]) {
        // do your job with selected assets
        let assetImage = self.getAssetThumbnail(asset: assets[0])
        let imgData = assetImage.jpegData(compressionQuality: 1)
        self.viewModel.imageData = imgData!
        print(self.viewModel.imageData)
        guard let profileID: Int = UserDefaultManager.shared.currentUser?.imageID else {
            let apiError = APIError()
            apiError.message = "error upload image".localized
            self.showError(error: apiError)
            return
        }
        self.userImageView.image = assetImage
    }
    
    func assetsPicker(controller: AssetsPickerViewController, shouldSelect asset: PHAsset, at indexPath: IndexPath) -> Bool {
        if controller.selectedAssets.count > 0 {
            // do your job here
            return false
        }
        return true
    }
    
    func assetsPicker(controller: AssetsPickerViewController, didSelect asset: PHAsset, at indexPath: IndexPath) {
        print("Hoa")
    }
    
    func assetsPicker(controller: AssetsPickerViewController, shouldDeselect asset: PHAsset, at indexPath: IndexPath) -> Bool {
        return true
    }
    
    func assetsPicker(controller: AssetsPickerViewController, didDeselect asset: PHAsset, at indexPath: IndexPath) {}
    
}
