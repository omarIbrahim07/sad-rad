//
//  EditProfileViewModel.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 11/27/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation

class EditProfileViewModel {
    
    var imageData: Data?
    
    public enum editingUser {
        case success
        case failed
    }
    
    public enum editingPassword {
        case success
        case failed
    }

    var error: APIError?

    var reloadUserData: (()->())?
    var reloadCitiesClosure: (()->())?
    var chooseCityClosure: (()->())?
    var updateLoadingStatus: (()->())?
    var updateEditingUser: (()->())?
    var updateEditingPassword: (()->())?

    // callback for interfaces
    var cities: [City]? {
        didSet {
            self.reloadCitiesClosure?()
        }
    }

    // callback for interfaces
    var choosedCity: City? {
        didSet {
            self.chooseCityClosure?()
        }
    }

    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }

    // callback for interfaces
    var editingUser: editingUser = .failed {
        didSet {
            self.updateEditingUser?()
        }
    }
    
    var editingPassword: editingPassword = .failed {
        didSet {
            self.updateEditingPassword?()
        }
    }


    func initFetch(locationID: Int) {
        state = .loading

        let params: [String : AnyObject] = [
            "location_id" : locationID as AnyObject,
        ]

        CitiesAPIManager().getCities(basicDictionary: params, onSuccess: { (cities) in

        self.cities = cities // Cache
        self.state = .populated

        }) { (error) in
            self.error = error
            self.state = .error
        }
    }

    func editProfile(name: String, email: String, image: Data?, phone: String, password: String) {

        let params: [String : AnyObject] = [
            "name" : name as AnyObject,
            "email" : email as AnyObject,
            "phone" : phone as AnyObject,
            "old_password" : password as AnyObject
        ]

        state = .loading

        AuthenticationAPIManager().editProfile(nameOfArray: "image", imageData: self.imageData, basicDictionary: params, onSuccess: { (user) in

            UserDefaultManager.shared.currentUser = user
            self.state = .populated
            self.editingUser = .success

        }) { (error) in
            self.error = error
            self.state = .error
            self.editingUser = .failed
        }
    }
    
    func editPassword(oldPassword: String, newPassword: String, confirmNewPassword: String) {

        let params: [String : AnyObject] = [
            "old_password" : oldPassword as AnyObject,
            "new_password" : newPassword as AnyObject,
            "confirm_password" : confirmNewPassword as AnyObject,
        ]

        state = .loading

        AuthenticationAPIManager().editProfile(nameOfArray: "image", imageData: nil, basicDictionary: params, onSuccess: { (user) in

            self.state = .populated
            self.editingPassword = .success

        }) { (error) in
            self.error = error
            self.state = .error
            self.editingUser = .failed
        }
    }

    func makeError(message: String) {
        let apiError = APIError()
        apiError.message = message
        self.error = apiError
        self.state = .error
    }

    func getError() -> APIError {
        return self.error!
    }


    func initAccountData() -> User? {
        return UserDefaultManager.shared.currentUser
    }

    func getCities() -> [City] {
        return cities!
    }

    func userPressed(city: City) {
        self.choosedCity = city
    }


}
