//
//  TermsViewController.swift
//  SadRad
//
//  Created by Omar Ibrahim on 11/16/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class TermsViewController: BaseViewController {
    
    lazy var viewModel: TermsViewModel = {
        return TermsViewModel()
    }()
    
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        configureView()
        configureTableView()
        
        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }
            
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                    
                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                case .error:
                    //                    self.stopLoadingWithError(error: <#T##APIError#>)
                    self.stopLoadingWithSuccess()
                case .empty:
                    print("")
                }
            }
        }
        
        viewModel.reloadStaticPage = { [weak self] () in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
        
        if let pageID: Int = self.viewModel.pageID {
            self.viewModel.initSafteyProcedures(pageID: pageID)
        }
    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "TermsAndConditionTableViewCell", bundle: nil), forCellReuseIdentifier: "TermsAndConditionTableViewCell")
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    func configureView() {
        if let pageID: Int = self.viewModel.pageID {
            if pageID == 1 {
                headerLabel.text = "help vc title".localized
            } else if pageID == 2 {
                headerLabel.text = "privacy policy vc title".localized
            } else if pageID == 3 {
                headerLabel.text = "contact us vc title".localized
            }
        }
    }

     // MARK: - Navigation
    
}

extension TermsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.viewModel.termsAndConditions != nil {
            return 1
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell: TermsAndConditionTableViewCell = tableView.dequeueReusableCell(withIdentifier: "TermsAndConditionTableViewCell") as? TermsAndConditionTableViewCell {
            
            if "Lang".localized == "en" {
                if let title: String = self.viewModel.termsAndConditions?.contentEn {
                    cell.setTermsAndConditions(title: title)
                    return cell
                }
            } else if "Lang".localized == "ar" {
                if let title: String = self.viewModel.termsAndConditions?.contentAr {
                    cell.setTermsAndConditions(title: title)
                    return cell
                }
            }
        }
        return UITableViewCell()
    }
    
    
}
