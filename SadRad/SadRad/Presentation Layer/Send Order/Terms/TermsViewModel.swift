//
//  TermsViewModel.swift
//  SadRad
//
//  Created by Omar Ibrahim on 11/16/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation

class TermsViewModel {
    
    var pageID: Int?
    
    var reloadStaticPage: (()->())?
    var updateLoadingStatus: (()->())?
    
    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    var termsAndConditions: StaticPage? {
        didSet {
            self.reloadStaticPage?()
        }
    }
    
    func initSafteyProcedures(pageID: Int) {
        state = .loading
        
        self.getSafteyProcedures(pageID: pageID)
    }
    
    private func getSafteyProcedures(pageID: Int) {
        
        let params: [String : AnyObject] = [
            "page_number" : pageID as AnyObject
        ]
        
        StaticAPIManager().getStaticPage(basicDictionary: params, onSuccess: { (staticPage) in
            
            if staticPage.count > 0 {
                self.termsAndConditions = staticPage[0]
            }
            self.state = .populated

        }) { (error) in
            self.state = .error
        }
    }

}
