//
//  BrandTableViewCell.swift
//  SadRad
//
//  Created by Omar Ibrahim on 10/27/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class BrandTableViewCell: UITableViewCell {

    var brandCellViewModel: CarBrandCellViewModel? {
        didSet {
            if "Lang".localized == "en" {
                if let brandName: String = self.brandCellViewModel?.nameEn {
                    brandLabel.text = brandName
                }
            } else if "Lang".localized == "ar" {
                if let brandName: String = self.brandCellViewModel?.nameAr {
                    brandLabel.text = brandName
                }
            }
        }
    }
    @IBOutlet weak var brandLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
