//
//  ChooseBrandsViewController.swift
//  SadRad
//
//  Created by Omar Ibrahim on 10/27/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

// MARK:- Delegate between two VCS
protocol SendCountriesDelegate {
    func sendCountries(countries: [String])
}

class ChooseBrandsViewController: BaseViewController {
    
    lazy var viewModel: ChooseBrandsViewModel = {
        ChooseBrandsViewModel()
    }()
    
    var error: APIError?
    
    var allCountries: [CarBrand] = [CarBrand]()
    var allSettingCountries: [String] = [String]()
    var settingCountries: [String] = [String]()
    var choosedCountries: [CarBrand] = [CarBrand]()
    
    var delegate: SendCountriesDelegate!
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        configureTableView()
//        if allSettingCountries.count > 0 {
//            bindCountries(countries: allSettingCountries, choosedCountries: settingCountries)
//        }
        initVM()
    }
    
    func initVM() {
        
        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }
            
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                    
                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                case .error:
                    self.error = self.viewModel.getError()
                    if let error = self.error {
                        self.stopLoadingWithError(error: error)
                    }
                case .empty:
                    print("")
                }
            }
        }
        
        viewModel.reloadTableViewClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
                
        viewModel.fetchCarBrands()
    }

    
    func configureTableView() {
        tableView.register(UINib(nibName: "BrandTableViewCell", bundle: nil), forCellReuseIdentifier: "BrandTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
    }
    
//    func bindCountries(countries: [String], choosedCountries: [String]) {
//        for n in 0...countries.count - 1 {
//            self.choosedCountries.append
//        }
//        if choosedCountries.count >= 1 {
//            for i in 0...choosedCountries.count - 1 {
//                for n in 0...countries.count - 1 {
//                    if self.choosedCountries[n].nameEn == choosedCountries[i] {
//                        self.choosedCountries[n].selected = true
//                    }
//                }
//            }
//        }
//        
//        //        self.choosedCountries = self.allCountries
//        
//        //        for t in 0...countries.count - 1 {
//        ////            print(allCountries[t].selected)
//        //            print(self.choosedCountries[t].selected)
//        //        }
//        self.tableView.reloadData()
//    }
    
     // MARK: - Navigation
    
}
