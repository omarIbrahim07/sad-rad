//
//  ChooseBrandsViewModel.swift
//  SadRad
//
//  Created by Omar Ibrahim on 10/27/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation

class ChooseBrandsViewModel {
    
    var error: APIError?

    var carsBrands: [CarBrand] = [CarBrand]()
    
    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    var carsBrandsCellViewModels: [CarBrandCellViewModel] = [CarBrandCellViewModel]() {
        didSet {
            self.reloadTableViewClosure?()
        }
    }
    
    var reloadTableViewClosure: (()->())?
    var updateLoadingStatus: (()->())?
    
    func fetchCarBrands() {
        state = .loading
        
        let params: [String : AnyObject] = [:]
        
        CarsAPIManager().getCarsBrands(basicDictionary: params, onSuccess: { (carsBrands) in

//        self.carsBrands = carsBrands // Cache
            self.processCarBrands(carsBrands: carsBrands)
        self.state = .populated

        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    func processCarBrands( carsBrands: [CarBrand] ) {
        self.carsBrands = carsBrands // Cache
        var vms = [CarBrandCellViewModel]()
        for carBrand in carsBrands {
            vms.append( createCarBrandCellViewModel(carBrand: carBrand))
        }
        self.carsBrandsCellViewModels = vms
    }
    
    func createCarBrandCellViewModel( carBrand: CarBrand ) -> CarBrandCellViewModel {
        return CarBrandCellViewModel(id: carBrand.id, nameEn: carBrand.nameEn, nameAr: carBrand.nameAr)
    }
    
    func getCarBrandCellViewModel( at indexPath: IndexPath ) -> CarBrandCellViewModel {
        return carsBrandsCellViewModels[indexPath.row]
    }
    
    func getError() -> APIError {
        return self.error!
    }

}
