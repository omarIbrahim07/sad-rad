//
//  CarBrandCellViewModel.swift
//  SadRad
//
//  Created by Omar Ibrahim on 10/27/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation

struct CarBrandCellViewModel {
    let id: Int?
    let nameEn: String?
    let nameAr: String?
    let selected: Bool? = false
}
