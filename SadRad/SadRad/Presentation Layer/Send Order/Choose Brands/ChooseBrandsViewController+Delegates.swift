//
//  ChooseBrandsViewController+Delegates.swift
//  SadRad
//
//  Created by Omar Ibrahim on 10/27/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

extension ChooseBrandsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return choosedCountries.count
        return self.viewModel.carsBrandsCellViewModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell: BrandTableViewCell = tableView.dequeueReusableCell(withIdentifier: "BrandTableViewCell") as? BrandTableViewCell {
            
            tableView.tableFooterView = UIView()
            cell.brandCellViewModel = self.viewModel.getCarBrandCellViewModel(at: indexPath)
            
            cell.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//            cell.accessoryType = choosedCountries[indexPath.row].selected == true ? .checkmark : .nones
            
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        // we can replace this code by only one code
        choosedCountries[indexPath.row].selected = !choosedCountries[indexPath.row].selected!
        self.settingCountries = []
//        for i in 0...choosedCountries.count - 1 {
//            if choosedCountries[i].selected == true {
//                self.settingCountries.append(choosedCountries[i].name ?? "")
//            }
//        }
        
        
        // MARK:- Edit 3lmashy
//        if self.settingCountries.count != 0 {
            self.delegate.sendCountries(countries: self.settingCountries)
//        }
        
        //        self.saveCountries()
        
        tableView.reloadData()
        
        // add checkmark to selected cell
        // This code is commented after item DataModel is added
        //        if tableView.cellForRow(at: indexPath)?.accessoryType == .checkmark {
        //            tableView.cellForRow(at: indexPath)?.accessoryType = .none
        //        } else {w
        //            tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
        //        }
        
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}
