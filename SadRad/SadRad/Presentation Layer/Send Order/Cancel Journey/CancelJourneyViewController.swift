//
//  CancelJourneyViewController.swift
//  SadRad
//
//  Created by Omar Ibrahim on 9/15/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class CancelJourneyViewController: BaseViewController {
    
    lazy var viewModel: CancelJourneyViewModel = {
       return CancelJourneyViewModel()
    }()
    
    var error: APIError?

    open var reasonsArray: [String] = ["High price", "far distant", "Changing appointment"]
    
    @IBOutlet weak var reasonForCancelTripLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
        setLocalization()
        configureTableView()
        initVM()
    }
    
    func setLocalization() {
        reasonForCancelTripLabel.text = "reason for cancel trip vc title".localized
    }
    
    func initVM() {
        
        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }
            
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                    
                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                case .error:
                    self.error = self.viewModel.getError()
                    if let error = self.error {
                        self.stopLoadingWithError(error: error)
                    }
                case .empty:
                    print("")
                }
            }
        }
        
        viewModel.reloadTableViewClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
        
        viewModel.reservationIsDeletedClosure = { [weak self] () in
            DispatchQueue.main.async {
                if let reservationDeleted = self?.viewModel.reservationIsDeleted {
                    if reservationDeleted == true {
                        self?.showCarPopUp(title: "reason for delete reservation alert title".localized, message: "reason for delete reservation alert message".localized, actionTitle: "done action title".localized)
                    }
                }
            }
        }
                
        viewModel.fetchReasonsForCancelTrip()
    }
    
    func showCarPopUp(title: String, message: String, actionTitle: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        //        let cancelAction = UIAlertAction(title: "إلغاء", style: .cancel, handler: nil)
        //        alertController.addAction(cancelAction)
        let openAction = UIAlertAction(title: actionTitle, style: .default) { (action) in
            self.presentMyHomeScreen()
        }
        //        let cancelAction = UIAlertAction(title: "cancel go to login".localized, style: .cancel)
        alertController.addAction(openAction)
        //        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }

    func cancelTrip() {
        guard let note: String = self.viewModel.note, note.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال ملاحظاتك"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        //        self.viewModel.rateTrip(tripID: tripID, reviewArray: self.viewModel.rateArray, note: note)
        self.viewModel.cancelTrip(tripID: self.viewModel.tripID ?? 0, arrayOfReasons: self.viewModel.choosedReasonsIdsArray, notes: note, carID: self.viewModel.carID ?? 0)
    }
    
    func configureView() {

    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "CancelJourneyTableViewCell", bundle: nil), forCellReuseIdentifier: "CancelJourneyTableViewCell")
        tableView.register(UINib(nibName: "NoteRateTableViewCell", bundle: nil), forCellReuseIdentifier: "NoteRateTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 20))
        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 20))
    }

    @IBAction func cancelTripButtonIsPressed(_ sender: Any) {
        self.cancelTrip()
    }
    // MARK: - Navigation

}
