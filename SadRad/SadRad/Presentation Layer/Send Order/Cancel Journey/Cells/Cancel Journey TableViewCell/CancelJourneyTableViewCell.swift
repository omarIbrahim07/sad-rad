//
//  CancelJourneyTableViewCell.swift
//  SadRad
//
//  Created by Omar Ibrahim on 9/15/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

protocol ReasonForCancelJourneyTableViewCellDelegate {
    func ReasonForCancelJourneyButtonIsPressed(tag: Int, choosed: Bool)
}

class CancelJourneyTableViewCell: UITableViewCell {
    
    var reasonForCancelDelegate: ReasonForCancelJourneyTableViewCellDelegate?
    
    var reasonCellViewModel : CancelJourneyCellViewModel? {
        didSet {
            if let reasonID: Int = self.reasonCellViewModel?.id {
                self.tag = reasonID
                self.reasonButton.tag = reasonID
            }
            if "Lang".localized == "en" {
                if let reasongName: String = self.reasonCellViewModel?.nameEn {
                    self.cancelTripReasonLabel.text = reasongName
                }
            } else if "Lang".localized == "ar" {
                if let reasongName: String = self.reasonCellViewModel?.nameAr {
                    self.cancelTripReasonLabel.text = reasongName
                }
            }
            if let choosed: Bool = self.reasonCellViewModel?.choosed {
                if choosed == false {
                    checkBoxImageView.image = UIImage(named: "UnChecked")
                    print("Cancel trip reason unchecked")
                } else if choosed == true {
                    checkBoxImageView.image = UIImage(named: "Checked")
                    print("Cancel trip reason checked")
                }
            }
        }
    }
    
    
    @IBOutlet weak var cancelTripReasonLabel: UILabel!
    @IBOutlet weak var reasonButton: UIButton!
    @IBOutlet weak var checkBoxImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func ReasonForCancelJourneyButtonIsPressed(tag: Int, choosed: Bool) {
        if let delegateValue = reasonForCancelDelegate {
            delegateValue.ReasonForCancelJourneyButtonIsPressed(tag: tag, choosed: choosed)
        }
    }
    
    @IBAction func reasonButtonIsPressed(_ sender: Any) {
        print("Reason button is pressed")
        ReasonForCancelJourneyButtonIsPressed(tag: reasonButton.tag, choosed: !(self.reasonCellViewModel?.choosed ?? false))
    }
    
}
