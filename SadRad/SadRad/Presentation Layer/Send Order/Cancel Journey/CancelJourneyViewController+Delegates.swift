//
//  CancelJourneyViewController+Delegates.swift
//  SadRad
//
//  Created by Omar Ibrahim on 9/15/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

extension CancelJourneyViewController: AddNoteTextViewTableViewCellDelegate {
    func addNoteTextViewPressed(note: String) {
        print(note)
        self.viewModel.note = note
    }
}

extension CancelJourneyViewController: ReasonForCancelJourneyTableViewCellDelegate {
    func ReasonForCancelJourneyButtonIsPressed(tag: Int, choosed: Bool) {
        print(tag)
        print(choosed)
        
        if choosed == true {
//            self.viewModel.choosedReasonsIdsArray[tag] = 1
            self.viewModel.choosedReasonsIdsArray.append(tag)
            self.viewModel.reasonsCellViewModels[tag - 1].choosed = true
        } else if choosed == false {
//            self.viewModel.choosedReasonsIdsArray[tag] = 0
            self.viewModel.choosedReasonsIdsArray.removeAll(where: { $0 == tag })
            self.viewModel.reasonsCellViewModels[tag - 1].choosed = false
        }
        print(self.viewModel.choosedReasonsIdsArray)
        self.tableView.reloadRows(at: [IndexPath(item: tag - 1, section: 0)], with: .automatic)
    }
}

extension CancelJourneyViewController: AddNoteTableViewCellDelegate {
    func addNoteTextFieldPressed(note: String) {
        print(note)
        self.viewModel.note = note
    }
}

extension CancelJourneyViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfReasonsCells + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == self.viewModel.numberOfReasonsCells {
            if let cell: NoteRateTableViewCell = tableView.dequeueReusableCell(withIdentifier: "NoteRateTableViewCell") as? NoteRateTableViewCell {
                
                if "Lang".localized == "en" {
                    cell.ratingLabel.text = "Note"
                } else if "Lang".localized == "ar" {
                    cell.ratingLabel.text = "ملحوظة"
                }
//                cell.addNoteDelegate = self
                cell.addNoteTextViewDelegate = self
                return cell
            }
        }

        if let cell: CancelJourneyTableViewCell = tableView.dequeueReusableCell(withIdentifier: "CancelJourneyTableViewCell") as? CancelJourneyTableViewCell {
            
            let cellVM = viewModel.getReasonCellViewModel(at: indexPath)
            cell.reasonCellViewModel = cellVM
            
            cell.reasonForCancelDelegate = self

            return cell
        }
        
        return UITableViewCell()
    }
    
    
}
