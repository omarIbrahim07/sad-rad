//
//  CancelJourneyCellViewModel.swift
//  SadRad
//
//  Created by Omar Ibrahim on 11/4/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation

struct CancelJourneyCellViewModel {
    let id: Int?
    let nameEn: String?
    let nameAr: String?
    var choosed: Bool?
}
