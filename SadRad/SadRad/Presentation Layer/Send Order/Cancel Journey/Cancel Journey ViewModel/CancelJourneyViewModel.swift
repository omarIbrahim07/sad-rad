//
//  CancelJourneyViewModel.swift
//  SadRad
//
//  Created by Omar Ibrahim on 11/4/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation

class CancelJourneyViewModel {
    
    var reasons: [ReasonForCancel] = [ReasonForCancel]()
    var choosedReasonsIdsArray: [Int] = [Int]()
    
    var tripID: Int?
    var carID: Int?
    
    var note: String?
    
    var error: APIError?
    
    var updateLoadingStatus: (()->())?
    var reloadTableViewClosure: (()->())?
    var reservationIsDeletedClosure: (()->())?
    var chooseCarConditioningClosure: (()->())?
    
    var reasonsCellViewModels: [CancelJourneyCellViewModel] = [CancelJourneyCellViewModel]() {
        didSet {
            self.reloadTableViewClosure?()
        }
    }
                
    var choosedCarConditioning: Bool? = false {
        didSet {
            self.chooseCarConditioningClosure?()
        }
    }
    
    var reservationIsDeleted: Bool? = false {
        didSet {
            self.reservationIsDeletedClosure?()
        }
    }
        
    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    var numberOfReasonsCells: Int {
        return reasonsCellViewModels.count
    }
    
    func fetchReasonsForCancelTrip() {
        state = .loading
        
        let params: [String : AnyObject] = [:]
        
        CancelTripAPIManager().getReasonsForCancelTrip(basicDictionary: params, onSuccess: { (reasons) in
            
            self.processReasonsForCancelTrip(reasons: reasons)
            self.state = .populated
            
        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    func processReasonsForCancelTrip( reasons: [ReasonForCancel] ) {
        self.reasons = reasons // Cache
        var vms = [CancelJourneyCellViewModel]()
        for reason in reasons {
            vms.append( createCancelTripCellViewModel(reason: reason))
//            choosedReasonsIdsArray.append(0)
        }
        self.reasonsCellViewModels = vms
    }
    
    func createCancelTripCellViewModel( reason: ReasonForCancel ) -> CancelJourneyCellViewModel {
        return CancelJourneyCellViewModel(id: reason.id, nameEn: reason.nameEn, nameAr: reason.nameAr, choosed: reason.choosed)
    }

                
    func changeCarConditioning() {
        self.choosedCarConditioning = !self.choosedCarConditioning!
    }
    
    func cancelTrip(tripID: Int, arrayOfReasons: [Int], notes: String, carID: Int) {
        
        let params: [String : AnyObject] = [
            "notes" : notes as AnyObject,
            "trip_id" : tripID as AnyObject,
            "reason_id[]" : arrayOfReasons as AnyObject,
            "car_id" : carID as AnyObject,
        ]
                
        self.state = .loading
        
        CancelTripAPIManager().cancelTrip(basicDictionary: params, onSuccess: { (saved) in
            print(saved)
            self.reservationIsDeleted = true
            self.state = .populated
        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    func getReasonCellViewModel( at indexPath: IndexPath ) -> CancelJourneyCellViewModel {
        return self.reasonsCellViewModels[indexPath.row]
    }
    
//    func getCarsTypes() -> [CarType] {
//        return carsTypes!
//    }
//
//    func userChoossedCarType(carType: CarType) {
//        self.choosedCarType = carType
//    }
        
    func getCarConditioningBool() -> Bool {
        return choosedCarConditioning!
    }
        
    func getError() -> APIError {
        return self.error!
    }
    
    
}
