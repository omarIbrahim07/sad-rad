//
//  SideMenuExViewController.swift
//  SadRad
//
//  Created by Omar Ibrahim on 9/10/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit
import MOLH
import AssetsPickerViewController
import Photos

class SideMenuExViewController: BaseViewController {
    
    var imageData: Data?
    
    let menuImages: [String] = [
        "main",
        "My reservations",
        "Privacy",
        "Help",
        "Connect--us",
        "provider",
        "language",
        "adjust 2",
        "Logoff",
    ]
    
    let menuArray: [String] = [
        "Search For Trip",
        "Bookings",
        "Privacy Policy",
        "Help",
        "Contact Us",
        "Sad Rad Partner",
                "عربي",
        "Settings"
    ]
        
    let arabicMenuArray: [String] = [
        "ابحث عن رحلة",
"حجوزاتي",
        "سياسة الخصوصية",
        "مساعدة",
        "تواصل معنا",
        "شريك صد رد",
        "English",
"الإعدادات"
    ]
        
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        configureView()
        configureTableView()
    }
    
    func configureView() {
        
    }
    
    func getAssetThumbnail(asset: PHAsset) -> UIImage {
        let manager = PHImageManager.default()
        let option = PHImageRequestOptions()
        var thumbnail = UIImage()
        option.isSynchronous = true
        manager.requestImage(for: asset, targetSize: CGSize(width: 100, height: 100), contentMode: .aspectFit, options: option, resultHandler: {(result, info)->Void in
            thumbnail = result!
        })
        return thumbnail
    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "MenuExTableViewCell", bundle: nil), forCellReuseIdentifier: "MenuExTableViewCell")
        tableView.register(UINib(nibName: "InfoTableViewCell", bundle: nil), forCellReuseIdentifier: "InfoTableViewCell")
        tableView.register(UINib(nibName: "ProviderTableViewCell", bundle: nil), forCellReuseIdentifier: "ProviderTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        
    }
    
    func uploadProfile(profileID: Int) {
        
        let params: [String : AnyObject] = [
            "profile_id" : profileID as AnyObject]

        CarsAPIManager().uploadImage(nameOfArray: "image", imageData: self.imageData, basicDictionary: params, onSuccess: { (image) in
            
            AuthenticationAPIManager().getUserProfile(onSuccess: { (user) in
                self.tableView.reloadData()
            }) { (error) in
                
            }
            
//            if saved == true {
//                print("Success")
//
//                AuthenticationAPIManager().getUserProfile(onSuccess: { (user) in
//                    self.tableView.reloadData()
//                }) { (error) in
//
//                }
////                self.myCarIsAdded = true
////                self.state = .populated
//                //                self.sendingOrder = .success
//            } else {
////                self.state = .empty
//                //                self.sendingOrder = .failed
//            }
        }) { (error) in
//            self.error = error
//            self.state = .error
        }
    }
    
    func changeLanguage() {
        MOLH.setLanguageTo(MOLHLanguage.currentAppleLanguage() == "en" ? "ar" : "en")
        MOLH.reset()
        reset()
    }
            
    // MARK: - Navigation
    func goToMyProfile() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "RatesViewController") as! RatesViewController
        self.present(viewController, animated: true, completion: nil)
    }

    func goToMyCars() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        let contentViewController = UINavigationController(rootViewController: viewController)
        viewController.goToMyCars()
        sideMenuViewController?.setContentViewController(contentViewController, animated: true)
        sideMenuViewController?.hideMenuViewController()
    }
    
    func goToEditProfile() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        let contentViewController = UINavigationController(rootViewController: viewController)
        viewController.goToEditProfile()
        sideMenuViewController?.setContentViewController(contentViewController, animated: true)
        sideMenuViewController?.hideMenuViewController()
    }
    
    func goToAddJourney() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        let contentViewController = UINavigationController(rootViewController: viewController)
        viewController.goToAddJourney()
        sideMenuViewController?.setContentViewController(contentViewController, animated: true)
        sideMenuViewController?.hideMenuViewController()
    }
    
    func goToDriverInfo() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        let contentViewController = UINavigationController(rootViewController: viewController)
        viewController.goToDriverMenu()
        sideMenuViewController?.setContentViewController(contentViewController, animated: true)
        sideMenuViewController?.hideMenuViewController()
    }
    
    func goToMyJourneis() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        let contentViewController = UINavigationController(rootViewController: viewController)
        viewController.goToMyJourneis()
        sideMenuViewController?.setContentViewController(contentViewController, animated: true)
        sideMenuViewController?.hideMenuViewController()
    }
    
    func goToMyBookings() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        let contentViewController = UINavigationController(rootViewController: viewController)
        viewController.goToMyBookings()
        sideMenuViewController?.setContentViewController(contentViewController, animated: true)
        sideMenuViewController?.hideMenuViewController()
    }
        
    func goToRate() {
        print("KKK")
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "RateViewController") as! RateViewController
        //        viewController.selectedService = service
        
        present(viewController, animated: true, completion: nil)
        //        navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    func goToCancelTrip() {
        print("KKK")
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "CancelJourneyViewController") as! CancelJourneyViewController
        //        viewController.selectedService = service
        
        present(viewController, animated: true, completion: nil)
        //        navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    func goToReserved() {
        print("KKK")
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "ReservedViewController") as! ReservedViewController
        //        viewController.selectedService = service
        
        present(viewController, animated: true, completion: nil)
        //        navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    func goToRates() {
        print("KKK")
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "RatesViewController") as! RatesViewController
        //        viewController.selectedService = service
        
        present(viewController, animated: true, completion: nil)
        //        navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    func goToMyReservations() {
        print("KKK")
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "MyReservationsViewController") as! MyReservationsViewController
        //        viewController.selectedService = service
        
        present(viewController, animated: true, completion: nil)
        //        navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    func goToTerms(pageID: Int) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "TermsViewController") as! TermsViewController
        //        viewController.selectedService = service
        viewController.viewModel.pageID = pageID
        
        present(viewController, animated: true, completion: nil)
    }
    
    func logout() {
        
        self.startLoading()
                
        AuthenticationAPIManager().logout(basicDictionary: [:], onSuccess: { (message) -> String in
            
            UserDefaultManager.shared.currentUser = nil
            self.goToLogIn()
            
            self.stopLoadingWithSuccess()
            return "70bby"
            
        }) { (error) in
            print(error)
        }
    }
    
    func goToLogIn() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "LoginNavigationVC")
        appDelegate.window!.rootViewController = viewController
        appDelegate.window!.makeKeyAndVisible()
    }

    func reset() {
        let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
        let stry = UIStoryboard(name: "Main", bundle: nil)
        rootviewcontroller.rootViewController = stry.instantiateViewController(withIdentifier: "MenuViewController")
    }
    
}
