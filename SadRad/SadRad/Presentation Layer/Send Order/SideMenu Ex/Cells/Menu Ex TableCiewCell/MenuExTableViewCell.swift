//
//  MenuExTableViewCell.swift
//  SadRad
//
//  Created by Omar Ibrahim on 9/10/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class MenuExTableViewCell: UITableViewCell {

    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var bView: UIView!
    @IBOutlet weak var menuLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
