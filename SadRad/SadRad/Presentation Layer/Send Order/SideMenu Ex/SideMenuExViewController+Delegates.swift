//
//  SideMenuExViewController+Delegates.swift
//  SadRad
//
//  Created by Omar Ibrahim on 9/10/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit
import AssetsPickerViewController
import Photos

extension SideMenuExViewController: profileImageTableViewCellDelegate {
    func profileImageButtonPressed() {
//        checkLoginAlert()
//        let picker = AssetsPickerViewController()
//        picker.pickerDelegate = self
//        present(picker, animated: true, completion: nil)
    }
    
    func myProfileButtonPressed() {
//        guard let id: Int = UserDefaultManager.shared.currentUser?.id, id > 0 else {
//            checkLoginAlert()
//            return
//        }
////        self.goToMyProfile()
//        self.goToEditProfile()
    }
}

extension SideMenuExViewController: UITableViewDelegate, UITableViewDataSource {
    
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return menuArray.count + 2
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
            if indexPath.row == 0 {
                if let cell: InfoTableViewCell = tableView.dequeueReusableCell(withIdentifier: "InfoTableViewCell", for: indexPath) as? InfoTableViewCell {
                    cell.delegate = self
                    if let user = UserDefaultManager.shared.currentUser {
                        if let name: String = user.name {
                            cell.userNameLabel.text = name
                        }
                        
                        if let image: String = user.image {
                            cell.userImage.loadImageFromUrl(imageUrl: ImageURL_USERS + image)
                        }
                        
                    }
                    return cell
                }
            }
            
            if let cell: MenuExTableViewCell = tableView.dequeueReusableCell(withIdentifier: "MenuExTableViewCell", for: indexPath) as? MenuExTableViewCell {
                                
                if "Lang".localized == "en" {
                    cell.iconImageView.image = UIImage(named: menuImages[indexPath.row - 1])
                    if indexPath.row != 9 {
                        cell.menuLabel.text = menuArray[indexPath.row - 1]
                    } else if indexPath.row == 9 {
                        if UserDefaultManager.shared.currentUser?.id == nil {
                            cell.menuLabel.text = "Login"
                        } else {
                            cell.menuLabel.text = "Logout"
                        }
                    }
                } else if "Lang".localized == "ar" {
                    cell.iconImageView.image = UIImage(named: menuImages[indexPath.row - 1])
                    if indexPath.row != 9 {
                        cell.menuLabel.text = arabicMenuArray[indexPath.row - 1]
                    } else if indexPath.row == 9 {
                        if UserDefaultManager.shared.currentUser?.id == nil {
                            cell.menuLabel.text = "تسجيل الدخول"
                        } else {
                            cell.menuLabel.text = "تسجيل الخروج"
                        }
                    }
                }
                
                return cell
            }
            
            return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
                
        if indexPath.row == 0 {

        } else if indexPath.row == 1 {
            self.sideMenuViewController?.hideMenuViewController()
        } else if indexPath.row == 2 {
            checkLoginAlert()
            guard let id: Int = UserDefaultManager.shared.currentUser?.id, id > 0 else {
                return
            }
            self.goToMyBookings()

            //            goToRate()
        } else if indexPath.row == 3 {
            goToTerms(pageID: 2)
            
            //            goToReserved()
        } else if indexPath.row == 4 {
            goToTerms(pageID: 1)
            
//            checkLoginAlert()
//            guard let id: Int = UserDefaultManager.shared.currentUser?.id, id > 0 else {
//                return
//            }
//            self.goToMyJourneis()
        } else if indexPath.row == 5 {
            goToTerms(pageID: 3)

//            checkLoginAlert()
//            guard let id: Int = UserDefaultManager.shared.currentUser?.id, id > 0 else {
//                return
//            }
//            self.goToAddJourney()

        } else if indexPath.row == 6 {
            self.goToDriverInfo()
//            goToRate()

        } else if indexPath.row == 7 {
            self.changeLanguage()
//            checkLoginAlert()
//            guard let id: Int = UserDefaultManager.shared.currentUser?.id, id > 0 else {
//                return
//            }
//            self.goToMyBookings()

        } else if indexPath.row == 8 {
            guard let id: Int = UserDefaultManager.shared.currentUser?.id, id > 0 else {
                checkLoginAlert()
                return
            }
            self.goToEditProfile()
        } else if indexPath.row == 9 {
            if UserDefaultManager.shared.currentUser?.id == nil {
                self.goToLogIn()
            } else {
                self.logout()
            }
        }
                
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}

extension SideMenuExViewController: AssetsPickerViewControllerDelegate {
    
    func assetsPickerCannotAccessPhotoLibrary(controller: AssetsPickerViewController) {}
    
    func assetsPickerDidCancel(controller: AssetsPickerViewController) {}
    
    func assetsPicker(controller: AssetsPickerViewController, selected assets: [PHAsset]) {
        // do your job with selected assets
        let assetImage = self.getAssetThumbnail(asset: assets[0])
        let imgData = assetImage.jpegData(compressionQuality: 1)
        self.imageData = imgData!
        print(imageData)
        guard let profileID: Int = UserDefaultManager.shared.currentUser?.imageID else {
            let apiError = APIError()
            apiError.message = "error upload image".localized
            self.showError(error: apiError)
            return
        }
        self.uploadProfile(profileID: profileID)
    }
    
    func assetsPicker(controller: AssetsPickerViewController, shouldSelect asset: PHAsset, at indexPath: IndexPath) -> Bool {
        if controller.selectedAssets.count > 0 {
            // do your job here
            return false
        }
        return true
    }
    
    func assetsPicker(controller: AssetsPickerViewController, didSelect asset: PHAsset, at indexPath: IndexPath) {
        print("Hoa")
    }
    
    func assetsPicker(controller: AssetsPickerViewController, shouldDeselect asset: PHAsset, at indexPath: IndexPath) -> Bool {
        return true
    }
    
    func assetsPicker(controller: AssetsPickerViewController, didDeselect asset: PHAsset, at indexPath: IndexPath) {}
    
}
