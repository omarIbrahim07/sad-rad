//
//  MyReservationsViewController+Delegates.swift
//  SadRad
//
//  Created by Omar Ibrahim on 9/16/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

extension MyReservationsViewController: ReservationButtonsDelegate {
    func rateTripButtonIsPressed(tripID: Int) {
        goToRateTrip(tripID: tripID)
    }
    
    func callDriverButtonIsPressed(driverPhoneNumber: String) {
        self.showCallAlert(phoneNumber: driverPhoneNumber)
    }
    
    func deleteReservationButtonIsPressed(reservationID: Int, carID: Int) {
        self.goToDeleteTrip(tripID: reservationID, carID: carID)
    }
}

extension MyReservationsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfReservationsCells
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell: ReservationTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ReservationTableViewCell") as? ReservationTableViewCell {
            
            let cellVM = viewModel.getReservationCellViewModel(at: indexPath)
            cell.reservationCellViewModel = cellVM
            cell.reservationButtonsDelegate = self

            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.viewModel.reservationPressed(at: indexPath.row)
        if let reservationChoosed: NewReservation = self.viewModel.getReservation() {
            if let tripID: Int = reservationChoosed.tripID {
                self.goToTripDetails(tripID: tripID)
            }
        }
    }
}
