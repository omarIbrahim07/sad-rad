//
//  MyReservationsViewModel.swift
//  SadRad
//
//  Created by Omar Ibrahim on 10/22/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation

class MyReservationsViewModel {
    
    enum ReservationsExistanceState {
        case noReservationsExisted
        case reservationsExisted
    }
    
    private var reservations: [Reservation] = [Reservation]()
    var selectedReservation: Reservation?
    
    private var newReservations: [NewReservation] = [NewReservation]()
    var selectedNewReservation: NewReservation?
    
    var error: APIError?
    
    var reservationsCellViewModels: [ReservationCellViewModel] = [ReservationCellViewModel]() {
        didSet {
            self.reloadTableViewClosure?()
        }
    }
    
    var reservationsExistance: ReservationsExistanceState = .noReservationsExisted {
        didSet {
            self.reservationsExistanceStateClosure?()
        }
    }
    
    var reloadTableViewClosure: (()->())?
    var updateLoadingStatus: (()->())?
    var deleteReservationClosure: (()->())?
    var reservationsExistanceStateClosure: (()->())?
    
    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    var deleted: Bool = false {
        didSet {
            self.deleteReservationClosure?()
        }
    }
    
    var numberOfReservationsCells: Int {
        return reservationsCellViewModels.count
    }
    
//    func initFetch() {
//        state = .loading
//
//        let params: [String : AnyObject] = [:]
//
//        ReservationsAPIManager().getReservations(basicDictionary: params, onSuccess: { (reservations) in
//
//            self.processReservations(reservations: reservations)
//            self.state = .populated
//
//        }) { (error) in
//            self.error = error
//            self.state = .error
//        }
//    }
    func initFetch() {
        state = .loading
        
        let params: [String : AnyObject] = [:]
        
        ReservationsAPIManager().getNewReservations(basicDictionary: params, onSuccess: { (newReservations) in
            
            self.processReservations(reservations: newReservations)
            self.state = .populated
            
        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    func deleteReservation(carID: Int) {
        state = .loading
        
        let params: [String : AnyObject] = [
            "car_id" : carID as AnyObject
        ]
        
        CarsAPIManager().deleteMyCar(basicDictionary: params, onSuccess: { (deleted) in
            
            print(deleted)
            self.deleted = deleted
            self.state = .populated
            
        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
//    func processReservations( reservations: [Reservation] ) {
//        self.reservations = reservations // Cache
    func processReservations( reservations: [NewReservation] ) {
        
        if reservations.count == 0 {
            self.reservationsExistance = .noReservationsExisted
        } else {
            self.reservationsExistance = .reservationsExisted
        }

        self.newReservations = reservations // Cache
        var vms = [ReservationCellViewModel]()
        for reservation in reservations {
            vms.append( createReservationCellViewModel(reservation: reservation))
        }
        self.reservationsCellViewModels = vms
    }
    
//    func createReservationCellViewModel( reservation: Reservation ) -> ReservationCellViewModel {
    func createReservationCellViewModel( reservation: NewReservation ) -> ReservationCellViewModel {
        if "Lang".localized == "en" {
//            return ReservationCellViewModel(reservationID: reservation.reservationID, tripID: reservation.tripID, fromGovernorate: reservation.tripInfo?.packupGovernorate?.nameEn, toGovernorate: reservation.tripInfo?.dropOffGovernorate?.nameEn, fromCity: reservation.tripInfo?.packupGovernorate?.nameEn, toCity: reservation.tripInfo?.packupCity?.nameEn, brand: reservation.tripInfo?.myCar?.carBrand?.nameEn, model: reservation.tripInfo?.myCar?.carModel?.nameEn, date: reservation.tripInfo?.day, time: reservation.tripInfo?.time, phoneNumber: reservation.driverInfo?.phoneNumber, driverName: reservation.driverInfo?.name, carID: reservation.tripInfo?.myCar?.id, images: reservation.tripInfo?.myCar?.images, status: reservation.tripInfo?.status, reviewStatus: reservation.reviewStatus)
            return ReservationCellViewModel(tripID: reservation.tripID, fromGovernorate: reservation.pickupGovernorate?.nameEn, toGovernorate: reservation.dropoffGovernorate?.nameEn, brand: reservation.carBrand?.nameEn, type: reservation.carType?.nameEn, date: reservation.tripDate, time: reservation.tripTime, phoneNumber: reservation.driverPhoneNumber, driverName: reservation.driverName, carID: reservation.carID, images: reservation.images, status: reservation.status, reviewStatus: reservation.reviewStatus, numberOfPersons: reservation.numberOfPersons)
        } else if "Lang".localized == "ar" {
//            return ReservationCellViewModel(reservationID: reservation.reservationID, tripID: reservation.tripID, fromGovernorate: reservation.tripInfo?.packupGovernorate?.nameAr, toGovernorate: reservation.tripInfo?.dropOffGovernorate?.nameAr, fromCity: reservation.tripInfo?.packupGovernorate?.nameAr, toCity: reservation.tripInfo?.packupCity?.nameAr, brand: reservation.tripInfo?.myCar?.carBrand?.nameAr, model: reservation.tripInfo?.myCar?.carModel?.nameAr, date: reservation.tripInfo?.day, time: reservation.tripInfo?.time, phoneNumber: reservation.driverInfo?.phoneNumber, driverName: reservation.driverInfo?.name, carID: reservation.tripInfo?.myCar?.id, images: reservation.tripInfo?.myCar?.images, status: reservation.tripInfo?.status, reviewStatus: reservation.reviewStatus)
            return ReservationCellViewModel(tripID: reservation.tripID, fromGovernorate: reservation.pickupGovernorate?.nameAr, toGovernorate: reservation.dropoffGovernorate?.nameAr, brand: reservation.carBrand?.nameAr, type: reservation.carType?.nameAr, date: reservation.tripDate, time: reservation.tripTime, phoneNumber: reservation.driverPhoneNumber, driverName: reservation.driverName, carID: reservation.carID, images: reservation.images, status: reservation.status, reviewStatus: reservation.reviewStatus, numberOfPersons: reservation.numberOfPersons)
        }
        return ReservationCellViewModel(tripID: reservation.tripID, fromGovernorate: reservation.pickupGovernorate?.nameEn, toGovernorate: reservation.dropoffGovernorate?.nameEn, brand: reservation.carBrand?.nameEn, type: reservation.carType?.nameEn, date: reservation.tripDate, time: reservation.tripTime, phoneNumber: reservation.driverPhoneNumber, driverName: reservation.driverName, carID: reservation.carID, images: reservation.images, status: reservation.status, reviewStatus: reservation.reviewStatus, numberOfPersons: reservation.numberOfPersons)
    }
    
    func getError() -> APIError {
        return self.error!
    }
    
    func getReservationCellViewModel( at indexPath: IndexPath ) -> ReservationCellViewModel {
        return reservationsCellViewModels[indexPath.row]
    }
    
    func getDeletedBool() -> Bool {
        return self.deleted
    }
    
    func getReservation() -> NewReservation? {
        return self.selectedNewReservation
    }
    
    func reservationPressed( at indexPath: Int ) {
        let reservation = self.newReservations[indexPath]
        
        self.selectedNewReservation = reservation
    }
    
    
}
