//
//  ReservationCellViewModel.swift
//  SadRad
//
//  Created by Omar Ibrahim on 10/22/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation

struct ReservationCellViewModel {
//    let reservationID: Int?
    let tripID: Int?
    //    let image: String?
    let fromGovernorate: String?
    let toGovernorate: String?
//    let fromCity: String?
//    let toCity: String?
    let brand: String?
    let type: String?
//    let model: String?
    let date: String?
    let time: String?
    let phoneNumber: String?
    let driverName: String?
    let carID: Int?
    let images: [String]?
    let status: Int?
    let reviewStatus: Int?
    let numberOfPersons: Int?
}
