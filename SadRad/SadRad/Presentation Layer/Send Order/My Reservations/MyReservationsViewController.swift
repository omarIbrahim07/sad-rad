//
//  MyReservationsViewController.swift
//  SadRad
//
//  Created by Omar Ibrahim on 9/16/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class MyReservationsViewController: BaseViewController {

    lazy var viewModel: MyReservationsViewModel = {
        return MyReservationsViewModel()
    }()
    
    var error: APIError?

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noReservationsLabel: UILabel!
    
    override func viewWillAppear(_ animated: Bool) {
        self.initVM()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setMyReservationsLocalization()
        configureView()
        configureTableView()
        initVM()
    }
    
    func setMyReservationsLocalization() {
        navigationItem.title = "my reservations vc title".localized
    }
    
    func configureView() {
        noReservationsLabel.text = "no reservations label".localized
        noReservationsLabel.isHidden = true
    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "ReservationTableViewCell", bundle: nil), forCellReuseIdentifier: "ReservationTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 20))
    }
    
    func initVM() {
        
        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }
            
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                    
                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                case .error:
                    self.error = self.viewModel.getError()
                    if let error = self.error {
                        self.stopLoadingWithError(error: error)
                    }
                case .empty:
                    print("")
                }
            }
        }
        
        viewModel.reloadTableViewClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
        
        viewModel.deleteReservationClosure = { [weak self] () in
            DispatchQueue.main.async {
                if let deleted: Bool = self?.viewModel.getDeletedBool() {
                    if deleted == true {
                        //                        self?.tableView.reloadData()
                        self?.viewModel.initFetch()
                    }
                }
            }
        }
        
        viewModel.reservationsExistanceStateClosure = { [weak self] () in
            guard let self = self else {
                return
            }
            
            switch self.viewModel.reservationsExistance {
                
            case .noReservationsExisted:
                self.noReservationsLabel.isHidden = false
                self.tableView.isHidden = true
            case .reservationsExisted:
                self.noReservationsLabel.isHidden = true
                self.tableView.isHidden = false
            }
        }
        
        viewModel.initFetch()
    }

    func showCallAlert(phoneNumber: String?) {
        
        guard let userPhone = phoneNumber, let url = URL(string: "telprompt://\(userPhone)") else {
            return
        }
        
        // create the alert
        let alert = UIAlertController(title: "show call alert title".localized, message: userPhone, preferredStyle: UIAlertController.Style.alert)
        // add the actions (buttons)

        alert.addAction(UIAlertAction(title: "show call alert action".localized, style: UIAlertAction.Style.destructive, handler: { action in
            UIApplication.shared.canOpenURL(url)
        }))
        alert.addAction(UIAlertAction(title: "cancel show call alert".localized, style: UIAlertAction.Style.cancel, handler: nil))
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }

    // MARK: - Navigation
    func goToDeleteTrip(tripID: Int, carID: Int) {
        if let cancelReservationVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CancelJourneyViewController") as? CancelJourneyViewController {
            //                rootViewContoller.test = "test String"
            cancelReservationVC.viewModel.tripID = tripID
            cancelReservationVC.viewModel.carID = carID
            self.present(cancelReservationVC, animated: true, completion: nil)
            print("Cancel Journey")
        }
    }
    
    func goToTripDetails(tripID: Int) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "JourneyViewController") as! JourneyViewController
        viewController.viewModel.tripID = tripID
        
        //        navigationController?.present(viewController, animated: true, completion: nil)
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func goToRateTrip(tripID: Int) {
        if let rateTripVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RateViewController") as? RateViewController {
            //                rootViewContoller.test = "test String"
            rateTripVC.viewModel.tripID = tripID
            self.present(rateTripVC, animated: true, completion: nil)
            print("Rate Trip")
        }
    }

}
