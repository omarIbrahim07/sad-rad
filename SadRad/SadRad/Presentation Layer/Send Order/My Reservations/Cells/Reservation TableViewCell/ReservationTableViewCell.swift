//
//  ReservationTableViewCell.swift
//  SadRad
//
//  Created by Omar Ibrahim on 9/16/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

protocol ReservationButtonsDelegate {
    func callDriverButtonIsPressed(driverPhoneNumber: String)
    func deleteReservationButtonIsPressed(reservationID: Int, carID: Int)
    func rateTripButtonIsPressed(tripID: Int)
}

class ReservationTableViewCell: UITableViewCell {
    
    var reservationButtonsDelegate: ReservationButtonsDelegate?
    
    var reservationCellViewModel : ReservationCellViewModel? {
        didSet {
//            if let reservationID: Int = self.reservationCellViewModel?.reservationID {
//                self.tag = reservationID
//                self.cancelTripButton.tag = reservationID
//            }
            if let tripID: Int = self.reservationCellViewModel?.tripID {
                self.cancelTripButton.tag = tripID
            }
            
            if let carID: Int = self.reservationCellViewModel?.carID {
                self.tag = carID
            }
            
            if self.reservationCellViewModel?.images?.count ?? 0 > 0 {
                let carImage = self.reservationCellViewModel?.images?[0]
                reservationImageView.loadImageFromUrl(imageUrl: MY_CARS_IMAGES_URL + carImage!)
            } else {
                reservationImageView.image = UIImage(named: "broken")
            }

//            if let carImage: String = self.reservationCellViewModel?.image {
            //                myCarImageView.loadImageFromUrl(imageUrl: MY_CARS_IMAGES_URL + carImage)
//            }
            if let fromGovernorate: String = self.reservationCellViewModel?.fromGovernorate {
                self.fromValueLabel.text = fromGovernorate
            }
            if let toGovernorate: String = self.reservationCellViewModel?.toGovernorate {
                self.toValueLabel.text = toGovernorate
            }
            if let type: String = self.reservationCellViewModel?.type, let brand: String = self.reservationCellViewModel?.brand {
                self.carBrandLabel.text = type + " " + brand
            }
            if let date: String = self.reservationCellViewModel?.date ,let time: String = self.reservationCellViewModel?.time {
                self.dateLabel.text = date + "   " + time.substring(to: 5)
            }
            if let driverName: String = self.reservationCellViewModel?.driverName {
                self.carDriverNameLabel.text = driverName
            }
            if let reviewStatus: Int = self.reservationCellViewModel?.reviewStatus {
                // Rate is finished
                if reviewStatus == 0 {
                    if let status = self.reservationCellViewModel?.status {
                        if status == 2 {
                            cancelTripLabel.text = "finish button title".localized
                            callImageView.image = UIImage(named: "Icon feather-phonee")
                            cancelTripImageView.image = UIImage(named: "ic_done_all_24px")
                            callImageView.isHidden = false
                            cancelTripImageView.isHidden = false
                            callDriverButton.isUserInteractionEnabled = true
                            cancelTripButton.isUserInteractionEnabled = false
                            phoneView.backgroundColor = #colorLiteral(red: 0.3607843137, green: 0.7764705882, blue: 0.7294117647, alpha: 1)
                            removeView.backgroundColor = #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1)
                        }
                    }
                }
                // Rate button is enabled
                else if reviewStatus == 1 {
                    cancelTripLabel.text = "reservation rate button title".localized
                    callImageView.image = UIImage(named: "Icon feather-phonee")
                    cancelTripImageView.image = UIImage(named: "Path 19")
                    callImageView.isHidden = false
                    cancelTripImageView.isHidden = false
                    callDriverButton.isUserInteractionEnabled = true
                    cancelTripButton.isUserInteractionEnabled = true
                    phoneView.backgroundColor = #colorLiteral(red: 0.3607843137, green: 0.7764705882, blue: 0.7294117647, alpha: 1)
                    removeView.backgroundColor = #colorLiteral(red: 0.3607843137, green: 0.7764705882, blue: 0.7294117647, alpha: 1)
                }
            }
            
            // Trip is not done yet
            if let status: Int = self.reservationCellViewModel?.status {
                if status == 1 {
                    cancelTripLabel.text = "cancel reservation button title".localized
                    callImageView.image = UIImage(named: "Icon feather-phonee")
                    cancelTripImageView.image = UIImage(named: "Icon material-cancel")
                    callImageView.isHidden = false
                    cancelTripImageView.isHidden = false
                    callDriverButton.isUserInteractionEnabled = true
                    cancelTripButton.isUserInteractionEnabled = true
                    phoneView.backgroundColor = #colorLiteral(red: 0.3607843137, green: 0.7764705882, blue: 0.7294117647, alpha: 1)
                    removeView.backgroundColor = #colorLiteral(red: 0.3607843137, green: 0.7764705882, blue: 0.7294117647, alpha: 1)
                }
            }
        }
    }
    
    @IBOutlet weak var reservationImageView: UIImageView!
    @IBOutlet weak var containView: UIView!
    @IBOutlet weak var fromValueLabel: UILabel!
    @IBOutlet weak var toValueLabel: UILabel!
    @IBOutlet weak var carBrandLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var reservedLabel: UILabel!
    @IBOutlet weak var editLabel: UILabel!
    @IBOutlet weak var removeLabel: UILabel!
    @IBOutlet weak var callDriverButton: UIButton!
    @IBOutlet weak var cancelTripButton: UIButton!
    @IBOutlet weak var carDriverNameLabel: UILabel!
    @IBOutlet weak var cancelTripLabel: UILabel!
    @IBOutlet weak var callImageView: UIImageView!
    @IBOutlet weak var cancelTripImageView: UIImageView!
    @IBOutlet weak var phoneView: UIView!
    @IBOutlet weak var removeView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureView()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configureView() {
        selectionStyle = .none
        cancelTripLabel.text = "cancel reservation button title".localized
        containView.addCornerRadius(raduis: 20, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
    }
    
    func callDriverButtonIsPressed(driverPhoneNumber: String) {
        if let delegateValue = reservationButtonsDelegate {
            delegateValue.callDriverButtonIsPressed(driverPhoneNumber: driverPhoneNumber)
        }
    }
    
    func rateTripButtonIsPressed(tripID: Int) {
        if let delegateValue = reservationButtonsDelegate {
            delegateValue.rateTripButtonIsPressed(tripID: tripID)
        }
    }
    
    func deleteReservationButtonIsPressed(reservationID: Int, carID: Int) {
        if let delegateValue = reservationButtonsDelegate {
            delegateValue.deleteReservationButtonIsPressed(reservationID: reservationID, carID: carID)
        }
    }
    
    @IBAction func callDriverButtonIsPressed(_ sender: Any) {
        if let phoneNumber: String = self.reservationCellViewModel?.phoneNumber {
            callDriverButtonIsPressed(driverPhoneNumber: phoneNumber)
        }
    }
    
    @IBAction func removeReservationButtonIsPressed(_ sender: Any) {
        if self.reservationCellViewModel?.reviewStatus == 1 {
            rateTripButtonIsPressed(tripID: self.cancelTripButton.tag)
            return
        }
        deleteReservationButtonIsPressed(reservationID: self.cancelTripButton.tag, carID: self.tag)
    }
    
}
