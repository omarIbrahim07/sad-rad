//
//  AddCarImagesViewController.swift
//  SadRad
//
//  Created by Omar Ibrahim on 11/18/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit
import AssetsPickerViewController
import Toast_Swift
import PKHUD
import Photos

protocol SendArrayOfImagesDataDelegate {
    func sendArrayOfImagesData(imagesArrayData: [Data])
}

class AddCarImagesViewController: CommonExampleController {

    var sendArrayOfImagesDataDelegate: SendArrayOfImagesDataDelegate?

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureNavigationBar()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
    }
    
    func configureView() {
        navigationItem.title = "Add images"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(saveAction))
    }
    
    //MARK:- Configurations
    func configureNavigationBar() {
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(saveAction))
    }
    
    //MARK:- Actions
    @objc fileprivate func saveAction() {
        saveAdPictures()
    }
    
    override func pressedPick(_ sender: Any) {
        let picker = AssetsPickerViewController()
        picker.pickerDelegate = self
        present(picker, animated: true, completion: nil)
    }
    
    //MARK:- API Calls
    func saveAdPictures() {
        
        var imageDataArr = [Data]()
        for asset in assets {
            let assetImage = getAssetThumbnail(asset: asset)
            let imgData = assetImage.jpegData(compressionQuality: 1)
            imageDataArr.append(imgData!)
        }
        
        if imageDataArr.count > 0 {
            self.sendArrayOfImagesDataDelegate?.sendArrayOfImagesData(imagesArrayData: imageDataArr)
            self.dismiss(animated: true, completion: nil)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }

    // MARK: - Navigation

    
    //MARK:- Helpers
    func showError(error: APIError) {
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
        var style: ToastStyle = ToastStyle.init()
        style.imageSize = CGSize(width: 44, height: 44)
        self.view.makeToast(error.message, duration: 3.0, position: .bottom, title: nil, image: nil, style: style, completion: nil)
    }
    
    func startLoading() {
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
        HUD.show(.progress)
    }
    
    func stopLoadingWithSuccess() {
        HUD.hide()
    }
    
    func stopLoadingWithError(error: APIError) {
        HUD.hide()
        showError(error: error)
    }
    
    func getAssetThumbnail(asset: PHAsset) -> UIImage {
        let manager = PHImageManager.default()
        let option = PHImageRequestOptions()
        var thumbnail = UIImage()
        option.isSynchronous = true
        manager.requestImage(for: asset, targetSize: CGSize(width: 100, height: 100), contentMode: .aspectFit, options: option, resultHandler: {(result, info)->Void in
            thumbnail = result!
        })
        return thumbnail
    }

}
