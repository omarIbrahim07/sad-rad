//
//  CarTypeViewModel.swift
//  SadRad
//
//  Created by Omar Ibrahim on 10/12/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation

struct CarTypeViewModel {
    let id: Int?
    let nameAr: String?
    let nameEn: String?
}
