//
//  AddCarViewModel.swift
//  SadRad
//
//  Created by Omar Ibrahim on 10/12/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation

class AddCarViewModel {
    
    var error: APIError?
        
    var updateLoadingStatus: (()->())?
    var chooseCarTypeClosure: (()->())?
    var chooseCarBrandClosure: (()->())?
    var chooseCarModelClosure: (()->())?
    var chooseCarConditioningClosure: (()->())?
    var updateMyCarInfoClosure: (()->())?
    var myCarIsAddedClosure: (()->())?
    var myCarIsUpdatedClosure: (()->())?
    
    // callback for interfaces
    var carsTypes: [CarType]?
    var carsModels: [CarModel]?
    var carsBrands: [CarBrand]?
    
    var choosedCarType: CarType? {
        didSet {
            self.chooseCarTypeClosure?()
        }
    }
    
    var choosedCarBrand: CarBrand? {
        didSet {
            self.chooseCarBrandClosure?()
        }
    }
    
    var choosedCarModel: CarModel? {
        didSet {
            self.chooseCarModelClosure?()
        }
    }
    
    var myCarInfo: MyCar? {
        didSet {
            self.updateMyCarInfoClosure?()
        }
    }
    
    var choosedCarConditioning: Bool? = false {
        didSet {
            self.chooseCarConditioningClosure?()
        }
    }
    
    var myCarIsAdded: Bool? = false {
        didSet {
            self.myCarIsAddedClosure?()
        }
    }
    
    var myCarIsUpdated: Bool? = false {
        didSet {
            self.myCarIsUpdatedClosure?()
        }
    }
    
    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    func fetchCarTypes() {
        state = .loading
        
        let params: [String : AnyObject] = [:]
        
        CarsAPIManager().getCarsTypes(basicDictionary: params, onSuccess: { (carsTypes) in

        self.carsTypes = carsTypes // Cache
        self.state = .populated

        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    func fetchCarBrands() {
        state = .loading
        
        let params: [String : AnyObject] = [:]
        
        CarsAPIManager().getCarsBrands(basicDictionary: params, onSuccess: { (carsBrands) in

        self.carsBrands = carsBrands // Cache
        self.state = .populated

        }) { (error) in
            self.error = error
            self.state = .error
        }
    }

    func fetchCarModels() {
        state = .loading
        
        let params: [String : AnyObject] = [:]
        
        CarsAPIManager().getCarsModels(basicDictionary: params, onSuccess: { (carsModels) in

        self.carsModels = carsModels // Cache
        self.state = .populated

        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    func fetchMyCarInformation(carID: Int) {
        state = .loading
        
        let params: [String : AnyObject] = [
            "car_id" : carID as AnyObject
        ]
        
        CarsAPIManager().getMyCarInfo(basicDictionary: params, onSuccess: { (carInfo) in

        self.myCarInfo = carInfo // Cache
        self.state = .populated

        }) { (error) in
            self.error = error
            self.state = .error
        }
    }

    func changeCarConditioning() {
        self.choosedCarConditioning = !self.choosedCarConditioning!
    }
    
    func addCar(imagesDataArray: [Data]?, edit: Bool, carID: Int?, status: Int, typeCarId: Int, airConditioning: Bool, numberOfPersons: String, maximumWeight: String?, model: String, brand: String, userID: Int, brandID: Int, modelID: Int) {
                
        let params: [String : AnyObject] = [
            "status" : status as AnyObject,
            "type_carid" : typeCarId as AnyObject,
            "Aircondtion" : airConditioning as AnyObject,
            "number_of_persons" : numberOfPersons as AnyObject,
            "maximum_weight" : maximumWeight as AnyObject,
            "model" : model as AnyObject,
            "brand_car" : brand as AnyObject,
            "user_id" : userID as AnyObject,
            "brand_id" : brandID as AnyObject,
            "model_id" : modelID as AnyObject,
            "car_id" : carID as AnyObject
        ]
                
        self.state = .loading
        
        if edit == false {
            CarsAPIManager().addMyCar(nameOfArray: "images[]", imagesDataArray: imagesDataArray, basicDictionary: params, onSuccess: { (saved) in
                if saved == true {
                    print("Success")
                    self.myCarIsAdded = true
                    self.state = .populated
                } else {
                    self.state = .empty
                }
            }) { (error) in
                self.error = error
                self.state = .error
            }
        } else if edit == true {
            CarsAPIManager().updateMyCar(nameOfArray: "images[]", imagesDataArray: imagesDataArray, basicDictionary: params, onSuccess: { (saved) in
                if saved == true {
                    print("Success")
                    self.myCarIsUpdated = true
                    self.state = .populated
                } else {
                    self.state = .empty
                }
            }) { (error) in
                self.error = error
                self.state = .error
            }
        }
    }
    
    func getCarsTypes() -> [CarType] {
        return carsTypes!
    }
    
    func userChoossedCarType(carType: CarType) {
        self.choosedCarType = carType
    }
    
    func getCarsBrands() -> [CarBrand] {
        return carsBrands!
    }
    
    func userChoossedCarBrand(carBrand: CarBrand) {
        self.choosedCarBrand = carBrand
    }

    func getCarsModels() -> [CarModel] {
        return carsModels!
    }
    
    func userChoossedCarModel(carModel: CarModel) {
        self.choosedCarModel = carModel
    }
    
    func getCarConditioningBool() -> Bool {
        return choosedCarConditioning!
    }
    
    func getCarInfo() -> MyCar {
        return myCarInfo!
    }
    
    func getError() -> APIError {
        return self.error!
    }
}
