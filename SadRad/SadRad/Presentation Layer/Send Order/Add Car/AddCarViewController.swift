//
//  AddCarViewController.swift
//  SadRad
//
//  Created by Omar Ibrahim on 9/9/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit
import AssetsPickerViewController
import Photos

class AddCarViewController: BaseViewController {

    lazy var viewModel: AddCarViewModel = {
        return AddCarViewModel()
    }()

    var error: APIError?
    var editBool: Bool? = false
    var carID: Int?
    var imagesDataArray: [Data] = [Data]()

    @IBOutlet weak var carTypeView: UIView!
    @IBOutlet weak var brandView: UIView!
    @IBOutlet weak var modelView: UIView!
    @IBOutlet weak var numberOfPersonsView: UIView!
    @IBOutlet weak var weightView: UIView!
    @IBOutlet weak var airConditionerView: UIView!
    @IBOutlet weak var addCarButton: UIButton!
    @IBOutlet weak var carTypeLabel: UILabel!
    @IBOutlet weak var brandValueLabel: UILabel!
    @IBOutlet weak var modelValueLabel: UILabel!
    @IBOutlet weak var maximumWeightTextfield: UITextField!
    @IBOutlet weak var numberOfPersonsTextfield: UITextField!
    @IBOutlet weak var airConditioningImageView: UIImageView!
    @IBOutlet weak var imagesStackViewHeight: NSLayoutConstraint!
    @IBOutlet weak var firstImageView: UIImageView!
    @IBOutlet weak var secondImageView: UIImageView!
    @IBOutlet weak var thirdImageView: UIImageView!
    @IBOutlet weak var fourthImageView: UIImageView!
    @IBOutlet weak var personLabel: UILabel!
    @IBOutlet weak var kgmLabel: UILabel!
    @IBOutlet weak var airConditionerLabel: UILabel!
    @IBOutlet weak var addImagesFromPhoneLabel: UILabel!
    @IBOutlet weak var maximumWightView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setNavigationTitle()
        setLocalization()
        configureView()
        closeKeypad()
        initVM()
    }
    
    func setNavigationTitle() {
        if editBool == true {
            navigationItem.title = "update car vc title".localized
        } else {
            navigationItem.title = "add car vc title".localized
        }
    }

    func configureView() {
        addCarButton.addCornerRadius(raduis: 8, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        carTypeView.addCornerRadius(raduis: 8, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        brandView.addCornerRadius(raduis: 8, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        numberOfPersonsView.addCornerRadius(raduis: 8, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        modelView.addCornerRadius(raduis: 8, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        weightView.addCornerRadius(raduis: 8, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        airConditionerView.addCornerRadius(raduis: 8, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        self.imagesStackViewHeight.constant = 100
        self.firstImageView.addCornerRadius(raduis: 8, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        self.secondImageView.addCornerRadius(raduis: 8, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        self.thirdImageView.addCornerRadius(raduis: 8, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        self.fourthImageView.addCornerRadius(raduis: 8, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)

        if "Lang".localized == "en" {
            numberOfPersonsTextfield.textAlignment = .left
            maximumWeightTextfield.textAlignment = .left
        } else if "Lang".localized == "ar" {
            numberOfPersonsTextfield.textAlignment = .right
            maximumWeightTextfield.textAlignment = .right
        }
        
        // Hide maximum weight firstly
        maximumWightView.isHidden = true
        
        maximumWeightTextfield.delegate = self
        numberOfPersonsTextfield.delegate = self
    }
    
    private func setLocalization() {
        carTypeLabel.text = "add car car type title".localized
        brandValueLabel.text = "add car brand title".localized
        modelValueLabel.text = "add car model title".localized
        numberOfPersonsTextfield.placeholder = "add car number of persons title".localized
        personLabel.text = "add car persons title".localized
        maximumWeightTextfield.placeholder = "add car maximum weight title".localized
        kgmLabel.text = "add car kgm title".localized
        airConditionerLabel.text = "add car air conditioner title".localized
        addImagesFromPhoneLabel.text = "add car add images from phone".localized
    }
    
    private func setMaximumWeightVision(carType: CarType) {
        if carType.hasMaximumWeight == 0 {
            maximumWeightTextfield.text = nil
            maximumWightView.isHidden = true
        } else if carType.hasMaximumWeight == 1 {
            maximumWightView.isHidden = false
        }
    }
    
    // MARK:- Init View Model
    func initVM() {
        
        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                
                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                case .error:
                    self.error = self.viewModel.getError()
                    if let error = self.error {
//                        self.showError(error: error)
                        self.stopLoadingWithError(error: error)
                    }
                case .empty:
                    print("")
                }
            }
        }
        
        viewModel.chooseCarTypeClosure = { [weak self] () in
            DispatchQueue.main.async { [weak self] in
                if let choosedCarType: CarType = self?.viewModel.choosedCarType {
                    self?.bindCarType(carType: choosedCarType)
                    self?.bindRequiredFields(carType: choosedCarType)
                }
            }
        }
        
        viewModel.chooseCarBrandClosure = { [weak self] () in
            DispatchQueue.main.async { [weak self] in
                if let choosedCarBrand: CarBrand = self?.viewModel.choosedCarBrand {
                    self?.bindCarBrand(carBrand: choosedCarBrand)
                }
            }
        }

        viewModel.chooseCarModelClosure = { [weak self] () in
            DispatchQueue.main.async { [weak self] in
                if let choosedCarModel: CarModel = self?.viewModel.choosedCarModel {
                    self?.bindCarModel(carModel: choosedCarModel)
                }
            }
        }
        
        viewModel.chooseCarConditioningClosure = { [weak self] () in
            DispatchQueue.main.async { [weak self] in
                if let choosedCarConditioningBool: Bool = self?.viewModel.getCarConditioningBool() {
                    self?.bindCarConditioningState(carConditioningBool: choosedCarConditioningBool)
                }
            }
        }
        
        viewModel.updateMyCarInfoClosure = { [weak self] () in
            DispatchQueue.main.async { [weak self] in
                if let myCarInfo: MyCar = self?.viewModel.getCarInfo() {
                    self?.bindMyCarInfo(myCarInfo: myCarInfo)
                }
            }
        }
        
        viewModel.myCarIsAddedClosure = { [weak self] () in
            DispatchQueue.main.async { [weak self] in
                if let isSaved: Bool = self?.viewModel.myCarIsAdded {
                    if isSaved == true {
                        print("7obbbbbbby")
                        self?.showCarPopUp(title: "add car alert title".localized, message: "add car alert message".localized, actionTitle: "done action title".localized)
                    } else {
                        print("5eeba")
                    }
                }
            }
        }
        
        viewModel.myCarIsUpdatedClosure = { [weak self] () in
            DispatchQueue.main.async { [weak self] in
                if let isSaved: Bool = self?.viewModel.myCarIsUpdated {
                    if isSaved == true {
                        print("7obbbbbbby")
                        self?.showCarPopUp(title: "update car alert title".localized, message: "update car alert message".localized, actionTitle: "done action title".localized)
                    } else {
                        print("5eeba")
                    }
                }
            }
        }
                
        viewModel.fetchCarTypes()
        viewModel.fetchCarBrands()
        viewModel.fetchCarModels()
        
        if self.editBool == true {
            addCarButton.setTitle("update car vc title".localized, for: .normal)
            if let carID: Int = self.carID {
                self.viewModel.fetchMyCarInformation(carID: carID)
            }
        } else {
            addCarButton.setTitle("add car vc title".localized, for: .normal)
        }
    }
    
    func showCarPopUp(title: String, message: String, actionTitle: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        //        let cancelAction = UIAlertAction(title: "إلغاء", style: .cancel, handler: nil)
        //        alertController.addAction(cancelAction)
        let openAction = UIAlertAction(title: actionTitle, style: .default) { (action) in
            self.presentMyHomeScreen()
        }
        //        let cancelAction = UIAlertAction(title: "cancel go to login".localized, style: .cancel)
        alertController.addAction(openAction)
        //        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    // MARK:- Show Time Options Picker
    func setupCarsTypes(carTypes: [CarType]) {
        
        let sheet = UIAlertController(title: "car type alert title".localized, message: "city alert message".localized, preferredStyle: .actionSheet)
        for carType in carTypes {
            if "Lang".localized == "ar" {
                sheet.addAction(UIAlertAction(title: carType.nameAr, style: .default, handler: {_ in
                    self.viewModel.userChoossedCarType(carType: carType)
                }))
            } else if "Lang".localized == "en" {
                sheet.addAction(UIAlertAction(title: carType.nameEn, style: .default, handler: {_ in
                    self.viewModel.userChoossedCarType(carType: carType)
                }))
            }
        }
        sheet.addAction(UIAlertAction(title: "cancel city".localized, style: .cancel, handler: nil))
        
        //        sheet.popoverPresentationController?.sourceView = sortChoiceLabel
        
        self.present(sheet, animated: true, completion: nil)
    }
    
    func setupCarsBrands(carBrands: [CarBrand]) {
        
        let sheet = UIAlertController(title: "car brand alert title".localized, message: "car brand alert message".localized, preferredStyle: .actionSheet)
        for carBrand in carBrands {
            if "Lang".localized == "ar" {
                sheet.addAction(UIAlertAction(title: carBrand.nameAr, style: .default, handler: {_ in
                    self.viewModel.userChoossedCarBrand(carBrand: carBrand)
                }))
            } else if "Lang".localized == "en" {
                sheet.addAction(UIAlertAction(title: carBrand.nameEn, style: .default, handler: {_ in
                    self.viewModel.userChoossedCarBrand(carBrand: carBrand)
                }))
            }
        }
        sheet.addAction(UIAlertAction(title: "cancel car brand".localized, style: .cancel, handler: nil))
        
        //        sheet.popoverPresentationController?.sourceView = sortChoiceLabel
        
        self.present(sheet, animated: true, completion: nil)
    }

    func setupCarsModels(carModels: [CarModel]) {
        
        let sheet = UIAlertController(title: "car model alert title".localized, message: "car model alert message".localized, preferredStyle: .actionSheet)
        for carModel in carModels {
            if "Lang".localized == "ar" {
                sheet.addAction(UIAlertAction(title: carModel.nameAr, style: .default, handler: {_ in
                    self.viewModel.userChoossedCarModel(carModel: carModel)
                }))
            } else if "Lang".localized == "en" {
                sheet.addAction(UIAlertAction(title: carModel.nameEn, style: .default, handler: {_ in
                    self.viewModel.userChoossedCarModel(carModel: carModel)
                }))
            }
        }
        sheet.addAction(UIAlertAction(title: "cancel car model".localized, style: .cancel, handler: nil))
        
        //        sheet.popoverPresentationController?.sourceView = sortChoiceLabel
        
        self.present(sheet, animated: true, completion: nil)
    }

    
    func bindCarType(carType: CarType) {
        if "Lang".localized == "ar" {
            self.carTypeLabel.text = carType.nameAr
        } else if "Lang".localized == "en" {
            self.carTypeLabel.text = carType.nameEn
        }
        self.setMaximumWeightVision(carType: carType)
    }
    
    func bindCarBrand(carBrand: CarBrand) {
        if "Lang".localized == "ar" {
            self.brandValueLabel.text = carBrand.nameAr
        } else if "Lang".localized == "en" {
            self.brandValueLabel.text = carBrand.nameEn
        }
    }

    func bindCarModel(carModel: CarModel) {
        if "Lang".localized == "ar" {
            self.modelValueLabel.text = carModel.nameAr
        } else if "Lang".localized == "en" {
            self.modelValueLabel.text = carModel.nameEn
        }
    }
    
    func bindRequiredFields(carType: CarType) {
        if carType.statusRequired == "1" {
//            maximumWeightStarImageView.isHidden = true
//            numberOfPersonsStarImageView.isHidden = false
        } else if carType.statusRequired == "2" {
//            maximumWeightStarImageView.isHidden = false
//            numberOfPersonsStarImageView.isHidden = true
        }
    }
    
    func bindCarConditioningState(carConditioningBool: Bool) {
        if carConditioningBool == true {
            airConditioningImageView.image = UIImage(named: "Checked")
        } else if carConditioningBool == false {
            airConditioningImageView.image = UIImage(named: "UnChecked")
        }
    }
    
    func bindMyCarInfo(myCarInfo: MyCar) {
        if let carType: CarType = myCarInfo.carType {
            self.viewModel.choosedCarType = carType
            self.setMaximumWeightVision(carType: carType)
        }
        if let carModel: CarModel = myCarInfo.carModel {
            self.viewModel.choosedCarModel = carModel
        }
        if let carBrand: CarBrand = myCarInfo.carBrand {
            self.viewModel.choosedCarBrand = carBrand
        }
        if let maximumWeight: Int = myCarInfo.maximimWeight {
            self.maximumWeightTextfield.text = String(maximumWeight)
        }
        if let numberOfPersons: Int = myCarInfo.numberOfPersons {
            self.numberOfPersonsTextfield.text = String(numberOfPersons)
        }
        if let airConditiong: Bool = myCarInfo.airConditioned {
            if airConditiong == true {
                self.viewModel.choosedCarConditioning = true
            } else if airConditiong == false {
                self.viewModel.choosedCarConditioning = false
            }
        }
        if "Lang".localized == "en" {
            if let carTypeName: String = myCarInfo.carType?.nameEn {
                self.carTypeLabel.text = carTypeName
            }
            if let carBrandName: String = myCarInfo.carBrand?.nameEn {
                self.brandValueLabel.text = carBrandName
            }
        } else if "Lang".localized == "ar" {
            if let carTypeName: String = myCarInfo.carType?.nameAr {
                self.carTypeLabel.text = carTypeName
            }
            if let carBrandName: String = myCarInfo.carBrand?.nameAr {
                self.brandValueLabel.text = carBrandName
            }
        }
        if myCarInfo.images?.count ?? 0 > 0 {
            self.imagesStackViewHeight.constant = 100
            if myCarInfo.images!.count >= 1 {
                firstImageView.loadImageFromUrl(imageUrl: MY_CARS_IMAGES_URL + (myCarInfo.images?[0])!)
            }
            if myCarInfo.images!.count >= 2 {
                secondImageView.loadImageFromUrl(imageUrl: MY_CARS_IMAGES_URL + (myCarInfo.images?[1])!)
            }
            if myCarInfo.images!.count >= 3 {
                thirdImageView.loadImageFromUrl(imageUrl: MY_CARS_IMAGES_URL + (myCarInfo.images?[2])!)
            }
            if myCarInfo.images!.count == 4 {
                fourthImageView.loadImageFromUrl(imageUrl: MY_CARS_IMAGES_URL + (myCarInfo.images?[3])!)
            }
        }
    }
        
    func closeKeypad() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc func viewTapped() {
        numberOfPersonsTextfield.endEditing(true)
        maximumWeightTextfield.endEditing(true)
    }
    
    func addCar(imagesDataArray: [Data]?) {
        
        guard let typeCarID: Int = self.viewModel.choosedCarType?.id, typeCarID > 0 else {
            let apiError = APIError()
            apiError.message = "يرجى إدخال نوع العربية"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        guard let carBrand: String = self.viewModel.choosedCarBrand?.nameEn, carBrand.count > 0 else {
            let apiError = APIError()
            apiError.message = "يرجى إدخال ماركة العربية"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        guard let carBrandID: Int = self.viewModel.choosedCarBrand?.id, carBrandID > 0 else {
            let apiError = APIError()
            apiError.message = "يرجى إدخال ماركة العربية"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        guard let carModel: String = self.viewModel.choosedCarBrand?.nameEn, carModel.count > 0 else {
            let apiError = APIError()
            apiError.message = "يرجى إدخال موديل العربية"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        guard let carModelID: Int = self.viewModel.choosedCarModel?.id, carModelID > 0 else {
            let apiError = APIError()
            apiError.message = "يرجى إدخال موديل العربية"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
//        if self.viewModel.choosedCarType?.hasMaximumWeight == 0 {
            guard let carNumberOfPersons: String = self.numberOfPersonsTextfield.text, carNumberOfPersons.count > 0 else {
                let apiError = APIError()
                apiError.message = "يرجى إدخال عدد الركاب في المركبة"
                self.viewModel.error = apiError
                self.viewModel.state = .error
                return
            }
//        }
//    else if self.viewModel.choosedCarType?.hasMaximumWeight == 1 {
        if self.viewModel.choosedCarType?.hasMaximumWeight == 1 {
            guard let carMaximumWeight: String = self.maximumWeightTextfield.text, carMaximumWeight.count > 0 else {
                let apiError = APIError()
                apiError.message = "يرجى إدخال أقصى وزن تتحمله المركبة"
                self.viewModel.error = apiError
                self.viewModel.state = .error
                return
            }
        }
               
        guard let airConditioning: Bool = self.viewModel.choosedCarConditioning, airConditioning == true || airConditioning == false else {
            let apiError = APIError()
            apiError.message = "يرجى إدخال حالة التكييف"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }

        if editBool == false {
            self.viewModel.addCar(imagesDataArray: self.imagesDataArray, edit: false, carID: nil, status: 0, typeCarId: typeCarID, airConditioning: airConditioning, numberOfPersons: self.numberOfPersonsTextfield.text!, maximumWeight: self.maximumWeightTextfield.text, model: carModel, brand: carBrand, userID: UserDefaultManager.shared.currentUser?.id ?? 0, brandID: carBrandID, modelID: carModelID)
        } else if editBool == true {
            guard let carID: Int = self.carID, carID > 0 else {
                let apiError = APIError()
                apiError.message = "يرجى إدخال العربية"
                self.viewModel.error = apiError
                self.viewModel.state = .error
                return
            }
            self.viewModel.addCar(imagesDataArray: self.imagesDataArray, edit: true, carID: carID, status: 0, typeCarId: typeCarID, airConditioning: airConditioning, numberOfPersons: self.numberOfPersonsTextfield.text!, maximumWeight: self.maximumWeightTextfield.text, model: carModel, brand: carBrand, userID: UserDefaultManager.shared.currentUser?.id ?? 0, brandID: carBrandID, modelID: carModelID)
        }
    }

    
    // MARK: - Navigation
    func goToAddImages() {
        let picker = AssetsPickerViewController()
        picker.pickerDelegate = self
        present(picker, animated: true, completion: nil)
    }
        
    // MARK:- Actions
    @IBAction func airConditionedButtonIsPressed(_ sender: Any) {
        self.viewModel.changeCarConditioning()
    }
    
    @IBAction func carsTypesButtonIsPressed(_ sender: Any) {
        if let carsTypes: [CarType] = self.viewModel.getCarsTypes() {
            if carsTypes.count > 0 {
                self.setupCarsTypes(carTypes: carsTypes)
            }
        }
    }
    
    @IBAction func carsBrandsButtonIsPressed(_ sender: Any) {
        if let carsBrands: [CarBrand] = self.viewModel.getCarsBrands() {
            if carsBrands.count > 0 {
                self.setupCarsBrands(carBrands: carsBrands)
            }
        }
    }

    @IBAction func carsModelsButtonIsPressed(_ sender: Any) {
        if let carsModels: [CarModel] = self.viewModel.getCarsModels() {
            if carsModels.count > 0 {
                self.setupCarsModels(carModels: carsModels)
            }
        }
    }
    
    @IBAction func addCarButtonIsPressed(_ sender: Any) {
        self.addCar(imagesDataArray: self.imagesDataArray)
    }
    
    @IBAction func addCarImagesButtonIsPressed(_ sender: Any) {
        self.goToAddImages()
    }
    
    func getAssetThumbnail(asset: PHAsset) -> UIImage {
        let manager = PHImageManager.default()
        let option = PHImageRequestOptions()
        var thumbnail = UIImage()
        option.isSynchronous = true
        manager.requestImage(for: asset, targetSize: CGSize(width: 100, height: 100), contentMode: .aspectFit, options: option, resultHandler: {(result, info)->Void in
            thumbnail = result!
        })
        return thumbnail
    }
    
    func appendImages(assets: [PHAsset]) {
        self.imagesStackViewHeight.constant = 100
        if assets.count >= 1 {
            firstImageView.image = getAssetThumbnail(asset: assets[0])
        }
        if assets.count >= 2 {
            secondImageView.image = getAssetThumbnail(asset: assets[1])
        }
        if assets.count >= 3 {
            thirdImageView.image = getAssetThumbnail(asset: assets[2])
        }
        if assets.count == 4 {
            fourthImageView.image = getAssetThumbnail(asset: assets[3])
        }
    }
    
    func deleteAllImages() {
        firstImageView.backgroundColor = .clear
        secondImageView.backgroundColor = .clear
        thirdImageView.backgroundColor = .clear
        fourthImageView.backgroundColor = .clear
        firstImageView.image = nil
        secondImageView.image = nil
        thirdImageView.image = nil
        fourthImageView.image = nil
    }
    
}

extension AddCarViewController: SendArrayOfImagesDataDelegate {
    func sendArrayOfImagesData(imagesArrayData: [Data]) {
        print(imagesArrayData)
        self.imagesDataArray = imagesArrayData
    }
}

// to handle
extension AddCarViewController: AssetsPickerViewControllerDelegate {
    
    func assetsPickerCannotAccessPhotoLibrary(controller: AssetsPickerViewController) {}
    func assetsPickerDidCancel(controller: AssetsPickerViewController) {}
    func assetsPicker(controller: AssetsPickerViewController, selected assets: [PHAsset]) {
        // do your job with selected assets
        for asset in assets {
            let assetImage = getAssetThumbnail(asset: asset)
            let imgData = assetImage.jpegData(compressionQuality: 1)
            self.imagesDataArray.append(imgData!)
        }
        self.deleteAllImages()
        self.appendImages(assets: assets)
        print(self.imagesDataArray)
    }
    func assetsPicker(controller: AssetsPickerViewController, shouldSelect asset: PHAsset, at indexPath: IndexPath) -> Bool {
        if controller.selectedAssets.count > 3 {
            // do your job here
            return false
        }
        return true
    }
    func assetsPicker(controller: AssetsPickerViewController, didSelect asset: PHAsset, at indexPath: IndexPath) {
        print("Hoa")
    }
    func assetsPicker(controller: AssetsPickerViewController, shouldDeselect asset: PHAsset, at indexPath: IndexPath) -> Bool {
        return true
    }
    func assetsPicker(controller: AssetsPickerViewController, didDeselect asset: PHAsset, at indexPath: IndexPath) {}
    
}

extension AddCarViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //For numers
        if textField == numberOfPersonsTextfield || textField == maximumWeightTextfield {
            let allowedCharacters = CharacterSet(charactersIn:"0123456789")//Here change this characters based on your requirement
            let characterSet = CharacterSet(charactersIn: string)
            return allowedCharacters.isSuperset(of: characterSet)
        }
        return true
    }
}
