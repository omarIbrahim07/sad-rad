//
//  LogoutTableViewCell.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 1/1/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class LogoutTableViewCell: UITableViewCell {

    @IBOutlet weak var logoutLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
    }
    
}
