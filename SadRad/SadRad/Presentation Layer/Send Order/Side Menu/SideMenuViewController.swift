//
//  SideMenuViewController.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 10/10/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit

class SideMenuViewController: UIViewController {
    
    let menuArray: [String] = [
        "Main",
        "Maintenance orders",
        "Offers",
        "Bids",
        "List of chats",
        "My Account",
        "Saftey procedures",
        "Share your experience",
        "Terms and conditions",
        "Promo code",
        "Complains",
        "Contact us",
        "About app",
//        "Block list",
    ]
    
    let arabicMenuArray: [String] = [
        "الرئيسية",
        "طلبات الصيانة",
        "العروض",
        "طلبات التسعير",
        "قائمة المحادثات",
        "حسابي",
        "إجراءات السلامة",
        "شارك تجربتك",
        "الشروط والأحكام",
        "الكود الترويجي",
        "شكاوى",
        "تواصل معنا",
        "حول التطبيق",
//        "قائمة المحظورين"
    ]
    
//    let menuImages: [String] = [
//        "home",
//        "improvement",
//        "offer",
//        "offer",
//        "chat-bubble",
//        "man-user",
//        "report-symbol",
//        "share",
//        "term",
//        "percentage",
//        "complaint",
//        "telephone",
//        "15579",
//    ]
    
    let menuImages: [String] = [
        "main",
        "orders",
        "offers_icon",
        "bid",
        "chats",
        "my_acc",
        "safety",
        "share",
        "terms",
        "offers",
        "report",
        "contact_us",
        "about_us",
//        "about_us",
    ]


    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
        configureMenuTableViewCell()
    }
    
    func configureView() {
        
    }
    
    func configureMenuTableViewCell() {
        tableView.register(UINib(nibName: "MenuTableViewCell", bundle: nil), forCellReuseIdentifier: "MenuTableViewCell")
        tableView.register(UINib(nibName: "InfoTableViewCell", bundle: nil), forCellReuseIdentifier: "InfoTableViewCell")
        tableView.register(UINib(nibName: "LogoutTableViewCell", bundle: nil), forCellReuseIdentifier: "LogoutTableViewCell")
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    func showCantLoginPopUp() {
        let alertController = UIAlertController(title: "go to login title".localized, message: "go to login alert message".localized, preferredStyle: .alert)
        //        let cancelAction = UIAlertAction(title: "إلغاء", style: .cancel, handler: nil)
        //        alertController.addAction(cancelAction)
        let openAction = UIAlertAction(title: "go to login button".localized, style: .default) { (action) in
            self.goToLogIn()
        }
        let cancelAction = UIAlertAction(title: "cancel go to login".localized, style: .cancel)
        alertController.addAction(openAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    // MARK:- Navigation
        
    func goToMyAccount() {
        if let myAccount = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "MyAccountViewController") as? MyAccountViewController {
            //                rootViewContoller.test = "test String"
            self.present(myAccount, animated: true, completion: nil)
            print("My Account")
        }
    }
    
//    func goToSafteyProcedures() {
//        if let safteyProceduresVC = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "SafteyProceduresViewController") as? SafteyProceduresViewController {
//            //                rootViewContoller.test = "test String"
//            self.present(safteyProceduresVC, animated: true, completion: nil)
//            print("Saftey Procedures")
//        }
//    }
    
//    func goToTermsAndConditions() {
//        if let termsAndConditionsVC = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "TermsAndConditionsViewController") as? TermsAndConditionsViewController {
//            //                rootViewContoller.test = "test String"
//            self.present(termsAndConditionsVC, animated: true, completion: nil)
//            print("Terms and conditions")
//        }
//    }
//
//    func goToComplainsAndSuggestions() {
//        if let complainsAndSuggstionsVC = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "ComplainsAndSuggestionsViewController") as? ComplainsAndSuggestionsViewController {
//            //                rootViewContoller.test = "test String"
//            self.present(complainsAndSuggstionsVC, animated: true, completion: nil)
//            print("Terms and conditions")
//        }
//    }
//
//    func goToAboutApp() {
//        if let complainsAndSuggstionsVC = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "AboutAppViewController") as? AboutAppViewController {
//            //                rootViewContoller.test = "test String"
//            self.present(complainsAndSuggstionsVC, animated: true, completion: nil)
//            print("About App")
//        }
//    }
    
    func logout() {
                
        AuthenticationAPIManager().logout(basicDictionary: [:], onSuccess: { (message) -> String in
            
            UserDefaultManager.shared.currentUser = nil
            self.goToLogIn()
            
            return "70bby"
            
        }) { (error) in
            print(error)
        }
    }
    
    func goToLogIn() {
        if let signInNavigationController = storyboard?.instantiateViewController(withIdentifier: "LoginNavigationVC") as? UINavigationController, let loginViewController = signInNavigationController.viewControllers[0] as? LoginViewController {

            UIApplication.shared.keyWindow?.rootViewController = signInNavigationController
        }
    }

    func goToImageDetails(imageURL: String) {
        if let imageDetailsVC = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "ImageZoomedViewController") as? ImageZoomedViewController {
            //                rootViewContoller.test = "test String"
            imageDetailsVC.imageURL = imageURL
            self.present(imageDetailsVC, animated: true, completion: nil)
            print("bidDetailsVC")
        }
    }
    
}

extension SideMenuViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuArray.count + 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            if let cell: InfoTableViewCell = tableView.dequeueReusableCell(withIdentifier: "InfoTableViewCell", for: indexPath) as? InfoTableViewCell {
//                cell.delegate = self
                if let user = UserDefaultManager.shared.currentUser {
//                    cell.emailLabel.text = user.email
//                    if let firstName: String = user.firstName, let lastName: String = user.lastName {
//                        cell.userNameLabel.text = firstName + " " + lastName
//                    }
//
//                    if let image: String = user.image {
//                        cell.userImage.loadImageFromUrl(imageUrl: ImageURL_USERS + image)
//                    }
//
//                    if let email: String = user.email {
//                        cell.emailLabel.text = email
//                    }
                }
                return cell
            }
        }
        // Block list edit
//        else if indexPath.row > 0 && indexPath.row < 15 {
        else if indexPath.row > 0 && indexPath.row < 14 {
            if let cell: MenuTableViewCell = tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCell", for: indexPath) as? MenuTableViewCell {
                
                
                if "Lang".localized == "en" {
                    cell.menuLabel.text = menuArray[indexPath.row - 1]
                } else if "Lang".localized == "ar" {
                    cell.menuLabel.text = arabicMenuArray[indexPath.row - 1]
                }
                cell.menuImage.image = UIImage(named: menuImages[indexPath.row - 1])
                return cell
            }
        }
        
        else if indexPath.row == 14 {
//        else if indexPath.row == 15 {
            if let cell: LogoutTableViewCell = tableView.dequeueReusableCell(withIdentifier: "LogoutTableViewCell", for: indexPath) as? LogoutTableViewCell {
                if "Lang".localized == "en" {
                    cell.logoutLabel.text = "logout".localized
                } else if "Lang".localized == "ar" {
                    cell.logoutLabel.text = "logout".localized
                }
                return cell
            }
        }
        
        return UITableViewCell()
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let ind = indexPath.row
        
//        if ind == 2 || ind == 3 || ind == 4 || ind == 5 || ind == 6 || ind == 10 || ind == 11 || ind == 14 || ind == 15 {
        if ind == 2 || ind == 3 || ind == 4 || ind == 5 || ind == 6 || ind == 10 || ind == 11 || ind == 14 {
            guard let id: Int = UserDefaultManager.shared.currentUser?.id, id > 0 else {
                self.showCantLoginPopUp()
                return
            }
        }
        
        if indexPath.row == 1 {
            dismiss(animated: true, completion: nil)
        }
        else if indexPath.row == 2 {
//            goToMentainanceOrders()
        } else if indexPath.row == 3 {
//            goToOffers()
        } else if indexPath.row == 4 {
//            goToBids()
        } else if indexPath.row == 5 {
//            goToListOfChats()
        } else if indexPath.row == 6 {
            goToMyAccount()
        } else if indexPath.row == 7 {
//            goToSafteyProcedures()
        } else if indexPath.row == 8 {
            
        } else if indexPath.row == 9 {
//            goToTermsAndConditions()
        } else if indexPath.row == 10 {
//            goToPromoCode()
        } else if indexPath.row == 11 {
//            goToComplainsAndSuggestions()
        } else if indexPath.row == 12 {
//            goToConnectWithUs()
        } else if indexPath.row == 13 {
//            goToAboutApp()
        } else if indexPath.row == 14 {
            self.logout()
        }
//        } else if indexPath.row == 14 {
//            self.logout()
//        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

//extension SideMenuViewController: profileImageTableViewCellDelegate {
//    func profileImageButtonPressed() {
////        if let image = UserDefaultManager.shared.currentUser?.image {
////            self.goToImageDetails(imageURL: ImageURL_USERS + image)
////        }
//    }
//}
//
