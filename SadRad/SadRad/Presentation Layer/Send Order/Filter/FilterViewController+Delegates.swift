//
//  FilterViewController+Delegates.swift
//  SadRad
//
//  Created by Omar Ibrahim on 9/17/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit
import TagListView

//MARK:- TagListViewDelegate
extension FilterViewController: TagListViewDelegate {
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) {
        print("Tag pressed: \(title), \(sender)")
        tagView.isSelected = !tagView.isSelected
        if let index: Int = sender.tagViews.index(of: tagView) {
//            if tagView.isSelected, let intrestID = allIntrestes[index].id {
//                selectedIntrestes.append("\(intrestID)")
//            }
//            else {
//                if let intrestID = allIntrestes[index].id, let indexToRemove = selectedIntrestes.index(of: "\(intrestID)") {
//                    selectedIntrestes.remove(at: indexToRemove)
//                }
//            }
            
            if index == 0 {
                self.viewModel.changeCarConditioning()
            } else if index == 1 {
                self.viewModel.changeDeliveryPackage()
            }
            
        }
    }
    
    func tagRemoveButtonPressed(_ title: String, tagView: TagView, sender: TagListView) {
        print("Tag Remove pressed: \(title), \(sender)")
        sender.removeTagView(tagView)
    }
}
