//
//  FilterViewController.swift
//  SadRad
//
//  Created by Omar Ibrahim on 9/17/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit
import TagListView

protocol SendDataDelegate {
    func sendData(airConditioning: Bool, deliveryPackage: Bool, model: Int)
}

class FilterViewController: BaseViewController {
    
    lazy var viewModel: FilterViewModel = {
        FilterViewModel()
    }()
    
    var delegate: SendDataDelegate?
    
    var min: Float?
    var max: Float?

//    var tagArray: [String] = ["E", "G", "Y", "Designer", "Company", "Designer", "Company", "E", "G", "Y", "Messi"]
    var tagArray: [String] = ["airconditioning", "delivery package"]
    var tagArrayAr: [String] = ["مكيف الهواء", "توصيل طرود"]
        
    @IBOutlet weak var containView: UIView!
    @IBOutlet weak var filterTagView: TagListView!
    @IBOutlet weak var modelLabel: UILabel!
    @IBOutlet weak var sliderView: UISlider!
    @IBOutlet weak var fromYearLabel: UILabel!
    @IBOutlet weak var toYearLabel: UILabel!
    @IBOutlet weak var tripValueLabel: UILabel!
    @IBOutlet weak var doneButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initVM()
        configureView()
        setLocalization()
        setLocalization()
        configureTagView()
        addModels()
        checkFirstTime()
    }
    
    func setLocalization() {
        modelLabel.text = "filter model label".localized
        doneButton.setTitle("filter done button title".localized, for: .normal)
    }
    
    // MARK:- Init View Model
    func initVM() {
                                
        viewModel.chooseCarConditioningClosure = { [weak self] () in
            DispatchQueue.main.async { [weak self] in
                if let choosedCarConditioningBool: Bool = self?.viewModel.getCarConditioningBool() {
                    if choosedCarConditioningBool == true {
                        self?.filterTagView.tagViews[0].isSelected = true
                    }
                }
            }
        }
        
        viewModel.chooseDeliveringPackageClosure = { [weak self] () in
            DispatchQueue.main.async { [weak self] in
                if let choosedCarDeliveryPackageBool: Bool = self?.viewModel.getCarDeliveryPackage() {
                    if choosedCarDeliveryPackageBool == true {
                        self?.filterTagView.tagViews[1].isSelected = true
                    }
                }
            }
        }
        
        viewModel.updateModelYearsClosure = { [weak self] () in
            DispatchQueue.main.async { [weak self] in
                if let updateModelYearsBool: Bool = self?.viewModel.updateModelYears {
                    if updateModelYearsBool == true {
                        self?.setModelYears()
                    }
                }
            }
        }
        
        self.viewModel.fetchFilterYears()
    }

    
    func configureView() {
        containView.addCornerRadius(raduis: 8, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        doneButton.addCornerRadius(raduis: doneButton.frame.height / 2 - 5, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
    }
    
    func addModels() {
        
        if "Lang".localized == "en" {
            if self.tagArray.count > 0 {
                for i in 0...self.tagArray.count - 1 {
                    self.filterTagView.addTag(self.tagArray[i])
                }
            }
        } else if "Lang".localized == "ar" {
            if self.tagArrayAr.count > 0 {
                for i in 0...self.tagArrayAr.count - 1 {
                    self.filterTagView.addTag(self.tagArrayAr[i])
                }
            }
        }
        
//        self.filterTagView.addTag("E")
//        self.filterTagView.addTag("G")
//        self.filterTagView.addTag("Y")
//        self.filterTagView.addTag("Designer")
//        self.filterTagView.addTag("Company")
//        self.filterTagView.addTag("Designer")
//        self.filterTagView.addTag("Company")
//        self.filterTagView.addTag("E")
//        self.filterTagView.addTag("G")
//        self.filterTagView.addTag("Y")
//        self.filterTagView.addTag("Messi")
    }
    
    //MARK:- Configuration
    func configureTagView() {
        filterTagView.delegate = self
//        filterTagView.alignment = .center
    }
    
    func checkFirstTime() {
        if let choosedCarConditioningBool: Bool = self.viewModel.getCarConditioningBool() {
            if choosedCarConditioningBool == true {
                self.filterTagView.tagViews[0].isSelected = true
            }
        }
        if let choosedCarDeliveryPackageBool: Bool = self.viewModel.getCarDeliveryPackage() {
            if choosedCarDeliveryPackageBool == true {
                self.filterTagView.tagViews[1].isSelected = true
            }
        }
    }
    
    func setModelYears() {
        if let strMinYear = self.viewModel.filterYears[0].nameEn {
            if let minYear = Float(strMinYear) {
                min = minYear
                sliderView.minimumValue = minYear
            }
        }
        
        if let strMaxYear = self.viewModel.filterYears[1].nameEn {
            if let maxYear = Float(strMaxYear) {
                max = maxYear
                sliderView.maximumValue = maxYear
            }
        }
        
        //        sliderView?.minimumValue = 2010
        //        sliderView?.maximumValue = 2020
        
        min = sliderView?.minimumValue
        max = sliderView?.maximumValue
        if self.viewModel.modelYear != nil {
            self.sliderView.value = Float(self.viewModel.modelYear!)
            self.fromYearLabel.text = String(Int(self.viewModel.modelYear!))
        } else {
            self.viewModel.modelYear = Int(min!)
            self.fromYearLabel.text = String(Int(min!))
            self.toYearLabel.text = String(Int(max!))
        }
    }

    // MARK: - Navigation
    func goToChooseBrands() {
        if let chooseBrandsVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ChooseBrandsViewController") as? ChooseBrandsViewController {
//            chooseBrandsVC.editBool = true
//            chooseBrandsVC.journeyID = journeyID
            self.present(chooseBrandsVC, animated: true, completion: nil)
            print("Add Journey")
        }
    }
    
    func deleteFilterConfiguration() {
        self.viewModel.modelYear = Int(sliderView.minimumValue)
        self.viewModel.choosedDeliveryPackage = false
        self.viewModel.choosedCarConditioning = false
        self.delegate?.sendData(airConditioning: self.viewModel.choosedCarConditioning ?? false, deliveryPackage: self.viewModel.choosedDeliveryPackage ?? false, model: self.viewModel.modelYear!)
        self.fromYearLabel.text = String(Int(min!))
        self.sliderView.setValue(min!, animated: true)
        if "Lang".localized == "en" {
            if self.tagArray.count > 0 {
                for i in 0...self.tagArray.count - 1 {
                    self.filterTagView.tagViews[i].isSelected = false
                }
            }
        } else if "Lang".localized == "ar" {
            if self.tagArrayAr.count > 0 {
                for i in 0...self.tagArrayAr.count - 1 {
                    self.filterTagView.tagViews[i].isSelected = false
                }
            }
        }
    }
    
    // MARK:- Actions
    @IBAction func brandsButtonIsPressed(_ sender: Any) {
        self.goToChooseBrands()
    }
    
    @IBAction func sliderValueChanged(_ sender: UISlider) {
//        let currentValue = Int(sender.value)
        print(sender.value)
//        let t = Float(sender.value) * Float(max! - min!) / 10
        print(Int(sender.value))
        self.viewModel.modelYear = Int(sender.value)
        self.fromYearLabel.text = String(Int(sender.value))
        
//        print(t * 2010)
//        label.text = "\(currentValue)"
    }
    
    // MARK:- Actions
    @IBAction func doneButtonIsPressed(_ sender: Any) {
        print("Done is pressed")
        self.delegate?.sendData(airConditioning: self.viewModel.choosedCarConditioning ?? false, deliveryPackage: self.viewModel.choosedDeliveryPackage ?? false, model: self.viewModel.modelYear!)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func removeButtonIsPressed(_ sender: Any) {
        print("Remove is pressed")
//        filterTagView.tagBackgroundColor = .white
        deleteFilterConfiguration()
    }
    
}
