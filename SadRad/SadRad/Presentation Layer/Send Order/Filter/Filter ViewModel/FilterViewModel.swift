//
//  FilterViewModel.swift
//  SadRad
//
//  Created by Omar Ibrahim on 10/26/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation

class FilterViewModel {
    
    var modelYear: Int?
    var error: APIError?
    var filterYears: [FilterYear] = [FilterYear]()

    var chooseCarConditioningClosure: (()->())?
    var chooseDeliveringPackageClosure: (()->())?
    var updateLoadingStatus: (()->())?
    var updateModelYearsClosure: (()->())?

    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }

    var choosedCarConditioning: Bool? {
        didSet {
            self.chooseCarConditioningClosure?()
        }
    }

    var choosedDeliveryPackage: Bool? {
        didSet {
            self.chooseDeliveringPackageClosure?()
        }
    }
    
    var updateModelYears: Bool? {
        didSet {
            self.updateModelYearsClosure?()
        }
    }
        
    func fetchFilterYears() {
        state = .loading
        
        let params: [String : AnyObject] = [:]
        
        SearchAPIManager().getFilterYears(basicDictionary: params, onSuccess: { (filterYears) in
            
            self.filterYears = filterYears // Cache
            self.updateModelYears = true
            self.state = .populated
            
        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    func changeCarConditioning() {
        self.choosedCarConditioning = !self.choosedCarConditioning!
    }

    func changeDeliveryPackage() {
        self.choosedDeliveryPackage = !self.choosedDeliveryPackage!
    }

    func getCarConditioningBool() -> Bool? {
        return choosedCarConditioning
    }

    func getCarDeliveryPackage() -> Bool? {
        return choosedDeliveryPackage
    }

}
