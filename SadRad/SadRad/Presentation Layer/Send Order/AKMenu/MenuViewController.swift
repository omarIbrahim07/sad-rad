//
//  MenuViewController.swift
//  SadRad
//
//  Created by Omar Ibrahim on 9/10/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit
import AKSideMenu

class MenuViewController: AKSideMenu {

    override public func awakeFromNib() {
        self.contentViewController = self.storyboard!.instantiateViewController(withIdentifier: "HomeNavigationVC")
        if "Lang".localized == "en" {
            self.leftMenuViewController = self.storyboard!.instantiateViewController(withIdentifier: "SideMenuExViewController") as? SideMenuExViewController
        } else if "Lang".localized == "ar" {
            self.rightMenuViewController = self.storyboard!.instantiateViewController(withIdentifier: "SideMenuExViewController")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension MenuViewController: AKSideMenuDelegate {
    
}
