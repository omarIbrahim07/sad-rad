//
//  ReservedViewController.swift
//  SadRad
//
//  Created by Omar Ibrahim on 9/15/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class ReservedViewController: BaseViewController {

    @IBOutlet weak var reservedLabel: UILabel!
    @IBOutlet weak var goToMyReservationLabel: UILabel!
    @IBOutlet weak var myReservationView: UIView!
    @IBOutlet weak var myReservationLabel: UILabel!
    @IBOutlet weak var arrowContainerView: UIView!
    @IBOutlet weak var arrowImageView: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
        setLocalization()
    }
    
    func setLocalization() {
        reservedLabel.text = "reservd label".localized
        goToMyReservationLabel.text = "reserved successfult".localized
        myReservationLabel.text = "my reservations label".localized
    }
    
    func configureView() {
        arrowContainerView.addCornerRadius(raduis: arrowContainerView.frame.height / 2, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        myReservationView.addCornerRadius(raduis: myReservationView.frame.height / 2, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        navigationItem.title = "reserved".localized
    }
    
    // MARK:- Actions
    @IBAction func closeButtonIsPressed(_ sender: Any) {
        print("Close button is pressed")
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func myReservationButtonIsPressed(_ sender: Any) {
        print("My reservation button is pressed")
        goToMyReservations()
    }
    
    // MARK: - Navigation
    func goToMyReservations() {
        print("KKK")
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "MyReservationsViewController") as! MyReservationsViewController
        //        viewController.selectedService = service
        
        present(viewController, animated: true, completion: nil)
        //        navigationController?.pushViewController(viewController, animated: true)
        
    }
}
