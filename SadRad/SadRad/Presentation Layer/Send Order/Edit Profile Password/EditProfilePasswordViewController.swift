//
//  EditProfilePasswordViewController.swift
//  SadRad
//
//  Created by Omar Ibrahim on 12/27/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class EditProfilePasswordViewController: BaseViewController {

    lazy var viewModel: EditProfileViewModel = {
        return EditProfileViewModel()
    }()

    var error: APIError?

    @IBOutlet weak var oldPasswordTextfield: UITextField!
    @IBOutlet weak var newPasswordTextfield: UITextField!
    @IBOutlet weak var confirmNewPasswordTextfield: UITextField!
    @IBOutlet weak var updatePasswordButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setLocalization()
        configureView()
        closeKeypad()
                
        initVM()
    }
    
    func setLocalization() {
        self.navigationItem.title = "edit password vc title".localized
        oldPasswordTextfield.placeholder = "edit password old password placeholder".localized
        newPasswordTextfield.placeholder = "edit password new password placeholder".localized
        confirmNewPasswordTextfield.placeholder = "edit password confirm password placeholder".localized
        updatePasswordButton.setTitle("edit password save button title".localized, for: .normal)
        if "Lang".localized == "en" {
            oldPasswordTextfield.textAlignment = .left
            newPasswordTextfield.textAlignment = .left
            confirmNewPasswordTextfield.textAlignment = .left
        } else if "Lang".localized == "ar" {
            oldPasswordTextfield.textAlignment = .right
            newPasswordTextfield.textAlignment = .right
            confirmNewPasswordTextfield.textAlignment = .right
        }
        //        firstNameTextField.placeholder = "first name".localized
        //        lastNameTextField.placeholder = "last name".localized
        //        phoneTextField.placeholder = "phone".localized
        //        emailTextField.placeholder = "email".localized
        //        cityLabel.text = "city".localized
        //        updateProfileButton.setTitle("update profile button".localized, for: .normal)
        //        changePasswordButton.setTitle("change password button".localized, for: .normal)
    }

    func configureView() {
        updatePasswordButton.addCornerRadius(raduis: 8, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        oldPasswordTextfield.addCornerRadius(raduis: 8, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        newPasswordTextfield.addCornerRadius(raduis: 8, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        confirmNewPasswordTextfield.addCornerRadius(raduis: 8, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
    }

    // MARK:- Init View Model
    func initVM() {
        
        viewModel.updateEditingPassword = { [weak self] () in
            
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.editingPassword {
                case .success:
                    self.showDonePopUp()
                case .failed:
                    print("Failed")
                }
            }
        }
        
        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }
            
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                    
                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                case .error:
                    self.error = self.viewModel.getError()
                    if let error = self.error {
                        self.stopLoadingWithError(error: error)
                        //                        self.showError(error: error)
                    }
                case .empty:
                    print("")
                }
            }
        }
                
    }
    
    // MARK:- Networking
    func editPassword() {
                
        guard let oldPassword: String = oldPasswordTextfield.text, oldPassword.count > 0, oldPassword.count > 7 else {
            let apiError = APIError()
            apiError.message = "error password textfield".localized
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        guard let newPassword: String = newPasswordTextfield.text, newPassword.count > 0, newPassword.count > 7 else {
            let apiError = APIError()
            apiError.message = "error password textfield".localized
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        guard let confirmNewPassword: String = confirmNewPasswordTextfield.text, newPassword == confirmNewPassword else {
            let apiError = APIError()
            apiError.message = "error confirm password".localized
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }

        self.viewModel.editPassword(oldPassword: oldPassword, newPassword: newPassword, confirmNewPassword: confirmNewPassword)
    }

    
    func closeKeypad() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc func viewTapped() {
        oldPasswordTextfield.endEditing(true)
        newPasswordTextfield.endEditing(true)
        confirmNewPasswordTextfield.endEditing(true)
    }
    
    func showDonePopUp() {
        let alertController = UIAlertController(title: "edit password alert title".localized, message: "edit password alert message".localized, preferredStyle: .alert)
        //        let cancelAction = UIAlertAction(title: "إلغاء", style: .cancel, handler: nil)
        //        alertController.addAction(cancelAction)
        let openAction = UIAlertAction(title: "done action title".localized, style: .default) { (action) in
            self.presentMyHomeScreen()
        }
        alertController.addAction(openAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func updatePasswordButtonIsPressed(_ sender: Any) {
        self.editPassword()
    }

    // MARK: - Navigation

}
