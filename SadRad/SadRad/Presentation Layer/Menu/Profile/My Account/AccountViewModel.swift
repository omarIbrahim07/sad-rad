//
//  AccountViewModel.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 11/18/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation

class AccountViewModel {
    
    var reloadUserData: (()->())?
    
    func initAccountData() -> User {
        if let user = UserDefaultManager.shared.currentUser {
            print(user)
            return user
        }
        return User()
    }
    
}
