//
//  EditPasswordViewController.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 12/1/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit

class EditPasswordViewController: BaseViewController {
        
    lazy var viewModel: EditPasswordViewModel = {
        return EditPasswordViewModel()
    }()
    
    var error: APIError?
    var email: String?
    var status: Bool?

    @IBOutlet weak var newPasswordTextfield: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var updatePasswordButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
        closeKeypad()
        setLocalization()
        initVM()
    }
    
    func setLocalization() {
        self.navigationItem.title = "update password vc title".localized
        newPasswordTextfield.placeholder = "new password placeholder".localized
        confirmPasswordTextField.placeholder = "confirm new password placeholder".localized
        updatePasswordButton.setTitle("update password button title".localized, for: .normal)
    }
    
    func configureView() {
        updatePasswordButton.addCornerRadius(raduis: 5.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        if "Lang".localized == "en" {
            newPasswordTextfield.textAlignment = .left
            confirmPasswordTextField.textAlignment = .left
        } else if "Lang".localized == "ar" {
            newPasswordTextfield.textAlignment = .right
            confirmPasswordTextField.textAlignment = .right
        }
    }
    
    func closeKeypad() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc func viewTapped() {
        newPasswordTextfield.endEditing(true)
        confirmPasswordTextField.endEditing(true)
    }
    
    // MARK:- Init View Model
    func initVM() {
        
        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                
                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                case .error:
                    self.error = self.viewModel.getError()
                    if let error = self.error {
                        self.stopLoadingWithError(error: error)
                    }
                case .empty:
                    print("")
                }
            }
        }
        
        viewModel.passwordIsChangedClosure = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
               
                if let passwordChanged = self.viewModel.passwordIsChanged {
                    if passwordChanged == true {
                        self.showPasswordIsChangedPopUp(title: "password is changed alert title".localized, message: "password is changed alert message".localized, actionTitle: "done action title".localized)
                    } else if passwordChanged == false {
                        self.viewModel.makeError(message: "لم يتم تحديث كلمة السر")
                    }
                }
            }
        }
        
        viewModel.updateStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
               
                self.status = self.viewModel.getStatus()
                if let status = self.status {
                    if status == true {
                        self.goToLogIn()
                    }
                }
            }
        }
    }
    
    func showPasswordIsChangedPopUp(title: String, message: String, actionTitle: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        //        let cancelAction = UIAlertAction(title: "إلغاء", style: .cancel, handler: nil)
        //        alertController.addAction(cancelAction)
        let openAction = UIAlertAction(title: actionTitle, style: .default) { (action) in
            self.goToLogIn()
        }
        //        let cancelAction = UIAlertAction(title: "cancel go to login".localized, style: .cancel)
        alertController.addAction(openAction)
        //        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    // MARK:- Actions
    @IBAction func updatePasswordIsPressed(_ sender: Any) {
        guard let password: String = newPasswordTextfield.text, password.count > 0, password.count > 7 else {
            let apiError = APIError()
            apiError.message = "error password textfield".localized
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        guard let confirmPassword: String = confirmPasswordTextField.text, password == confirmPassword else {
            let apiError = APIError()
            apiError.message = "error confirm password".localized
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        if let email: String = self.email {
            viewModel.changePassword(email: email, newPassword: password, confirmPassword: confirmPassword)
        }
    }
    
    

    
    // MARK: - Navigation
    func goToLogIn() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "LoginNavigationVC")
        UIApplication.shared.keyWindow?.rootViewController = viewController
    }

}
