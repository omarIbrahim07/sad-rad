//
//  EditPasswordViewModel.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 12/8/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation

class EditPasswordViewModel {
    
    var error: APIError?

    var updateLoadingStatus: (()->())?
    var updateStatus: (()->())?
    var passwordIsChangedClosure: (()->())?
    
    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
        
    // callback for interfaces
    var status: Bool? {
        didSet {
            self.updateStatus?()
        }
    }

    var passwordIsChanged: Bool? = false {
        didSet {
            self.passwordIsChangedClosure?()
        }
    }

    func changePassword(email: String, newPassword: String, confirmPassword: String) {
    state = .loading
    
    let params: [String : AnyObject] = [
        "email" : email as AnyObject,
        "new_password" : newPassword as AnyObject,
        "confirm_password" : confirmPassword as AnyObject
    ]
    
    AuthenticationAPIManager().changePasswordFromLogIn(basicDictionary: params, onSuccess: { (message) in
        
        self.state = .populated
        if message == "Password updated successfully" {
            self.passwordIsChanged = true
        } else {
            self.passwordIsChanged = false
        }


        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    func makeError(message: String) {
        let apiError = APIError()
        apiError.message = message
        self.error = apiError
        self.state = .error
    }

    func getError() -> APIError {
        return self.error!
    }

    func getStatus() -> Bool {
        return self.status!
    }
}
