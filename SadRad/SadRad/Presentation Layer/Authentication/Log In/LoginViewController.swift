//
//  LoginViewController.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 8/26/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit

class LoginViewController: BaseViewController {

    @IBOutlet weak var logInButton: UIButton!
    @IBOutlet weak var registrationButton: UIButton!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var forgetPasswordButton: UIButton!
    @IBOutlet weak var dontHaveAccountButton: UIButton!
    @IBOutlet weak var loginAsAguestButton: UIButton!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        setNavigationTitle()
        configureView()
        // Do any additional setup after loading the view.
        closeKeypad()
    }
    
    func setNavigationTitle() {
        navigationItem.title = "login vc title".localized
    }
    
    func closeKeypad() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc func viewTapped() {
        phoneTextField.endEditing(true)
        passwordTextField.endEditing(true)
    }
    
    func configureView() {
        
        dontHaveAccountButton.setTitle("dont have account button title".localized, for: .normal)
        registrationButton.setTitle("sign up".localized, for: .normal)
        logInButton.setTitle("log in".localized, for: .normal)
        forgetPasswordButton.setTitle("forget password".localized, for: .normal)
        phoneTextField.placeholder = "phone".localized
        passwordTextField.placeholder = "password".localized
        registrationButton.setTitle("sign up".localized, for: .normal)
        loginAsAguestButton.setTitle("login as aguest button".localized, for: .normal)

        self.navigationItem.title = "log in".localized
        
        if "Lang".localized == "en" {
            phoneTextField.textAlignment = .left
            passwordTextField.textAlignment = .left
        } else if "Lang".localized == "ar" {
            phoneTextField.textAlignment = .right
            passwordTextField.textAlignment = .right
        }
        
        logInButton.addCornerRadius(raduis: 5.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        registrationButton.addCornerRadius(raduis: 5.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        phoneTextField.delegate = self
    }
    
    func changeTextFieldPlaceHolderColor(textField: UITextField, placeHolderString: String,fontSize: CGFloat) {
        textField.attributedPlaceholder = NSAttributedString(string: placeHolderString, attributes: [.foregroundColor: #colorLiteral(red: 0.2901960784, green: 0.6745098039, blue: 0.9137254902, alpha: 1), .font: UIFont.boldSystemFont(ofSize: fontSize)])
    }
    
    func presentRegistration() {
        if let SignUpNavigationController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SignInNavigationVC") as? UINavigationController, let rootViewContoller = SignUpNavigationController.viewControllers[0] as? SignInViewController {
            //                rootViewContoller.test = "test String"
            self.present(SignUpNavigationController, animated: true, completion: nil)
        }
    }
    
    func logIn() {
        
        guard let phone = phoneTextField.text, phone.isEmpty == false else {
            let apiError = APIError()
            apiError.message = "error phone textfield".localized
            showError(error: apiError)
            return
        }

        guard let password = passwordTextField.text, password.isEmpty == false else {
            let apiError = APIError()
            apiError.message = "error password textfield".localized
            showError(error: apiError)
            return
        }

        let params: [String : AnyObject] = [
            "phone_number" : phone as AnyObject,
            "password" : password as AnyObject,
//            "app_id" : 4 as AnyObject,
            "fcm_token" : FirebaseToken as AnyObject
        ]
        
        startLoading()
        
        AuthenticationAPIManager().loginUser(basicDictionary: params, onSuccess: { (token) in
            
            self.getUserProfile()

        }) { (error) in
            self.stopLoadingWithError(error: error)
        }
        
    }
    
    func getUserProfile() {
        weak var weakSelf = self
        
        AuthenticationAPIManager().getUserProfile(onSuccess: { (_) in
            
            weakSelf?.stopLoadingWithSuccess()
            weakSelf?.presentHomeScreen()
            
        }) { (error) in
            weakSelf?.stopLoadingWithError(error: error)
        }
    }


    
    //MARK:- Navigation
    func presentHomeScreen() {
//        if let _ = UserDefaultManager.shared.currentUser, let _ = UserDefaultManager.shared.authorization {
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let viewController = storyboard.instantiateViewController(withIdentifier: "MenuViewController")
            appDelegate.window!.rootViewController = viewController
            appDelegate.window!.makeKeyAndVisible()
//        }
    }
    
    @IBAction func signUpButtonPressed(_ sender: Any) {
        print("Sign Up")
        presentRegistration()
    }
    
    @IBAction func forgetPasswordButtonPressed(_ sender: Any) {
        goToForgetPassword()
    }
    
    
    @IBAction func logInButtonPressed(_ sender: Any) {
        print("Home")
        logIn()
    }
    
    @IBAction func loginAsAguestButtonIsPressed(_ sender: Any) {
        print("Log in as aguest")
        presentHomeScreen()
    }
    
    func goToForgetPassword() {
        if let forgetPasswordVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ForgetPasswordViewController") as? ForgetPasswordViewController {
            //                rootViewContoller.test = "test String"
            navigationController?.pushViewController(forgetPasswordVC, animated: true)
            //            (forgetPasswordVC, animated: true, completion: nil)
        }
    }
    
}

extension LoginViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //For numers
        if textField == phoneTextField {
            let allowedCharacters = CharacterSet(charactersIn:"0123456789")//Here change this characters based on your requirement
            let characterSet = CharacterSet(charactersIn: string)
            return allowedCharacters.isSuperset(of: characterSet)
        }
        return true
    }
}
