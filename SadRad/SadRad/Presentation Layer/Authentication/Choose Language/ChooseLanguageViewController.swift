//
//  ChooseLanguageViewController.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 12/1/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit
import MOLH

class ChooseLanguageViewController: BaseViewController {

    @IBOutlet weak var englishButton: UIButton!
    @IBOutlet weak var arabicButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configure()
        checkLanguage()
    }
    
    func checkLanguage() {
        if UserDefaults.standard.bool(forKey: "First Launch") == true {
            self.goToLogIn()
        } else {
            print("Lesa")
        }
    }

    func configure() {
        self.navigationItem.title = "Choose Language"
        let radius = englishButton.frame.height / 2
        if "Lang".localized == "en" {
            englishButton.roundCorners([.topLeft, .bottomLeft], radius: radius)
            arabicButton.roundCorners([.topRight, .bottomRight], radius: radius)
        } else if "Lang".localized == "ar" {
            englishButton.roundCorners([.topRight, .bottomRight], radius: radius)
            arabicButton.roundCorners([.topLeft, .bottomLeft], radius: radius)
        }
    }
    
    //MARK:- Actions
    @IBAction func englishButtonIsPressed(_ sender: Any) {
        print("English")
        UserDefaults.standard.set(true, forKey: "First Launch")
        MOLH.setLanguageTo("en")
        englishButton.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        englishButton.titleLabel?.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        print("Lang".localized)
        MOLH.reset()
        resetFirstTime()
    }
    
    func resetFirstTime() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "LoginNavigationVC")
        UIApplication.shared.keyWindow?.rootViewController = viewController
    }
    
    @IBAction func arabicButtonIsPressed(_ sender: Any) {
        print("Arabic")
        UserDefaults.standard.set(true, forKey: "First Launch")
        MOLH.setLanguageTo("ar")
        arabicButton.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        arabicButton.titleLabel?.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        print("Lang".localized)
        MOLH.reset()
        resetFirstTime()
    }
    
    

    // MARK:- Navigation
    func goToLogIn() {
        print("Log in")
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "LoginNavigationVC")
        UIApplication.shared.keyWindow?.rootViewController = viewController
    }

}
