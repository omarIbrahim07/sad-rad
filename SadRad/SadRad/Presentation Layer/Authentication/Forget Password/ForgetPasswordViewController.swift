//
//  ForgetPasswordViewController.swift
//  SadRad
//
//  Created by Omar Ibrahim on 11/25/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit
import KWVerificationCodeView

class ForgetPasswordViewController: BaseViewController {
    
    lazy var viewModel: ForgetPasswordViewModel = {
        return ForgetPasswordViewModel()
    }()
    
    var error: APIError?
    var status: Bool?
    var email: String?
    var verificationCode: Int?

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var verifyCodeView: KWVerificationCodeView!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var sendEmailButton: UIButton!
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        submitButton.isEnabled = false
        verifyCodeView.delegate = self

        configureView()
        closeKeypad()
        initVM()
    }
    
    func configureView() {
        
        emailTextField.placeholder = "email".localized
        submitButton.setTitle("submit button".localized, for: .normal)
        sendEmailButton.setTitle("send message".localized, for: .normal)
        
        self.navigationItem.title = "forget pass".localized
        submitButton.addCornerRadius(raduis: 5.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        sendEmailButton.addCornerRadius(raduis: 5.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
    }
    
    func closeKeypad() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc func viewTapped() {
        emailTextField.endEditing(true)
        verifyCodeView.endEditing(true)
    }

    // MARK:- Init View Model
    func initVM() {
        
        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                
                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                case .error:
                    self.error = self.viewModel.getError()
                    if let error = self.error {
                        self.stopLoadingWithError(error: error)
                    }
                case .empty:
                    print("")
                }
            }
        }
        
        viewModel.updateStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
               
                self.status = self.viewModel.getStatus()
                if let status = self.status {
                    if status == true {
                        if let email = self.email {
                            self.goToEditPassword(email: email)
                        }
                    }
                }
            }
        }
        
        viewModel.verificationCodeIsSentClosure = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
               
                if let verificationCodeSent = self.viewModel.verificationCodeIsSent {
                    if verificationCodeSent == true {
                        self.viewModel.makeError(message: "success email".localized)
                    } else if verificationCodeSent == false {
                        self.viewModel.makeError(message: "error wrong email".localized)
                    }
                }
            }
        }
        
        viewModel.verificationCodeIsTrueClosure = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
               
                if let verificationCodeTrue = self.viewModel.verificationCodeIsTrue {
                    if verificationCodeTrue == true {
                        if let email = self.email {
                            self.goToEditPassword(email: email)
                        }
                    } else if verificationCodeTrue == false {
                        self.viewModel.makeError(message: "error wrong verification code".localized)
                    }
                }
            }
        }

        viewModel.updateVerificationCode = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                
                self.verificationCode = self.viewModel.getVerificationCode()
            }
        }
    }

    // MARK:- Actions
    @IBAction func sendMessageIsPressed(_ sender: Any) {
//        guard let phone: String = phoneTextField.text, phone.count == 10 else {
//            let apiError = APIError()
//            apiError.message = "برجاء إدخال رقم الجوال بشكل صحيح"
//            self.viewModel.error = apiError
//            self.viewModel.state = .error
//            return
//        }
        guard let email: String = emailTextField.text, email.isValidEmailAddress() == true else {
            let apiError = APIError()
            apiError.message = "error email textfield".localized
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        self.email = email
        viewModel.forgetPassword(email: email)
    }
    
    @IBAction func submitIsPressed(_ sender: Any) {
        if verifyCodeView.hasValidCode() {
                        
            guard let verificationCode: String = self.verifyCodeView.getVerificationCode(), verificationCode.count == 4 else {
                let apiError = APIError()
                apiError.message = "error verification code".localized
                self.viewModel.error = apiError
                self.viewModel.state = .error
                return
            }

            self.viewModel.enterVeificationCode(verificationCode: verificationCode)
        }

    }

    // MARK: - Navigation
    func goToEditPassword(email: String) {
        if let editPasswordVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EditPasswordViewController") as? EditPasswordViewController {
            editPasswordVC.email = email
            navigationController?.pushViewController(editPasswordVC, animated: true)
        }
    }
    

}

// MARK: - KWVerificationCodeViewDelegate
extension ForgetPasswordViewController: KWVerificationCodeViewDelegate {
  func didChangeVerificationCode() {
    submitButton.isEnabled = verifyCodeView.hasValidCode()
  }
}
