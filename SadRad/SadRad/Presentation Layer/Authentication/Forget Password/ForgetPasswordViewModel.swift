//
//  ForgetPasswordViewModel.swift
//  SadRad
//
//  Created by Omar Ibrahim on 11/25/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation

class ForgetPasswordViewModel {
    
    var error: APIError?

    var updateLoadingStatus: (()->())?
    var verificationCodeIsSentClosure: (()->())?
    var verificationCodeIsTrueClosure: (()->())?
    var updateStatus: (()->())?
    var updateVerificationCode: (()->())?
    
    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    // callback for interfaces
    var verificationCode: Int? {
        didSet {
            self.updateVerificationCode?()
        }
    }
    
    // callback for interfaces
    var status: Bool? {
        didSet {
            self.updateStatus?()
        }
    }

    var verificationCodeIsSent: Bool? = false {
        didSet {
            self.verificationCodeIsSentClosure?()
        }
    }
    
    var verificationCodeIsTrue: Bool? = false {
        didSet {
            self.verificationCodeIsTrueClosure?()
        }
    }

    func forgetPassword(email: String) {
        state = .loading
        
        let params: [String : AnyObject] = [
            "email" : email as AnyObject,
        ]
        
        AuthenticationAPIManager().forgetPassword(basicDictionary: params, onSuccess: { (message) in
            
            if message == "Check your inbox, we have sent a link to reset email." {
                print(message)
//                self.verificationCode = forgetPassObj.verifyCode
                self.verificationCodeIsSent = true
                self.state = .populated
            } else {
                self.verificationCodeIsSent = false
                self.state = .populated
            }

        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    func enterVeificationCode(verificationCode: String) {
        state = .loading
        
        let params: [String : AnyObject] = [
            "verifycode" : verificationCode as AnyObject,
        ]
        
        AuthenticationAPIManager().enterVerificationCode(basicDictionary: params, onSuccess: { (status) in
            
            if status == "true" {
                print(status)
                //                self.verificationCode = forgetPassObj.verifyCode
                self.verificationCodeIsTrue = true
                self.state = .populated
            } else {
                self.verificationCodeIsTrue = false
                self.state = .populated
            }
            
        }) { (error) in
            self.error = error
            self.state = .error
        }
    }

    func confirmVerificationCode(phone: String, verificationCode: Int) {
        
        if verificationCode == self.verificationCode {
            self.status = true
        } else {
            self.makeError(message: "هذا الكود غير صحيح")
        }

    }
    
    func getVerificationCode() -> Int {
        return verificationCode!
    }
    
    func makeError(message: String) {
        let apiError = APIError()
        apiError.message = message
        self.error = apiError
        self.state = .error
    }
        
    func getError() -> APIError {
        return self.error!
    }
    
    func getStatus() -> Bool {
        return self.status!
    }

}
