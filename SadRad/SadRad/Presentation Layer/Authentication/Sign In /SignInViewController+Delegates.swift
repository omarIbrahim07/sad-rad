//
//  SignInViewController+Delegates.swift
//  SadRad
//
//  Created by Omar Ibrahim on 1/6/21.
//  Copyright © 2021 EgyDesigner. All rights reserved.
//

import UIKit

extension SignInViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
      if textField == emailTextField {
                  let allowedCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.@_-"
                  let allowedCharacterSet = CharacterSet(charactersIn: allowedCharacters)
                  let typedCharacterSet = CharacterSet(charactersIn: string)
                  let alphabet = allowedCharacterSet.isSuperset(of: typedCharacterSet)
                  return alphabet


        } else {
          return false
      }
    }

}
