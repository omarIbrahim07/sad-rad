//
//  SignInViewController.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 8/26/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit

class SignInViewController: BaseViewController {
    
    lazy var viewModel: SignInViewModel = {
        return SignInViewModel()
    }()
    
    var checked: Bool = false
    var error: APIError?
    var user: User?
    
    var cities: [City] = [City]()
    var choosedCity: City?
    var genders: [GenderViewModel] = [GenderViewModel]()
    var choosedGender: GenderViewModel?
    
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var signUpButton: UIButton!
    
    override func viewDidLoad() {
                
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setNavigationTitle()
        configureView()
        closeKeypad()
        initVM()
    }
    
    func setNavigationTitle() {
        navigationItem.title = "sign up vc title".localized
    }
    
    // MARK:- Init View Model
    func initVM() {
        
        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                
                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                case .error:
                    self.error = self.viewModel.getError()
                    if let error = self.error {
//                        self.showError(error: error)
                        error.message = "Phone or email is already taken error".localized
                        self.stopLoadingWithError(error: error)
                    }
                case .empty:
                    print("")
                }
            }
        }
                
        viewModel.updateUser = { [weak self] () in
            DispatchQueue.main.async { [weak self] () in
                self?.user = self!.viewModel.getUser()
                if let user: User = self?.user {
                    if let _: Int = user.id {
                        self?.showRegistrationIsSucceededAlert(title: "registration alert title".localized, message: "registration alert message".localized)
                    }
                }
            }
        }
                        
    }
    
    func showRegistrationIsSucceededAlert(title: String, message: String) {
        let goToHomeAction = UIAlertAction(title: "registration is succeeded action".localized, style: .default) { (action) in
            self.presentMyHomeScreen()
        }
        self.showAlert(title: title, message: message, alertActions: [goToHomeAction])
    }
            
    func configureView() {
        
        firstNameTextField.placeholder = "first name".localized
        phoneTextField.placeholder = "phone".localized
        passwordTextField.placeholder = "password".localized
        confirmPasswordTextField.placeholder = "confirm".localized
        emailTextField.placeholder = "email".localized
        signUpButton.setTitle("sign up".localized, for: .normal)
        
        if "Lang".localized == "en" {
            firstNameTextField.textAlignment = .left
            emailTextField.textAlignment = .left
            phoneTextField.textAlignment = .left
            passwordTextField.textAlignment = .left
            confirmPasswordTextField.textAlignment = .left
        } else if "Lang".localized == "ar" {
            firstNameTextField.textAlignment = .right
            emailTextField.textAlignment = .right
            phoneTextField.textAlignment = .right
            passwordTextField.textAlignment = .right
            confirmPasswordTextField.textAlignment = .right
        }
        
        self.navigationItem.title = "sign up".localized
        signUpButton.addCornerRadius(raduis: 8.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        emailTextField.delegate = self
    }
    
    func closeKeypad() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc func viewTapped() {
        firstNameTextField.endEditing(true)
        emailTextField.endEditing(true)
        phoneTextField.endEditing(true)
        passwordTextField.endEditing(true)
        confirmPasswordTextField.endEditing(true)
    }
        
    //MARK:- Navigation
    func presentHomeScreen(isPushNotification: Bool = false) {
        //        if let _ = UserDefaultManager.shared.currentUser, let _ = UserDefaultManager.shared.authorization {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "MenuViewController")
        appDelegate.window!.rootViewController = viewController
        appDelegate.window!.makeKeyAndVisible()
        //        }
    }
        
    func signUp() {
        
        guard let name: String = firstNameTextField.text, name.count > 0 else {
              let apiError = APIError()
            apiError.message = "error name textfield".localized
              self.viewModel.error = apiError
              self.viewModel.state = .error
              return
          }
        
        if name.count < 60 {
        } else {
            let apiError = APIError()
            apiError.message = "error name more than sixty".localized
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
                
        guard let phone: String = phoneTextField.text, phone.count == 10 || phone.count == 11 else {
            let apiError = APIError()
            apiError.message = "error phone textfield".localized
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        guard let email: String = emailTextField.text, email.isValidEmailAddress() == true else {
            let apiError = APIError()
            apiError.message = "error email textfield".localized
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }

        guard let password: String = passwordTextField.text, password.count > 7 else {
            let apiError = APIError()
            apiError.message = "error password textfield".localized
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
                
        guard let confirmPassword: String = confirmPasswordTextField.text, confirmPassword == password else {
            let apiError = APIError()
            apiError.message = "error confirm password".localized
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        self.viewModel.signUp(name: name, password: password, phone: phone, email: email)
    }
    
    //MARK:- Navigation
    
    @IBAction func registrationIsPressed(_ sender: Any) {
        print("Registration is pressed")
        self.signUp()
    }
            
}
