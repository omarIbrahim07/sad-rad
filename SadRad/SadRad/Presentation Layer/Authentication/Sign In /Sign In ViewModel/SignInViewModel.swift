//
//  SignInViewModel.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 12/1/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation

class SignInViewModel {
    
    var error: APIError?

    var reloadCitiesClosure: (()->())?
    var chooseCityClosure: (()->())?
    var updateLoadingStatus: (()->())?
    var updateGender: (()->())?
    var updateUser: (()->())?
    
    // callback for interfaces
    var user: User? {
        didSet {
            updateUser?()
        }
    }
    
    // callback for interfaces
    var cities: [City]? {
        didSet {
            self.reloadCitiesClosure?()
        }
    }
    
    // callback for interfaces
    var gender: GenderViewModel? {
        didSet {
            updateGender?()
        }
    }
    
    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    // callback for interfaces
    var choosedCity: City? {
        didSet {
            self.chooseCityClosure?()
        }
    }


    func initFetch(locationID: Int) {
        state = .loading
        
        let params: [String : AnyObject] = [
            "location_id" : locationID as AnyObject,
        ]
        
        CitiesAPIManager().getCities(basicDictionary: params, onSuccess: { (cities) in

        self.cities = cities // Cache
        self.state = .populated

        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    func signUp(name: String, password: String, phone: String, email: String) {
        
        let params: [String : AnyObject] = [
            "name" : name as AnyObject,
            "password" : password as AnyObject,
            "phone_number" : phone as AnyObject,
            "email": email as AnyObject,
            "fcm_token" : FirebaseToken as AnyObject
        ]
        
//        AuthenticationAPIManager().registerUser(basicDictionary: params, onSuccess: { (userSignedObj) in
//
//        if userSignedObj.success == true {
//            self.user = userSignedObj.userSigned
//        } else {
//            self.makeError(message: "email or phone is used before")
//        }
        
        AuthenticationAPIManager().registerUser(basicDictionary: params, onSuccess: { (_) in
            
            self.getUserProfile()

        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    func getUserProfile() {
        weak var weakSelf = self
        
        AuthenticationAPIManager().getUserProfile(onSuccess: { (user) in
            
            self.user = user
            
        }) { (error) in
            self.error = error
            self.state = .error
        }
    }

    
    func makeError(message: String) {
        let apiError = APIError()
        apiError.message = message
        self.error = apiError
        self.state = .error
    }

    func getUser() -> User {
        return user!
    }
    
    func getCities() -> [City] {
        return cities!
    }
    
    func getGenders() -> [GenderViewModel] {
        return [GenderViewModel(id: 1, type: "Male", typeAr: "ذكر"), GenderViewModel(id: 2, type: "Female", typeAr: "أنثى")]
    }
    
    func getGender(gender: GenderViewModel) {
        self.gender = gender
    }
    
    func getError() -> APIError {
        return self.error!
    }


//    func processFetchedPhoto( cities: [City] ) {
//        self.cities = cities // Cache
//
//        var vms = [TechnicianCellViewModel]()
//        for city in cities {
//            vms.append( createCellViewModel(technician: technician) )
//        }
//        self.cellViewModels = vms
//    }
//
//        func createCellViewModel( technician: Technician ) -> TechnicianCellViewModel {
//
//            return TechnicianCellViewModel(name: technician.name!, rate: Double(technician.rating!), distance: technician.distance ?? 0.0)
//        }

    func userPressed(city: City) {
        self.choosedCity = city
    }
    
}
