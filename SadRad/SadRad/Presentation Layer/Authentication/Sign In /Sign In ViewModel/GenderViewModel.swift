//
//  GenderViewModel.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 12/2/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation

struct GenderViewModel {
    let id: Int
    let type: String
    let typeAr: String
}
