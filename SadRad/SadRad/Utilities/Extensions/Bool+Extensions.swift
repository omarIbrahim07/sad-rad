//
//  Bool+Extensions.swift
//  GameOn
//
//  Created by Hassan on 1/27/18.
//  Copyright © 2018 Hassan. All rights reserved.
//

import Foundation

extension Bool {
    init(_ integer: Int){
        if integer == 0 {
            self.init(false)
        } else {
            self.init(true)
        }
    }
}
