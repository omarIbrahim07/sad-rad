//
//  UITextField.swift
//  El-Zafer
//
//  Created by Omar Ibrahim on 5/19/19.
//  Copyright © 2019 Egy designer. All rights reserved.
//

import Foundation
import UIKit

extension UITextField {
    
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: newValue!])
        }
    }
    
    func setIcon(_ image: UIImage, imageState: String) {
        if imageState == "Home" {
            let iconView = UIImageView(frame:
                CGRect(x: 10, y: 5, width: 35, height: 25))
            iconView.image = image
            iconView.contentMode = .scaleAspectFit
            let iconContainerView: UIView = UIView(frame:
                CGRect(x: 20, y: 0, width: 50, height: 35))
            iconContainerView.addSubview(iconView)
            // this line is for close any user interaction for the image so i can generate the picker when i pressed on the image. 
            iconContainerView.isUserInteractionEnabled = false
            //////////////////////////////////////////////////////
            rightView = iconContainerView
            rightViewMode = .always
        } else if imageState == "Normal" {
            let iconView = UIImageView(frame:
                CGRect(x: 10, y: 0, width: 20, height: 25))
            iconView.image = image
            iconView.contentMode = .scaleToFill
            let iconContainerView: UIView = UIView(frame:
                CGRect(x: 0, y: 5, width: 20, height: 25))
            iconContainerView.addSubview(iconView)
            rightView = iconContainerView
            rightViewMode = .always
        }
    }
}
