//
//  CustomDateFormatTransformUTC.swift
//  GameOn
//
//  Created by Hassan on 5/12/18.
//  Copyright © 2018 Hassan. All rights reserved.
//

import Foundation
import ObjectMapper

open class CustomDateFormatTransformUTC: DateFormatterTransform {
    
    public init(formatString: String) {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.timeZone = TimeZone(identifier: "UTC")
        formatter.dateFormat = formatString
        
        super.init(dateFormatter: formatter)
    }
}
