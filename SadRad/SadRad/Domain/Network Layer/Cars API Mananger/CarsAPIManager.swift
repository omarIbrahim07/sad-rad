//
//  CarsAPIManager.swift
//  SadRad
//
//  Created by Omar Ibrahim on 10/11/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class CarsAPIManager: BaseAPIManager {

    func getMyCars(basicDictionary params:APIParams , onSuccess: @escaping ([MyCar])->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .get, path: GET_MY_CARS_URL, parameters: params)
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["data"] as? [[String : Any]] {
                let wrapper = Mapper<MyCar>().mapArray(JSONArray: jsonArray)
                onSuccess(wrapper)
            }
                
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func getCarsTypes(basicDictionary params:APIParams , onSuccess: @escaping ([CarType])->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .get, path: GET_CAR_TYPES_URL, parameters: params)
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [[String : Any]] = responseObject as? [[String : Any]] {
                let wrapper = Mapper<CarType>().mapArray(JSONArray: response)
                onSuccess(wrapper)
            }
                
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func getCarsBrands(basicDictionary params:APIParams , onSuccess: @escaping ([CarBrand])->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .get, path: GET_CAR_BRANDS_URL, parameters: params)
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let jsonResponse: [[String : Any]] = response["data"] as? [[String : Any]] {
                let wrapper = Mapper<CarBrand>().mapArray(JSONArray: jsonResponse)
                onSuccess(wrapper)
            }
                
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }

    func getCarsModels(basicDictionary params:APIParams , onSuccess: @escaping ([CarModel])->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .get, path: GET_CAR_MODELS_URL, parameters: params)
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let jsonResponse: [[String : Any]] = response["data"] as? [[String : Any]] {
                let wrapper = Mapper<CarModel>().mapArray(JSONArray: jsonResponse)
                onSuccess(wrapper)
            }
                
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func getMyCarInfo(basicDictionary params:APIParams , onSuccess: @escaping (MyCar) -> Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .get, path: GET_MY_CAR_INFORMATION, parameters: params)
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let data = response["data"] as? [String : Any], let wrapper = Mapper<MyCar>().map(JSON: data) {
                onSuccess(wrapper)
            }
                
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
        
    func addMyCar(nameOfArray: String, imagesDataArray: [Data]?, basicDictionary params:APIParams , onSuccess: @escaping (Bool)->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .post, path: ADD_MY_CAR_URL, parameters: params)
        
        super.performUploadArrayOfImagesNetworkRequest(imageDataArray: imagesDataArray, nameOfImageArray: nameOfArray, forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let saved = response["saved"] as? Bool {

            onSuccess(saved)

            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func updateMyCar(nameOfArray: String, imagesDataArray: [Data]?, basicDictionary params:APIParams , onSuccess: @escaping (Bool)->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .post, path: UPDATE_MY_CAR_URL, parameters: params)
        
        super.performUploadArrayOfImagesNetworkRequest(imageDataArray: imagesDataArray, nameOfImageArray: nameOfArray, forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let saved = response["updated"] as? Bool {

            onSuccess(saved)

            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func uploadImage(nameOfArray: String, imageData: Data?, basicDictionary params:APIParams , onSuccess: @escaping (String)->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .post, path: UPLOAD_PROFILE_IMAGE_URL, parameters: params)
        
        super.performUploadNetworkRequestInChat(nameOfImage: nameOfArray, imageData: imageData, forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let json: [String : Any] = response["data"] as? [String : Any], let image = json["image"] as? String {

            onSuccess(image)

            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }

    
    func deleteMyCar(basicDictionary params:APIParams , onSuccess: @escaping (Bool)->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .post, path: DELETE_MY_CAR_URL, parameters: params)
        
        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let saved = response["deleted"] as? Bool {

            onSuccess(saved)

            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }


}
