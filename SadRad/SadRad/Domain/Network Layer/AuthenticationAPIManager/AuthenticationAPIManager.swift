////
////  AuthenticationAPIManager.swift
////  GameOn
////
////  Created by Hassan on 12/18/18.
////  Copyright © 2018 GameOn. All rights reserved.
////
//
//import UIKit
//import ObjectMapper
//
//class AuthenticationAPIManager: BaseAPIManager {
//
//    func loginUser(basicDictionary params:APIParams , onSuccess: @escaping (User)->Void, onFailure: @escaping  (APIError)->Void) {
//
//        let engagementRouter = BaseRouter(method: .post, path: LOGIN_URL, parameters: params)
//
//        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
//
//            if let response: [String : Any] = responseObject as? [String : Any], let data: [String : Any] = response["data"] as? [String : Any], let wrapper = Mapper<User>().map(JSON: data) {
//                UserDefaultManager.shared.currentUser = wrapper
//                onSuccess(wrapper)
//            }
//            else {
//                let apiError = APIError()
//                onFailure(apiError)
//            }
//
//        }) { (apiError) in
//            onFailure(apiError)
//        }
//    }
//
//    func registerUser(basicDictionary params: APIParams, onSuccess: @escaping (User)->Void, onFailure: @escaping  (APIError)->Void) {
//
//        let engagementRouter = BaseRouter(method: .post, path: SIGNUP_URL, parameters: params)
//
//        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
//
//            if let response: [String : Any] = responseObject as? [String : Any], let data: [String : Any] = response["data"] as? [String : Any], let wrapper = Mapper<User>().map(JSON: data) {
//                onSuccess(wrapper)
//            }
//            else if let response: [String : Any] = responseObject as? [String : Any], let message = response["message"] as? String, message == "api_messages.The email has already been taken." {
//                onSuccess(User())
//            }
//            else {
//                let apiError = APIError()
//                onFailure(apiError)
//            }
//
//        }) { (apiError) in
//            onFailure(apiError)
//        }
//    }
//
//    func logout(basicDictionary params: APIParams, onSuccess: @escaping ()->Void, onFailure: @escaping  (APIError)->Void) {
//
//        let engagementRouter = BaseRouter(method: .post, path: LOGOUT_URL, parameters: params)
//
//        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
//
//            onSuccess()
//
//        }) { (apiError) in
//            onFailure(apiError)
//        }
//    }
//
//    func getProfile(basicDictionary params:APIParams , onSuccess: @escaping (User) -> Void, onFailure: @escaping  (APIError) -> Void) {
//
//        let engagementRouter = BaseRouter(method: .post, path: GET_PROFILE_DATA, parameters: params)
//
//        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
//
//            if let response: [String : Any] = responseObject as? [String : Any], let data: [String : Any] = response["data"] as? [String : Any], let wrapper = Mapper<User>().map(JSON: data) {
//                onSuccess(wrapper)
//            }
//
//            else {
//                let apiError = APIError()
//                onFailure(apiError)
//            }
//
//        }) { (apiError) in
//            onFailure(apiError)
//        }
//    }
//
//
//}

//
//  AuthenticationAPIManager.swift
//  GameOn
//
//  Created by Hassan on 12/18/18.
//  Copyright © 2018 GameOn. All rights reserved.
//

import UIKit
import ObjectMapper

class AuthenticationAPIManager: BaseAPIManager {
    
    static var shared = AuthenticationAPIManager()
    var userLoggedIn: Bool {
        get {
            if let _ = UserDefaultManager.shared.currentUser {
                return true
            }
            else {
                return false
            }
        }
    }
    
    var fbData: [String : Any]?
    
//    func registerUser(basicDictionary params:APIParams, onSuccess: @escaping (UserSignedObj) -> Void, onFailure: @escaping  (APIError) -> Void) {
//
//        let engagementRouter = BaseRouter(method: .post, path: REGISTER_CLIENT_URL, parameters: params)
//
//        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
//
//            if let response: [String : Any] = responseObject as? [String : Any], let userWrapper = Mapper<UserSignedObj>().map(JSON: response) {
//                onSuccess(userWrapper)
//                print(userWrapper)
//            }
//            else {
//                let apiError = APIError()
//                onFailure(apiError)
//            }
//
//        }) { (apiError) in
//            if apiError.responseStatusCode == 401 || apiError.responseStatusCode == 422 {
//                apiError.message = "البريد الالكتروني مٌستخدم من قبل"
//            }
//            onFailure(apiError)
//        }
//    }
//
    
    func registerUser(basicDictionary params:APIParams , onSuccess: @escaping (String)->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .post, path: REGISTER_CLIENT_URL, parameters: params)
        
        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let token: String = response["token"] as? String {
                let authorization = "Bearer \(token)"
                UserDefaultManager.shared.authorization = authorization
                onSuccess(authorization)
            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }

//    func loginUser(basicDictionary params:APIParams , onSuccess: @escaping (User)->Void, onFailure: @escaping  (APIError)->Void) {
//
//        let engagementRouter = BaseRouter(method: .post, path: LOGIN_URL, parameters: params)
//
//        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
//
//            if let response: [String : Any] = responseObject as? [String : Any], let token: String = response["token"] as? String, let user: [String : Any] = response["user"] as? [String : Any], let userWrapper = Mapper<User>().map(JSON: user) {
//                let authorization = "Bearer \(token)"
//                UserDefaultManager.shared.authorization = authorization
//                UserDefaultManager.shared.currentUser = userWrapper
//                onSuccess(userWrapper)
////                UserDefaultManager.shared.currentUser = user
//            }
//            else {
//                let apiError = APIError()
//                onFailure(apiError)
//            }
//
//        }) { (apiError) in
//            onFailure(apiError)
//        }
//    }
    
    func loginUser(basicDictionary params:APIParams , onSuccess: @escaping (String)->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .post, path: LOGIN_URL, parameters: params)
        
        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let token: String = response["token"] as? String {
                let authorization = "Bearer \(token)"
                UserDefaultManager.shared.authorization = authorization
                onSuccess(authorization)
            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func getUserProfile(onSuccess: @escaping (User) -> Void, onFailure: @escaping  (APIError) -> Void) {
        
        let engagementRouter = BaseRouter(method: .get, path: GET_USER_URL, parameters: [:])
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let data = response["data"] as? [String : Any], let userWrapper = Mapper<User>().map(JSON: data) {
                UserDefaultManager.shared.currentUser = userWrapper
                onSuccess(userWrapper)
            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
        
    }
    
    func editProfile(nameOfArray: String, imageData: Data?, basicDictionary params:APIParams , onSuccess: @escaping (User)->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .post, path: PROFILE_EDIT_URL, parameters: params)
        
        super.performUploadNetworkRequestInChat(nameOfImage: nameOfArray, imageData: imageData, forRouter: engagementRouter, onSuccess: { (responseObject) in

            if let response: [String : Any] = responseObject as? [String : Any], let data = response["data"] as? [String : Any], let userWrapper = Mapper<User>().map(JSON: data) {
                UserDefaultManager.shared.currentUser = userWrapper
                
                onSuccess(userWrapper)
            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func sendVerificationCode(basicDictionary params:APIParams , onSuccess: @escaping (String)->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .post, path: SEND_VERIFICATION_CODE_URL, parameters: params)
        
        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let message: String = response["status"] as? String {
                
                onSuccess(message)
            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func checkVerificationCode(basicDictionary params:APIParams , onSuccess: @escaping (Bool)->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .post, path: CHECK_VERIFICATION_CODE_URL, parameters: params)
        
        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let status: Bool = response["status"] as? Bool {
                
                onSuccess(status)
            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func forgetPassword(basicDictionary params:APIParams , onSuccess: @escaping (String)->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .post, path: FORGET_PASSWORD_URL, parameters: params)
        
        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let message: String = response["message"] as? String {

                onSuccess(message)
            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func enterVerificationCode(basicDictionary params:APIParams , onSuccess: @escaping (String)->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .post, path: ENTER_VERIFICATION_CODE_URL, parameters: params)
        
        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let message: String = response["verifycode"] as? String {

                onSuccess(message)
            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }

    
    func changePasswordFromLogIn(basicDictionary params:APIParams , onSuccess: @escaping (String)->Void, onFailure: @escaping  (APIError)->Void) {
    
    let engagementRouter = BaseRouter(method: .post, path: CHANGE_PASSWORD_URL, parameters: params)
    
    super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
        
        if let response: [String : Any] = responseObject as? [String : Any], let status: String = response["message"] as? String {
            
            onSuccess(status)
        }
        else {
            let apiError = APIError()
            onFailure(apiError)
        }
        
        }) { (apiError) in
            onFailure(apiError)
        }
    }

    
    func changePassword(basicDictionary params:APIParams , onSuccess: @escaping (String)->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .post, path: CHANGE_PASSWORD_URL, parameters: params)
        
        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let message: String = response["message"] as? String {
                
                onSuccess(message)
            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
//    func changeUserProfileWithImage(imageData imgData : Data? ,basicDictionary params:APIParams , onSuccess: @escaping (String)->Void, onFailure: @escaping  (APIError)->Void) {
//
//        let engagementRouter = BaseRouter(method: .post, path: EDIT_PROFILE_URL, parameters: params)
//
//        super.performUploadNetworkRequest(imageData: imgData,forRouter: engagementRouter, onSuccess: { (responseObject) in
//
//            if let response: [String : Any] = responseObject as? [String : Any], let message: String = response["message"] as? String {
////                UserDefaultManager.shared.currentUser = userWrapper
//
//                onSuccess(message)
//            }
//            else {
//                let apiError = APIError()
//                onFailure(apiError)
//            }
//
//        }) { (apiError) in
//            onFailure(apiError)
//        }
//    }
    
    func changeUserProfileWithImage(imageData imgData : Data? ,basicDictionary params:APIParams , onSuccess: @escaping (Bool)->Void, onFailure: @escaping  (APIError)->Void) {

        let engagementRouter = BaseRouter(method: .post, path: EDIT_PROFILE_URL, parameters: params)

        super.performUploadNetworkRequest(imageData: imgData,forRouter: engagementRouter, onSuccess: { (responseObject) in

            if let response: [String : Any] = responseObject as? [String : Any], let message: Bool = response["success"] as? Bool {
//                UserDefaultManager.shared.currentUser = userWrapper

                onSuccess(message)
            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }

        }) { (apiError) in
            onFailure(apiError)
        }
    }


    
    func logout(basicDictionary params:APIParams , onSuccess: @escaping (String)->String, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .get, path: LOGOUT_URL, parameters: params)
        
        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let message: String = response["message"] as? String {
                UserDefaultManager.shared.authorization = nil
                onSuccess(message)
            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func refreshToken(basicDictionary params:APIParams , onSuccess: @escaping (String) -> Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .post, path: REFRESH_TOKEN_URL, parameters: params)
        
        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let token: String = response["token"] as? String {
                let authorization = "Bearer \(token)"
                UserDefaultManager.shared.authorization = authorization
                onSuccess(authorization)
            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
            
    func changeUserProfile(imageData imgData : Data? ,basicDictionary params:APIParams , onSuccess: @escaping (User)->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .post, path: EDIT_PROFILE_URL, parameters: params)
        
        super.performUploadNetworkRequest(imageData: imgData,forRouter: engagementRouter, onSuccess: { (responseObject) in

            if let response: [String : Any] = responseObject as? [String : Any], let data = response["data"] as? [String : Any], let userWrapper = Mapper<User>().map(JSON: data) {
                UserDefaultManager.shared.currentUser = userWrapper

                
                onSuccess(userWrapper)
            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func getProfileData(basicDictionary params:APIParams, onSuccess: @escaping (ProfileData) -> Void, onFailure: @escaping  (APIError) -> Void) {
        
        let engagementRouter = BaseRouter(method: .get, path: GET_USER_PROFILE_DATA_URL, parameters: params)
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let data = response["data"] as? [String : Any], let userWrapper = Mapper<ProfileData>().map(JSON: data) {
                onSuccess(userWrapper)
            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func getDriverProfileData(basicDictionary params:APIParams, onSuccess: @escaping (ProfileData) -> Void, onFailure: @escaping  (APIError) -> Void) {
        
        let engagementRouter = BaseRouter(method: .get, path: GET_DRIVER_PROFILE_URL, parameters: params)
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let data = response["data"] as? [String : Any], let userWrapper = Mapper<ProfileData>().map(JSON: data) {
                onSuccess(userWrapper)
            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }

}

