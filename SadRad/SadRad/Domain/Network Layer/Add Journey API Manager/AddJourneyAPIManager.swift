//
//  AddJourneyAPIManager.swift
//  SadRad
//
//  Created by Omar Ibrahim on 10/15/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class AddJourneyAPIManager: BaseAPIManager {
    
    func getGovernrates(basicDictionary params:APIParams , onSuccess: @escaping ([Governorate])->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .get, path: GET_GOVERNRATES_URL, parameters: params)
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [[String : Any]] = responseObject as? [[String : Any]] {
                let wrapper = Mapper<Governorate>().mapArray(JSONArray: response)
                onSuccess(wrapper)
            }
                
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func getCities(basicDictionary params:APIParams , onSuccess: @escaping ([City])->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .get, path: GET_CITIES_URL, parameters: params)
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [[String : Any]] = responseObject as? [[String : Any]] {
                let wrapper = Mapper<City>().mapArray(JSONArray: response)
                onSuccess(wrapper)
            }
                
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func addJourney(basicDictionary params:APIParams , onSuccess: @escaping (Bool)->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .post, path: ADD_JOURNEY_URL, parameters: params)
        
        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let saved = response["saved"] as? Bool {

            onSuccess(saved)

            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func updateJourney(basicDictionary params:APIParams , onSuccess: @escaping (Bool)->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .post, path: UPDATE_JOURNEY_URL, parameters: params)
        
        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let saved = response["updated"] as? Bool {

            onSuccess(saved)

            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }

    func getMyJournies(basicDictionary params:APIParams , onSuccess: @escaping ([MyJourney])->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .get, path: GET_MY_JOURNIES_URL, parameters: params)
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["data"] as? [[String : Any]] {
                let wrapper = Mapper<MyJourney>().mapArray(JSONArray: jsonArray)
                onSuccess(wrapper)
            }
                
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }

    func deleteMyJourney(basicDictionary params:APIParams , onSuccess: @escaping (Bool)->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .post, path: DELETE_MY_JOURNEY_URL, parameters: params)
        
        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let saved = response["datadelete"] as? Bool {

            onSuccess(saved)

            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func doneMyJourney(basicDictionary params:APIParams , onSuccess: @escaping (String)->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .post, path: DONE_MY_JOURNEY_URL, parameters: params)
        
        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let saved = response["data"] as? String {

            onSuccess(saved)

            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }

    func startTrip(basicDictionary params:APIParams , onSuccess: @escaping (String)->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .post, path: START_MY_JOURNEY_URL, parameters: params)
        
        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let saved = response["data"] as? String {

            onSuccess(saved)

            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }

    func getMyJourneyInfo(basicDictionary params:APIParams , onSuccess: @escaping (MyJourney) -> Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .get, path: GET_MY_JOURNEY_INFORMATION, parameters: params)
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let data = response["data"] as? [String : Any], let wrapper = Mapper<MyJourney>().map(JSON: data) {
                onSuccess(wrapper)
            }
                
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func getNewTripDetails(basicDictionary params:APIParams , onSuccess: @escaping (NewTripInfo) -> Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .get, path: GET_MY_JOURNEY_INFORMATION, parameters: params)
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let data = response["data"] as? [String : Any], let wrapper = Mapper<NewTripInfo>().map(JSON: data) {
                onSuccess(wrapper)
            }
                
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }


}
