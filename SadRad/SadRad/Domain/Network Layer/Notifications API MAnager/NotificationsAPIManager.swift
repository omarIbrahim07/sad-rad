//
//  NotificationsAPIManager.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 12/12/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class NotificationsAPIManager: BaseAPIManager {

    func getNotifications(basicDictionary params:APIParams , onSuccess: @escaping ([Notificationn])->Void, onFailure: @escaping  (APIError)->Void) {

    let engagementRouter = BaseRouter(method: .get, path: GET_NOTIFICATIONS_URL, parameters: params)

    super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
        
        if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["data"] as? [[String : Any]] {
            let wrapper = Mapper<Notificationn>().mapArray(JSONArray: jsonArray)
            onSuccess(wrapper)
        }
            
        else {
            let apiError = APIError()
            onFailure(apiError)
        }
        
        }) { (apiError) in
            onFailure(apiError)
        }
    }
}
