//
//  API_CONSTANTS.swift
//  GameOn
//
//  Created by Hassan on 12/22/17.
//  Copyright © 2017 Hassan. All rights reserved.
//

import Foundation

/* OOOOM APIS */

let MainURL = "https://api.sadrad.app/api/"

let REFRESH_TOKEN_URL = "token/refresh"

let ImageURLServices = "http://app.oooomapp.com/uploads/services/"
let ImageURL_USERS = "https://api.sadrad.app/public/uploads/profile/"
let ImageURLWorkerWorks = "http://app.oooomapp.com/uploads/workerWork/"
let ImageURLOffers = "http://app.oooomapp.com/uploads/offers/"

// NEW APIS //
let MY_CARS_IMAGES_URL = "https://api.sadrad.app/public/uploads/cars/"
let PROFILE_IMAGE_URL = "https://api.sadrad.app/public/uploads/profile/"

// NEW APIS //
let GET_MY_CARS_URL = "cars"
let GET_CAR_TYPES_URL = "typescars"
let GET_CAR_MODELS_URL = "modelcars"
let GET_CAR_BRANDS_URL = "brandcars"
let GET_MY_CAR_INFORMATION = "car/info"
let ADD_MY_CAR_URL = "car/add"
let UPDATE_MY_CAR_URL = "car/update"
let DELETE_MY_CAR_URL = "car/delete"

let GET_GOVERNRATES_URL = "governorates"
let GET_CITIES_URL = "cities"
let ADD_JOURNEY_URL = "trip/store"
let UPDATE_JOURNEY_URL = "trip/update"
let GET_MY_JOURNIES_URL = "mytrips"
let DELETE_MY_JOURNEY_URL = "deletetrip"
let DONE_MY_JOURNEY_URL = "tripfinish"
let START_MY_JOURNEY_URL = "starttrip"
let GET_MY_JOURNEY_INFORMATION = "trips/info"

let GET_RESERVATIONS_URL = "Allmytrips"

let RATE_TRIP_URL = "review"

let GET_TRIPS_OFFERS_URL = "search"
let GET_FILTER_YEARS_URL = "getyears"
let GET_SORTED_TRIPS_OFFERS_URL = "sorttrips"
let RESERVE_TRIP_URL = "reservation"

let GET_REASONS_FOR_CANCEL_TRIP_URL = "reasonscancelreservation"
let CANCEL_TRIP_URL = "cancelreservation"
let UPLOAD_PROFILE_IMAGE_URL = "driverstore"

let GET_USER_PROFILE_DATA_URL = "profile"
let GET_DRIVER_PROFILE_URL = "profileshow"

let GET_SERVICES_URL = "services"
let LOGIN_URL = "login"
let GET_USER_URL = "user"
let REGISTER_CLIENT_URL = "register"
let SEND_VERIFICATION_CODE_URL = "verify"
let CHECK_VERIFICATION_CODE_URL = "verify"
let FORGET_PASSWORD_URL = "forgetpassword"
let ENTER_VERIFICATION_CODE_URL = "verifycode"
let CHANGE_PASSWORD_FROM_LOGIN_URL = "forgetpassword"
let CHANGE_PASSWORD_URL = "changepassword"
let GET_TECHNICIANS_URL = "workers"
let GET_MENTINANCE_ORDERS_URL = "orders"
let GET_ORDER_DETAILS_URL = "orders"
let SUBMIT_ACTION_ON_ORDER_URL = "orders/save"
let GET_BIDS_URL = "bids"
let GET_BID_OFFERS_URL = "bidoffers"
let GET_STATIC_PAGE_URL = "staticpages"
let SEND_ORDER_URL = "orders/save"
let SEND_BID_URL = "bids/save"
let SEND_FEEDBACK_URL = "feedback/save"
let RATE_ORDER_URL = "orders/save"
let SEND_PROMOCODE_URL = "promocodes/save"
let GET_PROMOCODE_URL = "promocodes"
let EDIT_PROFILE_URL = "edituser"

let PROFILE_EDIT_URL = "profile/edit"

let GET_NOTIFICATIONS_URL = "notifications"
let GET_OFFERS_URL = "offers"
let GET_OFFER_DETAILS_URL = "offers"

let GET_FILE_UPLOAD_URL = "files/save"

let GET_ROOM_ID_URL = "chatrooms/save"
let GET_LIST_OF_CHATS_URL = "chatrooms"


























let GET_ADDITIONAL_SERVICES_URL = "additionalServices"
let GET_ALL_LOCATIONS_URL = "locations"
let GET_ABOUT_URL = "staticPages/"
let GET_FEEDBACK_ELEMENTS_URL = "feedbackItems"
let GET_TYPES_URL = "categories"
let POST_ORDER_URL = "orders/save"
let POST_ORDERS_EXTRA_SERVICES_URL = "orders/addservices"
let POST_SHIPPING_LOCATION_URL = "trackings/save"

let GET_POSTS = ""

let REGISTER_URL = "register"
//let LOGIN_URL = "login"
let LOGOUT_URL = "logout"
//let GET_USER_URL = "profile"
//let EDIT_PROFILE_URL = "editprofile"

let GIVE_FEEDBACK = "appFeedbacks/save"
let GET_SHIPPING_LOCATIONS_URL = "locations"

let RATE_DRIVER_URL = "driverRatings/save"
let RATE_SERVICES_URL = "orderFeedbacks/save"

let GET_MY_PACKAGES_URL = "orders"
let GET_PACKAGE_URL = "orders"
let GET_TRACKED_PAKAGES = "serviceorders"

let GET_NOTIFICATIONS = "notifications"

/*********************************************************************************/

let GET_MAIN_CATEGORIES_URL = "cats"
let GET_SUB_CATEGORIES_URL = "subcats/"
let GET_COUNTRIES_URL = "countries"
//let GET_CITIES_URL = "cites/"
let GET_SECONDARY_CATEGORIES_URL = "ads/cat/"
let GET_SPECIAL_SECONDARY_CATEGORIES_URL = "ads/featured/cat/"
let GET_ADVERTISEMENT_URL = "ad/"

let GET_ALL_ADVERTISEMENTS = "ads"
let GET_ALL_SPECIAL_ADVERTISEMENTS = "ads/featured"
let GET_ALL_MY_ADVERTISEMENTS = "ads/user/"
let GET_ALL_MY_SPECIAL_ADVERTISEMENTS = "ads/featured/user/"
let GET_SIMILAR_ADVERTISEMENTS_URL = "similarads/ad/"
let GET_SAVED_ADVERTISEMENTS_URL = "user/savedads/"
let GET_USER_ADVERTISEMENT_BY_USER_ID = "ads/user/"
let GET_SOLD_ADVERTISEMENTS_URL = "ads/sold/user/"
let GET_SEARCHED_ADVERTISEMENTS_URL = "search"

let registerURL = "register"
let getUserURL = "user"

let FIRST_LEVEL_POST_ADVERTISEMENT_URL = "postad"
let EDIT_FIRST_LEVEL_POST_ADVERTISEMENT_URL = "editad/"
//let CHANGE_PASSWORD_URL = "user/changepassword"
let CHANGE_USER_PROFILE_URL = "user/editprofile"
let SAVE_POST_URL = "ads/savead"
let CHECK_SAVED_POST_URL = "ads/issaved"
let DELETE_POST_URL = "ad/delete/"
let GET_POST_TYPES_URL = "posttypes"
let SELL_ADVERTISEMENT_URL = "setsold/ad/"

let SAVE_AD_PICS = "postpics"
let GET_MAIN_BANNER_URL = "mainbanner"

let GET_MESSAGES_URL = "messages"
let GET_CHAT_URL = "chat"
let GET_CHAT_MESSAGES_URL = "chat"
let TEXT_CHAT_MESSAGE_URL = "sendmsg"

let GET_INTERESTS_URL = "get-interests"
let GET_POSTS_URL = "get-posts"
//let LOGOUT_URL = "logout"
let ADD_NEW_POST_URL = "add-post"
let GET_NOTIFICATION_URL = "get-notifications"
let GET_READ_POST_URL = "get-read-post"
let ADD_FOLLOW_POST = "add-follow-post"
let DELETE_POST = "delete-post"
let REPORT_POST = "add-feedback-post"
let DELETE_COMMENT = "delete-comment-post"
let DELETE_REPLY = "delete-comment-replies-post"
let LIKE_COMMENT = "add-like-comment-post"
let LIKE_REPLY = "add-like-comment-replies-post"
let GET_PROFILE_DATA = "profile"

