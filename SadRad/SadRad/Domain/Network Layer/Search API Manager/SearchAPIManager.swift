//
//  SearchAPIManager.swift
//  SadRad
//
//  Created by Omar Ibrahim on 10/25/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class SearchAPIManager: BaseAPIManager {
    
    func getTripsOffers(basicDictionary params:APIParams , onSuccess: @escaping ([MyJourney])->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .get, path: GET_TRIPS_OFFERS_URL, parameters: params)
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["trips"] as? [[String : Any]] {
                let wrapper = Mapper<MyJourney>().mapArray(JSONArray: jsonArray)
                onSuccess(wrapper)
            }
                
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func getSortedTripsOffers(basicDictionary params:APIParams , onSuccess: @escaping ([MyJourney])->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .get, path: GET_SORTED_TRIPS_OFFERS_URL, parameters: params)
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["trips"] as? [[String : Any]] {
                let wrapper = Mapper<MyJourney>().mapArray(JSONArray: jsonArray)
                onSuccess(wrapper)
            }
                
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func reserveTrip(basicDictionary params:APIParams , onSuccess: @escaping (Bool)->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .post, path: RESERVE_TRIP_URL, parameters: params)
        
        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let saved = response["seved"] as? Bool {

            onSuccess(saved)

            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func getFilterYears(basicDictionary params:APIParams , onSuccess: @escaping ([FilterYear])->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .get, path: GET_FILTER_YEARS_URL, parameters: params)
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["models"] as? [[String : Any]] {
                let wrapper = Mapper<FilterYear>().mapArray(JSONArray: jsonArray)
                onSuccess(wrapper)
            }
                
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
//    func getTripsOffers(basicDictionary params:APIParams , onSuccess: @escaping ([Reservation])->Void, onFailure: @escaping  (APIError)->Void) {
//
//        let engagementRouter = BaseRouter(method: .get, path: GET_TRIPS_OFFERS_URL, parameters: params)
//
//        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
//
//            if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["data"] as? [[String : Any]] {
//                let wrapper = Mapper<Reservation>().mapArray(JSONArray: jsonArray)
//                onSuccess(wrapper)
//            }
//
//            else {
//                let apiError = APIError()
//                onFailure(apiError)
//            }
//
//        }) { (apiError) in
//            onFailure(apiError)
//        }
//    }

}
