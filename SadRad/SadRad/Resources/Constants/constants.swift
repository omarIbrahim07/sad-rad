//
//  constants.swift
//  GameOn
//
//  Created by Hassan on 12/22/17.
//  Copyright © 2017 Hassan. All rights reserved.
//

import Foundation
import UIKit

let GoogleClientID = "245060057438-g8g7dhp3emmo5cjdvfijcjarovqmev76.apps.googleusercontent.com"

//MARK:- User default keys
let kAuthorization = "UserDefaults_Authorization"
let kUserData = "UserDefaults_UserData"

var FirebaseToken : String!

let uuid = NSUUID().uuidString
let kUserDefault = UserDefaults.standard

let appDelegate = UIApplication.shared.delegate as! AppDelegate


let screenWidth = UIScreen.main.bounds.width
let screenHeight = UIScreen.main.bounds.height


let iPhoneXNavigationBarHeight: CGFloat = 88
let normalNavigationBarHeight: CGFloat = 64

let mainBlueColor = UIColor().hexStringToUIColor(hex: "#4367B2")

let generalError = "An Error has occured"
let ALL_COLLECTIONVIEW_CELL = 1

let nckSelectHomeTab = "nckSelectHomeTab"

let nckUpdatePost = "nckUpdatePost"

let ncSelectHomeTab = Notification.Name(nckSelectHomeTab)

let ncUpdatePost = Notification.Name(nckUpdatePost)

let AppStoreURL = "https://itunes.apple.com/us/app/"
let GoogleBannerUnitID = "ca-app-pub-9861973495408000/4276858027"

let mainColor = #colorLiteral(red: 0.3607843137, green: 0.7764705882, blue: 0.7294117647, alpha: 1)
let similarWhiteColor = #colorLiteral(red: 0.9725490196, green: 0.9725490196, blue: 0.9725490196, alpha: 1)
let moovGhameaColor = #colorLiteral(red: 0.2274509804, green: 0.2784313725, blue: 0.3490196078, alpha: 1)
let romadyColor = #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1)
let asfarColor = #colorLiteral(red: 0.9921568627, green: 0.8745098039, blue: 0.07843137255, alpha: 1)
let ahmarColor = #colorLiteral(red: 1, green: 0, blue: 0, alpha: 1)
